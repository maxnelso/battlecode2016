![](diamond.png)

# mid high diamonds
Here is our codebase for [Battlecode 2016](http://battlecode.org). The source for our final submission can be found at `battlecode/src/infestorbot2` (excuse the uninspired name).

**Team Members**: Justin Venezuela, Max Nelson, Ryan Cheu

## Running
The Eclipse `.project` file can be found in `battlecode/ide/eclipse`. The engine can be run with `ant run` in the `battlecode` folder.

## Notes
* `battlecode/src` contains our various bots. Most of them were bots we made for testing. We had a server running the [archon tool](https://www.npmjs.com/package/archon) continuously on each commit, matching our current bot under development with our various other bots. For example, `swannbotimmortalprotocol` was our turtling bot and we made sure any bot we submitted scored well against it.
* `scripts` contains, as you might imagine, various scripts. For example, `scripts/scrimmage_spy.py` continuously scraped the scrimmage rankings page, keeping an eye on which teams were losing against which other teams. Justin wrote this script for [Battlecode 2012](https://bitbucket.org/Cixelyn/bcode2012-bot).
* `battlecode/maps` contains a bunch of maps we used for testing micro, navigation, scout exploration, etc..

## Special thanks
* Thanks to the devs for all their help in the IRC and for making another great Battlecode game.
* Thanks to Nick Mohr for theorycrafting with us.
* Thanks to various teams of past years' Battlecodes for opening their code publicly. We freely borrowed navigation and army micro from [fun gamers](https://bitbucket.org/Cixelyn/bcode2012-bot), [The_Duck 2014](https://github.com/TheDuck314/battlecode2014), and [The_Duck 2015](https://github.com/TheDuck314/battlecode2015), just to name a few.
