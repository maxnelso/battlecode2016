"""
Test scouting on a few particular maps
"""

__author__ = 'Max Nelson (maxnelso@mit.edu)'

import copy
import subprocess
import sys
import os
import threading

def run_match():
  output = subprocess.check_output('ant headless -buildfile ../battlecode/build.xml -Dbc.conf=scouting_test.conf', shell=True).split('\n')

  total_rounds = 0;
  map_name = ""
  for line in output:
    line = line.split();
    if 'on' in line:
      map_index = line.index('on') + 1
      map_name = line[map_index]
    if '(round' in line:
      round_index = line.index('(round') + 1
      rounds = int(line[round_index][:-1])
      total_rounds += rounds
      print 'Rounds on %s: %d' % (map_name, rounds)
  print 'Total rounds %d' % (total_rounds,)

def main():
  team_a = 'scouttestbot'
  team_b = 'idlebot'
  run_match()

if __name__ == '__main__':
  main()
