#!/bin/sh
team=$1

rm -rf src/prevCommit
cp -r src/$team src/prevCommit

for file in `find src/prevCommit|grep java`
do
  sed -ir "s/$team/prevCommit/g" $file
done