team=$1

cd ../battlecode
rm -rf src/team270
cp -r src/$team src/team270
temp=`mktemp`
for file in `find src/team270|grep java`
do
  sed -ir "s/$team/team270/g" $file
  sed -ir "s/[a-zA-Z]\+.setIndicatorString[(][^;]\+\;\|System.out.println[(][^;]\+;\|[a-zA-Z]*.?log[(][^;]\+;/ /g" $file
done

ant -Dteam=team270 jar
