#!/bin/sh

team=dragoonbot


cd ../battlecode
rm -rf src/Fenix
cp -r src/currentRun src/Fenix

for file in `find src/Fenix|grep java`
do
  sed -ir "s/currentRun/Fenix/g" $file
done

rm -rf src/currentRun
cp -r src/$team src/currentRun

for file in `find src/currentRun|grep java`
do
  sed -ir "s/$team/currentRun/g" $file
done

