package flashbot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MessageDataTest {

  @Test
  public void noBitsBuild() {
    MessageData data = new MessageData.Builder()
        .build();
    assertEquals(0, data.getData());
  }

  @Test
  public void oneAddBuild() {
    MessageData data = new MessageData.Builder()
        .addBits(4, 6)
        .build();
    assertEquals(6 << (32 - 4), data.getData());
  }

  @Test
  public void oneAddBuildOverflow() {
    MessageData data = new MessageData.Builder()
        .addBits(4, 255)
        .build();
    assertEquals(15 << (32 - 4), data.getData());
  }

  @Test
  public void twoAddsBuild() {
    MessageData data = new MessageData.Builder()
        .addBits(29, 0)
        .addBits(3, 7)
        .build();
    assertEquals(7, data.getData());
  }

  @Test
  public void threeAddsBuild() {
    MessageData data = new MessageData.Builder()
        .addBits(2, 3)
        .addBits(26, 0)
        .addBits(4, 1)
        .build();
    assertEquals((3 << (32 - 2)) + 1, data.getData());
  }

  @Test
  public void getPayload() {
    MessageData data = new MessageData.Builder()
        .addBits(29, 0)
        .addBits(3, 7)
        .build();
    assertEquals(7, data.getPayload(29, 31));
  }

  @Test
  public void getAllPayloads() {
    MessageData data = new MessageData.Builder()
        .addBits(29, 0)
        .addBits(3, 7)
        .build();
    int[] lengths = new int[] {
      29, 2, 1
    };
    int[] expectedPayloads = new int[] {
      0, 3, 1
    };
    int[] actualPayloads = data.getAllPayloads(lengths);
    for (int i = 0; i < 3; i++) {
      assertEquals(expectedPayloads[i], actualPayloads[i]);
    }
  }
}
