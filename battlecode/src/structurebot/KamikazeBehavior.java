package structurebot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class KamikazeBehavior implements Behavior {

  private static final int DISINTEGRATE_DISTANCE = 2;

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {
    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    int closestEnemyDist = 99999;
    MapLocation closestEnemyLoc = null;
    for (int i = 0; i < nearbyRobots.length; i++) {
      RobotInfo robot = nearbyRobots[i];
      int dist = rc.getLocation().distanceSquaredTo(robot.location);
      if (dist < closestEnemyDist
          && robot.team == rc.getTeam().opponent()) {
        closestEnemyDist = dist;
        closestEnemyLoc = robot.location;
      }
    }

    RobotInfo myInfo = rc.senseRobotAtLocation(rc.getLocation());
    if (myInfo != null
        && closestEnemyLoc != null
        && (myInfo.viperInfectedTurns > 0 || myInfo.zombieInfectedTurns > 0)
        && closestEnemyLoc.distanceSquaredTo(rc.getLocation()) <= DISINTEGRATE_DISTANCE) {
      rc.disintegrate();
      return;
    }

    Direction moveDir = closestEnemyLoc != null
        ? rc.getLocation().directionTo(closestEnemyLoc)
        : (rc.getTeam() == Team.A ? Direction.EAST : Direction.WEST);
    if (rc.isCoreReady() && rc.canMove(moveDir)) {
      rc.move(moveDir);
    }
  }
}
