package structurebot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface MessageSender {
  public void sendArchonStatus(RobotController rc) throws GameActionException;
}
