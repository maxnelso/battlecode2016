package structurebot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class ShootBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {
    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    MapLocation attackLoc = attackSystem.attackLeastHealthInfectedEnemy(rc, nearbyRobots);
    if (attackLoc == null) {
      attackLoc = attackSystem.attackLeastHealthEnemy(rc, nearbyRobots);
    }
    if (attackLoc == null) {
      attackLoc = attackSystem.attackLeastHealthZombie(rc, nearbyRobots);
    }

    if (attackLoc != null || !rc.isCoreReady()) {
      return;
    }

    Direction moveDir = rc.getTeam() == Team.A ? Direction.EAST : Direction.WEST;
    Direction[] dirs = new Direction[] {
      moveDir,
      moveDir.rotateLeft(),
      moveDir.rotateRight(),
      moveDir.rotateLeft().rotateLeft(),
      moveDir.rotateRight().rotateRight()
    };
    for (int i = 0; i < dirs.length; i++) {
      Direction d = dirs[i];
      if (rc.canMove(d)) {
        rc.move(d);
        return;
      }
    }
  }
}
