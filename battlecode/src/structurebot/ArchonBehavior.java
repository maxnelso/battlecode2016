package structurebot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class ArchonBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {
    int p = (rc.getID() % 256);
    for (int i = 0; i < archonStatus.numArchons; i++) {
      rc.setIndicatorLine(rc.getLocation(), archonStatus.locs[i], p, 0, 0);
    }

    if (rc.getRoundNum() % 5 == 0) {
      messageSender.sendArchonStatus(rc);
    }
  }
}
