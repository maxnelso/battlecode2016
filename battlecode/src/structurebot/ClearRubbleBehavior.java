package structurebot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;

public class ClearRubbleBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    Direction moveDir = Direction.NORTH;
    if (rc.senseRubble(rc.getLocation().add(moveDir)) >= GameConstants.RUBBLE_SLOW_THRESH) {
      rc.clearRubble(moveDir);
      return;
    }

    if (rc.canMove(moveDir)) {
      rc.move(moveDir);
      return;
    }
  }

}
