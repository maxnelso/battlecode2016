package structurebot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class NoOpBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {}
}
