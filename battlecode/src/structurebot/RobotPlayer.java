package structurebot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import structurebot.MessageReceiver.MessageSummary;

public class RobotPlayer {
  public static void run(RobotController rc) {
    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
      case SOLDIER:
      case GUARD:
      case TURRET:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
      case VIPER:
      case SCOUT:
      case TTM:
      case ZOMBIEDEN:
      default:
        behavior = new ClearRubbleBehavior();
        break;
    }

    Messenger messenger = new Messenger();
    AttackSystem attackSystem = new AttackSystem();

    while (true) {
      try {
        MessageSummary messageSummary = messenger.receiveMessages(rc);
        behavior.behave(
            rc,
            messenger /* messageSender */,
            attackSystem,
            messageSummary.archonStatus);
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
