package structurebot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.Signal;

public class Messenger implements MessageSender, MessageReceiver {

  private static final int NEARBY_DISTANCE = 10 * 10 * 2;

  private enum MessageType {
    ARCHON_STATUS(0);

    int opCode;

    private MessageType(int opCode) {
      this.opCode = opCode;
    }

    static MessageType fromOpCode(int opCode) {
      for (MessageType t : MessageType.values()) {
        if (t.opCode == opCode) {
          return t;
        }
      }
      return null;
    }
  }

  @Override
  public void sendArchonStatus(RobotController rc) throws GameActionException {
    sendArchonStatus(rc, NEARBY_DISTANCE);
  }

  @Override
  public MessageSummary receiveMessages(RobotController rc) throws GameActionException {
    ArchonStatus.Builder archonStatusBuilder = new ArchonStatus.Builder();

    Signal s;
    while ((s = rc.readSignal()) != null) {
      if (s.getTeam() != rc.getTeam()) {
        continue;
      }
      int[] data = s.getMessage();
      if (data == null || data.length != 2) {
        continue;
      }
      MessageType t = MessageType.fromOpCode(data[0]);
      if (t == null) {
        continue;
      }
      switch (t) {
        case ARCHON_STATUS:
          archonStatusBuilder.addArchon(s.getRobotID(), s.getLocation());
          break;
        default:
          break;
      }
    }

    return new MessageSummary(archonStatusBuilder.build());
  }

  private void sendArchonStatus(RobotController rc, int radius) throws GameActionException {
    rc.broadcastMessageSignal(MessageType.ARCHON_STATUS.opCode, 0 /* data */, radius);
  }
}
