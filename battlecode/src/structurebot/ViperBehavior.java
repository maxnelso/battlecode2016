package structurebot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class ViperBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc,
      MessageSender messageSender,
      AttackSystem attackSystem,
      ArchonStatus archonStatus) throws GameActionException {
    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    MapLocation attackLoc = attackSystem.infectAllies(rc, nearbyRobots, RobotType.SCOUT);
    if (attackLoc == null) {
      attackLoc = attackSystem.attackLeastHealthInfectedEnemy(rc, nearbyRobots);
    }
    if (attackLoc == null) {
      attackLoc = attackSystem.attackLeastHealthEnemy(rc, nearbyRobots);
    }
    if (attackLoc == null) {
      attackLoc = attackSystem.attackLeastHealthZombie(rc, nearbyRobots);
    }

    Direction moveDir = rc.getTeam() == Team.A ? Direction.EAST : Direction.WEST;
    if (attackLoc == null && rc.isCoreReady() && rc.canMove(moveDir)) {
      rc.move(moveDir);
    }
  }
}
