package structurebot;

import battlecode.common.MapLocation;

public class ArchonStatus {
  public static class Builder {
    private final int[] ids;
    private final MapLocation[] locs;
    private int numArchons;

    public Builder() {
      ids = new int[100];
      locs = new MapLocation[100];
      numArchons = 0;
    }

    public void addArchon(int id, MapLocation loc) {
      ids[numArchons] = id;
      locs[numArchons] = loc;
      numArchons++;
    }

    public ArchonStatus build() {
      return new ArchonStatus(this);
    }
  }

  public final int[] ids;
  public final MapLocation[] locs;
  public final int numArchons;

  private ArchonStatus(Builder builder) {
    numArchons = builder.numArchons;
    ids = new int[numArchons];
    locs = new MapLocation[numArchons];
    for (int i = 0; i < numArchons; i++) {
      ids[i] = builder.ids[i];
      locs[i] = builder.locs[i];
    }
  }
}
