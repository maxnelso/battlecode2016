package structurebot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class AttackSystem {

  /** @return The location of the least health infected enemy, or null if there are none. */
  public MapLocation attackLeastHealthInfectedEnemy(
      RobotController rc, RobotInfo[] nearbyRobots) throws GameActionException {
    return attackLeastHealthOnTeam(
        rc,
        nearbyRobots,
        true /* onlyInfected */,
        rc.getTeam().opponent());
  }

  /** @return The location of the least health enemy, or null if there are no enemies. */
  public MapLocation attackLeastHealthEnemy(
      RobotController rc, RobotInfo[] nearbyRobots) throws GameActionException {
    return attackLeastHealthOnTeam(
        rc,
        nearbyRobots,
        false /* onlyInfected */,
        rc.getTeam().opponent());
  }

  /** @return The location of the least health zombie, or null if there are no zombies. */
  public MapLocation attackLeastHealthZombie(
      RobotController rc, RobotInfo[] nearbyRobots) throws GameActionException {
    return attackLeastHealthOnTeam(
        rc,
        nearbyRobots,
        false /* onlyInfected */,
        Team.ZOMBIE);
  }

  private MapLocation attackLeastHealthOnTeam(
      RobotController rc,
      RobotInfo[] nearbyRobots,
      boolean onlyInfected,
      Team team) throws GameActionException {
    double leastHealth = 99999;
    MapLocation leastHealthLoc = null;

    for (int i = 0; i < nearbyRobots.length; i++) {
      RobotInfo robot = nearbyRobots[i];
      if (robot.team == team
          && robot.health < leastHealth
          && rc.canAttackLocation(robot.location)
          && (!onlyInfected || robot.viperInfectedTurns > 0 || robot.zombieInfectedTurns > 0)) {
        leastHealth = robot.health;
        leastHealthLoc = robot.location;
      }
    }

    if (rc.isWeaponReady() && leastHealthLoc != null && rc.canAttackLocation(leastHealthLoc)) {
      rc.attackLocation(leastHealthLoc);
    }

    return leastHealthLoc;
  }

  /**
   * @return The location of an uninfected ally robot of the given type, or null if there isn't
   *     one.
   */
  public MapLocation infectAllies(
      RobotController rc, RobotInfo[] nearbyRobots, RobotType type) throws GameActionException {
    return infect(rc, nearbyRobots, type, true /* attackAllies */);
  }

  /** @return The location of an uninfected enemy robot, or null if there isn't one. */
  public MapLocation infectEnemies(
      RobotController rc, RobotInfo[] nearbyRobots) throws GameActionException {
    return infect(rc, nearbyRobots, null /* type */, false /* attackAllies */);
  }

  private MapLocation infect(
      RobotController rc,
      RobotInfo[] nearbyRobots,
      RobotType type,
      boolean attackAllies) throws GameActionException {
    MapLocation uninfectedLoc = null;

    for (int i = 0; i < nearbyRobots.length; i++) {
      RobotInfo robot = nearbyRobots[i];
      if (robot.team == (attackAllies ? rc.getTeam() : rc.getTeam().opponent())
          && (type == null || robot.type == type)
          && robot.viperInfectedTurns == 0
          && robot.zombieInfectedTurns == 0
          && rc.canAttackLocation(robot.location)) {
        uninfectedLoc = robot.location;
        break;
      }
    }

    if (rc.isWeaponReady()
        && uninfectedLoc != null
        && rc.canAttackLocation(uninfectedLoc)) {
      rc.attackLocation(uninfectedLoc);
    }

    return uninfectedLoc;
  }
}
