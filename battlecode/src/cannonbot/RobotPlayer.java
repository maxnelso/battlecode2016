package cannonbot;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class RobotPlayer {

  public static void run(RobotController rc) throws GameActionException {
    while (true) {
      switch (rc.getType()) {
        case SOLDIER:
        case GUARD:
        case TURRET:
        case VIPER:
          shoot(rc);
          break;
        default:
          break;
      }
      Clock.yield();
    }
  }

  private static void shoot(RobotController rc) throws GameActionException {
    RobotInfo[] robots = rc.senseNearbyRobots();

    int closestDist = 99999;
    MapLocation closest = null;
    for (int i = 0; i < robots.length; i++) {
      RobotInfo r = robots[i];
      if (r.team == rc.getTeam().opponent()) {
        int dist = rc.getLocation().distanceSquaredTo(r.location);
        if (dist < closestDist) {
          closestDist = dist;
          closest = r.location;
        }
      }
    }

    if (closest != null) {
      Direction d = rc.getLocation().directionTo(closest);
      if (rc.isCoreReady() && rc.canMove(d)) {
        rc.move(d);
      }
      if (rc.isWeaponReady() && rc.canAttackLocation(closest)) {
        rc.attackLocation(closest);
      }
    }
  }
}
