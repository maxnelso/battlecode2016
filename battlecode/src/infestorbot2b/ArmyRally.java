package infestorbot2b;

import battlecode.common.MapLocation;

public interface ArmyRally {

  public MapLocation getRally();
}
