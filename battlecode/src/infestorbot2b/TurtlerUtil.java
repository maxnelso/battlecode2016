package infestorbot2b;

public class TurtlerUtil {

  private static final int TURTLE_MIN_TURRETS = 5;
  private static final int TURTLE_MAX_TURRET_BOUNDING_BOX_DIMENSION = 13;

  public static boolean isTurtleTurretBoundingBox(BoundingBox turretBoundingBox) {
    return turretBoundingBox.getNumElements() >= TURTLE_MIN_TURRETS
        && turretBoundingBox.getXRange() <= TURTLE_MAX_TURRET_BOUNDING_BOX_DIMENSION
        && turretBoundingBox.getYRange() <= TURTLE_MAX_TURRET_BOUNDING_BOX_DIMENSION;
  }
}
