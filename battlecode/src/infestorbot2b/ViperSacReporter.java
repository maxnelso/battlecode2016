package infestorbot2b;

import battlecode.common.MapLocation;

public interface ViperSacReporter {

  public class SacAttack {

    public final MapLocation armyRallyLoc;
    public final int sacRound;

    public SacAttack(MapLocation armyRallyLoc, int sacRound) {
      this.armyRallyLoc = armyRallyLoc;
      this.sacRound = sacRound;
    }
  }

  public void reportSacAttack(MapLocation armyRallyLoc, int sacRound);

  public SacAttack getSacAttack();
}
