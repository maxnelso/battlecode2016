package infestorbot2b;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ViperSacManager implements ViperSacReporter, ViperSacCalculator {

  private static final int MIN_ALLIES_TO_VIPER_SAC = 60;
  private static final int MIN_VIPER_SAC_DETECTION_ROUND = 1500;
  private static final int VIPER_SAC_ROUND = 2400;
  private static final int VIPER_SAC_MESSAGE_FREQUENCY = 20;
  private static final int VIPER_SAC_RALLY_LINEAR_DISTANCE_FROM_ENEMY = 26;

  private final RobotController rc;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ZombieDenReporter zombieDenReporter;
  private final EnemyTurretCache enemyTurretCache;
  private final ArchonHider archonHider;

  private SacAttack sacAttack;

  public ViperSacManager(
      RobotController rc,
      MapBoundaryCalculator mapBoundaryCalculator,
      ZombieDenReporter zombieDenReporter,
      EnemyTurretCache enemyTurretCache,
      ArchonHider archonHider) {
    this.rc = rc;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.zombieDenReporter = zombieDenReporter;
    this.enemyTurretCache = enemyTurretCache;
    this.archonHider = archonHider;
    sacAttack = null;
  }

  @Override
  public void computeAndShareViperSac(MessageSender messageSender) throws GameActionException {
    if (sacAttack != null) {
      if (rc.getRoundNum() % VIPER_SAC_MESSAGE_FREQUENCY == 0) {
        messageSender.sendViperSacEverywhere(sacAttack.armyRallyLoc, sacAttack.sacRound);
      }
      return;
    }

    BoundingBox turretBoundingBox = enemyTurretCache.getTurretBoundingBox();
    if (rc.getRobotCount() < MIN_ALLIES_TO_VIPER_SAC
        || zombieDenReporter.getClosestDen(rc.getLocation()) != null
        || !mapBoundaryCalculator.allBoundariesKnown()
        || rc.getRoundNum() < MIN_VIPER_SAC_DETECTION_ROUND
        || !TurtlerUtil.isTurtleTurretBoundingBox(turretBoundingBox)) {
      return;
    }

    MapLocation enemyLoc = turretBoundingBox.getCenter();
    if (enemyLoc == null) {
      return;
    }
    MapLocation archonHidingLoc = computeArchonHidingLocation(enemyLoc);
    MapLocation armyRallyLoc = computeArmyRallyLocation(enemyLoc, archonHidingLoc);
    archonHider.reportArchonHidingLocation(archonHidingLoc);
    messageSender.sendViperSacEverywhere(armyRallyLoc, VIPER_SAC_ROUND);
    sacAttack = new SacAttack(armyRallyLoc, VIPER_SAC_ROUND);
  }

  @Override
  public void reportSacAttack(MapLocation armyRallyLoc, int sacRound) {
    if (sacAttack == null) {
      sacAttack = new SacAttack(armyRallyLoc, sacRound);
    }
  }

  @Override
  public SacAttack getSacAttack() {
    return sacAttack;
  }

  private MapLocation computeArchonHidingLocation(MapLocation enemyLoc) {
    int minX = mapBoundaryCalculator.getMinX();
    int maxX = mapBoundaryCalculator.getMaxX();
    int minY = mapBoundaryCalculator.getMinY();
    int maxY = mapBoundaryCalculator.getMaxY();

    boolean inLowerXHalf = enemyLoc.x < (minX + maxX) / 2;
    boolean inLowerYHalf = enemyLoc.y < (minY + maxY) / 2;
    MapLocation corner1 = inLowerXHalf == inLowerYHalf
        ? new MapLocation(maxX, minY) : new MapLocation(minX, minY);
    MapLocation corner2 = inLowerXHalf == inLowerYHalf
        ? new MapLocation(minX, maxY) : new MapLocation(maxX, maxY);

    MapLocation myLoc = rc.getLocation();
    int dist1 = myLoc.distanceSquaredTo(corner1);
    int dist2 = myLoc.distanceSquaredTo(corner2);
    return dist1 < dist2 ? corner1 : corner2;
  }

  private MapLocation computeArmyRallyLocation(
      MapLocation enemyLoc, MapLocation archonHidingLoc) {
    int minX = mapBoundaryCalculator.getMinX();
    int maxX = mapBoundaryCalculator.getMaxX();
    int minY = mapBoundaryCalculator.getMinY();
    int maxY = mapBoundaryCalculator.getMaxY();

    MapLocation oppositeCorner = new MapLocation(
        minX + maxX - archonHidingLoc.x, minY + maxY - archonHidingLoc.y);
    double linearDist = Math.sqrt(oppositeCorner.distanceSquaredTo(enemyLoc));
    double cornerWeight = linearDist <= VIPER_SAC_RALLY_LINEAR_DISTANCE_FROM_ENEMY
        ? linearDist : VIPER_SAC_RALLY_LINEAR_DISTANCE_FROM_ENEMY;
    double enemyLocWeight = linearDist - cornerWeight;
    return new MapLocation(
        (int) ((cornerWeight * oppositeCorner.x + enemyLocWeight * enemyLoc.x) / linearDist),
        (int) ((cornerWeight * oppositeCorner.y + enemyLocWeight * enemyLoc.y) / linearDist));
  }
}
