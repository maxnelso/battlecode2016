package infestorbot2b;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class MapBoundaryCalculator {

  private static final int UNKNOWN_MAP_BOUNDARY_DELTA_INT = 101;

  private final RobotController rc;
  private final SymmetryCalculator symmetryCalculator;

  private int minX;
  private int maxX;
  private int minY;
  private int maxY;

  private boolean minXKnown;
  private boolean maxXKnown;
  private boolean minYKnown;
  private boolean maxYKnown;

  private boolean newBoundariesKnown;

  public MapBoundaryCalculator(RobotController rc, SymmetryCalculator symmetryCalculator) {
    this.rc = rc;
    this.symmetryCalculator = symmetryCalculator;

    minX = 0;
    maxX = 0;
    minY = 0;
    maxY = 0;
    minXKnown = false;
    maxXKnown = false;
    minYKnown = false;
    maxYKnown = false;
    newBoundariesKnown = false;
  }

  public void update() throws GameActionException {
    int range;
    switch (rc.getType()) {
      case ARCHON:
        range = 4;
        break;
      case SCOUT:
        range = 5;
        break;
      case SOLDIER:
      case GUARD:
      case VIPER:
      case TURRET:
      case TTM:
        range = 3;
        break;
      default:
        range = 1;
    }

    MapLocation myLoc = rc.getLocation();
    if (!minXKnown && !rc.onTheMap(myLoc.add(-range, 0))) {
      for (int i = 1; i <= range; i++) {
        if (!rc.onTheMap(myLoc.add(-i, 0))) {
          reportMinX(myLoc.x - i + 1);
          break;
        }
      }
      newBoundariesKnown = true;
    }
    if (!minYKnown && !rc.onTheMap(myLoc.add(0, -range))) {
      for (int i = 1; i <= range; i++) {
        if (!rc.onTheMap(myLoc.add(0, -i))) {
          reportMinY(myLoc.y - i + 1);
          break;
        }
      }
      newBoundariesKnown = true;
    }
    if (!maxXKnown && !rc.onTheMap(myLoc.add(range, 0))) {
      for (int i = 1; i <= range; i++) {
        if (!rc.onTheMap(myLoc.add(i, 0))) {
          reportMaxX(myLoc.x + i - 1);
          break;
        }
      }
      newBoundariesKnown = true;
    }
    if (!maxYKnown && !rc.onTheMap(myLoc.add(0, range))) {
      for (int i = 1; i <= range; i++) {
        if (!rc.onTheMap(myLoc.add(0, i))) {
          reportMaxY(myLoc.y + i - 1);
          break;
        }
      }
      newBoundariesKnown = true;
    }
  }

  public void shareNewMapBoundaries(MessageSender messageSender) throws GameActionException {
    if (!newBoundariesKnown) {
      return;
    }

    MapLocation myLoc = rc.getLocation();
    int minXDelta = minXKnown ? (myLoc.x - minX) : UNKNOWN_MAP_BOUNDARY_DELTA_INT;
    int maxXDelta = maxXKnown ? (maxX - myLoc.x) : UNKNOWN_MAP_BOUNDARY_DELTA_INT;
    int minYDelta = minYKnown ? (myLoc.y - minY) : UNKNOWN_MAP_BOUNDARY_DELTA_INT;
    int maxYDelta = maxYKnown ? (maxY - myLoc.y) : UNKNOWN_MAP_BOUNDARY_DELTA_INT;
    messageSender.sendMapBoundaries(minXDelta, maxXDelta, minYDelta, maxYDelta);
    newBoundariesKnown = false;
  }

  public void reportAllBoundaries(
      MapLocation senderLoc, int minXDelta, int maxXDelta, int minYDelta, int maxYDelta) {
    if (minXDelta != UNKNOWN_MAP_BOUNDARY_DELTA_INT) {
      reportMinX(senderLoc.x - minXDelta);
    }
    if (maxXDelta != UNKNOWN_MAP_BOUNDARY_DELTA_INT) {
      reportMaxX(senderLoc.x + maxXDelta);
    }
    if (minYDelta != UNKNOWN_MAP_BOUNDARY_DELTA_INT) {
      reportMinY(senderLoc.y - minYDelta);
    }
    if (maxYDelta != UNKNOWN_MAP_BOUNDARY_DELTA_INT) {
      reportMaxY(senderLoc.y + maxYDelta);
    }
  }

  public void reportMinX(int minX) {
    if (!minXKnown) {
      this.minX = minX;
      minXKnown = true;
    }
    if (!maxXKnown) {
      Integer maxX = symmetryCalculator.getOppositeXCoordinate(minX);
      if (maxX != null) {
        this.maxX = maxX;
        maxXKnown = true;
      }
    }
  }

  public void reportMaxX(int maxX) {
    if (!maxXKnown) {
      this.maxX = maxX;
      maxXKnown = true;
    }
    if (!minXKnown) {
      Integer minX = symmetryCalculator.getOppositeXCoordinate(maxX);
      if (minX != null) {
        this.minX = minX;
        minXKnown = true;
      }
    }
  }

  public void reportMinY(int minY) {
    if (!minYKnown) {
      this.minY = minY;
      minYKnown = true;
    }
    if (!maxYKnown) {
      Integer maxY = symmetryCalculator.getOppositeYCoordinate(minY);
      if (maxY != null) {
        this.maxY = maxY;
        maxYKnown = true;
      }
    }
  }

  public void reportMaxY(int maxY) {
    if (!maxYKnown) {
      this.maxY = maxY;
      maxYKnown = true;
    }
    if (!minYKnown) {
      Integer minY = symmetryCalculator.getOppositeYCoordinate(maxY);
      if (minY != null) {
        this.minY = minY;
        minYKnown = true;
      }
    }
  }

  public boolean isMinXKnown() {
    return minXKnown;
  }

  public boolean isMaxXKnown() {
    return maxXKnown;
  }

  public boolean isMinYKnown() {
    return minYKnown;
  }

  public boolean isMaxYKnown() {
    return maxYKnown;
  }

  public boolean allBoundariesKnown() {
    return minXKnown && minYKnown && maxXKnown && maxYKnown;
  }

  public int getMinX() {
    return minX;
  }

  public int getMaxX() {
    return maxX;
  }

  public int getMinY() {
    return minY;
  }

  public int getMaxY() {
    return maxY;
  }

  public void showDebugInfo() {
    rc.setIndicatorString(1, "minX? " + minX + ", maxX? " + maxX
        + ", minY? " + minY + ", maxY? " + maxY);
  }
}
