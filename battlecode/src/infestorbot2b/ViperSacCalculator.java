package infestorbot2b;

import battlecode.common.GameActionException;

public interface ViperSacCalculator {

  public void computeAndShareViperSac(MessageSender messageSender) throws GameActionException;
}
