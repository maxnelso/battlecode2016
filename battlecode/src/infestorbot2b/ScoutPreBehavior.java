package infestorbot2b;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class ScoutPreBehavior implements PreBehavior {

  private final RobotController rc;
  private final MessageReceiver messageReceiver;
  private final MessageSender messageSender;
  private final ZombieDenReporter zombieDenReporter;
  private final EnemyTurretCache enemyTurretCache;
  private final MapBoundaryCalculator mapBoundaryCalculator;

  public ScoutPreBehavior(
      RobotController rc,
      Radar radar,
      MessageReceiver messageReceiver,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      ZombieDenReporter zombieDenReporter,
      EnemyTurretCache enemyTurretCache,
      MapBoundaryCalculator mapBoundaryCalculator,
      EeHanTimingCalculator eeHanTimingCalculator,
      SymmetryCalculator symmetryCalculator) {
    this.rc = rc;
    this.messageReceiver = messageReceiver;
    this.messageSender = messageSender;
    this.zombieDenReporter = zombieDenReporter;
    this.enemyTurretCache = enemyTurretCache;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();
    updateAndShareZombieDens();
    updateAndShareEnemyTurrets();
    updateAndShareMapBoundaries();
  }

  private void updateAndShareZombieDens() throws GameActionException {
    zombieDenReporter.invalidateNearbyDestroyedDens();
    zombieDenReporter.searchForNewDens();
    zombieDenReporter.shareNewlyAddedAndRemovedDens(messageSender);
  }

  private void updateAndShareEnemyTurrets() throws GameActionException {
    enemyTurretCache.invalidateNearbyEnemyTurrets();
    enemyTurretCache.shareTurretBoundingBox(messageSender);
    int frequency = getSharingFrequency();
    int random = (rc.getRoundNum() + frequency / 3 + rc.getID());
    if (random % frequency == 0) {
      enemyTurretCache.shareRandomEnemyTurret(messageSender);
    }
  }

  private void updateAndShareMapBoundaries() throws GameActionException {
    mapBoundaryCalculator.update();
    mapBoundaryCalculator.shareNewMapBoundaries(messageSender);
    mapBoundaryCalculator.showDebugInfo();
  }

  private int getSharingFrequency() throws GameActionException {
    return Math.max(
        (rc.getRobotCount() / 10) + 1,
        (rc.getRoundNum() / 200) + 1);
  }
}
