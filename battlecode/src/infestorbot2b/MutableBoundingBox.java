package infestorbot2b;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class MutableBoundingBox implements BoundingBox {

  private int minX;
  private int maxX;
  private int minY;
  private int maxY;
  private int numElements;

  public MutableBoundingBox() {
    minX = 99999;
    maxX = -99999;
    minY = 99999;
    maxY = -99999;
    numElements = 0;
  }

  public void addMapLocation(MapLocation loc) {
    numElements++;
    if (loc.x < minX) {
      minX = loc.x;
    }
    if (loc.x > maxX) {
      maxX = loc.x;
    }
    if (loc.y < minY) {
      minY = loc.y;
    }
    if (loc.y > maxY) {
      maxY = loc.y;
    }
  }

  /** Returns whether the box changed. */
  public boolean merge(MapLocation topLeftCorner, int xRange, int yRange, int numElements) {
    int oldArea = getArea();
    minX = topLeftCorner.x < minX ? topLeftCorner.x : minX;
    maxX = topLeftCorner.x + xRange > maxX ? topLeftCorner.x + xRange : maxX;
    minY = topLeftCorner.y < minY ? topLeftCorner.y : minY;
    maxY = topLeftCorner.y + yRange > maxY ? topLeftCorner.y + yRange : maxY;
    this.numElements = numElements > this.numElements ? numElements : this.numElements;
    return oldArea == getArea();
  }

  @Override
  public MapLocation getTopLeftCorner() {
    return numElements == 0 ? null : new MapLocation(minX, minY);
  }

  @Override
  public MapLocation getCenter() {
    return numElements == 0 ? null : new MapLocation((minX + maxX) / 2, (minY + maxY) / 2);
  }

  @Override
  public int getNumElements() {
    return numElements;
  }

  @Override
  public int getXRange() {
    return numElements == 0 ? 0 : maxX - minX;
  }

  @Override
  public int getYRange() {
    return numElements == 0 ? 0 : maxY - minY;
  }

  @Override
  public int getArea() {
    return numElements == 0 ? 0 : (maxX - minX) * (maxY - minY);
  }

  @Override
  public boolean contains(MapLocation loc) {
    return containsWithMargins(loc, 0 /* margins */);
  }

  @Override
  public boolean containsWithMargins(MapLocation loc, int margins) {
    return numElements != 0
        && loc.x >= minX - margins
        && loc.x <= maxX + margins
        && loc.y >= minY - margins
        && loc.y <= maxY + margins;
  }

  @Override
  public void showDebugInfo(RobotController rc) {
    if (numElements > 0) {
      MapLocation c1 = new MapLocation(minX, minY);
      MapLocation c2 = new MapLocation(maxX, minY);
      MapLocation c3 = new MapLocation(maxX, maxY);
      MapLocation c4 = new MapLocation(minX, maxY);
      rc.setIndicatorLine(c1, c2, 255, 255, 255);
      rc.setIndicatorLine(c2, c3, 255, 255, 255);
      rc.setIndicatorLine(c3, c4, 255, 255, 255);
      rc.setIndicatorLine(c4, c1, 255, 255, 255);
      rc.setIndicatorLine(rc.getLocation(), c3, 255, 255, 255);
    }
  }
}
