package infestorbot2b;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import infestorbot2b.AlliedArchonTracker.AlliedArchonInfo;

public class HealingBehavior implements Behavior {

  private final int ARCHON_TIMEOUT = 150;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;
  private final AttackSystem attackSystem;
  private final Radar radar;

  private MapLocation closestAlliedArchonLoc;

  public HealingBehavior(
      RobotController rc,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker,
      Radar radar,
      AttackSystem attackSystem) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
    this.attackSystem = attackSystem;
    this.radar = radar;
  }

  @Override
  public void run() throws GameActionException {
    // Try and retreat first
    if (rc.isCoreReady()) {
      // Check to see if there are any archons in sensor range
      MapLocation target = null;
      if (closestAlliedArchonLoc != null) {
        target = closestAlliedArchonLoc;
      }
      rc.setIndicatorString(0, "HEALING " + target);
      if (target != null) {
        RobotInfo[] hostiles = radar.getNearbyHostiles();
        if (hostiles.length > 0) {
          navigation.smartRetreat(target, hostiles);
        } else if (rc.getLocation().distanceSquaredTo(target) <= 9) {
          navigation.directToAvoidingAlliedArchons(target, 2 /* avoidDistance */,
              true /* clearRubble */);
        } else {
          if (!navigation.bugTo(target, true /* avoidAttackers */, true /* clearRubble */)) {
            if (!navigation.retreatFromTurrets()) {
              navigation.bugTo(target, false /* avoidAttackers */, true /* clearRubble */);
            }
          }
        }

      }
    }
    if (rc.isWeaponReady()) {
      RobotInfo[] enemies = radar.getNearbyEnemies();
      RobotInfo[] allies = radar.getNearbyAllies();
      MapLocation target = attackSystem.getBestEnemyToShoot(rc, enemies,
          enemies.length, allies,
          allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
      RobotInfo[] zombies = radar.getNearbyZombies();
      target = attackSystem.getBestZombieToShoot(rc, zombies,
          zombies.length, allies, allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
    }
  }

  public boolean shouldHeal() {
    if (rc.getType() != RobotType.VIPER) {
      return false;
    }

    boolean willDieFromViper = rc.getViperInfectedTurns() * 2 >= rc.getHealth();
    if (willDieFromViper) {
      return false;
    }

    RobotInfo[] allies = radar.getNearbyAllies();
    int closestDist = 999999;
    closestAlliedArchonLoc = null;
    MapLocation myLoc = rc.getLocation();
    for (int i = allies.length; --i >= 0;) {
      RobotInfo ally = allies[i];
      if (ally.type == RobotType.ARCHON) {
        int dist = ally.location.distanceSquaredTo(ally.location);
        if (dist < closestDist) {
          closestAlliedArchonLoc = ally.location;
          closestDist = dist;
        }
      }
    }

    if (closestAlliedArchonLoc == null) {
      AlliedArchonInfo[] alliedArchons = alliedArchonTracker.getAlliedArchons();
      int closestReportedDist = 999999;
      int roundNum = rc.getRoundNum();
      for (int i = alliedArchons.length; --i >= 0;) {
        AlliedArchonInfo archon = alliedArchons[i];
        if (roundNum - archon.timestamp > ARCHON_TIMEOUT) {
          continue;
        }
        int dist = archon.loc.distanceSquaredTo(myLoc);
        if (dist < closestReportedDist) {
          closestReportedDist = dist;
          closestAlliedArchonLoc = archon.loc;
        }
      }
    }

    rc.setIndicatorString(0, " " + closestAlliedArchonLoc + " " + rc.getRoundNum());
    if (closestAlliedArchonLoc == null) {
      return false;
    }
    rc.setIndicatorString(0, " " + closestAlliedArchonLoc + " " + rc.getRoundNum());
    RobotInfo[] hostiles = radar.getNearbyHostiles();
    Direction toClosestArchon = myLoc.directionTo(closestAlliedArchonLoc);
    for (int i = hostiles.length; --i >= 0;) {
      MapLocation loc = hostiles[i].location;
      if (myLoc.directionTo(loc) == toClosestArchon) {
        return false;
      }
    }
    return true;
  }
}
