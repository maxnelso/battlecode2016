package infestorbot2b;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface AttackSystem {
  public MapLocation getBestEnemyToShoot(RobotController rc, RobotInfo[] enemies,
      int numEnemies,
      RobotInfo[] allies, int numAllies);

  public MapLocation getBestZombieToShoot(RobotController rc, RobotInfo[] zombies,
      int numEnemies,
      RobotInfo[] allies, int numAllies);
}
