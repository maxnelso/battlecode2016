package infestorbot2b;

import battlecode.common.GameActionException;

public interface MessageReceiver {

  public void receiveMessages() throws GameActionException;
}
