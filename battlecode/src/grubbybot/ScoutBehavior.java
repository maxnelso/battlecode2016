package grubbybot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class ScoutBehavior implements Behavior {

  private final int MAP_BOUNDARY_SHARE_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 20000;
  private static final int MESSAGING_BYTECODE_LIMIT = 0;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final PartScanner partScanner;
  private final ArchonTracker archonTracker;
  private final ZombieDragger zombieDragger;

  public ScoutBehavior(
      NavigationSystem navigation,
      Radar radar,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      ArchonTracker archonTracker,
      ZombieDragger zombieDragger,
      PartScanner partScanner) {
    this.navigation = navigation;
    this.radar = radar;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.partScanner = partScanner;
    this.archonTracker = archonTracker;
    this.zombieDragger = zombieDragger;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    partScanner.scanForParts(rc, interestingTargets);
    mapBoundaryCalculator.update(rc);

    interestingTargets.shareRandomTarget(rc, messageSender);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
    shareExtendedBroadcast(rc, messageSender);

    if (!zombieDragger.dragZombies(rc)) {
      MapLocation target = explorationCalculator.calculate(rc);
      navigation.directTo(rc, target, true /* avoidAttackers */, false /* clearRubble */,
          false /* onlyForward */);
    }
    if ((rc.getRoundNum() - rc.getID()) % MAP_BOUNDARY_SHARE_FREQUENCY == 0) {
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }
  }

  private void shareExtendedBroadcast(RobotController rc, MessageSender sender)
      throws GameActionException {
    RobotInfo closestEnemyUnableToMove = radar.getClosestUnableToMoveEnemyOrZombie();
    if (closestEnemyUnableToMove != null) {
      sender.sendEnemyInfo(rc, closestEnemyUnableToMove);
    }
  }
}