package grubbybot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import grubbybot.ArchonTracker.ArchonStatus;
import grubbybot.InterestingTargets.Interest;
import grubbybot.InterestingTargets.InterestingTarget;

public class ArchonBehavior implements Behavior {

  private final int MAX_MAKE_SCOUT_ROUND = 50;
  private final int SCOUTS_TO_MAKE = 2;
  private final int MAP_AND_TARGET_SHARING_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 6000;
  private static final int MESSAGING_BYTECODE_LIMIT = 7000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final RepairSystem repairSystem;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final ExplorationCalculator explorationCalculator;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ArchonTracker archonTracker;
  private final PartScanner partScanner;

  private int scoutsMade;

  public ArchonBehavior(
      NavigationSystem navigation,
      Radar radar,
      RepairSystem repairSystem,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      ExplorationCalculator explorationCalculator,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker,
      PartScanner partScanner) {
    this.navigation = navigation;
    this.radar = radar;
    this.repairSystem = repairSystem;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.explorationCalculator = explorationCalculator;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.archonTracker = archonTracker;
    this.partScanner = partScanner;

    scoutsMade = 0;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    mapBoundaryCalculator.update(rc);
    partScanner.scanForParts(rc, interestingTargets);
    MapLocation loc = repairSystem.repairMostHealthAllyThatNeedsRepairing(rc);
    rc.setIndicatorString(2, "Repairing loc " + loc);

    MapLocation currentTarget = null;

    if (scoutsMade < SCOUTS_TO_MAKE
        && rc.getRoundNum() < MAX_MAKE_SCOUT_ROUND
        && makeUnit(rc, RobotType.SCOUT)) {
      scoutsMade++;
    } else {
      MapLocation adjacentNeutralRobot = radar.getAdjacentNeutralRobot(rc.getLocation());

      if (rc.isCoreReady() && adjacentNeutralRobot != null) {
        rc.activate(adjacentNeutralRobot);
      } else {
        makeUnit(rc, RobotType.SOLDIER);
      }

      InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
          rc.getLocation(),
          false /* includeDens */,
          true /* includeParts */,
          true /* includeNeutralRobots */);
      currentTarget = interestingTarget == null
          ? explorationCalculator.calculate(rc)
          : interestingTarget.loc;
      rc.setIndicatorString(0, "Target = " + currentTarget.toString());
      // Invalidate the current interesting target if necessary.
      if (interestingTarget != null && rc.canSenseLocation(currentTarget)) {
        if (interestingTarget.interest == Interest.PARTS && rc.senseParts(currentTarget) < 1) {
          interestingTargets.reportPartsRetrieved(currentTarget);
        } else if (interestingTarget.interest == Interest.NEUTRAL_ROBOT) {
          RobotInfo robot = rc.senseRobotAtLocation(currentTarget);
          if (robot == null || robot.team != Team.NEUTRAL) {
            interestingTargets.reportNeutralRobotActivated(currentTarget);
          }
        }
      }

      navigation.directTo(rc, currentTarget, true /* avoidAttackers */, true /* clearRubble */,
          false /* onlyForward */);
    }

    if ((rc.getRoundNum() - rc.getID()) % MAP_AND_TARGET_SHARING_FREQUENCY == 0) {
      interestingTargets.shareRandomTarget(rc, messageSender);
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }

    archonTracker.updateSelfInformation(rc, currentTarget);
    archonTracker.shareRandomArchonInformation(rc, messageSender);

    setDebugIndicatorLines(rc, archonTracker, interestingTargets, currentTarget);
  }

  private boolean makeUnit(RobotController rc, RobotType type) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }

    if (radar.getClosestEnemyOrZombie() != null) {
      return false;
    }

    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 0; i < 8; i++) {
      if (rc.hasBuildRequirements(type) && rc.canBuild(d, type)) {
        rc.build(d, type);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }

  private void setDebugIndicatorLines(
      RobotController rc,
      ArchonTracker archonTracker,
      InterestingTargets interestingTargets,
      MapLocation currentTarget) {
    int[] currentTargetColor = new int[] {
      255, 255, 255
    };
    int[] denColor = new int[] {
      0, 255, 0
    };
    int[] partsColor = new int[] {
      255, 0, 255
    };
    int[] neutralColor = new int[] {
      100, 100, 100
    };
    int[] enemyArchonColor = new int[] {
      100, 0, 0
    };
    int[] alliedArchonColor = new int[] {
      0, 0, 100
    };

    MapLocation myLoc = rc.getLocation();
    if (currentTarget != null) {
      rc.setIndicatorLine(
          myLoc,
          currentTarget,
          currentTargetColor[0],
          currentTargetColor[1],
          currentTargetColor[2]);
    }

    InterestingTarget<Integer>[] dens = interestingTargets.getDens();
    for (int i = 0; i < dens.length; i++) {
      MapLocation den = dens[i].loc;
      rc.setIndicatorLine(myLoc, den, denColor[0], denColor[1], denColor[2]);
    }

    InterestingTarget<Integer>[] parts = interestingTargets.getParts();
    for (int i = 0; i < parts.length; i++) {
      MapLocation part = parts[i].loc;
      rc.setIndicatorLine(myLoc, part, partsColor[0], partsColor[1], partsColor[2]);
    }

    InterestingTarget<RobotType>[] neutrals = interestingTargets.getNeutralRobots();
    for (int i = 0; i < neutrals.length; i++) {
      MapLocation neutral = neutrals[i].loc;
      rc.setIndicatorLine(myLoc, neutral, neutralColor[0], neutralColor[1], neutralColor[2]);
    }

    ArchonStatus[] enemyArchons = archonTracker.getEnemyArchons(rc);
    for (int i = 0; i < enemyArchons.length; i++) {
      rc.setIndicatorLine(
          myLoc, enemyArchons[i].lastKnownLocation, enemyArchonColor[0],
          enemyArchonColor[1],
          enemyArchonColor[2]);
    }

    ArchonStatus[] alliedArchons = archonTracker.getAlliedArchons(rc);
    for (int i = 0; i < alliedArchons.length; i++) {
      rc.setIndicatorLine(
          myLoc, alliedArchons[i].lastKnownLocation, alliedArchonColor[0],
          alliedArchonColor[1],
          alliedArchonColor[2]);
    }
  }
}
