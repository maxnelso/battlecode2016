package grubbybot;

import battlecode.common.GameConstants;
import battlecode.common.MapLocation;

public class ArrayMapLocationSet implements MapLocationSet {

  private static final int SET_SIZE = GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_WIDTH;

  private boolean[] set;

  public ArrayMapLocationSet() {
    set = new boolean[SET_SIZE];
  }

  @Override
  public boolean contains(MapLocation loc) {
    return set[getHash(loc)];
  }

  @Override
  public void add(MapLocation loc) {
    set[getHash(loc)] = true;
  }

  @Override
  public void remove(MapLocation loc) {
    set[getHash(loc)] = false;
  }

  private static int getHash(MapLocation loc) {
    return (loc.x + 16000 + GameConstants.MAP_MAX_HEIGHT * (loc.y + 16000))
        % (GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_WIDTH);
  }
}
