package dryadbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ArchonBehavior implements Behavior {

  private static final int SELF_ARCHON_LOCATION_MESSAGE_DISTANCE_MULTIPLIER = 9;

  private final RobotController rc;
  private final Radar radar;
  private final MessageSender messageSender;
  private final AlliedArchonTracker alliedArchonTracker;
  private final RepairSystem repairSystem;

  private final ScoutUnitOrder scoutUnitOrder;
  private final DryadArcherUnitOrder dryadArcherUnitOrder;

  private final OpeningArchonBehavior openingBehavior;
  private final FightingArchonBehavior fightingBehavior;
  private final PickerUpperArchonBehavior pickerUpperBehavior;
  private final LeaderArchonBehavior leaderBehavior;
  private final FollowerArchonBehavior followerBehavior;
  private final ExploringBehavior exploringBehavior;

  public ArchonBehavior(
      RobotController rc,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      RepairSystem repairSystem,
      ZombieDenReporter zombieDenReporter) {
    this.rc = rc;
    this.radar = radar;
    this.messageSender = messageSender;
    this.alliedArchonTracker = alliedArchonTracker;
    this.repairSystem = repairSystem;

    scoutUnitOrder = new ScoutUnitOrder();
    dryadArcherUnitOrder = new DryadArcherUnitOrder(rc);

    openingBehavior = new OpeningArchonBehavior(rc, scoutUnitOrder);
    fightingBehavior = new FightingArchonBehavior(rc, radar, navigation, messageSender);
    pickerUpperBehavior = new PickerUpperArchonBehavior(rc, navigation);
    leaderBehavior = new LeaderArchonBehavior(rc, navigation, zombieDenReporter);
    followerBehavior = new FollowerArchonBehavior(rc, navigation, alliedArchonTracker);
    PatrolWaypointCalculator patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);
    exploringBehavior = new ExploringBehavior(rc, navigation, patrolWaypointCalculator);
  }

  @Override
  public void run() throws GameActionException {
    messageSender.sendSelfArchonLocation(SELF_ARCHON_LOCATION_MESSAGE_DISTANCE_MULTIPLIER);
    if (openingBehavior.isOpeningOver()
        && radar.getNearbyHostiles().length == 0
        && UnitSpawner.spawn(rc, dryadArcherUnitOrder.getNextUnit())) {
      dryadArcherUnitOrder.computeNextUnit();
    }
    getCurrentBehavior().run();
    MapLocation target = repairSystem.getBestAllyToHeal(radar.getNearbyAllies());
    if (target != null) {
      rc.setIndicatorString(2, "Healing target " + target + " " + rc.getRoundNum());
      rc.repair(target);
    }
  }

  private Behavior getCurrentBehavior() throws GameActionException {
    if (!openingBehavior.isOpeningOver()) {
      return openingBehavior;
    }
    if (radar.getNearbyHostiles().length != 0) {
      return fightingBehavior;
    }
    if (pickerUpperBehavior.getTarget() != null) {
      return pickerUpperBehavior;
    }
    if (alliedArchonTracker.getLowestIdAlliedArchon(rc.getLocation()) != null) {
      return followerBehavior;
    }
    if (leaderBehavior.getTarget() != null) {
      return leaderBehavior;
    }
    return exploringBehavior;
  }
}
