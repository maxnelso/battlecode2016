package dryadbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import dryadbot.AlliedArchonTracker.AlliedArchonInfo;
import dryadbot.EnemyArchonTracker.EnemyArchonInfo;

public class ZombieDraggingScoutBehavior implements Behavior {
  private static final int DIVEBOMB_DISTANCE = 24;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final Radar radar;
  private final EnemyArchonTracker enemyArchonTracker;
  private final AlliedArchonTracker alliedArchonTracker;

  public ZombieDraggingScoutBehavior(RobotController rc, NavigationSystem navigation, Radar radar,
      EnemyArchonTracker enemyArchonTracker, AlliedArchonTracker alliedArchonTracker) {
    this.rc = rc;
    this.navigation = navigation;
    this.radar = radar;
    this.enemyArchonTracker = enemyArchonTracker;
    this.alliedArchonTracker = alliedArchonTracker;
  }

  @Override
  public void run() throws GameActionException {
    rc.setIndicatorString(0, "DRAGGING");

    if (!isAttackableByZombieNextTwoTurns()) {
      return;
    }
    EnemyArchonInfo[] enemyArchons = enemyArchonTracker.getSortedEnemyArchonInfos(rc.getLocation());

    // Try running to each enemy
    for (int i = 0; i < enemyArchons.length; i++) {
      MapLocation loc = enemyArchons[i].loc;
      if (rc.getLocation().distanceSquaredTo(loc) <= DIVEBOMB_DISTANCE) {

        if (rc.getLocation().distanceSquaredTo(loc) <= 13) {
          rc.disintegrate();
        }
        if (rc.getInfectedTurns() > 0) { // Divebomb
          if (navigation.directTo(loc,
              false /* avoidEnemies */,
              false /* clearRubble */)) {
            rc.setIndicatorString(1, "Dragging " + loc + " " + rc
                .getRoundNum() + " with divebomb " + true);
            return;
          }
        } else { // Wait to be infected
          return;
        }
      } else {
        rc.setIndicatorString(1, "Dragging to enemy " + loc);
        if (!navigation.directToOnlyForward(loc,
            true /* avoidEnemies */,
            false /* clearRubble */)) {
          navigation.directToOnlyForward(loc,
              false /* avoidEnemies */,
              false /* clearRubble */);
          return;
        }
      }
    }

    // If all else fails just run away from the closest allied archon
    AlliedArchonInfo closestArchon = alliedArchonTracker.getClosestAlliedArchon(rc.getLocation());
    if (closestArchon == null) {
      return;
    }
    Direction d = rc.getLocation().directionTo(closestArchon.loc).opposite();
    MapLocation dragLocation = rc.getLocation().add(d, 100);
    rc.setIndicatorString(1, "Dragging away from allied archon " + dragLocation + " " + rc
        .getRoundNum());
    if (!navigation.directToOnlyForward(dragLocation,
        true /* avoidEnemies */,
        false /* clearRubble */)) {
      navigation.directToOnlyForward(dragLocation,
          false /* avoidEnemies */,
          false /* clearRubble */);
    }
  }

  private boolean isAttackableByZombieNextTwoTurns() {
    RobotInfo[] zombies = radar.getNearbyZombies();
    for (int i = zombies.length; --i >= 0;) {
      RobotInfo zombie = zombies[i];
      int dist = zombie.location.distanceSquaredTo(rc.getLocation());
      if (dist > 10) {
        if (zombie.type == RobotType.RANGEDZOMBIE
            && dist <= RobotType.RANGEDZOMBIE.attackRadiusSquared) {
          return true;
        }
        continue;
      } else if (dist <= 2) {
        if (zombie.weaponDelay < 3) {
          return true;
        }
      } else {
        if (zombie.coreDelay < 2 && zombie.weaponDelay < 2) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean shouldDrag() {
    RobotInfo[] zombies = radar.getNearbyZombies();
    int outbreakLevel = rc.getRoundNum() / 300;
    double multiplier;
    switch (outbreakLevel) {
      case 0:
        multiplier = 1;
        break;
      case 1:
        multiplier = 1.1;
        break;
      case 2:
        multiplier = 1.2;
        break;
      case 3:
        multiplier = 1.3;
        break;
      case 4:
        multiplier = 1.5;
        break;
      case 5:
        multiplier = 1.7;
        break;
      case 6:
        multiplier = 2.0;
        break;
      case 7:
        multiplier = 2.3;
        break;
      case 8:
        multiplier = 2.6;
        break;
      case 9:
        multiplier = 3.0;
        break;
      default:
        outbreakLevel -= 9;
        multiplier = 3.0 + outbreakLevel;
        break;
    }
    int score = 0;
    for (int i = zombies.length; --i >= 0;) {
      RobotInfo zombie = zombies[i];
      switch (zombie.type) {
        case STANDARDZOMBIE:
          score += 2;
          break;
        case RANGEDZOMBIE:
          score += 10;
          break;
        case BIGZOMBIE:
          score += 10;
          break;
        case FASTZOMBIE:
          score += 10;
          break;
        default:
          break;
      }
    }
    rc.setIndicatorString(2, " " + (score * multiplier));
    return score * multiplier >= 20;
  }
}
