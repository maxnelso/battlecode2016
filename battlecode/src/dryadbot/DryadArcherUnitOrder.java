package dryadbot;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class DryadArcherUnitOrder implements UnitOrder {

  private RobotController rc;
  private RobotType nextUnit;
  private static final int MAKE_VIPER_ROUND = 300;

  public DryadArcherUnitOrder(RobotController rc) {
    this.rc = rc;
    this.nextUnit = RobotType.SOLDIER;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (rc.getRoundNum() <= MAKE_VIPER_ROUND) {
      nextUnit = RobotType.SOLDIER;
      return;
    }
    nextUnit = Math.random() < .4 ? RobotType.VIPER : RobotType.SOLDIER;
  }
}
