package dryadbot;

import battlecode.common.GameActionException;

public class NoOpPreBehavior implements PreBehavior {

  @Override
  public void preRun() throws GameActionException {}
}
