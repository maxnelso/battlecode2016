package dryadbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ExploringBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;

  public ExploringBehavior(
      RobotController rc,
      NavigationSystem navigation,
      PatrolWaypointCalculator patrolWaypointCalculator) {
    this.rc = rc;
    this.navigation = navigation;

    mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    explorationCalculator = new ExplorationCalculator(
        rc, mapBoundaryCalculator, patrolWaypointCalculator);
  }

  @Override
  public void run() throws GameActionException {
    mapBoundaryCalculator.update();
    MapLocation loc = explorationCalculator.calculate();
    rc.setIndicatorString(0, "I'm exploring " + loc + ".");
    if (!navigation.directTo(loc, true /* avoidAttackers */, false /* clearRubble */)) {
      navigation.directTo(loc, false /* avoidAttackers */, false /* clearRubble */);
    }
  }
}
