package dryadbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class LeaderArchonBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final ZombieDenReporter zombieDenReporter;

  private MapLocation currentTarget;
  private int currentTargetRound;

  public LeaderArchonBehavior(
      RobotController rc,
      NavigationSystem navigation,
      ZombieDenReporter zombieDenReporter) {
    this.rc = rc;
    this.navigation = navigation;
    this.zombieDenReporter = zombieDenReporter;

    currentTarget = null;
    currentTargetRound = -1;
  }

  @Override
  public void run() throws GameActionException {
    MapLocation target = getTarget();
    if (target != null) {
      rc.setIndicatorString(0, "I'm leading, target is " + target + ".");
      navigation.directTo(target, false /* avoidAttackers */, true /* clearRubble */);
    } else {
      rc.setIndicatorString(0, "I'm a lost leader.");
      navigation.moveRandomly();
    }
  }

  public MapLocation getTarget() {
    int roundNum = rc.getRoundNum();
    if (currentTargetRound >= roundNum) {
      return currentTarget;
    }

    currentTargetRound = roundNum;
    currentTarget = zombieDenReporter.getClosestDen();
    return currentTarget;
  }
}
