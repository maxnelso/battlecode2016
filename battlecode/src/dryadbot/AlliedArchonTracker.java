package dryadbot;

import battlecode.common.MapLocation;

public interface AlliedArchonTracker {

  public static class AlliedArchonInfo {
    public final int id;
    public final MapLocation loc;

    public AlliedArchonInfo(int id, MapLocation loc) {
      this.id = id;
      this.loc = loc;
    }
  }

  public void reportAlliedArchon(int archonId, MapLocation archonLoc);

  public AlliedArchonInfo getClosestAlliedArchon(MapLocation myLoc);

  public AlliedArchonInfo getLowestIdAlliedArchon(MapLocation myLoc);

  public boolean isInRangeOfAlliedArchon(MapLocation loc, int range);

  public void showDebugInfo();
}
