package dryadbot;

import battlecode.common.GameActionException;

public interface MessageReceiver {

  public void receiveMessages() throws GameActionException;
}
