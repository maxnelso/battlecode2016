package dryadbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class FightingArchonBehavior implements Behavior {

  private final RobotController rc;
  private final Radar radar;
  private final NavigationSystem navigation;
  private final MessageSender messageSender;

  public FightingArchonBehavior(
      RobotController rc,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender) {
    this.rc = rc;
    this.radar = radar;
    this.navigation = navigation;
    this.messageSender = messageSender;
  }

  @Override
  public void run() throws GameActionException {
    MapLocation myLoc = rc.getLocation();
    RobotInfo closestHostile = RadarUtils.getClosestRobot(radar.getNearbyHostiles(), myLoc);
    if (closestHostile == null) {
      return;
    }

    messageSender.sendArmyRallyLocation(closestHostile.location);

    // TODO(jven): Use allied strength.
    boolean alliesNearMe = radar.getNearbyAllies().length >= 3;
    int dist = myLoc.distanceSquaredTo(closestHostile.location);
    if (dist <= 20 || !alliesNearMe) {
      Direction retreatDir = myLoc.directionTo(closestHostile.location).opposite();
      MapLocation target = myLoc.add(retreatDir, 10);
      if (!navigation.directTo(target, true /* avoidAttackers */, false /* clearRubble */)) {
        navigation.directTo(target, false /* avoidAttackers */, false /* clearRubble */);
      }
      rc.setIndicatorString(0, "I'm retreating " + retreatDir + "!");
    } else {
      rc.setIndicatorString(0, "I'm in a fight!");
    }
  }
}
