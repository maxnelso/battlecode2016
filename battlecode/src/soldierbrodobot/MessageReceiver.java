package soldierbrodobot;

import battlecode.common.GameActionException;

public interface MessageReceiver {

  public void receiveMessages() throws GameActionException;
}
