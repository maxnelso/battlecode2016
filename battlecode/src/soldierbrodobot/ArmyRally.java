package soldierbrodobot;

import battlecode.common.MapLocation;

public interface ArmyRally {

  public MapLocation getRally();
}
