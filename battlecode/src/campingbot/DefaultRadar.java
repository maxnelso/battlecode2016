package campingbot;

import battlecode.common.Clock;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class DefaultRadar implements Radar {

  private static final int MAX_ROBOTS = 100;
  private static final int ZOMBIE_DRAGGING_STRENGTH = 50;

  private final InterestingTargets interestingTargets;
  private final ArchonTracker archonTracker;

  private static class Forces {
    // private final boolean computeBuddyTurret;

    private RobotInfo[] robots;
    private int numRobots;
    private RobotInfo leastHealth;
    private RobotInfo leastHealthInAttackRange;
    private RobotInfo leastHealthAttackerInAttackRange;
    private RobotInfo leastHealthInfectedInAttackRange;
    private RobotInfo leastHealthNotInfectedInAttackRange;
    private RobotInfo closest;
    private RobotInfo closestUnableToMove;
    private int closestDist;
    private int closestUnableToMoveDist;
    private int strength;

    private int closestArchonDist;
    private RobotInfo closestArchon;

    public Forces(
        ReportedRobotInfos reportedRobots,
        RobotController rc,
        boolean computeBuddyTurrets) {
      // this.computeBuddyTurret = computeBuddyTurret;
      robots = new RobotInfo[MAX_ROBOTS];
      numRobots = 0;
      leastHealth = null;
      leastHealthInAttackRange = null;
      leastHealthAttackerInAttackRange = null;
      leastHealthInfectedInAttackRange = null;
      leastHealthNotInfectedInAttackRange = null;
      closestDist = 0;
      closestArchonDist = 0;
      closest = null;
      if (reportedRobots != null && rc != null) {
        for (int i = 0; i < reportedRobots.numReportedRobots; i++) {
          addRobot(reportedRobots.reportedRobots[i], rc);
        }
      }
    }

    public void addRobot(RobotInfo robot, RobotController rc) {
      MapLocation myLoc = rc.getLocation();
      int attackRange = rc.getType().attackRadiusSquared;

      if (numRobots < MAX_ROBOTS) {
        robots[numRobots] = robot;
        numRobots++;
      }

      strength += getStrengthOfRobot(robot);
      int dist = myLoc.distanceSquaredTo(robot.location);
      if (closest == null || dist < closestDist) {
        closestDist = dist;
        closest = robot;
      }
      if (closestArchon == null || dist < closestArchonDist) {
        closestArchonDist = dist;
        closestArchon = robot;
      }

      if (leastHealth == null || robot.health < leastHealth.health) {
        leastHealth = robot;
      }
      if (myLoc.distanceSquaredTo(robot.location) <= attackRange) {
        if (leastHealthInAttackRange == null || robot.health < leastHealthInAttackRange.health) {
          leastHealthInAttackRange = robot;
        }
        if (robot.type.canAttack()
            && (leastHealthAttackerInAttackRange == null
                || robot.health < leastHealthAttackerInAttackRange.health)) {
          leastHealthAttackerInAttackRange = robot;
        }
        if (robot.viperInfectedTurns + robot.zombieInfectedTurns > 0) {
          if (leastHealthInfectedInAttackRange == null
              || robot.health < leastHealthInfectedInAttackRange.health) {
            leastHealthInfectedInAttackRange = robot;
          }
        } else {
          if (leastHealthNotInfectedInAttackRange == null
              || robot.health < leastHealthNotInfectedInAttackRange.health) {
            leastHealthNotInfectedInAttackRange = robot;
          }
        }
      }

      if (robot.coreDelay >= 1 || robot.type == RobotType.TURRET) {
        if (closestUnableToMove == null || dist < closestUnableToMoveDist) {
          closestUnableToMove = robot;
          closestUnableToMoveDist = dist;
        }
      }
    }

    private int getStrengthOfRobot(RobotInfo robotInfo) {
      switch (robotInfo.type) {
        case SOLDIER:
          return 10;
        case GUARD:
          return 2;
        case VIPER:
          return 4;
        case TTM:
          return 1;
        case TURRET:
          return 30;
        case SCOUT:
          return 0;
        case ARCHON:
          return 1;
        case STANDARDZOMBIE:
          return 2;
        case RANGEDZOMBIE:
          return 10;
        case FASTZOMBIE:
          return 10;
        case BIGZOMBIE:
          return 5;
        case ZOMBIEDEN:
        default:
          return 0;
      }
    }
  }

  public static class ReportedRobotInfos {

    public RobotInfo[] reportedRobots;
    public int numReportedRobots;

    public ReportedRobotInfos() {
      reportedRobots = new RobotInfo[MAX_ROBOTS];
      numReportedRobots = 0;
    }
  }

  private Forces allyForces;
  private Forces enemyForces;
  private Forces neutralForces;
  private Forces zombieForces;

  private ReportedRobotInfos allyReportedRobots;
  private ReportedRobotInfos enemyReportedRobots;
  private ReportedRobotInfos zombieReportedRobots;

  public DefaultRadar(InterestingTargets interestingTargets, ArchonTracker archonTracker) {
    this.interestingTargets = interestingTargets;
    this.archonTracker = archonTracker;

    allyReportedRobots = new ReportedRobotInfos();
    enemyReportedRobots = new ReportedRobotInfos();
    zombieReportedRobots = new ReportedRobotInfos();

    allyForces = new Forces(allyReportedRobots, null, true /* computeBuddyTurrets */);
    enemyForces = new Forces(enemyReportedRobots, null, false /* computeBuddyTurrets */);
    neutralForces = new Forces(null, null, false /* computeBuddyTurrets */);
    zombieForces = new Forces(zombieReportedRobots, null, false /* computeBuddyTurrets */);

  }

  @Override
  public void update(RobotController rc, int bytecodeLimit) {
    int initialBytecodesLeft = Clock.getBytecodesLeft();

    // TODO Some mechanism for X bytecode cleanups?
    cleanupReportedRobots(rc);

    allyForces = new Forces(allyReportedRobots, rc, true /* computeBuddyTurrets */);
    enemyForces = new Forces(enemyReportedRobots, rc, false /* computeBuddyTurrets */);
    neutralForces = new Forces(null, null, false /* computeBuddyTurrets */);
    zombieForces = new Forces(zombieReportedRobots, rc, false /* computeBuddyTurrets */);

    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    for (int i = 0; i < nearbyRobots.length; i++) {
      Team team = nearbyRobots[i].team;
      Forces forces;
      if (team == rc.getTeam()) {
        forces = allyForces;
      } else if (team == rc.getTeam().opponent()) {
        forces = enemyForces;
      } else if (team == Team.NEUTRAL) {
        forces = neutralForces;
      } else {
        forces = zombieForces;
      }

      forces.addRobot(nearbyRobots[i], rc);
      interestingTargets.reportRobotInfo(rc, nearbyRobots[i]);
      if (nearbyRobots[i].type.equals(RobotType.ARCHON)) {
        archonTracker.processArchon(nearbyRobots[i].ID,
            nearbyRobots[i].location,
            null /* target */,
            rc.getRoundNum(),
            nearbyRobots[i].team == rc.getTeam());
      }

      int bytecodesLeft = Clock.getBytecodesLeft();
      if (bytecodesLeft < 500 || initialBytecodesLeft - bytecodesLeft >= bytecodeLimit) {
        return;
      }
    }
  }

  @Override
  public MapLocation getClosestAlly() {
    return allyForces.closest != null
        ? allyForces.closest.location
        : null;
  }

  @Override
  public MapLocation getClosestEnemy() {
    return enemyForces.closest != null
        ? enemyForces.closest.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthAllyInRepairRange() {
    return allyForces.leastHealthNotInfectedInAttackRange != null
        ? allyForces.leastHealthNotInfectedInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getClosestEnemyOrZombie() {
    return enemyForces.closest != null
        ? enemyForces.closest.location
        : (zombieForces.closest != null ? zombieForces.closest.location : null);
  }

  @Override
  public MapLocation getClosestZombie() {
    return (zombieForces.closest != null ? zombieForces.closest.location : null);
  }

  @Override
  public MapLocation getLeastHealthEnemyAttackerInAttackRange() {
    return enemyForces.leastHealthAttackerInAttackRange != null
        ? enemyForces.leastHealthAttackerInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthEnemyInAttackRange() {
    return enemyForces.leastHealthInAttackRange != null
        ? enemyForces.leastHealthInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthZombieAttackerInAttackRange() {
    return zombieForces.leastHealthAttackerInAttackRange != null
        ? zombieForces.leastHealthAttackerInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthZombieInAttackRange() {
    return zombieForces.leastHealthInAttackRange != null
        ? zombieForces.leastHealthInAttackRange.location
        : null;
  }

  @Override
  public boolean isAttackableByZombies(MapLocation loc) {
    return isAttackableByForces(zombieForces, loc);
  }

  @Override
  public boolean isAttackableByEnemiesOrZombies(MapLocation loc) {
    return isAttackableByForces(enemyForces, loc) || isAttackableByForces(zombieForces, loc);
  }

  @Override
  public void addReportedRobot(RobotController rc, RobotInfo robotInfo) {
    if (robotInfo.team == rc.getTeam() && allyReportedRobots.numReportedRobots < MAX_ROBOTS) {
      allyReportedRobots.reportedRobots[allyReportedRobots.numReportedRobots++] = robotInfo;
    } else if (robotInfo.team == rc.getTeam().opponent()
        && enemyReportedRobots.numReportedRobots < MAX_ROBOTS) {
      enemyReportedRobots.reportedRobots[enemyReportedRobots.numReportedRobots++] = robotInfo;
    } else if (robotInfo.team == Team.ZOMBIE
        && zombieReportedRobots.numReportedRobots < MAX_ROBOTS) {
      zombieReportedRobots.reportedRobots[zombieReportedRobots.numReportedRobots++] = robotInfo;
    }
  }

  @Override
  public RobotInfo getClosestUnableToMoveEnemyOrZombie() {
    return enemyForces.closestUnableToMove != null
        ? enemyForces.closestUnableToMove
        : (zombieForces.closestUnableToMove != null ? zombieForces.closestUnableToMove : null);
  }

  @Override
  public MapLocation getAdjacentNeutralRobot(MapLocation loc) {
    return neutralForces.closest != null && neutralForces.closest.location.isAdjacentTo(loc)
        ? neutralForces.closest.location
        : null;
  }

  @Override
  public int getEnemyStrength() {
    return enemyForces.strength;
  }

  @Override
  public int getAllyStrength() {
    return allyForces.strength;
  }

  @Override
  public boolean shouldEngageEnemies() {
    return (zombieForces.strength <= 12) && (allyForces.strength >= enemyForces.strength);
  }

  private boolean isAttackableByForces(Forces forces, MapLocation loc) {
    for (int i = 0; i < forces.numRobots; i++) {
      RobotInfo robot = forces.robots[i];
      MapLocation robotAttackFromLoc = robot.type.canMove()
          ? robot.location.add(robot.location.directionTo(loc))
          : robot.location;
      if (robot.type.canAttack()
          && loc.distanceSquaredTo(robotAttackFromLoc) <= robot.type.attackRadiusSquared) {
        return true;
      }
    }

    return false;
  }

  private void cleanupForceReportedRobots(ReportedRobotInfos reportedRobots, RobotController rc) {
    int index = 0;
    while (index < reportedRobots.numReportedRobots) {
      RobotInfo robotInfo = reportedRobots.reportedRobots[index];
      RobotInfo newRobotInfo = new RobotInfo(robotInfo.ID,
          robotInfo.team,
          robotInfo.type,
          robotInfo.location,
          Math.max(robotInfo.coreDelay - 1, 0),
          Math.max(robotInfo.weaponDelay - 1, 0),
          robotInfo.attackPower,
          robotInfo.health,
          robotInfo.maxHealth,
          robotInfo.zombieInfectedTurns,
          robotInfo.viperInfectedTurns);

      if (rc.getID() < robotInfo.ID) { // We move before them, will still be in
                                       // the position they were as long as
                                       // their delay > 0
        if (robotInfo.coreDelay < 1) { // Could have moved
          reportedRobots.numReportedRobots--;
          reportedRobots.reportedRobots[index] = reportedRobots.reportedRobots[reportedRobots.numReportedRobots];
          reportedRobots.reportedRobots[reportedRobots.numReportedRobots] = null;
        } else {
          reportedRobots.reportedRobots[index] = newRobotInfo;
          ++index;
        }
      } else { // We move after them, they could move if their delay will go to
               // 0
        if (newRobotInfo.coreDelay < 1) { // Could have moved (with extra turn)
          reportedRobots.numReportedRobots--;
          reportedRobots.reportedRobots[index] = reportedRobots.reportedRobots[reportedRobots.numReportedRobots];
          reportedRobots.reportedRobots[reportedRobots.numReportedRobots] = null;
        } else {
          reportedRobots.reportedRobots[index] = newRobotInfo;
          ++index;
        }
      }
    }
  }

  private void cleanupReportedRobots(RobotController rc) {
    cleanupForceReportedRobots(allyReportedRobots, rc);
    cleanupForceReportedRobots(enemyReportedRobots, rc);
    cleanupForceReportedRobots(zombieReportedRobots, rc);
  }

  @Override
  public boolean shouldDragZombies() {
    return zombieForces.strength >= ZOMBIE_DRAGGING_STRENGTH;
  }

  public RobotInfo closestAlliedArchon() {
    return allyForces.closestArchon;
  }
}
