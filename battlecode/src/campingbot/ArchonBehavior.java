package campingbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import campingbot.ArchonTracker.ArchonStatus;

public class ArchonBehavior implements Behavior {

  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 6000;
  private static final int MESSAGING_BYTECODE_LIMIT = 7000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final RepairSystem repairSystem;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final ExplorationCalculator explorationCalculator;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ArchonTracker archonTracker;
  private final PartScanner partScanner;
  private final TurretSpawner turretSpawner;

  private boolean madeScout;

  public ArchonBehavior(
      NavigationSystem navigation,
      Radar radar,
      RepairSystem repairSystem,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      ExplorationCalculator explorationCalculator,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker,
      PartScanner partScanner) {
    this.navigation = navigation;
    this.radar = radar;
    this.repairSystem = repairSystem;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.explorationCalculator = explorationCalculator;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.archonTracker = archonTracker;
    this.partScanner = partScanner;
    this.turretSpawner = new TurretSpawner();

    madeScout = false;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    mapBoundaryCalculator.update(rc);
    partScanner.scanForParts(rc, interestingTargets);
    repairSystem.repairLeastHealthAlly(rc);
    archonTracker.updateSelfInformation(rc, null);

    int centerX = 0;
    int centerY = 0;
    ArchonStatus[] alliedArchons = archonTracker.getAlliedArchons(rc);
    int numArchons = alliedArchons.length;
    for (int i = 0; i < numArchons; i++) {
      MapLocation loc = alliedArchons[i].lastKnownLocation;
      rc.setIndicatorLine(rc.getLocation(), loc, 255, 255, 200);
      centerX += loc.x;
      centerY += loc.y;
    }
    MapLocation center = new MapLocation(centerX / numArchons, centerY / numArchons);

    if (!madeScout && rc.getRoundNum() < 50 && makeUnit(rc, RobotType.SCOUT)) {
      madeScout = true;
    } else {
      MapLocation adjacentNeutralRobot = radar.getAdjacentNeutralRobot(rc.getLocation());
      if (rc.isCoreReady() && adjacentNeutralRobot != null) {
        rc.activate(adjacentNeutralRobot);
      }

      if (rc.getLocation().distanceSquaredTo(center) > 20) {
        rc.setIndicatorString(0, "Moving towards other archons.");
        navigation.directTo(
            rc,
            center,
            true /* avoidAttackers */,
            true /* clearRubble */,
            false /* onlyForward */);
      } else {
        turretSpawner.buildTurretNear(rc, navigation, center);
      }
    }

    interestingTargets.shareRandomTarget(rc, messageSender);
    mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
  }

  private boolean makeUnit(RobotController rc, RobotType type) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }

    if (radar.getClosestEnemyOrZombie() != null) {
      return false;
    }

    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 0; i < 8; i++) {
      if (rc.hasBuildRequirements(type) && rc.canBuild(d, type)) {
        rc.build(d, type);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }
}
