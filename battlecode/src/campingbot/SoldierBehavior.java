package campingbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import campingbot.ArchonTracker.ArchonStatus;
import campingbot.InterestingTargets.InterestingTarget;

public class SoldierBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 4000;

  private static final int RUN_HEALTH = 10;

  private static final int STOP_HEALING_HEALTH = 30;

  private final NavigationSystem navigation;
  private final AttackSystem attackSystem;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final Radar radar;
  private final InterestingTargets interestingTargets;
  private final ArchonTracker archonTracker;

  public SoldierBehavior(
      NavigationSystem navigation,
      AttackSystem attackSystem,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      Radar radar,
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker) {
    this.navigation = navigation;
    this.attackSystem = attackSystem;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.radar = radar;
    this.interestingTargets = interestingTargets;
    this.archonTracker = archonTracker;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  };

  @Override
  public void behave(RobotController rc) throws GameActionException {
    mapBoundaryCalculator.update(rc);
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    archonTracker.updateClosest(rc);

    ArchonStatus closestAlliedArchon = archonTracker.getClosestAlliedArchon();

    boolean inHealingRange = closestAlliedArchon != null &&
        closestAlliedArchon.lastKnownLocation.distanceSquaredTo(rc
            .getLocation()) < RobotType.ARCHON.attackRadiusSquared;
    rc.setIndicatorString(2, "in healing range? : " + inHealingRange);
    boolean attackersNearby = radar.getClosestEnemyOrZombie() != null;

    if (inHealingRange && rc.getHealth() < STOP_HEALING_HEALTH && !attackersNearby) {
      heal(rc);
    } else if (rc.getHealth() < RUN_HEALTH) {
      lowHealth(rc);
    } else {
      defaultBehavior(rc);
    }
  }

  public void defaultBehavior(RobotController rc) throws GameActionException {
    boolean attackersNearby = radar.getClosestEnemyOrZombie() != null;
    if (attackersNearby) {
      fight(rc);
    } else {
      explore(rc);
    }
  }

  public void heal(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "HEAL");

    ArchonStatus closestAllyArchon = archonTracker.getClosestAlliedArchon();

    // If there's no archons with known positions, just return to default
    if (closestAllyArchon == null) {
      defaultBehavior(rc);
      return;
    }

    boolean nearArchon = rc.getLocation()
        .distanceSquaredTo(closestAllyArchon.lastKnownLocation) < 13;
    if (nearArchon) {
      RobotInfo robotAtLocation = rc.senseRobotAtLocation(closestAllyArchon.lastKnownLocation);
      if (robotAtLocation == null || robotAtLocation.type != RobotType.ARCHON) {
        // If we get to where we thought the closest archon was and no archon is
        // there, mark archon as not there and go to next
        archonTracker.getClosestAlliedArchon().notFoundAtLocation = true;
        archonTracker.updateClosest(rc);
        heal(rc);
      }
    } else {
      navigation.bugTo(rc, closestAllyArchon.lastKnownLocation, true, false);
    }
  }

  public void lowHealth(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "LOW HEALTH");
    boolean infected = rc.isInfected();
    boolean isViperInfection = rc.getViperInfectedTurns() > 0;

    boolean inDanger = radar.isAttackableByEnemiesOrZombies(rc.getLocation()) || isViperInfection;

    if (infected && inDanger) {
      // If in danger and infected, try to move towards enemy
      MapLocation closestEnemy = radar.getClosestEnemy();
      if (closestEnemy != null) {
        if (navigation.directTo(rc, closestEnemy, false, false, true)) {
          return;
        } else {
          attackSystem.attackLeastHealthEnemy(rc);
        }
      } else {
        MapLocation closestZombie = radar.getClosestZombie();
        if (navigation.retreatFrom(rc, closestZombie)) {
          return;
        } else {
          attackSystem.attackLeastHealthZombie(rc);
        }
      }
    } else if (infected) {
      // If infected but not in immediate danger, run away
      MapLocation closestDanger = radar.getClosestEnemyOrZombie();
      if (closestDanger != null) {
        navigation.retreatFrom(rc, closestDanger);
      }
    } else if (inDanger) {
      // If in danger, but not infected, try running
      // TODO: consider only running when running makes you safe
      MapLocation closestDanger = radar.getClosestEnemyOrZombie();
      if (closestDanger != null) {
        navigation.retreatFrom(rc, closestDanger);
      } else {
        // If we can't run, just fight
        fight(rc);
      }
    } else {
      // Not in danger, not infected, move towards archon to heal
      heal(rc);
    }

  }

  private void fight(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "FIGHT");

    MapLocation myLoc = rc.getLocation();

    if (radar.isAttackableByZombies(myLoc)) {
      MapLocation zombie = radar.getClosestZombie();
      if (zombie != null) {
        navigation.retreatFrom(rc, zombie);
      }
    }

    MapLocation target = attackSystem.attackLeastHealthEnemy(rc);
    if (target == null) {
      target = attackSystem.attackLeastHealthZombie(rc);
    }
    if (target == null) {
      target = radar.getClosestEnemyOrZombie();
    }

    if (!rc.isCoreReady()) {
      return;
    }

    int dist = rc.getLocation().distanceSquaredTo(target);
    if (radar.shouldEngageEnemies()) {
      if (dist > RobotType.SOLDIER.attackRadiusSquared) {
        navigation.directTo(rc, target, false /* avoidAttackers */, false /* clearRubble */,
            false /* onlyForward */);
      } else if (dist < 8 || rc.senseNearbyRobots(13, rc.getTeam().opponent()).length > 4) {
        navigation.retreatFrom(rc, target);
      }
    } else {
      navigation.retreatFrom(rc, target);
    }
  }

  @SuppressWarnings("rawtypes")
  private void explore(RobotController rc) throws GameActionException {
    InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
        rc.getLocation(),
        true /* includeDens */,
        false /* includeParts */,
        false /* includeNeutralRobots */);
    if (interestingTarget != null && rc.canSenseLocation(interestingTarget.loc)) {
      RobotInfo robot = rc.senseRobotAtLocation(interestingTarget.loc);
      if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
        interestingTargets.reportDenDestroyed(interestingTarget.loc);
        interestingTarget = null;
      }
    }

    MapLocation target = interestingTarget == null
        ? null
        : interestingTarget.loc;
    if (target == null) {
      target = explorationCalculator.calculate(rc);
    }

    rc.setIndicatorString(0, "EXPLORING TO " + target);
    navigation.directTo(rc, target, true /* avoidAttackers */, true /* clearRubble */,
        false /* onlyForward */);
  }
}