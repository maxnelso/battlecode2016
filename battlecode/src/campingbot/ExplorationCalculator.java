package campingbot;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ExplorationCalculator {

  private static final int EXPLORE_TIMEOUT = 50;
  private static final int FAR_AWAY_DISTANCE = 100;

  private final MapBoundaryCalculator mapBoundaryCalculator;

  private Direction unknownBoundaryDir;
  private MapLocation[] patrolLocs;
  private int patrolLocIndex;
  private int currentExploreTime;

  public ExplorationCalculator(MapBoundaryCalculator mapBoundaryCalculator) {
    this.mapBoundaryCalculator = mapBoundaryCalculator;

    patrolLocs = null;
    patrolLocIndex = -1;
    currentExploreTime = 0;
  }

  public MapLocation calculate(RobotController rc) {
    return mapBoundaryCalculator.allBoundariesKnown()
        ? visitSection(rc)
        : findUnknownBoundaries(rc);
  }

  private MapLocation findUnknownBoundaries(RobotController rc) {
    MapLocation myLoc = rc.getLocation();
    boolean minXKnown = mapBoundaryCalculator.isMinXKnown();
    boolean minYKnown = mapBoundaryCalculator.isMinYKnown();
    boolean maxXKnown = mapBoundaryCalculator.isMaxXKnown();
    boolean maxYKnown = mapBoundaryCalculator.isMaxYKnown();
    if (unknownBoundaryDir != null) {
      if (minXKnown && !maxXKnown && unknownBoundaryDir.dx < 0
          || minYKnown && !maxYKnown && unknownBoundaryDir.dy < 0
          || maxXKnown && !minXKnown && unknownBoundaryDir.dx > 0
          || maxYKnown && !minYKnown && unknownBoundaryDir.dy > 0
          || currentExploreTime > EXPLORE_TIMEOUT) {
        unknownBoundaryDir = null;
      }
    }

    if (unknownBoundaryDir != null) {
      currentExploreTime++;
      return myLoc.add(unknownBoundaryDir, FAR_AWAY_DISTANCE);
    }

    Direction[] dirs = possibleDirections(minXKnown, minYKnown, maxXKnown, maxYKnown);
    unknownBoundaryDir = dirs[(rc.getID() + rc.getRoundNum()) % (dirs.length)];
    currentExploreTime = 0;
    return myLoc.add(unknownBoundaryDir, FAR_AWAY_DISTANCE);
  }

  private Direction[] possibleDirections(
      boolean minX, boolean minY, boolean maxX, boolean maxY) {
    if (!minX && !minY && !maxX && !maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
        Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST
      };
    } else if (!minX && !minY && !maxX && maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.WEST, Direction.NORTH_WEST
      };
    } else if (!minX && !minY && maxX && !maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST
      };
    } else if (!minX && !minY && maxX && maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.WEST, Direction.NORTH_WEST
      };
    } else if (!minX && minY && !maxX && !maxY) {
      return new Direction[] {
        Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST
      };
    } else if (!minX && minY && !maxX && maxY) {
      return new Direction[] {
        Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH_WEST,
        Direction.WEST, Direction.NORTH_WEST
      };
    } else if (!minX && minY && maxX && !maxY) {
      return new Direction[] {
        Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST
      };
    } else if (!minX && minY && maxX && maxY) {
      return new Direction[] {
        Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST
      };
    } else if (minX && !minY && !maxX && !maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH
      };
    } else if (minX && !minY && !maxX && maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST
      };
    } else if (minX && !minY && maxX && !maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.SOUTH_EAST, Direction.SOUTH,
        Direction.SOUTH_WEST, Direction.NORTH_WEST
      };
    } else if (minX && !minY && maxX && maxY) {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST
      };
    } else if (minX && minY && !maxX && !maxY) {
      return new Direction[] {
        Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH
      };
    } else if (minX && minY && !maxX && maxY) {
      return new Direction[] {
        Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST
      };
    } else if (minX && minY && maxX && !maxY) {
      return new Direction[] {
        Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST
      };
    } else {
      return new Direction[] {
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
        Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST
      };
    }
  }

  private MapLocation visitSection(RobotController rc) {
    if (patrolLocs == null) {
      patrolLocs = computePatrolLocs();
      patrolLocIndex = 0;
      currentExploreTime = 0;
    }

    if (rc.canSenseLocation(patrolLocs[patrolLocIndex])
        || currentExploreTime > EXPLORE_TIMEOUT) {
      patrolLocIndex = (patrolLocIndex + 1) % patrolLocs.length;
      currentExploreTime = 0;
    }

    currentExploreTime++;
    return patrolLocs[patrolLocIndex];
  }

  private MapLocation[] computePatrolLocs() {
    int minX = mapBoundaryCalculator.getMinX();
    int maxX = mapBoundaryCalculator.getMaxX();
    int minY = mapBoundaryCalculator.getMinY();
    int maxY = mapBoundaryCalculator.getMaxY();

    return new MapLocation[] {
      computePatrolLoc(minX, maxX, minY, maxY, 0, 0),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 0),
      computePatrolLoc(minX, maxX, minY, maxY, 2, 0),
      computePatrolLoc(minX, maxX, minY, maxY, 2, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 0, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 0, 2),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 2),
      computePatrolLoc(minX, maxX, minY, maxY, 2, 2),
      computePatrolLoc(minX, maxX, minY, maxY, 2, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 0, 1)
    };
  }

  private MapLocation computePatrolLoc(
      int minX, int maxX, int minY, int maxY, int maxXWeight, int maxYWeight) {
    return new MapLocation(
        ((2 - maxXWeight) * minX + maxXWeight * maxX) / 2,
        ((2 - maxYWeight) * minY + maxYWeight * maxY) / 2);
  }
}
