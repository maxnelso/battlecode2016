package campingbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class UnpackedTurretBehavior implements Behavior {

  private static final int MIN_IDLE_ROUNDS = 30;
  private static final int MAX_IDLE_ROUNDS = 100;

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 5000;

  private final AttackSystem attackSystem;
  private final Radar radar;

  private int idleRounds;

  public UnpackedTurretBehavior(Radar radar, AttackSystem attackSystem) {
    this.attackSystem = attackSystem;
    this.radar = radar;

    idleRounds = MIN_IDLE_ROUNDS;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (rc.isCoreReady() && (rc.getRoundNum() / 300) % 2 == 1) {
      rc.pack();
    }
  }
}
