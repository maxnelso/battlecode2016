package campingbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class TurretBehavior implements Behavior {

  private final Behavior packedBehavior;
  private final Behavior unpackedBehavior;

  public TurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      AttackSystem attackSystem,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker) {
    packedBehavior = new PackedTurretBehavior(
        navigation, radar, mapBoundaryCalculator, archonTracker);
    unpackedBehavior = new UnpackedTurretBehavior(radar, attackSystem);
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (rc.getType() == RobotType.TURRET) {
      unpackedBehavior.behave(rc);
    } else {
      packedBehavior.behave(rc);
    }
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return rc.getType() == RobotType.TURRET
        ? unpackedBehavior.getMessagingBytecodeLimit(rc)
        : packedBehavior.getMessagingBytecodeLimit(rc);
  }
}
