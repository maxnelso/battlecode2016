package campingbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class TurretSpawner {

  private final int[][] deltas;

  public TurretSpawner() {
    deltas = getDeltas();
  }

  public void buildTurretNear(RobotController rc, NavigationSystem navigation, MapLocation loc)
      throws GameActionException {
    if (!rc.isCoreReady() || !rc.hasBuildRequirements(RobotType.TURRET)) {
      return;
    }

    MapLocation myLoc = rc.getLocation();
    MapLocation freeSpace = getFreeSpaceNear(rc, loc);
    if (freeSpace == null) {
      return;
    }

    if (myLoc.isAdjacentTo(freeSpace)) {
      Direction d = myLoc.directionTo(freeSpace);
      if (rc.canBuild(d, RobotType.TURRET)) {
        rc.build(d, RobotType.TURRET);
        return;
      }
    }

    navigation.bugTo(rc, freeSpace, true /* avoidAttackers */, true /* clearRubble */);
  }

  private MapLocation getFreeSpaceNear(
      RobotController rc, MapLocation loc) throws GameActionException {
    for (int i = 0; i < deltas.length; i++) {
      int[] delta = deltas[i];
      MapLocation freeSpace = loc.add(delta[0], delta[1]);
      if (rc.canSense(freeSpace)
          && rc.onTheMap(freeSpace)
          && !rc.isLocationOccupied(freeSpace)
          && rc.senseRubble(freeSpace) < GameConstants.RUBBLE_OBSTRUCTION_THRESH) {
        return freeSpace;
      }
    }

    return null;
  }

  private static int[][] getDeltas() {
    return new int[][] {
      new int[] {
        0, 0
        },
      new int[] {
        0, -1
        },
      new int[] {
        -1, -1
        },
      new int[] {
        -1, 0
        },
      new int[] {
        -1, 1
        },
      new int[] {
        0, 1
        },
      new int[] {
        1, 1
        },
      new int[] {
        1, 0
        },
      new int[] {
        1, -1
        },
      new int[] {
        1, -2
        },
      new int[] {
        0, -2
        },
      new int[] {
        -1, -2
        },
      new int[] {
        -2, -2
        },
      new int[] {
        -2, -1
        },
      new int[] {
        -2, 0
        },
      new int[] {
        -2, 1
        },
      new int[] {
        -2, 2
        },
      new int[] {
        -1, 2
        },
      new int[] {
        0, 2
        },
      new int[] {
        1, 2
        },
      new int[] {
        2, 2
        },
      new int[] {
        2, 1
        },
      new int[] {
        2, 0
        },
      new int[] {
        2, -1
        },
      new int[] {
        2, -2
        },
      new int[] {
        2, -3
        },
      new int[] {
        1, -3
        },
      new int[] {
        0, -3
        },
      new int[] {
        -1, -3
        },
      new int[] {
        -2, -3
        },
      new int[] {
        -3, -3
        },
      new int[] {
        -3, -2
        },
      new int[] {
        -3, -1
        },
      new int[] {
        -3, 0
        },
      new int[] {
        -3, 1
        },
      new int[] {
        -3, 2
        },
      new int[] {
        -3, 3
        },
      new int[] {
        -2, 3
        },
      new int[] {
        -1, 3
        },
      new int[] {
        0, 3
        },
      new int[] {
        1, 3
        },
      new int[] {
        2, 3
        },
      new int[] {
        3, 3
        },
      new int[] {
        3, 2
        },
      new int[] {
        3, 1
        },
      new int[] {
        3, 0
        },
      new int[] {
        3, -1
        },
      new int[] {
        3, -2
        },
      new int[] {
        3, -3
        },
    };
  }
}
