package campingbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class PackedTurretBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 1000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 3000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ArchonTracker archonTracker;

  public PackedTurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker) {
    this.navigation = navigation;
    this.radar = radar;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.archonTracker = archonTracker;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (rc.isCoreReady() && (rc.getRoundNum() / 300) % 2 == 0) {
      rc.unpack();
    }
    navigation.moveRandomly(rc);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }
}
