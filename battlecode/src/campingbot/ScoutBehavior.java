package campingbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ScoutBehavior implements Behavior {

  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 20000;
  private static final int MESSAGING_BYTECODE_LIMIT = 0;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final PartScanner partScanner;
  private final ArchonTracker archonTracker;

  public ScoutBehavior(
      NavigationSystem navigation,
      Radar radar,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      ArchonTracker archonTracker,
      PartScanner partScanner) {
    this.navigation = navigation;
    this.radar = radar;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.partScanner = partScanner;
    this.archonTracker = archonTracker;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    partScanner.scanForParts(rc, interestingTargets);
    mapBoundaryCalculator.update(rc);

    MapLocation target = explorationCalculator.calculate(rc);
    navigation.directTo(rc, target, true /* avoidAttackers */, false /* clearRubble */,
        false /* onlyForward */);

    interestingTargets.shareRandomTarget(rc, messageSender);
    mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
  }
}