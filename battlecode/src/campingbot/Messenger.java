package campingbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public class Messenger implements MessageSender, MessageReceiver {

  private static final int OP_CODE_BITS = 4;
  private static final int UNKNOWN_BOUNDARY_COORDINATE = 16005;

  // NOTE: The maximum op code is (2 ** MAX_OP_CODE_BITS) - 1.
  private enum MessageType {
    ARCHON_LOCATION(0),
    DEN_LOCATION(1),
    PART_LOCATION(2),
    NEUTRAL_LOCATION(3),
    MIN_BOUNDARY(4),
    MAX_BOUNDARY(5),
    ENEMY_INFO(6);

    int opCode;

    private MessageType(int opCode) {
      this.opCode = opCode;
    }

    static MessageType fromOpCode(int opCode) {
      for (MessageType t : MessageType.values()) {
        if (t.opCode == opCode) {
          return t;
        }
      }
      return null;
    }
  }

  private final InterestingTargets interestingTargets;
  private final ArchonTracker archonTracker;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final Radar radar;

  public Messenger(
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator) {
    this.interestingTargets = interestingTargets;
    this.archonTracker = archonTracker;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.radar = radar;
  }

  @Override
  public void sendArchonLocation(
      RobotController rc,
      MapLocation location,
      int timestamp,
      int id,
      boolean alliedArchon) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.ARCHON_LOCATION.opCode)
        .addBits(12, timestamp)
        .addBits(1, alliedArchon ? 1 : 0)
        .addBits(15, id)
        .build();
    MessageData secondData = MessageData.fromMapLocation(location);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendDenLocation(
      RobotController rc, MapLocation loc) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.DEN_LOCATION.opCode)
        .addBits(1, 0)
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendEnemyInfo(
      RobotController rc,
      RobotInfo robotInfo) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.ENEMY_INFO.opCode)
        .addBits(4, (int) robotInfo.coreDelay)
        .addBits(4, (int) robotInfo.health)
        .addBits(4, robotInfo.type.ordinal())
        .addBits(15, robotInfo.ID)
        .build();
    MessageData secondData = MessageData.fromMapLocation(robotInfo.location);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendRemoveDenLocation(
      RobotController rc, MapLocation loc) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.DEN_LOCATION.opCode)
        .addBits(1, 1)
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendPartLocation(
      RobotController rc, MapLocation loc, int numParts) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.PART_LOCATION.opCode)
        .addBits(1, 0)
        .addBits(20, numParts)
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendRemovePartLocation(
      RobotController rc, MapLocation loc) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.PART_LOCATION.opCode)
        .addBits(1, 1)
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendNeutralRobotLocation(
      RobotController rc,
      MapLocation loc,
      RobotType robotType) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.NEUTRAL_LOCATION.opCode)
        .addBits(1, 0)
        .addBits(10, robotType.ordinal())
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendRemoveNeutralRobotLocation(
      RobotController rc,
      MapLocation loc) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, MessageType.NEUTRAL_LOCATION.opCode)
        .addBits(1, 1)
        .build();
    MessageData secondData = MessageData.fromMapLocation(loc);
    broadcast(rc, firstData, secondData);
  }

  @Override
  public void sendMinXBoundary(
      RobotController rc, int minX) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, minX, UNKNOWN_BOUNDARY_COORDINATE);
  }

  @Override
  public void sendMinYBoundary(
      RobotController rc, int minY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, UNKNOWN_BOUNDARY_COORDINATE, minY);
  }

  @Override
  public void sendMinLocBoundary(
      RobotController rc, int minX, int minY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, minX, minY);
  }

  @Override
  public void sendMaxXBoundary(
      RobotController rc, int maxX) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, maxX, UNKNOWN_BOUNDARY_COORDINATE);
  }

  @Override
  public void sendMaxYBoundary(
      RobotController rc, int maxY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, UNKNOWN_BOUNDARY_COORDINATE, maxY);
  }

  @Override
  public void sendMaxLocBoundary(
      RobotController rc, int maxX, int maxY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, maxX, maxY);
  }

  private void sendLocBoundary(
      RobotController rc, MessageType messageType, int x, int y) throws GameActionException {
    MessageData firstData = new MessageData.Builder()
        .addBits(OP_CODE_BITS, messageType.opCode)
        .build();
    MessageData secondData = MessageData.fromMapLocation(new MapLocation(x, y));
    broadcast(rc, firstData, secondData);
  }

  @Override

  public void receiveMessages(
      RobotController rc, int bytecodeLimit) throws GameActionException {
    int initialBytecodesLeft = Clock.getBytecodesLeft();

    Signal s;
    int bytecodesLeft = Clock.getBytecodesLeft();
    while ((s = rc.readSignal()) != null
        && bytecodesLeft > 500
        && (initialBytecodesLeft - bytecodesLeft) < bytecodeLimit) {
      bytecodesLeft = Clock.getBytecodesLeft();
      if (s.getTeam() != rc.getTeam()) {
        if (archonTracker.isIdEnemyArchon(s.getID())) {
          archonTracker.processArchon(s.getID(), s.getLocation(), s.getLocation(), rc.getRoundNum(),
              false /* alliedArchon */);
        }
        continue;
      }
      int[] data = s.getMessage();
      if (data == null || data.length != 2) {
        continue;
      }
      MessageData firstData = MessageData.fromSignal(s, true /* firstData */);
      MessageData secondData = MessageData.fromSignal(s, false /* firstData */);
      int opCode = firstData.getPayload(0, OP_CODE_BITS - 1);
      MessageType t = MessageType.fromOpCode(opCode);
      if (t == null) {
        continue;
      }
      int[] firstDataPayloads;
      MapLocation secondDataLoc = secondData.toMapLocation();
      boolean deleted;
      RobotType robotType;
      int id;
      switch (t) {
        case ARCHON_LOCATION:
          firstDataPayloads = firstData.getAllPayloads(new int[] {
            OP_CODE_BITS, 12, 1, 15
          });
          int timestamp = firstDataPayloads[1];
          boolean alliedArchon = firstDataPayloads[2] == 1;
          id = firstDataPayloads[3];
          MapLocation loc = secondData.toMapLocation();
          archonTracker.processArchon(id, loc, null /* target */, timestamp,
              alliedArchon);
          break;
        case DEN_LOCATION:
          firstDataPayloads = firstData.getAllPayloads(new int[] {
            OP_CODE_BITS, 1
          });
          deleted = firstDataPayloads[1] == 1;
          if (deleted) {
            interestingTargets.reportDenDestroyed(secondDataLoc);
          } else {
            interestingTargets.reportDen(secondDataLoc);
          }
          break;
        case PART_LOCATION:
          firstDataPayloads = firstData.getAllPayloads(new int[] {
            OP_CODE_BITS, 1, 20
          });
          deleted = firstDataPayloads[1] == 1;
          int numParts = firstDataPayloads[2];
          if (deleted) {
            interestingTargets.reportPartsRetrieved(secondDataLoc);
          } else {
            interestingTargets.reportParts(secondDataLoc, numParts);
          }
          break;
        case NEUTRAL_LOCATION:
          firstDataPayloads = firstData.getAllPayloads(new int[] {
            OP_CODE_BITS, 1, 10
          });
          deleted = firstDataPayloads[1] == 1;
          robotType = RobotType.values()[firstDataPayloads[2]];
          if (deleted) {
            interestingTargets.reportNeutralRobotActivated(secondDataLoc);
          } else {
            interestingTargets.reportNeutralRobot(secondDataLoc, robotType);
          }
          break;
        case MIN_BOUNDARY:
          if (secondDataLoc.x != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMinX(secondDataLoc.x);
          }
          if (secondDataLoc.y != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMinY(secondDataLoc.y);
          }
          break;
        case MAX_BOUNDARY:
          if (secondDataLoc.x != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMaxX(secondDataLoc.x);
          }
          if (secondDataLoc.y != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMaxY(secondDataLoc.y);
          }
          break;
        case ENEMY_INFO:
          firstDataPayloads = firstData.getAllPayloads(new int[] {
            OP_CODE_BITS, 4, 4, 4, 15
          });

          robotType = RobotType.values()[firstDataPayloads[3]];

          id = firstDataPayloads[4];
          float coreDelay = firstDataPayloads[1];
          if (!robotType.canMove()) { // Give some artificial core delay for
                                      // turrets
            coreDelay = 5;
          }
          if (s.getID() < id && rc.getID() > id) { // Enemy robot took a turn
            coreDelay = Math.max(coreDelay - 1, 0);
          }

          if (coreDelay < 1) { // Enemy robot had a chance to move
            continue;
          }

          RobotInfo robotInfo = new RobotInfo(id /* id */,
              rc.getTeam().opponent(),
              robotType,
              secondData.toMapLocation(),
              coreDelay /* coreDelay */,
              -1 /* weaponDelay */,
              robotType.attackPower,
              firstDataPayloads[2] /* health */,
              robotType.maxHealth,
              -1 /* zombieInfectedTurns */, // TODO Maybe want?
              -1 /* viperInfectedTurns */); // TODO Maybe want?

          radar.addReportedRobot(rc, robotInfo);
          break;
        default:
          break;
      }
    }
    rc.emptySignalQueue();
  }

  private void broadcast(
      RobotController rc,
      MessageData firstData,
      MessageData secondData) throws GameActionException {
    rc.broadcastMessageSignal(
        firstData.getData(), secondData.getData(), 2 * rc.getType().sensorRadiusSquared);
  }
}
