package brodobot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class DefaultEnemyTurretCache implements EnemyTurretCache {

  private static final int MAX_TURRETS = 6;
  private static final int TURRET_LINEAR_RANGE = 7;
  private static final int MIN_TURRET_BOUNDING_BOX_AREA_FOR_CHECKING_IN_RANGE = 121;
  private static final int MAX_TURRET_BOUNDING_BOX_AREA_TO_SHARE = 121;
  private static final int TURRET_BOUNDING_BOX_SHARE_FREQUENCY = 10;

  private static class TurretInfo {
    private final MapLocation loc;
    private final boolean present;
    private final int timestamp;

    private TurretInfo(MapLocation loc, boolean present, int timestamp) {
      this.loc = loc;
      this.present = present;
      this.timestamp = timestamp;
    }

    public TurretInfo asPresent(int newTimestamp) {
      if (present) {
        return this;
      }

      return new TurretInfo(loc, true /* present */, newTimestamp);
    }

    public TurretInfo asAbsent(int newTimestamp) {
      if (!present) {
        return this;
      }

      return new TurretInfo(loc, false /* present */, newTimestamp);
    }
  }

  private final RobotController rc;
  private final TurretInfo[] turrets;
  private final MutableBoundingBox turretBoundingBox;
  private int numTurrets;
  private int shareIndex;
  private int checkDist;
  private boolean shareBoundingBox;

  public DefaultEnemyTurretCache(RobotController rc) {
    this.rc = rc;
    turrets = new TurretInfo[MAX_TURRETS + 1];
    turretBoundingBox = new MutableBoundingBox();
    checkDist = rc.getType() == RobotType.SCOUT ? RobotType.TURRET.attackRadiusSquared : 55;
    shareBoundingBox = false;
  }

  @Override
  public void reportEnemyTurretPresent(MapLocation loc, int timestamp) {
    for (int i = numTurrets; --i >= 0;) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc)) {
        if (turret.timestamp < timestamp && !turret.present) {
          turrets[i] = turret.asPresent(timestamp);
        }
        return;
      }
    }

    if (numTurrets < turrets.length) {
      turrets[numTurrets++] = new TurretInfo(loc, true /* present */, timestamp);
      turretBoundingBox.addMapLocation(loc);
      shareBoundingBox = true;
    }
    maybeRemoveFurthestTurret();
  }

  @Override
  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp) {
    for (int i = numTurrets; --i >= 0;) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc)) {
        if (turret.timestamp < timestamp && turret.present) {
          turrets[i] = turret.asAbsent(timestamp);
        }
        return;
      }
    }

    if (numTurrets < turrets.length) {
      turrets[numTurrets++] = new TurretInfo(loc, false /* present */, timestamp);
    }
    maybeRemoveFurthestTurret();
  }

  @Override
  public void reportTurretBoundingBox(
      MapLocation topLeftCorner, int xRange, int yRange, int numTurrets) {
    if (xRange * yRange <= turretBoundingBox.getArea()) {
      return;
    }

    if (turretBoundingBox.merge(topLeftCorner, xRange, yRange, numTurrets)) {
      // If the bounding box changed, share it.
      shareBoundingBox = true;
    }
  }

  private void maybeRemoveFurthestTurret() {
    MapLocation myLoc = rc.getLocation();
    if (numTurrets > MAX_TURRETS) {
      int furthestDistance = 99999;
      int furthestIndex = -1;
      for (int i = numTurrets; --i >= 0;) {
        TurretInfo turret = turrets[i];
        int dist = myLoc.distanceSquaredTo(turret.loc);
        if (furthestIndex == -1 || turret.timestamp < furthestDistance) {
          furthestDistance = dist;
          furthestIndex = i;
        }
      }
      if (furthestIndex != numTurrets - 1) {
        turrets[furthestIndex] = turrets[numTurrets - 1];
      }
      numTurrets--;
    }
  }

  @Override
  public boolean isInEnemyTurretRange(MapLocation loc) {
    if (turretBoundingBox.getNumElements() > 0
        && turretBoundingBox.getArea() <= MIN_TURRET_BOUNDING_BOX_AREA_FOR_CHECKING_IN_RANGE) {
      return turretBoundingBox.containsWithMargins(loc, TURRET_LINEAR_RANGE /* margins */);
    }

    for (int i = numTurrets; --i >= 0;) {
      if (turrets[i].present && loc.distanceSquaredTo(turrets[i].loc) <= checkDist) {
        return true;
      }
    }

    return false;
  }

  @Override
  public Direction getDirectionToTurretInRange(MapLocation loc) {
    if (turretBoundingBox.getNumElements() > 0
        && turretBoundingBox.getArea() <= MIN_TURRET_BOUNDING_BOX_AREA_FOR_CHECKING_IN_RANGE) {
      return loc.directionTo(turretBoundingBox.getCenter());
    }

    for (int i = numTurrets; --i >= 0;) {
      if (turrets[i].present && loc.distanceSquaredTo(turrets[i].loc) <= checkDist) {
        return loc.directionTo(turrets[i].loc);
      }
    }

    return null;
  };

  @Override
  public void invalidateNearbyEnemyTurrets() throws GameActionException {
    int roundNum = rc.getRoundNum();
    Team enemyTeam = rc.getTeam().opponent();
    for (int i = numTurrets; --i >= 0;) {
      TurretInfo turret = turrets[i];
      if (turret.present && rc.canSenseLocation(turret.loc)) {
        RobotInfo robot = rc.senseRobotAtLocation(turret.loc);
        if (robot == null || robot.team != enemyTeam || robot.type != RobotType.TURRET) {
          turrets[i] = turret.asAbsent(roundNum /* newTimestamp */);
        }
      }
    }
  }

  @Override
  public BoundingBox getTurretBoundingBox() {
    return turretBoundingBox;
  }

  @Override
  public void shareRandomEnemyTurret(MessageSender messageSender) throws GameActionException {
    if (numTurrets == 0) {
      return;
    }

    shareIndex = shareIndex % numTurrets;
    TurretInfo turret = turrets[shareIndex];
    if (turrets[shareIndex].present) {
      messageSender.sendEnemyTurretLocation(turret.loc, turret.timestamp);
    } else {
      messageSender.sendEnemyTurretLocationRemoved(turret.loc, turret.timestamp);
    }
    shareIndex++;
  }

  @Override
  public void shareTurretBoundingBox(MessageSender messageSender) throws GameActionException {
    if (!shareBoundingBox
        || !rc.isCoreReady()
        || turretBoundingBox.getNumElements() == 0
        || turretBoundingBox.getArea() > MAX_TURRET_BOUNDING_BOX_AREA_TO_SHARE
        || (rc.getRoundNum() + rc.getID()) % TURRET_BOUNDING_BOX_SHARE_FREQUENCY != 0) {
      return;
    }
    messageSender.sendEnemyTurretBoundingBoxEverywhere(
        turretBoundingBox.getTopLeftCorner(),
        turretBoundingBox.getXRange(),
        turretBoundingBox.getYRange(),
        turretBoundingBox.getNumElements());
    shareBoundingBox = false;
  };

  @Override
  public void showDebugInfo() {
    for (int i = numTurrets; --i >= 0;) {
      TurretInfo turret = turrets[i];
      if (turret.present) {
        rc.setIndicatorLine(rc.getLocation(), turret.loc, 50, 50, 50);
      }
    }
    turretBoundingBox.showDebugInfo(rc);
  }
}
