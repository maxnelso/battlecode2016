package brodobot;

import battlecode.common.MapLocation;

public interface ArmyRally {

  public MapLocation getRally();
}
