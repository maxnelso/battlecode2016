package brodobot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class FightingArchonBehavior implements Behavior {

  private static final int RALLY_MESSAGE_DISTANCE_MULTIPLIER = 9;
  private static final int ARMY_RALLY_MESSAGE_FREQUENCY = 10;

  private final RobotController rc;
  private final Radar radar;
  private final NavigationSystem navigation;
  private final MessageSender messageSender;
  private final DryadArcherUnitOrder dryadArcherUnitOrder;
  private final MarineUnitOrder marineUnitOrder;

  private RobotInfo currentHostileFighter;
  private int currentHostileFighterRound;

  private RobotInfo[] hostiles;

  public FightingArchonBehavior(
      RobotController rc,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender,
      DryadArcherUnitOrder dryadArcherUnitOrder,
      MarineUnitOrder marineUnitOrder) {
    this.rc = rc;
    this.radar = radar;
    this.navigation = navigation;
    this.messageSender = messageSender;
    this.dryadArcherUnitOrder = dryadArcherUnitOrder;
    this.marineUnitOrder = marineUnitOrder;

    currentHostileFighter = null;
    currentHostileFighterRound = -1;
  }

  @Override
  public void run() throws GameActionException {
    RobotInfo hostileFighter = getHostileFighter();
    if (hostileFighter == null) {
      rc.setIndicatorString(0, "I'm a lost fighter.");
      return;
    }

    MapLocation myLoc = rc.getLocation();
    UnitOrder unitOrder = radar.getNearbyZombies().length > 0
        ? marineUnitOrder
        : dryadArcherUnitOrder;
    Direction retreatDir = myLoc.directionTo(hostileFighter.location).opposite();
    if (hostileFighter.location.distanceSquaredTo(myLoc) > RobotType.VIPER.attackRadiusSquared) {
      // Try to spawn someone to protect me.
      if (UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), retreatDir)
          || UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), retreatDir.rotateLeft())
          || UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), retreatDir.rotateRight())) {
        unitOrder.computeNextUnit();
      }
    }

    if (rc.getRoundNum() % ARMY_RALLY_MESSAGE_FREQUENCY == 0) {
      messageSender.sendArmyRallyLocation(
          hostileFighter.location, RALLY_MESSAGE_DISTANCE_MULTIPLIER);
    }

    MapLocation target = myLoc.add(retreatDir, 10);
    if (!navigation.smartRetreat(target, hostiles)) {
      // Retreating didn't work, just try digging out
      navigation.directTo(target, false /* avoidAttackers */, true /* clearRubble */);
    }
    rc.setIndicatorString(0, "I'm fighting, retreating " + retreatDir + "!");
  }

  public RobotInfo getHostileFighter() {
    int roundNum = rc.getRoundNum();
    if (currentHostileFighterRound >= roundNum) {
      return currentHostileFighter;
    }

    currentHostileFighterRound = roundNum;
    currentHostileFighter = null;

    hostiles = radar.getNearbyHostiles();
    int closestDist = -1;
    for (int i = hostiles.length; --i >= 0;) {
      RobotInfo robot = hostiles[i];
      if (robot.type.canAttack()) {
        int dist = rc.getLocation().distanceSquaredTo(robot.location);
        if (closestDist == -1 || dist < closestDist) {
          closestDist = dist;
          currentHostileFighter = robot;
        }
      }
    }

    return currentHostileFighter;
  }
}
