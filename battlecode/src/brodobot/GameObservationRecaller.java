package brodobot;

public interface GameObservationRecaller {

  public boolean didWeReportViperSacLastGame();

  public boolean didWeAttemptViperSacLastGame();
}
