package brodobot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import brodobot.ViperSacReporter.SacAttack;

public class ApocalypseBehavior implements Behavior {

  private static final int MIN_DISTANCE_FROM_RALLY_TO_INCLUDE_IN_APOCALYPSE = 200;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final ViperSacReporter viperSacReporter;
  private final ViperSacCalculator viperSacCalculator;
  private final MessageSender messageSender;

  public ApocalypseBehavior(
      RobotController rc,
      NavigationSystem navigation,
      ViperSacReporter viperSacReporter,
      ViperSacCalculator viperSacCalculator,
      MessageSender messageSender) {
    this.rc = rc;
    this.navigation = navigation;
    this.viperSacReporter = viperSacReporter;
    this.viperSacCalculator = viperSacCalculator;
    this.messageSender = messageSender;
  }

  @Override
  public void run() throws GameActionException {
    SacAttack sacAttack = viperSacReporter.getSacAttack();
    int roundNum = rc.getRoundNum();
    if (sacAttack != null) {
      rc.setIndicatorString(0, "Aaaaahhhh! End of the world at round " + sacAttack.sacRound
          + "! Taking shelter at " + sacAttack.armyRallyLoc + "." + " " + rc.getRoundNum());
      boolean closeToRally = rc.getLocation().distanceSquaredTo(
          sacAttack.armyRallyLoc) <= MIN_DISTANCE_FROM_RALLY_TO_INCLUDE_IN_APOCALYPSE;
      if (roundNum >= sacAttack.sacRound) {
        vipersAttackAllies();
        if (rc.getInfectedTurns() > 0 || !closeToRally) {
          rc.disintegrate();
          return;
        }
      } else {
        if (!navigation.directTo(
            sacAttack.armyRallyLoc, true /* avoidAttackers */, true /* clearRubble */)) {
          if (!navigation.retreatFromTurrets()) {
            navigation.directTo(
                sacAttack.armyRallyLoc, false /* avoidAttackers */, true /* clearRubble */);
          }
        }
        if (rc.getType() == RobotType.ARCHON && rc.isCoreReady() && closeToRally) {
          viperSacCalculator.computeAndShareViperSac(messageSender);
        }
      }
    } else {
      rc.setIndicatorString(0, "I'm lost during the apocalypse.");
    }
  }

  private void vipersAttackAllies() throws GameActionException {
    if (rc.getType() == RobotType.VIPER && rc.isWeaponReady()) {
      rc.attackLocation(rc.getLocation());
    }
  }
}
