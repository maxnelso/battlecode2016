package brodobot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ArchonBehavior implements Behavior, PreBehavior {

  // Keep in sync with allied archon tracker.
  private final static int SELF_ARCHON_LOCATION_MESSAGE_FREQUENCY = 10;

  private final RobotController rc;
  private final Radar radar;
  private final MessageReceiver messageReceiver;
  private final MessageSender messageSender;
  private final RepairSystem repairSystem;
  private final ZombieDenReporter zombieDenReporter;
  private final EnemyTurretCache enemyTurretCache;
  private final ViperSacCalculator viperSacCalculator;
  private final ViperSacReporter viperSacReporter;
  private final ArchonHider archonHider;

  private final ScoutsAndSoldiersUnitOrder scoutsAndSoldiersUnitOrder;
  private final MarineUnitOrder marineUnitOrder;
  private final DryadArcherUnitOrder dryadArcherUnitOrder;

  private final OpeningArchonBehavior openingBehavior;
  private final HidingArchonBehavior hidingArchonBehavior;
  private final ApocalypseBehavior apocalypseBehavior;
  private final FightingArchonBehavior fightingBehavior;
  private final PickerUpperArchonBehavior pickerUpperBehavior;
  private final LeaderArchonBehavior leaderBehavior;
  private final ExploringBehavior exploringBehavior;

  private int justSpawnedTimer;

  public ArchonBehavior(
      RobotController rc,
      MapBoundaryCalculator mapBoundaryCalculator,
      Radar radar,
      NavigationSystem navigation,
      MessageReceiver messageReceiver,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      RepairSystem repairSystem,
      ZombieDenReporter zombieDenReporter,
      EnemyTurretCache enemyTurretCache,
      PickupLocationReporter pickupLocationReporter,
      ViperSacCalculator viperSacCalculator,
      ViperSacReporter viperSacReporter,
      ArchonHider archonHider) {
    this.rc = rc;
    this.radar = radar;
    this.messageReceiver = messageReceiver;
    this.messageSender = messageSender;
    this.repairSystem = repairSystem;
    this.zombieDenReporter = zombieDenReporter;
    this.enemyTurretCache = enemyTurretCache;
    this.viperSacCalculator = viperSacCalculator;
    this.viperSacReporter = viperSacReporter;
    this.archonHider = archonHider;

    marineUnitOrder = new MarineUnitOrder();
    dryadArcherUnitOrder = new DryadArcherUnitOrder(rc);
    scoutsAndSoldiersUnitOrder = new ScoutsAndSoldiersUnitOrder();

    openingBehavior = new OpeningArchonBehavior(rc, scoutsAndSoldiersUnitOrder);
    hidingArchonBehavior = new HidingArchonBehavior(
        rc, navigation, messageSender, archonHider, viperSacCalculator);
    apocalypseBehavior = new ApocalypseBehavior(
        rc, navigation, viperSacReporter, viperSacCalculator, messageSender);
    fightingBehavior = new FightingArchonBehavior(rc, radar, navigation,
        messageSender, dryadArcherUnitOrder, marineUnitOrder);
    pickerUpperBehavior = new PickerUpperArchonBehavior(rc, navigation);
    leaderBehavior = new LeaderArchonBehavior(rc, navigation, pickupLocationReporter);
    PatrolWaypointCalculator patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);
    exploringBehavior = new ExploringBehavior(
        rc, navigation, mapBoundaryCalculator, radar, patrolWaypointCalculator);

    justSpawnedTimer = 0;
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();

    int p = rc.getRoundNum() + rc.getID();
    if (p % SELF_ARCHON_LOCATION_MESSAGE_FREQUENCY == 0) {
      messageSender.sendSelfArchonLocation();
    }

    if (rc.isCoreReady() && justSpawnedTimer == 0) {
      zombieDenReporter.shareAllDens(messageSender);
      // zombieDenReporter.showDebugInfo();
      justSpawnedTimer = -1;
    }
    if (openingBehavior.isOpeningOver()
        && fightingBehavior.getHostileFighter() == null
        && archonHider.getArchonHidingLocation() == null
        && UnitSpawner.spawn(rc, dryadArcherUnitOrder.getNextUnit())) {
      justSpawnedTimer = dryadArcherUnitOrder.getNextUnit().buildTurns;
      dryadArcherUnitOrder.computeNextUnit();
    }

    if (viperSacReporter.getSacAttack() == null) {
      viperSacCalculator.computeAndShareViperSac(messageSender);
    }
  }

  @Override
  public void run() throws GameActionException {
    getCurrentBehavior().run();
    MapLocation target = repairSystem.getBestAllyToHeal(radar.getNearbyAllies());
    if (target != null) {
      rc.setIndicatorString(2, "Healing " + target + ".");
      rc.repair(target);
    } else {
      rc.setIndicatorString(2, "");
    }
    if (justSpawnedTimer > 0) {
      justSpawnedTimer--;
    }

    enemyTurretCache.getTurretBoundingBox().showDebugInfo(rc);
  }

  private Behavior getCurrentBehavior() throws GameActionException {
    if (archonHider.getArchonHidingLocation() != null) {
      return hidingArchonBehavior;
    }
    if (viperSacReporter.getSacAttack() != null) {
      return apocalypseBehavior;
    }
    if (fightingBehavior.getHostileFighter() != null) {
      return fightingBehavior;
    }
    if (!openingBehavior.isOpeningOver()) {
      return openingBehavior;
    }
    if (pickerUpperBehavior.getTarget() != null) {
      return pickerUpperBehavior;
    }
    if (leaderBehavior.getTarget() != null) {
      return leaderBehavior;
    }
    return exploringBehavior;
  }
}
