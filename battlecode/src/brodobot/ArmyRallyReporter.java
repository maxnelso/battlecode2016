package brodobot;

import battlecode.common.MapLocation;

public interface ArmyRallyReporter {

  public void reportRally(MapLocation rally);
}
