package brodobot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface BoundingBox {

  public MapLocation getTopLeftCorner();

  public MapLocation getCenter();

  public int getNumElements();

  public int getXRange();

  public int getYRange();

  public int getArea();

  public boolean contains(MapLocation loc);

  public boolean containsWithMargins(MapLocation loc, int margins);

  public void showDebugInfo(RobotController rc);
}
