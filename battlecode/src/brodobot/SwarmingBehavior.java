package brodobot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import brodobot.AlliedArchonTracker.AlliedArchonInfo;

public class SwarmingBehavior implements Behavior {

  private static final int ARCHON_TIMEOUT = 50;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;

  private AlliedArchonInfo closestAlliedArchon;

  public SwarmingBehavior(
      RobotController rc,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
  }

  @Override
  public void run() throws GameActionException {
    if (closestAlliedArchon != null) {
      MapLocation target = getArchonSurroundTarget(closestAlliedArchon.loc);
      rc.setIndicatorString(0, "I'm swarming. " + (rc.getRoundNum()
          - closestAlliedArchon.timestamp) + " " + closestAlliedArchon.loc + " " + target);
      maybeClearRubble(target);
      RobotPlayer.profiler.split("before navigation to closest archon");
      if (!navigation.directToAvoidingAlliedArchons(
          target, 2 /* avoidDist */, true /* clearRubble */)) {
        if (!navigation.directTo(target, true /* avoidAttackers */, true /* clearRubble */)) {
          navigation.retreatFromTurrets();
        }
      }
      RobotPlayer.profiler.split("after navigation to closest archon");
    } else {
      rc.setIndicatorString(0, "I'm lost.");
      navigation.moveRandomly();
      RobotPlayer.profiler.split("after moving randomly");
    }
  }

  private MapLocation getArchonSurroundTarget(MapLocation archonLoc) {
    return archonLoc.add(DirectionUtils.movableDirections[rc.getID() % 8], 2);
  }

  private void maybeClearRubble(MapLocation target) throws GameActionException {
    MapLocation myLoc = rc.getLocation();
    if (rc.isCoreReady() && rc.getType() != RobotType.TTM && myLoc.distanceSquaredTo(target) <= 8) {
      // Just clear rubble
      Direction d = DirectionUtils.getRandomMovableDirection();
      for (int i = 8; --i >= 0;) {
        if (rc.senseRubble(myLoc.add(d)) >= GameConstants.RUBBLE_SLOW_THRESH) {
          rc.clearRubble(d);
          return;
        }
        d = d.rotateLeft();
      }
    }
  }

  public boolean shouldSwarm() {
    AlliedArchonInfo[] alliedArchons = alliedArchonTracker.getAlliedArchons();
    int closestDist = 999999;
    int roundNum = rc.getRoundNum();
    closestAlliedArchon = null;
    MapLocation myLoc = rc.getLocation();
    for (int i = alliedArchons.length; --i >= 0;) {
      AlliedArchonInfo archon = alliedArchons[i];
      if (roundNum - archon.timestamp > ARCHON_TIMEOUT) {
        continue;
      }
      int dist = archon.loc.distanceSquaredTo(myLoc);
      if (dist < closestDist) {
        closestDist = dist;
        closestAlliedArchon = archon;
      }
    }
    return closestAlliedArchon != null;
  }
}
