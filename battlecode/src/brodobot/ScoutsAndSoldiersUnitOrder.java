package brodobot;

import battlecode.common.RobotType;

public class ScoutsAndSoldiersUnitOrder implements UnitOrder {

  private int scoutsMade;
  private int soldiersMade;
  private RobotType nextUnit;

  public ScoutsAndSoldiersUnitOrder() {
    scoutsMade = 0;
    soldiersMade = 0;
    nextUnit = RobotType.SCOUT;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (nextUnit == RobotType.SCOUT) {
      scoutsMade++;
      nextUnit = RobotType.SOLDIER;
    } else {
      soldiersMade++;
      if (soldiersMade >= 7 * scoutsMade) {
        nextUnit = RobotType.SCOUT;
      }
    }
  }
}
