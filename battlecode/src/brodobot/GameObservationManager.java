package brodobot;

import battlecode.common.RobotController;

public class GameObservationManager implements GameObservationRecorder, GameObservationRecaller {

  private enum Observation {
    REPORTED_VIPER_SAC(0),
    ATTEMPTED_VIPER_SAC(1);

    private final int memoryIndex;

    private Observation(int memoryIndex) {
      this.memoryIndex = memoryIndex;
    }
  }

  private final RobotController rc;
  private final long[] lastGameMemory;

  public GameObservationManager(RobotController rc) {
    this.rc = rc;
    lastGameMemory = rc.getTeamMemory();
  }

  @Override
  public boolean didWeReportViperSacLastGame() {
    return lastGameMemory[Observation.REPORTED_VIPER_SAC.memoryIndex] > 0;
  }

  @Override
  public boolean didWeAttemptViperSacLastGame() {
    return lastGameMemory[Observation.ATTEMPTED_VIPER_SAC.memoryIndex] > 0;
  }

  @Override
  public void weReportedViperSacLastGame() {
    rc.setTeamMemory(Observation.REPORTED_VIPER_SAC.memoryIndex, 1);
  }

  @Override
  public void weAttemptedViperSacLastGame() {
    rc.setTeamMemory(Observation.ATTEMPTED_VIPER_SAC.memoryIndex, 1);
  }

}
