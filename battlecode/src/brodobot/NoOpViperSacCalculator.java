package brodobot;

import battlecode.common.GameActionException;

public class NoOpViperSacCalculator implements ViperSacCalculator {

  @Override
  public void computeAndShareViperSac(MessageSender messageSender) throws GameActionException {}
}
