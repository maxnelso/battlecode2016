package infestorbot2;

import battlecode.common.RobotController;

public class BehaviorFactory {

  public static class BehaviorInfo {

    public final PreBehavior preBehavior;
    public final Behavior behavior;

    public BehaviorInfo(PreBehavior preBehavior, Behavior behavior) {
      this.preBehavior = preBehavior;
      this.behavior = behavior;
    }
  }

  public static BehaviorInfo createForRobotController(RobotController rc) {
    switch (rc.getType()) {
      case ARCHON:
        return archon(rc);
      case SCOUT:
        return scout(rc);
      case SOLDIER:
        return soldier(rc);
      case GUARD:
        return guard(rc);
      case VIPER:
        return viper(rc);
      case TURRET:
        return turret(rc);
      default:
        NoOpBehavior b = new NoOpBehavior(rc);
        return new BehaviorInfo(b /* preBehavior */, b /* behavior */);
    }
  }

  private static BehaviorInfo archon(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    RepairSystem repairSystem = new DefaultRepairSystem(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, radar, enemyBoundingBoxReporter);
    PickupLocationReporter pickupLocationReporter = new DefaultPickupLocationReporter(
        rc, symmetryCalculator);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    ArchonHider archonHider = new DefaultArchonHider();
    ViperSacManager viperSacManager = new ViperSacManager(
        rc, radar, mapBoundaryCalculator, zombieDenReporter, enemyBoundingBoxReporter, archonHider);

    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        new NoOpBasicMessages(),
        armyRallyReporter,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        pickupLocationReporter,
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);

    ArchonBehavior b = new ArchonBehavior(
        rc,
        mapBoundaryCalculator,
        radar,
        navigation,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        repairSystem,
        zombieDenReporter,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        pickupLocationReporter,
        viperSacManager /* viperSacCalculator */,
        viperSacManager /* viperSacReporter */,
        archonHider);
    return new BehaviorInfo(b /* preBehavior */, b /* behavior */);
  }

  private static BehaviorInfo scout(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    EnemyArchonTracker enemyArchonTracker = new DefaultEnemyArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    BasicMessages basicMessages = new DefaultBasicMessages();
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, radar, enemyBoundingBoxReporter);
    PickupLocationReporter pickupLocationReporter = new DefaultPickupLocationReporter(
        rc, symmetryCalculator);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    BuddyDistantHostileReporter buddyHostileReporter = new BuddyDistantHostileReporter(rc);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        radar,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyBoundingBoxReporter,
        new DefaultArchonHider());
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        buddyHostileReporter,
        zombieDenReporter,
        basicMessages,
        armyRallyReporter,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        pickupLocationReporter,
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());

    PreBehavior preBehavior = new ScoutPreBehavior(
        rc,
        radar,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        zombieDenReporter,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        mapBoundaryCalculator,
        eeHanTimingManager /* eeHanTimingCalculator */,
        symmetryCalculator);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    Behavior behavior = new ScoutBehavior(
        rc,
        navigation,
        mapBoundaryCalculator,
        radar,
        basicMessages,
        enemyArchonTracker,
        alliedArchonTracker,
        enemyTurretCache,
        messageSenderReceiver,
        pickupLocationReporter,
        buddyHostileReporter,
        viperSacManager /* viperSacReporter */);
    return new BehaviorInfo(preBehavior, behavior);
  }

  private static BehaviorInfo soldier(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, radar, enemyBoundingBoxReporter);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    BasicMessages basicMessages = new DefaultBasicMessages();

    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        radar,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyBoundingBoxReporter,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior soldierBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        mapBoundaryCalculator,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        basicMessages);
    return new BehaviorInfo(soldierBehavior /* preBehavior */, soldierBehavior /* behavior */);
  }

  private static BehaviorInfo guard(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new NonPersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, radar, enemyBoundingBoxReporter);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    BasicMessages basicMessages = new DefaultBasicMessages();

    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        radar,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyBoundingBoxReporter,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior rangedArmyBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        mapBoundaryCalculator,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        basicMessages);
    return new BehaviorInfo(
        rangedArmyBehavior /* preBehavior */, rangedArmyBehavior /* behavior */);
  }

  private static BehaviorInfo viper(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, radar, enemyBoundingBoxReporter);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        radar,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyBoundingBoxReporter,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BasicMessages basicMessages = new DefaultBasicMessages();
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new ViperAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(
        rc, rc.getZombieSpawnSchedule());

    RangedArmyBehavior rangedArmyBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        mapBoundaryCalculator,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        basicMessages);
    return new BehaviorInfo(rangedArmyBehavior /* preBehavior */,
        rangedArmyBehavior /* behavior */);
  }

  private static BehaviorInfo turret(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    SymmetryCalculator symmetryCalculator = new InitialArchonLocationSymmetryCalculator(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(
        rc, radar, symmetryCalculator);
    ExtendedRadar extendedRadar = new ExtendedRadar(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyBoundingBoxReporter enemyBoundingBoxReporter = new DefaultEnemyBoundingBoxReporter(
        rc, radar);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc, enemyBoundingBoxReporter);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, extendedRadar /* radar */, enemyBoundingBoxReporter);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc, symmetryCalculator);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        radar,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyBoundingBoxReporter,
        new DefaultArchonHider());
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        extendedRadar /* distantHostileReporter */,
        zombieDenReporter,
        new NoOpBasicMessages(),
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        enemyBoundingBoxReporter,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor());
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new TurretAttackSystem();
    TurretBehavior turretBehavior = new TurretBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        extendedRadar /* radar */,
        navigation,
        mapBoundaryCalculator,
        alliedArchonTracker,
        attackSystem,
        defaultArmyRally,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */);
    return new BehaviorInfo(turretBehavior /* preBehavior */, turretBehavior /* behavior */);
  }
}
