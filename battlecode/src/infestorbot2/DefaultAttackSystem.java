package infestorbot2;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DefaultAttackSystem implements AttackSystem {

  @Override
  public MapLocation getBestEnemyToShoot(RobotController rc,
      RobotInfo[] enemies,
      int numEnemies,
      RobotInfo[] allies,
      int numAllies) {
    if (!rc.isWeaponReady()) {
      return null;
    }
    RobotInfo ret = null;
    double bestTurnsToKill = 99999;
    double bestWeaponDelay = 99999;
    for (int i = numEnemies; --i >= 0;) {
      if (!rc.canAttackLocation(enemies[i].location)) {
        continue;
      }
      RobotInfo info = enemies[i];
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location, allies,
          numAllies);
      double turnsToKill = info.health / numNearbyAllies;
      if (turnsToKill < bestTurnsToKill) {
        bestTurnsToKill = turnsToKill;
        bestWeaponDelay = info.weaponDelay;
        ret = info;
      } else if (turnsToKill == bestTurnsToKill) {
        double actionDelay = info.weaponDelay;
        if (actionDelay < bestWeaponDelay) {
          bestWeaponDelay = actionDelay;
          ret = info;
        }
      }
    }
    if (ret != null) {
      return ret.location;
    }
    return null;
  }

  public MapLocation getBestZombieToShoot(RobotController rc,
      RobotInfo[] zombies,
      int numZombies,
      RobotInfo[] allies,
      int numAllies) {

    if (!rc.isWeaponReady()) {
      return null;
    }
    RobotInfo bestNonRanged = null;
    double bestNonTurnsToKill = 99999;
    double bestNonWeaponDelay = 99999;

    RobotInfo bestRanged = null;
    double bestRangedTurnsToKill = 99999;
    double bestRangedWeaponDelay = 99999;
    for (int i = numZombies; --i >= 0;) {
      if (!rc.canAttackLocation(zombies[i].location)) {
        continue;
      }

      RobotInfo info = zombies[i];
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location, allies,
          numAllies);
      double turnsToKill = info.health / numNearbyAllies;
      if (info.type == RobotType.RANGEDZOMBIE) {
        if (turnsToKill < bestRangedTurnsToKill) {
          bestRangedTurnsToKill = turnsToKill;
          bestRangedWeaponDelay = info.weaponDelay;
          bestRanged = info;
        } else if (turnsToKill == bestRangedTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestRangedWeaponDelay) {
            bestRangedWeaponDelay = actionDelay;
            bestRanged = info;
          }
        }
      } else {
        if (turnsToKill < bestNonTurnsToKill) {
          bestNonTurnsToKill = turnsToKill;
          bestNonWeaponDelay = info.weaponDelay;
          bestNonRanged = info;
        } else if (turnsToKill == bestNonTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestNonWeaponDelay) {
            bestNonWeaponDelay = actionDelay;
            bestNonRanged = info;
          }
        }
      }
    }

    if (bestRanged != null) {
      return bestRanged.location;
    }
    if (bestNonRanged != null) {
      return bestNonRanged.location;
    }
    return null;
  }

  // TODO Could be optimized by assuming soldier range?
  private static int getNumAlliesWhoCanAttackLocation(RobotController rc, MapLocation location,
      RobotInfo[] allies, int numAllies) {
    return rc.senseNearbyRobots(location, rc.getType().attackRadiusSquared, rc.getTeam()).length;
  }
}
