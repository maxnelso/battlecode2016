package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import infestorbot2.AlliedArchonTracker.AlliedArchonInfo;

public class FollowerArchonBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;

  public FollowerArchonBehavior(
      RobotController rc, NavigationSystem navigation, AlliedArchonTracker alliedArchonTracker) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
  }

  @Override
  public void run() throws GameActionException {
    AlliedArchonInfo leader = alliedArchonTracker.getLowestIdAlliedArchon(rc.getLocation());
    if (leader != null) {
      if (!navigation.directToAvoidingAlliedArchons(
          leader.loc, 2 /* avoidDist */, true /* clearRubble */)) {
        navigation.retreatFromTurrets();
      }
    } else {
      navigation.moveRandomly();
    }
  }

  public boolean shouldFollow() {
    AlliedArchonInfo leader = alliedArchonTracker.getLowestIdAlliedArchon(rc.getLocation());
    return leader != null;
  }
}
