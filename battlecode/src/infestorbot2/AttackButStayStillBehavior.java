package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class AttackButStayStillBehavior implements Behavior {
  private final RobotController rc;
  private final AttackSystem attackSystem;
  private final Radar radar;

  public AttackButStayStillBehavior(
      RobotController rc,
      Radar radar,
      AttackSystem attackSystem) {
    this.rc = rc;
    this.attackSystem = attackSystem;
    this.radar = radar;
  }

  @Override
  public void run() throws GameActionException {
    RobotInfo[] enemies = radar.getNearbyEnemies();
    RobotInfo[] zombies = radar.getNearbyZombies();
    RobotInfo[] allies = radar.getNearbyAllies();

    MapLocation target = attackSystem.getBestEnemyToShoot(rc, enemies, enemies.length, allies,
        allies.length);
    if (target != null) {
      if (rc.isWeaponReady()) {
        rc.attackLocation(target);
        return;
      }
    }
    target = attackSystem.getBestZombieToShoot(rc, zombies, zombies.length, allies,
        allies.length);
    if (rc.isWeaponReady() && target != null) {
      rc.attackLocation(target);
      return;
    }
  }
}
