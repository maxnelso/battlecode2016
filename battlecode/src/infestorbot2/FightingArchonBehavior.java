package infestorbot2;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class FightingArchonBehavior implements Behavior {

  private static final int RALLY_MESSAGE_DISTANCE_MULTIPLIER = 9;
  private static final int ARMY_RALLY_MESSAGE_FREQUENCY = 10;

  private final RobotController rc;
  private final Radar radar;
  private final NavigationSystem navigation;
  private final MessageSender messageSender;
  private final DryadArcherUnitOrder dryadArcherUnitOrder;
  private final MarineUnitOrder marineUnitOrder;
  private final GuardUnitOrder guardUnitOrder;

  private RobotInfo currentHostileFighter;
  private int currentHostileFighterRound;
  private boolean defensiveBuild;
  private int runTime;

  private RobotInfo[] hostiles;

  public FightingArchonBehavior(
      RobotController rc,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender,
      DryadArcherUnitOrder dryadArcherUnitOrder,
      MarineUnitOrder marineUnitOrder,
      GuardUnitOrder guardUnitOrder) {
    this.rc = rc;
    this.radar = radar;
    this.navigation = navigation;
    this.messageSender = messageSender;
    this.dryadArcherUnitOrder = dryadArcherUnitOrder;
    this.marineUnitOrder = marineUnitOrder;
    this.guardUnitOrder = guardUnitOrder;

    currentHostileFighter = null;
    currentHostileFighterRound = -1;
    defensiveBuild = false;
    runTime = 0;
  }

  @Override
  public void run() throws GameActionException {
    RobotInfo hostileFighter = getHostileFighter();
    if (hostileFighter == null) {
      return;
    }

    MapLocation myLoc = rc.getLocation();
    UnitOrder unitOrder = getUnitOrder();
    Direction retreatDir = myLoc.directionTo(hostileFighter.location).opposite();
    Direction buildDir = defensiveBuild ? retreatDir.opposite() : retreatDir;
    if ((hostileFighter.location.distanceSquaredTo(myLoc) > RobotType.VIPER.attackRadiusSquared
        || defensiveBuild)
        && runTime < 0) {
      // Try to spawn someone to protect me.
      if (UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), buildDir)
          || UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), buildDir.rotateLeft())
          || UnitSpawner.spawnInDirection(rc, unitOrder.getNextUnit(), buildDir.rotateRight())) {
        if (defensiveBuild) {
          runTime = RobotType.GUARD.buildTurns + 10;
        }
        unitOrder.computeNextUnit();
      }
    }

    runTime--;

    if (rc.getRoundNum() % ARMY_RALLY_MESSAGE_FREQUENCY == 0) {
      messageSender.sendArmyRallyLocation(
          hostileFighter.location, RALLY_MESSAGE_DISTANCE_MULTIPLIER);
    }

    MapLocation target = myLoc.add(retreatDir, 10);
    if (!navigation.smartRetreat(target, hostiles)) {
      // Retreating didn't work, just try digging out
      navigation.directTo(target, false /* avoidAttackers */, true /* clearRubble */);
    }
  }

  public RobotInfo getHostileFighter() {
    int roundNum = rc.getRoundNum();
    if (currentHostileFighterRound >= roundNum) {
      return currentHostileFighter;
    }

    currentHostileFighterRound = roundNum;
    currentHostileFighter = null;

    hostiles = radar.getNearbyHostiles();
    int closestDist = -1;
    for (int i = hostiles.length; --i >= 0;) {
      RobotInfo robot = hostiles[i];
      if (robot.type.canAttack()) {
        int dist = rc.getLocation().distanceSquaredTo(robot.location);
        if (closestDist == -1 || dist < closestDist) {
          closestDist = dist;
          currentHostileFighter = robot;
        }
      }
    }

    return currentHostileFighter;
  }

  private UnitOrder getUnitOrder() {
    RobotInfo[] zombies = radar.getNearbyZombies();
    RobotInfo[] allies = radar.getNearbyAllies();
    if (rc.getRoundNum() > 600 && allies.length == 0 && zombies.length > 1 && rc
        .getRobotCount() < 10) {
      defensiveBuild = true;
      return guardUnitOrder;
    } else if (zombies.length > 0) {
      defensiveBuild = false;
      return marineUnitOrder;
    }
    defensiveBuild = false;
    return dryadArcherUnitOrder;
  }
}
