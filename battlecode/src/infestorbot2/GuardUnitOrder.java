package infestorbot2;

import battlecode.common.RobotType;

public class GuardUnitOrder implements UnitOrder {

  @Override
  public RobotType getNextUnit() {
    return RobotType.GUARD;
  }

  @Override
  public void computeNextUnit() {}
}
