package infestorbot2;

import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ViperAttackSystem implements AttackSystem {

  @Override
  public MapLocation getBestEnemyToShoot(RobotController rc,
      RobotInfo[] enemies,
      int numEnemies,
      RobotInfo[] allies,
      int numAllies) {

    if (!rc.isWeaponReady()) {
      return null;
    }
    RobotInfo bestNonInfected = null;
    double bestNonInfectedScore = -999999;
    MapLocation myLoc = rc.getLocation();

    RobotInfo bestNotGonnaDie = null;
    int leastTurnsOfInctionNotGonnaDie = 9999;
    RobotInfo bestGonnaDie = null;
    int leastTurnsOfInctionGonnaDie = 9999;

    for (int i = numEnemies; --i >= 0;) {
      if (!rc.canAttackLocation(enemies[i].location)) {
        continue;
      }
      RobotInfo info = enemies[i];

      if (info.type == RobotType.ARCHON && numAllies >= 4 && info.health <= 750) {
        // Don't create a zombie outbreak if we are just gonna kill him anyway
        continue;
      }

      RobotInfo[] alliesNearTarget = rc.senseNearbyRobots(info.location, 2, rc.getTeam());

      // Always prefer to poison people
      if (info.viperInfectedTurns == 0 && alliesNearTarget.length == 0) {
        double score = getNonInfectedScore(info, myLoc);
        if (score > bestNonInfectedScore) {
          bestNonInfectedScore = score;
          bestNonInfected = info;
        }
      } else {
        int turnsOfInfection = info.viperInfectedTurns;
        if (turnsOfInfection * GameConstants.VIPER_INFECTION_DAMAGE >= info.health) {
          if (turnsOfInfection < leastTurnsOfInctionGonnaDie) {
            leastTurnsOfInctionGonnaDie = turnsOfInfection;
            bestGonnaDie = info;
          }
        } else {
          if (turnsOfInfection < leastTurnsOfInctionNotGonnaDie) {
            leastTurnsOfInctionNotGonnaDie = turnsOfInfection;
            bestNotGonnaDie = info;
          }
        }
      }
    }
    if (bestNonInfected != null) {
      return bestNonInfected.location;
    }
    if (bestNotGonnaDie != null) {
      return bestNotGonnaDie.location;
    }
    if (bestGonnaDie != null) {
      return bestGonnaDie.location;
    }
    return null;
  }

  public MapLocation getBestZombieToShoot(RobotController rc,
      RobotInfo[] zombies,
      int numZombies,
      RobotInfo[] allies,
      int numAllies) {

    if (!rc.isWeaponReady()) {
      return null;
    }
    RobotInfo bestNonRanged = null;
    double bestNonTurnsToKill = 99999;
    double bestNonWeaponDelay = 99999;

    RobotInfo bestRanged = null;
    double bestRangedTurnsToKill = 99999;
    double bestRangedWeaponDelay = 99999;
    for (int i = numZombies; --i >= 0;) {
      if (!rc.canAttackLocation(zombies[i].location)) {
        continue;
      }

      RobotInfo info = zombies[i];
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location, allies,
          numAllies);
      double turnsToKill = info.health / numNearbyAllies;
      if (info.type == RobotType.RANGEDZOMBIE) {
        if (turnsToKill < bestRangedTurnsToKill) {
          bestRangedTurnsToKill = turnsToKill;
          bestRangedWeaponDelay = info.weaponDelay;
          bestRanged = info;
        } else if (turnsToKill == bestRangedTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestRangedWeaponDelay) {
            bestRangedWeaponDelay = actionDelay;
            bestRanged = info;
          }
        }
      } else {
        if (turnsToKill < bestNonTurnsToKill) {
          bestNonTurnsToKill = turnsToKill;
          bestNonWeaponDelay = info.weaponDelay;
          bestNonRanged = info;
        } else if (turnsToKill == bestNonTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestNonWeaponDelay) {
            bestNonWeaponDelay = actionDelay;
            bestNonRanged = info;
          }
        }
      }
    }

    if (bestRanged != null) {
      return bestRanged.location;
    }
    if (bestNonRanged != null) {
      return bestNonRanged.location;
    }
    return null;
  }

  // TODO Could be optimized by assuming soldier range?
  private static int getNumAlliesWhoCanAttackLocation(RobotController rc, MapLocation location,
      RobotInfo[] allies, int numAllies) {
    return rc.senseNearbyRobots(location, RobotType.SOLDIER.attackRadiusSquared, rc
        .getTeam()).length;
  }

  private double getNonInfectedScore(RobotInfo r, MapLocation myLoc) {
    double score = 0;
    if (r.type == RobotType.VIPER || r.type == RobotType.TURRET || r.type == RobotType.TTM) {
      score += 1000000;
    }
    double canOneShotHealth = GameConstants.VIPER_INFECTION_DAMAGE * 20;
    boolean canOneShot = r.health <= canOneShotHealth;
    if (canOneShot) {
      score += 10000;
    }

    score -= r.health * 10;
    int dist = r.location.distanceSquaredTo(myLoc);
    score += dist;
    return score;
  }
}
