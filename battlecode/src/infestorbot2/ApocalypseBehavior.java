package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import infestorbot2.ViperSacReporter.SacAttack;

public class ApocalypseBehavior implements Behavior {

  private static final int MIN_DISTANCE_FROM_RALLY_TO_INCLUDE_IN_APOCALYPSE = 200;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final ViperSacReporter viperSacReporter;
  private final ViperSacCalculator viperSacCalculator;
  private final MessageSender messageSender;

  public ApocalypseBehavior(
      RobotController rc,
      NavigationSystem navigation,
      ViperSacReporter viperSacReporter,
      ViperSacCalculator viperSacCalculator,
      MessageSender messageSender) {
    this.rc = rc;
    this.navigation = navigation;
    this.viperSacReporter = viperSacReporter;
    this.viperSacCalculator = viperSacCalculator;
    this.messageSender = messageSender;
  }

  @Override
  public void run() throws GameActionException {
    SacAttack sacAttack = viperSacReporter.getSacAttack();
    int roundNum = rc.getRoundNum();
    if (sacAttack != null) {
      boolean closeToRally = rc.getLocation().distanceSquaredTo(
          sacAttack.armyRallyLoc) <= MIN_DISTANCE_FROM_RALLY_TO_INCLUDE_IN_APOCALYPSE;
      if (roundNum >= sacAttack.sacRound) {
        if (rc.getInfectedTurns() > 0 || !closeToRally) {
          rc.disintegrate();
          return;
        }
        vipersAttackAllies();
      } else {
        if (rc.getType() == RobotType.ARCHON && rc.isCoreReady() && closeToRally) {
          UnitSpawner.spawn(rc, RobotType.SCOUT);
          viperSacCalculator.computeAndShareViperSac(messageSender);
        }
        if (!navigation.directTo(
            sacAttack.armyRallyLoc, true /* avoidAttackers */, true /* clearRubble */)) {
          if (!navigation.retreatFromTurrets()) {
            navigation.directTo(
                sacAttack.armyRallyLoc, false /* avoidAttackers */, true /* clearRubble */);
          }
        }
      }
    }
  }

  private void vipersAttackAllies() throws GameActionException {
    if (rc.getType() == RobotType.VIPER && rc.isWeaponReady()) {
      rc.attackLocation(rc.getLocation());
    }
  }
}
