package infestorbot2;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Team;

public class InitialArchonLocationSymmetryCalculator implements SymmetryCalculator {

  private enum Symmetry {
    HORIZONTAL,
    VERTICAL,
    ROTATION,
    UNKNOWN
  }

  private Symmetry symmetry;
  private Integer horizontalTwiceMidX;
  private Integer verticalTwiceMidY;
  private MapLocation rotationTwiceCenter;

  public InitialArchonLocationSymmetryCalculator(RobotController rc) {
    computeFromArchonLocations(
        rc.getInitialArchonLocations(Team.A),
        rc.getInitialArchonLocations(Team.B));
  }

  private void computeFromArchonLocations(
      MapLocation[] archonsA, MapLocation[] archonsB) {
    int numArchons = archonsA.length;
    if (numArchons == 0 || numArchons != archonsB.length) {
      symmetry = Symmetry.UNKNOWN;
      return;
    }

    Integer horizontal = getHorizontal(archonsA, archonsB, numArchons);
    Integer vertical = getVertical(archonsA, archonsB, numArchons);
    MapLocation rotation = getRotation(archonsA, archonsB, numArchons);
    if (horizontal != null && vertical == null && rotation == null) {
      horizontalTwiceMidX = horizontal;
      symmetry = Symmetry.HORIZONTAL;
      return;
    }
    if (horizontal == null && vertical != null && rotation == null) {
      verticalTwiceMidY = vertical;
      symmetry = Symmetry.VERTICAL;
      return;
    }
    if (horizontal == null && vertical == null && rotation != null) {
      rotationTwiceCenter = rotation;
      symmetry = Symmetry.ROTATION;
      return;
    }

    symmetry = Symmetry.UNKNOWN;
  }

  private static Integer getHorizontal(
      MapLocation[] archonsA, MapLocation[] archonsB, int numArchons) {
    MapLocation firstA = archonsA[0];
    for (int i = numArchons; --i >= 0;) {
      if (firstA.y != archonsB[i].y) {
        continue;
      }
      int twiceMidX = firstA.x + archonsB[i].x;
      if (isHorizontalWithMidX(twiceMidX, archonsA, archonsB, numArchons)) {
        return Integer.valueOf(twiceMidX);
      }
    }
    return null;
  }

  private static boolean isHorizontalWithMidX(
      int twiceMidX, MapLocation[] archonsA, MapLocation[] archonsB, int numArchons) {
    MapLocationIntMap set = new ArrayMapLocationIntMap();
    for (int i = numArchons; --i >= 0;) {
      set.increment(archonsA[i]);
    }
    for (int i = numArchons; --i >= 0;) {
      set.decrement(new MapLocation(twiceMidX - archonsB[i].x, archonsB[i].y));
    }
    for (int i = numArchons; --i >= 0;) {
      if (set.getValue(archonsA[i]) != 0) {
        return false;
      }
    }
    return true;
  }

  private static Integer getVertical(
      MapLocation[] archonsA, MapLocation[] archonsB, int numArchons) {
    MapLocation firstA = archonsA[0];
    for (int i = numArchons; --i >= 0;) {
      if (firstA.x != archonsB[i].x) {
        continue;
      }
      int twiceMidY = firstA.y + archonsB[i].y;
      if (isVerticalWithMidY(twiceMidY, archonsA, archonsB, numArchons)) {
        return Integer.valueOf(twiceMidY);
      }
    }
    return null;
  }

  private static boolean isVerticalWithMidY(
      int twiceMidY, MapLocation[] archonsA, MapLocation[] archonsB, int numArchons) {
    MapLocationIntMap set = new ArrayMapLocationIntMap();
    for (int i = numArchons; --i >= 0;) {
      set.increment(archonsA[i]);
    }
    for (int i = numArchons; --i >= 0;) {
      set.decrement(new MapLocation(archonsB[i].x, twiceMidY - archonsB[i].y));
    }
    for (int i = numArchons; --i >= 0;) {
      if (set.getValue(archonsA[i]) != 0) {
        return false;
      }
    }
    return true;
  }

  private static MapLocation getRotation(
      MapLocation[] archonsA, MapLocation[] archonsB, int numArchons) {
    MapLocation firstA = archonsA[0];
    for (int i = numArchons; --i >= 0;) {
      MapLocation twiceCenter = new MapLocation(firstA.x + archonsB[i].x, firstA.y + archonsB[i].y);
      if (isRotationWithCenter(twiceCenter, archonsA, archonsB, numArchons)) {
        return twiceCenter;
      }
    }

    return null;
  }

  private static boolean isRotationWithCenter(
      MapLocation twiceCenter,
      MapLocation[] archonsA,
      MapLocation[] archonsB,
      int numArchons) {
    MapLocationIntMap map = new ArrayMapLocationIntMap();
    for (int j = numArchons; --j >= 0;) {
      map.increment(archonsA[j]);
      map.decrement(twiceCenter.add(-archonsB[j].x, -archonsB[j].y));
    }
    for (int j = numArchons; --j >= 0;) {
      if (map.getValue(archonsA[j]) != 0) {
        return false;
      }
    }
    return true;
  }

  @Override
  public MapLocation getOpposite(MapLocation loc) {
    int x = loc.x;
    int y = loc.y;
    switch (symmetry) {
      case HORIZONTAL:
        return horizontalTwiceMidX == null
            ? null
            : new MapLocation(horizontalTwiceMidX - x, y);
      case VERTICAL:
        return verticalTwiceMidY == null
            ? null
            : new MapLocation(x, verticalTwiceMidY - y);
      case ROTATION:
        return rotationTwiceCenter == null
            ? null
            : new MapLocation(rotationTwiceCenter.x - x, rotationTwiceCenter.y - y);
      case UNKNOWN:
      default:
        return null;
    }
  }

  @Override
  public Integer getOppositeXCoordinate(int x) {
    switch (symmetry) {
      case HORIZONTAL:
        return horizontalTwiceMidX == null ? null : horizontalTwiceMidX - x;
      case ROTATION:
        return rotationTwiceCenter == null ? null : rotationTwiceCenter.x - x;
      case VERTICAL:
      default:
        return null;
    }
  };

  @Override
  public Integer getOppositeYCoordinate(int y) {
    switch (symmetry) {
      case VERTICAL:
        return verticalTwiceMidY == null ? null : verticalTwiceMidY - y;
      case ROTATION:
        return rotationTwiceCenter == null ? null : rotationTwiceCenter.y - y;
      case HORIZONTAL:
      default:
        return null;
    }
  };

  @Override
  public void showDebugInfo(RobotController rc) {}
}
