package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class OpeningArchonBehavior implements Behavior {

  private static final int EARLY_SCOUTS = 1;
  private static final int EARLY_SCOUT_TIMEOUT = 200;

  private final RobotController rc;
  private final UnitOrder unitOrder;

  private int scoutsAndSoldiersMade;

  public OpeningArchonBehavior(RobotController rc, UnitOrder unitOrder) {
    this.rc = rc;
    this.unitOrder = unitOrder;
    scoutsAndSoldiersMade = 0;
  }

  @Override
  public void run() throws GameActionException {
    if (!isOpeningOver() && UnitSpawner.spawn(rc, unitOrder.getNextUnit())) {
      unitOrder.computeNextUnit();
      scoutsAndSoldiersMade++;
    }
  }

  public boolean isOpeningOver() {
    return scoutsAndSoldiersMade >= EARLY_SCOUTS || rc
        .getRoundNum() >= EARLY_SCOUT_TIMEOUT;
  }
}
