package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import infestorbot2.EeHanTimingReporter.TimingAttack;

public class TimingAttackBehavior implements Behavior {

  private final RobotController rc;
  private final EeHanTimingReporter eeHanTimingReporter;
  private final NavigationSystem navigation;

  public TimingAttackBehavior(
      RobotController rc,
      EeHanTimingReporter eeHanTimingReporter,
      NavigationSystem navigation) {
    this.rc = rc;
    this.eeHanTimingReporter = eeHanTimingReporter;
    this.navigation = navigation;
  }

  @Override
  public void run() throws GameActionException {
    TimingAttack timingAttack = eeHanTimingReporter.getTimingAttack();
    if (timingAttack != null) {
      int roundNum = rc.getRoundNum();
      boolean avoidAttackers = roundNum < timingAttack.startRound;
      navigation.directTo(timingAttack.location, avoidAttackers, true /* clearRubble */);
    } else {
      navigation.moveRandomly();
    }
  }
}
