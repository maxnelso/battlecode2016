package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class ScoutPreBehavior implements PreBehavior {

  private final RobotController rc;
  private final MessageReceiver messageReceiver;
  private final MessageSender messageSender;
  private final ZombieDenReporter zombieDenReporter;
  private final EnemyTurretCache enemyTurretCache;
  private final EnemyBoundingBoxReporter enemyBoundingBoxReporter;
  private final MapBoundaryCalculator mapBoundaryCalculator;

  public ScoutPreBehavior(
      RobotController rc,
      Radar radar,
      MessageReceiver messageReceiver,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      ZombieDenReporter zombieDenReporter,
      EnemyTurretCache enemyTurretCache,
      EnemyBoundingBoxReporter enemyBoundingBoxReporter,
      MapBoundaryCalculator mapBoundaryCalculator,
      EeHanTimingCalculator eeHanTimingCalculator,
      SymmetryCalculator symmetryCalculator) {
    this.rc = rc;
    this.messageReceiver = messageReceiver;
    this.messageSender = messageSender;
    this.zombieDenReporter = zombieDenReporter;
    this.enemyTurretCache = enemyTurretCache;
    this.enemyBoundingBoxReporter = enemyBoundingBoxReporter;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();
    updateAndShareZombieDens();
    updateAndShareEnemyTurrets();
    updateAndShareEnemyBoundingBox();
    updateAndShareMapBoundaries();
  }

  private void updateAndShareZombieDens() throws GameActionException {
    zombieDenReporter.invalidateNearbyDestroyedDens();
    zombieDenReporter.searchForNewDens();
    zombieDenReporter.shareNewlyAddedAndRemovedDens(messageSender);
  }

  private void updateAndShareEnemyTurrets() throws GameActionException {
    enemyTurretCache.invalidateNearbyEnemyTurrets();
    int frequency = getSharingFrequency();
    int random = (rc.getRoundNum() + frequency / 3 + rc.getID());
    if (random % frequency == 0) {
      enemyTurretCache.shareRandomEnemyTurret(messageSender);
    }
  }

  private void updateAndShareEnemyBoundingBox() throws GameActionException {
    enemyBoundingBoxReporter.update();
    enemyBoundingBoxReporter.shareEnemyBoundingBox(messageSender);
  }

  private void updateAndShareMapBoundaries() throws GameActionException {
    mapBoundaryCalculator.update();
    mapBoundaryCalculator.shareNewMapBoundaries(messageSender);
  }

  private int getSharingFrequency() throws GameActionException {
    return Math.max(
        (rc.getRobotCount() / 10) + 1,
        (rc.getRoundNum() / 200) + 1);
  }
}
