package infestorbot2;

import battlecode.common.RobotType;

public interface UnitOrder {

  public RobotType getNextUnit();

  public void computeNextUnit();
}
