package infestorbot2;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class MutableBoundingBox implements BoundingBox {

  private int minX;
  private int maxX;
  private int minY;
  private int maxY;
  private boolean isEmpty;

  public MutableBoundingBox() {
    minX = 99999;
    maxX = -99999;
    minY = 99999;
    maxY = -99999;
    isEmpty = true;
  }

  /** Returns whether the box changed. */
  public boolean addMapLocation(MapLocation loc) {
    isEmpty = false;
    boolean changed = false;
    if (loc.x < minX) {
      minX = loc.x;
      changed = true;
    }
    if (loc.x > maxX) {
      maxX = loc.x;
      changed = true;
    }
    if (loc.y < minY) {
      minY = loc.y;
      changed = true;
    }
    if (loc.y > maxY) {
      maxY = loc.y;
      changed = true;
    }
    return changed;
  }

  /** Returns whether the box changed. */
  public boolean merge(MapLocation topLeftCorner, int xRange, int yRange) {
    isEmpty = false;
    int oldArea = getArea();
    minX = topLeftCorner.x < minX ? topLeftCorner.x : minX;
    maxX = topLeftCorner.x + xRange > maxX ? topLeftCorner.x + xRange : maxX;
    minY = topLeftCorner.y < minY ? topLeftCorner.y : minY;
    maxY = topLeftCorner.y + yRange > maxY ? topLeftCorner.y + yRange : maxY;
    return oldArea != getArea();
  }

  @Override
  public MapLocation getTopLeftCorner() {
    return isEmpty ? null : new MapLocation(minX, minY);
  }

  @Override
  public MapLocation getCenter() {
    return isEmpty ? null : new MapLocation((minX + maxX) / 2, (minY + maxY) / 2);
  }

  @Override
  public boolean isEmpty() {
    return isEmpty;
  }

  @Override
  public int getXRange() {
    return isEmpty ? 0 : maxX - minX;
  }

  @Override
  public int getYRange() {
    return isEmpty ? 0 : maxY - minY;
  }

  @Override
  public int getArea() {
    return isEmpty ? 0 : (maxX - minX) * (maxY - minY);
  }

  @Override
  public boolean contains(MapLocation loc) {
    return containsWithMargins(loc, 0 /* margins */);
  }

  @Override
  public boolean containsWithMargins(MapLocation loc, int margins) {
    return !isEmpty
        && loc.x >= minX - margins
        && loc.x <= maxX + margins
        && loc.y >= minY - margins
        && loc.y <= maxY + margins;
  }

  @Override
  public void showDebugInfo(RobotController rc) {
    if (!isEmpty) {
      MapLocation c1 = new MapLocation(minX, minY);
      MapLocation c2 = new MapLocation(maxX, minY);
      MapLocation c3 = new MapLocation(maxX, maxY);
      MapLocation c4 = new MapLocation(minX, maxY);
      rc.setIndicatorLine(c1, c2, 255, 255, 255);
      rc.setIndicatorLine(c2, c3, 255, 255, 255);
      rc.setIndicatorLine(c3, c4, 255, 255, 255);
      rc.setIndicatorLine(c4, c1, 255, 255, 255);
      rc.setIndicatorLine(rc.getLocation(), c3, 255, 255, 255);
    }
  }
}
