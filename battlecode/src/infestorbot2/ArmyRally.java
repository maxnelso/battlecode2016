package infestorbot2;

import battlecode.common.MapLocation;

public interface ArmyRally {

  public MapLocation getRally();
}
