package infestorbot2;

import battlecode.common.GameActionException;

public class NoOpViperSacCalculator implements ViperSacCalculator {

  @Override
  public void computeAndShareViperSac(MessageSender messageSender) throws GameActionException {}

  @Override
  public void maybeCancelViperSacAndShare(MessageSender messageSender) throws GameActionException {}
}
