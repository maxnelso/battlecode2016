package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DefaultEnemyBoundingBoxReporter implements EnemyBoundingBoxReporter {

  private final int MIN_BOUNDING_BOX_TRACKING_ROUND = 400;

  private final RobotController rc;
  private final Radar radar;
  private final MutableBoundingBox enemyBoundingBox;
  private boolean shareBoundingBox;

  public DefaultEnemyBoundingBoxReporter(RobotController rc, Radar radar) {
    this.rc = rc;
    this.radar = radar;
    enemyBoundingBox = new MutableBoundingBox();
    shareBoundingBox = false;
  }

  @Override
  public void update() {
    if (rc.getRoundNum() < MIN_BOUNDING_BOX_TRACKING_ROUND) {
      return;
    }

    RobotInfo[] enemies = radar.getNearbyEnemies();
    if (enemies.length > 0) {
      int p = (int) (Math.random() * enemies.length);
      if (enemies[p].type != RobotType.SCOUT
          && enemies[p].type != RobotType.ARCHON
          && enemies[p].type != RobotType.GUARD
          && enemyBoundingBox.addMapLocation(enemies[p].location)) {
        shareBoundingBox = true;
      }
    }
  }

  @Override
  public void reportEnemy(MapLocation loc) {
    enemyBoundingBox.addMapLocation(loc);
    shareBoundingBox = true;
  }

  @Override
  public void reportBoundingBox(MapLocation topLeftCorner, int xRange, int yRange) {
    enemyBoundingBox.merge(topLeftCorner, xRange, yRange);
  }

  @Override
  public BoundingBox getEnemyBoundingBox() {
    return enemyBoundingBox;
  }

  @Override
  public void shareEnemyBoundingBox(MessageSender messageSender) throws GameActionException {
    if (!rc.isCoreReady()
        || !shareBoundingBox
        || radar.getNearbyHostiles().length != 0) {
      return;
    }

    shareBoundingBox = false;
    messageSender.sendEnemyBoundingBoxEverywhere(
        enemyBoundingBox.getTopLeftCorner(),
        enemyBoundingBox.getXRange(),
        enemyBoundingBox.getYRange());
  }

  @Override
  public void showDebugInfo() {
    enemyBoundingBox.showDebugInfo(rc);
  }
}
