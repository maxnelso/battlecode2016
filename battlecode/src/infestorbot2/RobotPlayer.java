package infestorbot2;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import infestorbot2.BehaviorFactory.BehaviorInfo;

public class RobotPlayer {

  public static void run(RobotController rc) {
    Random.seed(rc.getRoundNum());

    BehaviorInfo b = BehaviorFactory.createForRobotController(rc);
    PreBehavior preBehavior = b.preBehavior;
    Behavior behavior = b.behavior;

    // Empty all messages received during build time.
    rc.emptySignalQueue();

    while (true) {
      try {
        int currentRound = rc.getRoundNum();
        preBehavior.preRun();
        behavior.run();
      } catch (GameActionException e) {}
      // In case of bytecode overages, clear all messages before ending the
      // turn.
      rc.emptySignalQueue();
      Clock.yield();
    }
  }
}
