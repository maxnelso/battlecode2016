package infestorbot2;

import battlecode.common.MapLocation;

public interface ArchonHider {

  public void reportArchonHidingLocation(MapLocation archonHidingLoc);

  public MapLocation getArchonHidingLocation();
}
