package infestorbot2;

public class TurtlerUtil {

  private static final int TURTLE_MAX_ENEMY_BOUNDING_BOX_DIMENSION = 13;

  public static boolean isTurtleEnemyBoundingBox(BoundingBox boundingBox) {
    return !boundingBox.isEmpty()
        && boundingBox.getXRange() <= TURTLE_MAX_ENEMY_BOUNDING_BOX_DIMENSION
        && boundingBox.getYRange() <= TURTLE_MAX_ENEMY_BOUNDING_BOX_DIMENSION;
  }
}
