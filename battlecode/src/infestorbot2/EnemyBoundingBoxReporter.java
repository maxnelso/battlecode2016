package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public interface EnemyBoundingBoxReporter {

  public void update();

  public void reportEnemy(MapLocation loc);

  public void reportBoundingBox(MapLocation topLeftCorner, int xRange, int yRange);

  public BoundingBox getEnemyBoundingBox();

  public void shareEnemyBoundingBox(MessageSender messageSender) throws GameActionException;

  public void showDebugInfo();
}
