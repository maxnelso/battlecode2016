package infestorbot2;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface SymmetryCalculator {

  /**
   * Returns the map location opposite the given one accounting for the map symmetry, or null if
   * the map symmetry is unknown.
   */
  public MapLocation getOpposite(MapLocation loc);

  /**
   * Returns the x coordinate opposite the given one according to the symmetry, or null if the
   * symmetry is not known or does not apply to x coordinates.
   */
  public Integer getOppositeXCoordinate(int x);

  /**
   * Returns the y coordinate opposite the given one according to the symmetry, or null if the
   * symmetry is not known or does not apply to y coordinates.
   */
  public Integer getOppositeYCoordinate(int y);

  public void showDebugInfo(RobotController rc);
}
