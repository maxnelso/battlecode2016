package infestorbot2;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface BoundingBox {

  public MapLocation getTopLeftCorner();

  public MapLocation getCenter();

  public boolean isEmpty();

  public int getXRange();

  public int getYRange();

  public int getArea();

  public boolean contains(MapLocation loc);

  public boolean containsWithMargins(MapLocation loc, int margins);

  public void showDebugInfo(RobotController rc);
}
