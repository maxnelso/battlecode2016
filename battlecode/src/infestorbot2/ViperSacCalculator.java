package infestorbot2;

import battlecode.common.GameActionException;

public interface ViperSacCalculator {

  public void computeAndShareViperSac(MessageSender messageSender) throws GameActionException;

  public void maybeCancelViperSacAndShare(MessageSender messageSender) throws GameActionException;
}
