package infestorbot2;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DuckNavigationSystem implements NavigationSystem {

  private final RobotController rc;
  private final Radar radar;
  private final AlliedArchonTracker alliedArchonTracker;
  private final EnemyTurretCache enemyTurretCache;

  private MapLocation bugDestination;
  private BugState bugState;
  private WallSide bugWallSide;
  private int bugStartDistSq;
  private Direction bugLastMoveDir;
  private Direction bugLookStartDir;
  private int bugRotationCount;
  private int bugMovesSinceSeenObstacle;

  private enum BugState {
    DIRECT,
    BUG
  }

  private enum WallSide {
    LEFT,
    RIGHT
  }

  public DuckNavigationSystem(
      RobotController rc,
      Radar radar,
      AlliedArchonTracker alliedArchonTracker,
      EnemyTurretCache enemyTurretCache) {
    this.rc = rc;
    this.radar = radar;
    this.alliedArchonTracker = alliedArchonTracker;
    this.enemyTurretCache = enemyTurretCache;
    bugState = BugState.DIRECT;
    bugMovesSinceSeenObstacle = 0;
    bugWallSide = Math.random() < .5 ? WallSide.LEFT : WallSide.RIGHT;
  }

  @Override
  public boolean directTo(
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    NavigationSafetyPolicy policy = getPolicy(avoidAttackers);
    if (!checkForwardDirectTowards(loc, policy, clearRubble)) {
      return directTowards(loc, policy, clearRubble, NavigationUtil.getBackwardsDirections(
          rc.getLocation(),
          loc));
    }
    return true;
  }

  @Override
  public boolean directToOnlyForward(
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    NavigationSafetyPolicy policy = getPolicy(avoidAttackers);
    if (!checkForwardDirectTowards(loc, policy, clearRubble)) {
      return false;
    }
    return true;
  }

  @Override
  public boolean directToOnlyForwardAndSides(
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException {
    return rc.isCoreReady()
        ? directTowards(loc, getPolicy(avoidAttackers), clearRubble,
            NavigationUtil.getForwardAndSideDirections(
                rc.getLocation(),
                loc))
        : false;
  }

  @Override
  public boolean directToWithoutBlockingAllyRetreat(
      Direction dir, MapLocation retreatFromLoc) throws GameActionException {
    return rc.isCoreReady()
        ? directTowards(
            rc.getLocation().add(dir),
            new AvoidBlockingAllyRetreatPolicy(retreatFromLoc),
            false /* clearRubble */,
            NavigationUtil.getNonDiagonalDirections(dir))
        : false;
  }

  @Override
  public boolean directToOnlyNonDiagonal(Direction dir) throws GameActionException {
    return rc.isCoreReady()
        ? directTowards(
            rc.getLocation().add(dir),
            getPolicy(false /* avoidAttackers */),
            false /* clearRubble */,
            NavigationUtil.getNonDiagonalDirections(dir))
        : false;
  }

  @Override
  public boolean directToAvoidingAlliedArchons(
      MapLocation loc,
      int avoidDist,
      boolean clearRubble) throws GameActionException {
    return rc.isCoreReady()
        ? directTowards(
            loc,
            new AvoidAlliedArchonsPolicy(alliedArchonTracker, enemyTurretCache, avoidDist),
            clearRubble,
            NavigationUtil.getAllDirections(rc.getLocation(), loc))
        : false;
  };

  @Override
  public boolean directToWithMaximumEnemyExposure(
      MapLocation loc,
      int maximumEnemyExposure) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    int[] numEnemiesAttackingDirs = getNumEnemiesAttackingMoveDirs(radar);

    Direction toEnemy = rc.getLocation().directionTo(loc);
    Direction[] tryDirs = new Direction[] {
      toEnemy, toEnemy.rotateLeft(), toEnemy.rotateRight()
    };

    Direction diagonalGood = Direction.NONE;
    Direction nonDiagonalRubble = Direction.NONE;
    Direction diagonalRubble = Direction.NONE;
    MapLocation myLoc = rc.getLocation();
    for (int i = tryDirs.length; --i >= 0;) {
      Direction tryDir = tryDirs[i];
      if (!rc.canMove(tryDir)) {
        continue;
      }
      if (numEnemiesAttackingDirs[tryDir.ordinal()] > maximumEnemyExposure) {
        continue;
      }

      boolean belowThreshold = rc.senseRubble(myLoc.add(tryDir)) < GameConstants.RUBBLE_SLOW_THRESH;
      boolean nonDiagonal = !tryDir.isDiagonal();
      if (belowThreshold && nonDiagonal) {
        rc.move(tryDir);
        return true;
      } else if (belowThreshold && !nonDiagonal) {
        diagonalGood = tryDir;
      } else if (!belowThreshold && nonDiagonal) {
        nonDiagonalRubble = tryDir;
      } else if (!belowThreshold && !nonDiagonal) {
        diagonalRubble = tryDir;
      }
    }

    // TODO Try rearranging these?
    if (diagonalGood != Direction.NONE) {
      rc.move(diagonalGood);
      return true;
    } else if (nonDiagonalRubble != Direction.NONE) {
      rc.move(nonDiagonalRubble);
      return true;
    } else if (diagonalRubble != Direction.NONE) { // TODO Maybe try removing ?
      rc.move(diagonalRubble);
      return true;
    }
    return false;
  }

  @Override
  public boolean noDiagonalTowardsBattle(MapLocation loc)
      throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }

    Direction[] allDirs = NavigationUtil.getForwardAndSideDirections(rc.getLocation(), loc);
    Direction bestDir = Direction.NONE;
    MapLocation myLoc = rc.getLocation();
    for (Direction d : allDirs) {
      if (d.isDiagonal()) {
        continue;
      }

      boolean canMove = rc.canMove(d);
      if (canMove && rc.senseRubble(myLoc.add(d)) < GameConstants.RUBBLE_SLOW_THRESH) {
        rc.move(d);
        return true;
      } else if (canMove) {
        bestDir = d;
      }
    }
    if (bestDir != Direction.NONE) {
      rc.move(bestDir);
      return true;
    }

    return false;
  }

  @Override
  public boolean retreatFromTurrets() throws GameActionException {
    Direction turretDir = enemyTurretCache.getDirectionToTurretInRange(rc.getLocation());
    return turretDir != null && directTo(
        rc.getLocation().add(turretDir.opposite(), 5),
        false /* avoidAttackers */,
        false /* clearRubble */);
  };

  // Built with
  // public static void tmp() {
  // MapLocation center = new MapLocation(0, 0);
  // for (int ex = -5; ex <= +5; ex++) {
  // System.out.print("{");
  // for (int ey = -5; ey <= +5; ey++) {
  // MapLocation enemyLoc = new MapLocation(ex, ey);
  // ArrayList<Integer> attacked = new ArrayList<Integer>();
  // for (int dir = 0; dir < 8; dir++) {
  // MapLocation moveLoc = center.add(Direction.values()[dir]);
  // if (moveLoc.distanceSquaredTo(enemyLoc) <=
  // RobotType.SOLDIER.attackRadiusSquared)
  // attacked.add(dir);
  // }
  // System.out.print("{");
  // for (int i = 0; i < attacked.size(); i++) {
  // System.out.print(attacked.get(i));
  // if (i < attacked.size() - 1)
  // System.out.print(",");
  // }
  // System.out.print("}");
  // if (ey < +5) {
  // System.out.print(",");
  // int spaces = Math.min(16, 17 - 2 * attacked.size());
  // for (int i = 0; i < spaces; i++)
  // System.out.print(" ");
  // }
  // }
  // System.out.println("}");
  // }
  // }

  private static int[][][] attackNotes = {
    {
      {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
      },
    {
      {}, {}, {
        7
      }, {
        6, 7
      }, {
        5, 6, 7
      }, {
        5, 6, 7
      }, {
        5, 6, 7
      }, {
        5, 6
      }, {
        5
      }, {}, {}
      },
    {
      {}, {
        7
      }, {
        0, 6, 7
      }, {
        0, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        4, 5, 6, 7
      }, {
        4, 5, 6
      }, {
        5
      }, {}
      },
    {
      {}, {
        0, 7
      }, {
        0, 1, 6, 7
      }, {
        0, 1, 2, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 2, 3, 4, 5, 6, 7
      }, {
        3, 4, 5, 6
      }, {
        4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1
      }, {
        0, 1, 2, 7
      }, {
        0, 1, 2, 3, 4, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6
      }, {
        2, 3, 4, 5
      }, {
        3, 4
      }, {}
      },
    {
      {}, {
        1
      }, {
        0, 1, 2
      }, {
        0, 1, 2, 3
      }, {
        0, 1, 2, 3, 4
      }, {
        0, 1, 2, 3, 4
      }, {
        0, 1, 2, 3, 4
      }, {
        1, 2, 3, 4
      }, {
        2, 3, 4
      }, {
        3
      }, {}
      },
    {
      {}, {}, {
        1
      }, {
        1, 2
      }, {
        1, 2, 3
      }, {
        1, 2, 3
      }, {
        1, 2, 3
      }, {
        2, 3
      }, {
        3
      }, {}, {}
      },
    {
      {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
      }
  };

  private int[] getNumEnemiesAttackingMoveDirs(Radar radar) {
    int[] numEnemiesAttackingMoveDirs = new int[8];
    RobotInfo[] nearbyEnemies = radar.getNearbyEnemies();
    for (int i = nearbyEnemies.length; i-- > 0;) {
      RobotInfo info = nearbyEnemies[i];
      if (info.type.canAttack()) {
        MapLocation enemyLoc = info.location;
        if (Math.abs(enemyLoc.x - rc.getLocation().x) <= 5 &&
            Math.abs(enemyLoc.y - rc.getLocation().y) <= 5) {
          int[] attackedDirs = attackNotes[5 + enemyLoc.x - rc.getLocation().x][5 + enemyLoc.y
              - rc.getLocation().y];
          for (int j = attackedDirs.length; j-- > 0;) {
            numEnemiesAttackingMoveDirs[attackedDirs[j]]++;
          }
        }
      }
    }
    return numEnemiesAttackingMoveDirs;
  }

  @Override
  public boolean bugTo(
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    return bugTowards(loc, getPolicy(avoidAttackers), clearRubble);
  }

  @Override
  public boolean moveRandomly() throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 8; --i >= 0;) {
      if (rc.canMove(d)) {
        rc.move(d);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }

  @Override
  public boolean smartRetreat(MapLocation destination, RobotInfo[] hostiles)
      throws GameActionException {
    int minDanger = -1;
    Direction bestDir = null;
    MapLocation myLoc = rc.getLocation();
    for (Direction d : NavigationUtil.getAllDirections(rc.getLocation(), destination)) {
      if (rc.canMove(d)) {
        int danger = 0;
        MapLocation testLoc = myLoc.add(d);

        // makes units scared of edges to avoid getting cornered by big packs
        if (hostiles.length >= 3) {
          if (d.isDiagonal()) {
            // both map edges need to be considered
            danger += dangerFromEdge(d.rotateLeft(), testLoc);
            danger += dangerFromEdge(d.rotateRight(), testLoc);
          } else {
            // only map edge in the direction of d
            danger += dangerFromEdge(d, testLoc);
            danger += dangerFromEdge(d.rotateRight(), testLoc);
            danger += dangerFromEdge(d.rotateLeft(), testLoc);

          }
        }

        for (RobotInfo h : hostiles) {
          if (testLoc.distanceSquaredTo(h.location) <= h.type.attackRadiusSquared) {
            danger += h.type.attackPower;
          }
          if (minDanger != -1 && danger > minDanger) {
            break;
          }
        }
        if (danger == 0) {
          move(d, false);
          return true;
        }

        if (danger < minDanger || minDanger == -1) {
          bestDir = d;
          minDanger = danger;
        }
      }
    }

    if (bestDir != null) {
      move(bestDir, false);
      return true;
    }

    return false;
  }

  // should be called with cardinal directions
  private int dangerFromEdge(Direction d, MapLocation testLoc) throws GameActionException {
    int totalDanger = 0;
    for (int i = 3; --i > 0;) {
      if (!rc.onTheMap(testLoc.add(d, i))) {
        // squared because being very close to an edge is very bad
        totalDanger += (5 - i) * (5 - i);
      } else {
        break;
      }
    }
    return totalDanger;

  }

  private boolean checkForwardDirectTowards(
      MapLocation destination,
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {
    MapLocation myLoc = rc.getLocation();
    Direction[] dirs = NavigationUtil.getForwardDirections(myLoc, destination);
    double lowestScore = 99999999;
    Direction bestDir = null;
    for (int i = 0; i < dirs.length; i++) {
      if (!safeToMove(dirs[i], policy, clearRubble)) {
        continue;
      }
      if (rc.getType() == RobotType.SCOUT) {
        move(dirs[i], false /* clearRubble */);
        return true;
      }
      MapLocation loc = myLoc.add(dirs[i]);
      double rubble = rc.senseRubble(loc);
      double score = loc.equals(destination)
          ? -10000
          : (rubble < GameConstants.RUBBLE_SLOW_THRESH ? i : rubble + i);
      if ((clearRubble || rubble < GameConstants.RUBBLE_OBSTRUCTION_THRESH)
          && score < lowestScore) {
        lowestScore = score;
        bestDir = dirs[i];
      }
    }

    return bestDir != null && move(bestDir, clearRubble);
  }

  private boolean directTowards(
      MapLocation destination,
      NavigationSafetyPolicy policy,
      boolean clearRubble,
      Direction[] dirs) throws GameActionException {
    int length = dirs.length;
    for (int i = 0; i < length; i++) {
      Direction dir = dirs[i];
      if (safeToMove(dir, policy, clearRubble)) {
        move(dir, clearRubble);
        return true;
      }
    }

    return false;
  }

  private boolean safeToMove(
      Direction dir,
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {

    MapLocation testLocation = rc.getLocation().add(dir);
    if (!policy.isSafeToMoveTo(rc, testLocation)) {
      return false;
    }
    if (rc.canMove(dir)) {
      return true;
    }
    return clearRubble
        && rc.onTheMap(testLocation)
        && rc.senseRobotAtLocation(testLocation) == null;
  }

  private boolean move(Direction dir, boolean clearRubble) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    if (clearRubble
        && rc.getType() != RobotType.TTM
        && rc.getType() != RobotType.TURRET
        && rc.senseRubble(rc.getLocation().add(dir)) >= GameConstants.RUBBLE_SLOW_THRESH) {
      rc.clearRubble(dir);
      return true;
    } else if (rc.canMove(dir)) {
      rc.move(dir);
      return true;
    }
    return false;
  }

  private NavigationSafetyPolicy getPolicy(boolean avoidAttackers) {
    return avoidAttackers
        ? new AvoidAttackingUnitsPolicy(radar, enemyTurretCache)
        : new NoSafetyPolicy();
  }

  private boolean bugTowards(
      MapLocation dest,
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {
    if (!dest.equals(bugDestination)) {
      bugDestination = dest;
      bugState = BugState.DIRECT;
    }

    if (rc.getLocation().equals(dest)) {
      return false;
    }

    return bugMove(policy, clearRubble);
  }

  private void startBug(
      NavigationSafetyPolicy policy, boolean clearRubble) throws GameActionException {
    bugStartDistSq = rc.getLocation().distanceSquaredTo(bugDestination);
    bugLastMoveDir = rc.getLocation().directionTo(bugDestination);
    bugLookStartDir = rc.getLocation().directionTo(bugDestination);
    bugRotationCount = 0;
    bugMovesSinceSeenObstacle = 0;

    if (bugWallSide == null) {
      // try to intelligently choose on which side we will keep the wall
      Direction leftTryDir = bugLastMoveDir.rotateLeft();
      for (int i = 0; i < 3; i++) {
        if (!safeToMove(leftTryDir, policy, clearRubble))
          leftTryDir = leftTryDir.rotateLeft();
        else
          break;
      }
      Direction rightTryDir = bugLastMoveDir.rotateRight();
      for (int i = 0; i < 3; i++) {
        if (!safeToMove(rightTryDir, policy, clearRubble))
          rightTryDir = rightTryDir.rotateRight();
        else
          break;
      }
      if (bugDestination.distanceSquaredTo(rc.getLocation().add(leftTryDir)) < bugDestination
          .distanceSquaredTo(rc.getLocation().add(rightTryDir))) {
        bugWallSide = WallSide.RIGHT;
      } else {
        bugWallSide = WallSide.LEFT;
      }
    }
  }

  private Direction findBugMoveDir(
      NavigationSafetyPolicy policy, boolean clearRubble) throws GameActionException {
    bugMovesSinceSeenObstacle++;
    Direction dir = bugLookStartDir;
    for (int i = 8; i-- > 0;) {
      if (safeToMove(dir, policy, clearRubble))
        return dir;
      dir = (bugWallSide == WallSide.LEFT ? dir.rotateRight() : dir.rotateLeft());
      bugMovesSinceSeenObstacle = 0;
    }
    return null;
  }

  private int numRightRotations(Direction start, Direction end) {
    return (end.ordinal() - start.ordinal() + 8) % 8;
  }

  private int numLeftRotations(Direction start, Direction end) {
    return (-end.ordinal() + start.ordinal() + 8) % 8;
  }

  private int calculateBugRotation(Direction moveDir) {
    if (bugWallSide == WallSide.LEFT) {
      return numRightRotations(bugLookStartDir, moveDir) - numRightRotations(bugLookStartDir,
          bugLastMoveDir);
    } else {
      return numLeftRotations(bugLookStartDir, moveDir) - numLeftRotations(bugLookStartDir,
          bugLastMoveDir);
    }
  }

  private boolean bugMove(Direction dir) throws GameActionException {
    move(dir, false /* clearRubble */);
    bugRotationCount += calculateBugRotation(dir);
    bugLastMoveDir = dir;
    if (bugWallSide == WallSide.LEFT) {
      bugLookStartDir = dir.rotateLeft().rotateLeft();
    } else {
      bugLookStartDir = dir.rotateRight().rotateRight();
    }
    return true;
  }

  private boolean detectBugIntoEdge(Direction proposedMoveDir) throws GameActionException {
    if (proposedMoveDir == null) {
      return false;
    }
    if (bugWallSide == WallSide.LEFT) {
      return !rc.onTheMap(rc.getLocation().add(proposedMoveDir.rotateLeft()));
    } else {
      return !rc.onTheMap(rc.getLocation().add(proposedMoveDir.rotateRight()));
    }
  }

  private void reverseBugWallFollowDir(RobotController rc, NavigationSafetyPolicy policy,
      boolean clearRubble)
          throws GameActionException {
    bugWallSide = (bugWallSide == WallSide.LEFT ? WallSide.RIGHT : WallSide.LEFT);
    startBug(policy, clearRubble);
  }

  private boolean bugTurn(
      NavigationSafetyPolicy policy, boolean clearRubble) throws GameActionException {
    Direction dir = findBugMoveDir(policy, clearRubble);
    if (detectBugIntoEdge(dir)) {
      reverseBugWallFollowDir(rc, policy, clearRubble);
      dir = findBugMoveDir(policy, clearRubble);
    }
    if (dir != null) {
      return bugMove(dir);
    }

    return false;
  }

  private boolean canEndBug(RobotController rc) {
    if (bugMovesSinceSeenObstacle >= 4)
      return true;
    return (bugRotationCount <= 0 || bugRotationCount >= 8) && rc.getLocation().distanceSquaredTo(
        bugDestination) <= bugStartDistSq;
  }

  private boolean bugMove(
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {
    // Check if we can stop bugging at the *beginning* of the turn
    if (bugState == BugState.BUG) {
      if (canEndBug(rc)) {
        bugState = BugState.DIRECT;
      }
    }

    // If DIRECT mode, try to go directly to target
    if (bugState == BugState.DIRECT) {
      if (!directTowards(bugDestination, policy, clearRubble, NavigationUtil.getForwardDirections(rc
          .getLocation(), bugDestination))) {
        bugState = BugState.BUG;
        startBug(policy, clearRubble);
      } else {
        return true;
      }
    }

    // If that failed, or if bugging, bug
    if (bugState == BugState.BUG) {
      return bugTurn(policy, clearRubble);
    }

    return false;
  }

}