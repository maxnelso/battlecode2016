package infestorbot2;

import battlecode.common.RobotType;

public class ScoutsAndSoldiersUnitOrder implements UnitOrder {

  private RobotType nextUnit;

  public ScoutsAndSoldiersUnitOrder() {
    nextUnit = RobotType.SCOUT;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (nextUnit == RobotType.SCOUT) {
      nextUnit = RobotType.SOLDIER;
    }
  }
}
