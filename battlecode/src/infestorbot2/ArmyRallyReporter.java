package infestorbot2;

import battlecode.common.MapLocation;

public interface ArmyRallyReporter {

  public void reportRally(MapLocation rally);
}
