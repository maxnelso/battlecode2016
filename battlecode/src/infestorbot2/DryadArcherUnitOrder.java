package infestorbot2;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class DryadArcherUnitOrder implements UnitOrder {

  private RobotController rc;
  private RobotType nextUnit;
  private static final int MAKE_VIPER_ROUND = 0;
  private int turretsMade;
  private int vipersMade;
  private int soldiersMade;
  private boolean shouldMakeScout;

  public DryadArcherUnitOrder(RobotController rc, boolean shouldMakeScout) {
    this.rc = rc;
    this.nextUnit = RobotType.SOLDIER;
    this.turretsMade = 0;
    this.vipersMade = 0;
    this.shouldMakeScout = shouldMakeScout;
    this.soldiersMade = 0;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (shouldMakeScout) {
      shouldMakeScout = false;
      nextUnit = RobotType.SCOUT;
      return;
    }

    if (rc.getRoundNum() <= MAKE_VIPER_ROUND) {
      nextUnit = RobotType.SOLDIER;
      return;
    }

    if (soldiersMade + vipersMade == 15) {
      nextUnit = RobotType.SCOUT;
      soldiersMade = 0;
      vipersMade = 0;
      return;
    }

    double rand = Math.random();
    if (rand <= .45) {
      if (3 * vipersMade <= turretsMade) {
        nextUnit = RobotType.VIPER;
        vipersMade++;
      } else {
        nextUnit = RobotType.VIPER;
        turretsMade++;
      }
    } else {
      nextUnit = RobotType.SOLDIER;
      soldiersMade++;
    }
  }
}
