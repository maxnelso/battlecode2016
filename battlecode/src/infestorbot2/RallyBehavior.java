package infestorbot2;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class RallyBehavior implements Behavior {

  private final RobotController rc;
  private final ArmyRally armyRally;
  private final NavigationSystem navigation;

  public RallyBehavior(RobotController rc, ArmyRally armyRally, NavigationSystem navigation) {
    this.rc = rc;
    this.armyRally = armyRally;
    this.navigation = navigation;
  }

  @Override
  public void run() throws GameActionException {
    MapLocation rally = armyRally.getRally();
    if (rally != null) {
      navigation.directToAvoidingAlliedArchons(rally, 2 /* avoidDist */, true /* clearRubble */);
    } else {
      navigation.moveRandomly();
    }
  }
}
