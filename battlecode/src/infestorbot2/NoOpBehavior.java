package infestorbot2;

import battlecode.common.RobotController;

public class NoOpBehavior implements PreBehavior, Behavior {

  public NoOpBehavior(RobotController rc) {}

  @Override
  public void preRun() {}

  @Override
  public void run() {}
}
