package flashbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {
  public static void run(RobotController rc) {
    BytecodeProfiler profiler = new NoOpBytecodeProfiler();

    ArchonTracker archonTracker = new ArchonTracker();
    PartScanner partScanner = new PartScanner();
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache();
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator();
    EeHanTiming eeHanTiming = new EeHanTiming();
    InterestingTargets interestingTargets = new DefaultInterestingTargets(
        enemyTurretCache, profiler);
    Radar radar = new DefaultRadar(interestingTargets, archonTracker, enemyTurretCache);
    RadarAttackRepairSystem attackRepairSystem = new RadarAttackRepairSystem(radar);
    DuckNavigationSystem navigation = new DuckNavigationSystem(radar, enemyTurretCache);
    ExplorationCalculator explorationCalculator = new ExplorationCalculator(mapBoundaryCalculator);
    ZombieDragger zombieDragger = new ZombieDragger(radar, navigation, archonTracker);
    BasicMessages basicMessages = new DefaultBasicMessages();
    Messenger messenger = MessengerFactory.create(
        rc,
        interestingTargets,
        archonTracker,
        radar,
        mapBoundaryCalculator,
        basicMessages,
        enemyTurretCache,
        eeHanTiming);

    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior(
            navigation,
            radar,
            attackRepairSystem /* repairSystem */,
            messenger /* messageSender */,
            interestingTargets,
            explorationCalculator,
            mapBoundaryCalculator,
            archonTracker,
            partScanner,
            enemyTurretCache,
            eeHanTiming,
            profiler);
        break;
      case SOLDIER:
      case VIPER:
        behavior = new SoldierBehavior(
            navigation,
            attackRepairSystem /* attackSystem */,
            mapBoundaryCalculator,
            explorationCalculator,
            radar,
            interestingTargets,
            archonTracker,
            eeHanTiming,
            profiler);
        break;
      case GUARD:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
        behavior = new ShootBehavior(navigation, attackRepairSystem /* attackSystem */);
        break;
      case SCOUT:
        behavior = new ScoutBehavior(
            rc.getID(),
            navigation,
            radar,
            messenger /* messageSender */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator,
            archonTracker,
            basicMessages,
            zombieDragger,
            partScanner,
            enemyTurretCache,
            eeHanTiming);
        break;
      case TURRET:
      case TTM:
        behavior = new TurretBehavior(
            navigation,
            radar,
            attackRepairSystem /* attackSystem */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator,
            archonTracker,
            enemyTurretCache,
            eeHanTiming);
        break;
      case ZOMBIEDEN:
      default:
        behavior = new NoOpBehavior();
        break;
    }

    while (true) {
      try {
        profiler.start();
        messenger.receiveMessages(
            rc, behavior.getMessagingBytecodeLimit(rc));
        profiler.split("after receiving messages");
        behavior.behave(rc);
        profiler.end();
        profiler.printToConsole();
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
