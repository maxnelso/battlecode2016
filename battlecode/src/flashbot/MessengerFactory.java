package flashbot;

import battlecode.common.RobotController;

public class MessengerFactory {

  public static Messenger create(
      RobotController rc,
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator,
      BasicMessages basicMessages,
      EnemyTurretCache enemyTurretCache,
      EeHanTiming eeHanTiming) {
    boolean subscribeToArchons = false;
    boolean subscribeToDens = false;
    boolean subscribeToParts = false;
    boolean subscribeToNeutralRobots = false;
    boolean subscribeToMapBoundaries = false;
    boolean subscribeToEnemyInfo = false;
    boolean subscribeToEnemyTurrets = false;
    boolean subscribeToTimingAttacks = false;
    switch (rc.getType()) {
      case ARCHON:
      case SCOUT:
        subscribeToArchons = true;
        subscribeToDens = true;
        subscribeToParts = true;
        subscribeToNeutralRobots = true;
        subscribeToMapBoundaries = true;
        subscribeToEnemyTurrets = true;
        subscribeToTimingAttacks = true;
        break;
      case SOLDIER:
      case GUARD:
      case VIPER:
      case TURRET:
      case TTM:
        subscribeToArchons = true;
        subscribeToDens = true;
        subscribeToMapBoundaries = true;
        subscribeToEnemyInfo = true;
        subscribeToEnemyTurrets = true;
        subscribeToTimingAttacks = true;
        break;
      default:
        break;
    }

    return new Messenger(
        interestingTargets,
        archonTracker,
        radar,
        mapBoundaryCalculator,
        basicMessages,
        enemyTurretCache,
        eeHanTiming,
        subscribeToArchons,
        subscribeToDens,
        subscribeToParts,
        subscribeToNeutralRobots,
        subscribeToMapBoundaries,
        subscribeToEnemyInfo,
        subscribeToEnemyTurrets,
        subscribeToTimingAttacks);
  }
}
