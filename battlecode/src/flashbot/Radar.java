package flashbot;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface Radar {

  public void addReportedRobot(RobotController rc, RobotInfo robotInfo);

  public void update(RobotController rc, int bytecodeLimit);

  public MapLocation getClosestAlly();

  public MapLocation getClosestEnemy();

  public MapLocation getLeastHealthAllyInRepairRange();

  public MapLocation getClosestEnemyOrZombie();

  public MapLocation getClosestZombie();

  public MapLocation getLeastHealthEnemyAttackerInAttackRange();

  public MapLocation getLeastHealthEnemyInAttackRange();

  public MapLocation getLeastHealthZombieAttackerInAttackRange();

  public MapLocation getLeastHealthZombieInAttackRange();

  public MapLocation getAdjacentNeutralRobot(MapLocation loc);

  public boolean isAttackableByZombies(MapLocation loc);

  public boolean isAttackableByEnemiesOrZombies(MapLocation loc);

  public boolean shouldDragZombies();

  public int getEnemyStrength();

  public int getAllyStrength();

  public int getZombieStrength();

  public RobotInfo getBuddyTarget(RobotController rc, RobotInfo buddy);

  public int getNumEnemiesWhoCanAttackUs(RobotController rc);

  public boolean canOneShotEnemyOrNotGetHit(RobotController rc);

  public int getMaxAlliesWhoCanAttackEnemy(RobotController rc);

  public int getNumAlliesWhoCanAttackLocation(RobotController rc, MapLocation location);

  public boolean shouldEngageEnemies();

  public RobotInfo closestAlliedArchon();

  public int[] getNumEnemiesAttackingMoveDirs(RobotController rc);

  public MapLocation getBestEnemyToShoot(RobotController rc);

  public int getNumAttackableEnemies(RobotController rc);

  public Direction getBestRetreatDirection(RobotController rc);

  public MapLocation getMostHealthAllyWhoNeedsRepaired();

  public boolean shouldStayNextToTurret(RobotController rc, MapLocation turret);

  public RobotInfo getClosestFriendlyTurret();
}
