package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface RepairSystem {

  public MapLocation repairLeastHealthAlly(RobotController rc) throws GameActionException;

  public MapLocation repairMostHealthAllyThatNeedsRepairing(RobotController rc)
      throws GameActionException;
}
