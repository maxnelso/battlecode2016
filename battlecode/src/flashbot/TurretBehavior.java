package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class TurretBehavior implements Behavior {

  private final Behavior packedBehavior;
  private final Behavior unpackedBehavior;

  public TurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      AttackSystem attackSystem,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      ArchonTracker archonTracker,
      EnemyTurretCache enemyTurretCache,
      EeHanTiming eeHanTiming) {
    packedBehavior = new PackedTurretBehavior(
        navigation,
        radar,
        mapBoundaryCalculator,
        explorationCalculator,
        interestingTargets,
        archonTracker,
        enemyTurretCache,
        eeHanTiming);
    unpackedBehavior = new UnpackedTurretBehavior(radar, attackSystem);
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    RobotInfo[] adjacentRobots = rc.senseNearbyRobots(2);
    boolean adjacentScout = false;
    for (RobotInfo robot : adjacentRobots) {
      if (robot.type.equals(RobotType.SCOUT)) {
        adjacentScout = true;
        break;
      }
    }
    if (!adjacentScout) {
      rc.broadcastSignal(2 * rc.getType().sensorRadiusSquared);
    }
    if (rc.getType() == RobotType.TURRET) {
      unpackedBehavior.behave(rc);
    } else {
      packedBehavior.behave(rc);
    }
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return rc.getType() == RobotType.TURRET
        ? unpackedBehavior.getMessagingBytecodeLimit(rc)
        : packedBehavior.getMessagingBytecodeLimit(rc);
  }
}
