package flashbot;

import battlecode.common.MapLocation;

public class AvoidAttackingUnitsPolicy implements NavigationSafetyPolicy {

  private final Radar radar;
  private final EnemyTurretCache enemyTurretCache;

  public AvoidAttackingUnitsPolicy(Radar radar, EnemyTurretCache enemyTurretCache) {
    this.radar = radar;
    this.enemyTurretCache = enemyTurretCache;
  }

  @Override
  public boolean isSafeToMoveTo(MapLocation loc) {
    return !radar.isAttackableByEnemiesOrZombies(loc)
        && !enemyTurretCache.isInEnemyTurretRange(loc);
  }
}