package flashbot;

import battlecode.common.MapLocation;

public class NoSafetyPolicy implements NavigationSafetyPolicy {

  @Override
  public boolean isSafeToMoveTo(MapLocation loc) {
    return true;
  }
}