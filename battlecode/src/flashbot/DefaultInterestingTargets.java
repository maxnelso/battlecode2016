package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class DefaultInterestingTargets implements InterestingTargets {

  private static final int LOCS_ARRAY_MAX_SIZE = 200;

  private static class InterestingTargetState<T> {
    private final InterestingTarget<T>[] targets;
    private final MapLocationSet addedTargets;
    private final MapLocationSet deletedTargets;
    private int totalTargets;

    @SuppressWarnings("unchecked")
    public InterestingTargetState() {
      targets = new InterestingTarget[LOCS_ARRAY_MAX_SIZE];
      addedTargets = new ArrayMapLocationIntMap();
      deletedTargets = new ArrayMapLocationIntMap();
      totalTargets = 0;
    }

    public void cleanup() {
      for (int i = 0; i < totalTargets; i++) {
        if (targets[i] == null) {
          return;
        }
        if (deletedTargets.contains(targets[i].loc)) {
          if (i == totalTargets - 1) {
            targets[i] = null;
          } else {
            targets[i] = targets[totalTargets - 1];
            targets[totalTargets - 1] = null;
            i--;
          }
          totalTargets--;
        }
      }
    }
  }

  private final EnemyTurretCache enemyTurretCache;
  private final BytecodeProfiler profiler;
  private final InterestingTargetState<Integer> denTargets;
  private final InterestingTargetState<Integer> partsTargets;
  private final InterestingTargetState<RobotType> neutralTargets;

  private int shareIndex;

  public DefaultInterestingTargets(EnemyTurretCache enemyTurretCache, BytecodeProfiler profiler) {
    this.enemyTurretCache = enemyTurretCache;
    this.profiler = profiler;
    this.denTargets = new InterestingTargetState<Integer>();
    this.partsTargets = new InterestingTargetState<Integer>();
    this.neutralTargets = new InterestingTargetState<RobotType>();

    shareIndex = 0;
  }

  @Override
  public void reportDen(MapLocation denLoc) {
    InterestingTarget<Integer> denTarget = new InterestingTarget<Integer>(
        Interest.DEN, denLoc, 0 /* value */);
    reportInterestingTarget(denTarget, denTargets);
  }

  @Override
  public void reportParts(MapLocation partsLoc, int parts) {
    InterestingTarget<Integer> partsTarget = new InterestingTarget<Integer>(
        Interest.PARTS, partsLoc, parts);
    reportInterestingTarget(partsTarget, partsTargets);
  }

  @Override
  public void reportNeutralRobot(MapLocation neutralLoc, RobotType robotType) {
    InterestingTarget<RobotType> neutralTarget = new InterestingTarget<RobotType>(
        Interest.NEUTRAL_ROBOT, neutralLoc, robotType);
    reportInterestingTarget(neutralTarget, neutralTargets);
  }

  private <T> void reportInterestingTarget(
      InterestingTarget<T> newTarget, InterestingTargetState<T> state) {
    if (state.deletedTargets.contains(newTarget.loc)
        || state.addedTargets.contains(newTarget.loc)) {
      return;
    }
    if (add(state.targets, newTarget, state.totalTargets)) {
      state.totalTargets++;
    }
    state.addedTargets.add(newTarget.loc);
  }

  private <T> boolean add(InterestingTarget<T>[] array, InterestingTarget<T> el, int currentSize) {
    if (currentSize >= LOCS_ARRAY_MAX_SIZE) {
      return false;
    }

    for (int i = 0; i < currentSize; i++) {
      if (array[i].loc.equals(el.loc)) {
        return false;
      }
    }

    array[currentSize] = el;
    return true;
  }

  @Override
  public void reportDenDestroyed(MapLocation denLoc) {
    InterestingTarget<Integer> dummyDenTarget = new InterestingTarget<Integer>(
        Interest.DEN, denLoc, 0 /* value */);
    reportInterestingTargetRemoved(denLoc, denTargets, dummyDenTarget);
  }

  @Override
  public void reportPartsRetrieved(MapLocation partsLoc) {
    InterestingTarget<Integer> dummyPartsTarget = new InterestingTarget<Integer>(
        Interest.PARTS, partsLoc, 0);
    reportInterestingTargetRemoved(partsLoc, partsTargets, dummyPartsTarget);
  }

  @Override
  public void reportNeutralRobotActivated(MapLocation neutralLoc) {
    InterestingTarget<RobotType> dummyNeutralTarget = new InterestingTarget<RobotType>(
        Interest.NEUTRAL_ROBOT, neutralLoc, RobotType.ARCHON);
    reportInterestingTargetRemoved(neutralLoc, neutralTargets, dummyNeutralTarget);
  }

  private <T> void reportInterestingTargetRemoved(
      MapLocation removedTarget,
      InterestingTargetState<T> targets,
      InterestingTarget<T> dummyAddTarget) {
    if (!targets.deletedTargets.contains(removedTarget)) {
      targets.deletedTargets.add(removedTarget);
    }
    if (!targets.addedTargets.contains(removedTarget)) {
      targets.addedTargets.add(removedTarget);
      if (add(targets.targets, dummyAddTarget, targets.totalTargets)) {
        targets.totalTargets++;
      }
    }
  }

  @Override
  public void reportRobotInfo(RobotController rc, RobotInfo robot) {
    if ((robot.team == rc.getTeam().opponent() || robot.team == Team.ZOMBIE)
        && robot.type == RobotType.ZOMBIEDEN) {
      reportDen(robot.location);
    }
    if (robot.team == Team.NEUTRAL) {
      reportNeutralRobot(robot.location, robot.type);
    }
  }

  @Override
  public void shareRandomTarget(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    int totalDens = denTargets.totalTargets;
    int totalParts = partsTargets.totalTargets;
    int totalNeutrals = neutralTargets.totalTargets;
    int totalTargets = totalDens + totalParts + totalNeutrals;
    if (totalTargets == 0) {
      return;
    }

    shareIndex = shareIndex % totalTargets;
    if (shareIndex < totalDens) {
      InterestingTarget<Integer> denTarget = denTargets.targets[shareIndex];
      if (denTargets.deletedTargets.contains(denTarget.loc)) {
        messageSender.sendRemoveDenLocation(rc, denTarget.loc);
      } else {
        messageSender.sendDenLocation(rc, denTarget.loc);
      }
    } else if (shareIndex < totalDens + totalParts) {
      InterestingTarget<Integer> partsTarget = partsTargets.targets[shareIndex - totalDens];
      if (partsTargets.deletedTargets.contains(partsTarget.loc)) {
        messageSender.sendRemovePartLocation(rc, partsTarget.loc);
      } else {
        messageSender.sendPartLocation(rc, partsTarget.loc, partsTarget.value);
      }
    } else if (shareIndex < totalDens + totalParts + totalNeutrals) {
      InterestingTarget<RobotType> neutralTarget = neutralTargets.targets[shareIndex - totalDens
          - totalParts];
      if (neutralTargets.deletedTargets.contains(neutralTarget.loc)) {
        messageSender.sendRemoveNeutralRobotLocation(rc, neutralTarget.loc);
      } else {
        messageSender.sendNeutralRobotLocation(rc, neutralTarget.loc, neutralTarget.value);
      }
    }
    shareIndex++;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public InterestingTarget getClosestTarget(
      MapLocation myLoc,
      boolean includeDens,
      boolean includeParts,
      boolean includeNeutralRobots) {
    ClosestInterestingTarget closestTarget = new ClosestInterestingTarget();

    if (includeDens) {
      closestTarget.update(myLoc, denTargets);
    }
    if (includeParts) {
      closestTarget.update(myLoc, partsTargets);
    }
    if (includeNeutralRobots) {
      closestTarget.update(myLoc, neutralTargets);
    }

    return closestTarget.target;
  }

  @SuppressWarnings("rawtypes")
  private class ClosestInterestingTarget {
    int minDist;
    InterestingTarget target;

    ClosestInterestingTarget() {
      minDist = 99999;
      target = null;
    }

    void update(MapLocation myLoc, InterestingTargetState state) {
      for (int i = 0; i < state.totalTargets; i++) {
        MapLocation loc = state.targets[i].loc;
        if (state.deletedTargets.contains(loc) || enemyTurretCache.isInEnemyTurretRange(loc)) {
          continue;
        }
        int dist = myLoc.distanceSquaredTo(loc);
        if (dist < minDist) {
          minDist = dist;
          target = state.targets[i];
        }
      }
    }
  }

  @Override
  public void cleanupRemovedTargets() {
    denTargets.cleanup();
    partsTargets.cleanup();
    neutralTargets.cleanup();
  }

  @Override
  public void showDebugInfo(RobotController rc) {
    int[] denColor = new int[] {
      0, 255, 0
    };
    int[] partsColor = new int[] {
      255, 0, 255
    };
    int[] neutralColor = new int[] {
      100, 100, 100
    };

    MapLocation myLoc = rc.getLocation();
    InterestingTarget<Integer>[] dens = getDens();
    for (int i = 0; i < dens.length; i++) {
      MapLocation den = dens[i].loc;
      rc.setIndicatorLine(myLoc, den, denColor[0], denColor[1], denColor[2]);
    }

    InterestingTarget<Integer>[] parts = getParts();
    for (int i = 0; i < parts.length; i++) {
      MapLocation part = parts[i].loc;
      rc.setIndicatorLine(myLoc, part, partsColor[0], partsColor[1], partsColor[2]);
    }

    InterestingTarget<RobotType>[] neutrals = getNeutralRobots();
    for (int i = 0; i < neutrals.length; i++) {
      MapLocation neutral = neutrals[i].loc;
      rc.setIndicatorLine(myLoc, neutral, neutralColor[0], neutralColor[1], neutralColor[2]);
    }
  }

  private InterestingTarget<Integer>[] getDens() {
    return getActiveTargets(denTargets);
  }

  private InterestingTarget<Integer>[] getParts() {
    return getActiveTargets(partsTargets);
  }

  private InterestingTarget<RobotType>[] getNeutralRobots() {
    return getActiveTargets(neutralTargets);
  }

  @SuppressWarnings("unchecked")
  private <T> InterestingTarget<T>[] getActiveTargets(InterestingTargetState<T> state) {
    int activeTargets = 0;
    for (int i = 0; i < state.totalTargets; i++) {
      if (!state.deletedTargets.contains(state.targets[i].loc)) {
        activeTargets++;
      }
    }

    if (activeTargets == 0) {
      return new InterestingTarget[] {};
    }

    InterestingTarget<T>[] targets = new InterestingTarget[activeTargets];
    int index = 0;
    for (int i = 0; i < state.totalTargets; i++) {
      if (!state.deletedTargets.contains(state.targets[i].loc)) {
        targets[index++] = state.targets[i];
      }
    }
    return targets;
  }
}
