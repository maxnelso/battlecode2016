package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public interface InterestingTargets {

  public enum Interest {
    DEN,
    PARTS,
    NEUTRAL_ROBOT,
  }

  public static class InterestingTarget<T> {
    public final Interest interest;
    public final MapLocation loc;
    public final T value;

    public InterestingTarget(
        Interest interest,
        MapLocation loc,
        T value) {
      this.interest = interest;
      this.loc = loc;
      this.value = value;
    }
  }

  public void reportDen(MapLocation denLoc);

  public void reportParts(MapLocation partsLoc, int parts);

  public void reportNeutralRobot(MapLocation neutralLoc, RobotType robotType);

  public void reportDenDestroyed(MapLocation denLoc);

  public void reportPartsRetrieved(MapLocation partsLoc);

  public void reportNeutralRobotActivated(MapLocation neutralLoc);

  public void reportRobotInfo(RobotController rc, RobotInfo robot);

  public void shareRandomTarget(
      RobotController rc, MessageSender messageSender) throws GameActionException;

  @SuppressWarnings("rawtypes")
  public InterestingTarget getClosestTarget(
      MapLocation myLoc,
      boolean includeDens,
      boolean includeParts,
      boolean includeNeutralRobots);

  public void cleanupRemovedTargets() throws GameActionException;

  public void showDebugInfo(RobotController rc);
}
