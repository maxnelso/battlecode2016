package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class PartScanner {

  private static final int PART_RUBBLE_THRESHOLD = 40000;
  private boolean firstTime;

  public PartScanner() {
    firstTime = true;
  }

  public void scanForParts(
      RobotController rc, InterestingTargets interestingTargets)
          throws GameActionException {
    MapLocation locationsToCheck[];
    if (firstTime) {
      firstTime = false;
      locationsToCheck = MapLocation.getAllMapLocationsWithinRadiusSq(
          rc.getLocation(), rc.getType().sensorRadiusSquared);
    } else {
      locationsToCheck = MapLocationUtils.getFringeSquares(rc.getLocation(),
          rc.getType().sensorRadiusSquared);
    }
    for (int i = 0; i < locationsToCheck.length; i++) {
      int numParts = (int) rc.senseParts(locationsToCheck[i]);
      if (numParts > 1 && rc.senseRubble(locationsToCheck[i]) < PART_RUBBLE_THRESHOLD) {
        interestingTargets.reportParts(locationsToCheck[i], numParts);
      } else {
        // interestingTargets.reportPartsRetrieved(locationsToCheck[i],
        // rc.getRoundNum());
      }
    }
  }
}
