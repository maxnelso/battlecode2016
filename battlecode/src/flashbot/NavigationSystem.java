package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface NavigationSystem {

  public boolean directTo(
      RobotController rc,
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble,
      boolean onlyForward) throws GameActionException;

  public boolean directToWithMaximumEnemyExposure(
      RobotController rc,
      MapLocation loc,
      int maximumEnemyExposure) throws GameActionException;

  public boolean retreatFrom(RobotController rc) throws GameActionException;

  public boolean moveRandomly(RobotController rc) throws GameActionException;

  public boolean bugTo(
      RobotController rc,
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException;
}
