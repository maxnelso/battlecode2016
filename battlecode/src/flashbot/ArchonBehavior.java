package flashbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import flashbot.InterestingTargets.Interest;
import flashbot.InterestingTargets.InterestingTarget;

public class ArchonBehavior implements Behavior {

  private static final int MAX_MAKE_SCOUT_ROUND = 50;
  private static final int MAKE_TURRET_ROUND = 300;
  private static final int SCOUTS_TO_MAKE = 2;
  private static final int MAP_AND_TARGET_SHARING_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 6000;
  private static final int MESSAGING_BYTECODE_LIMIT = 7000;
  private static final int TIMING_ATTACK_MIN_SUPPLY = 75;
  private static final int TIMING_ATTACK_MIN_TURRETS = 10;
  private static final int TIMING_ATTACK_RALLY_TIME = 200;
  private static final int TIMING_ATTACK_DURATION = 100;

  private RobotType nextUnit;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final RepairSystem repairSystem;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final ExplorationCalculator explorationCalculator;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ArchonTracker archonTracker;
  private final PartScanner partScanner;
  private final EnemyTurretCache enemyTurretCache;
  private final EeHanTiming eeHanTiming;
  private final BytecodeProfiler profiler;
  private final PatrolWaypointCalculator patrolWaypointCalculator;

  private MapLocation lastSafeAllyStrengthLoc;
  private int scoutsMade;
  private int scaredTimeRemaining;

  public ArchonBehavior(
      NavigationSystem navigation,
      Radar radar,
      RepairSystem repairSystem,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      ExplorationCalculator explorationCalculator,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker,
      PartScanner partScanner,
      EnemyTurretCache enemyTurretCache,
      EeHanTiming eeHanTiming,
      BytecodeProfiler profiler) {
    this.navigation = navigation;
    this.radar = radar;
    this.repairSystem = repairSystem;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.explorationCalculator = explorationCalculator;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.archonTracker = archonTracker;
    this.partScanner = partScanner;
    this.enemyTurretCache = enemyTurretCache;
    this.eeHanTiming = eeHanTiming;
    this.profiler = profiler;
    this.patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);

    nextUnit = RobotType.SOLDIER;
    lastSafeAllyStrengthLoc = null;
    scaredTimeRemaining = 0;
    scoutsMade = 0;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void behave(RobotController rc) throws GameActionException {
    profiler.split("start of behave");
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    profiler.split("radar update");
    mapBoundaryCalculator.update(rc);
    profiler.split("map boundary update");
    partScanner.scanForParts(rc, interestingTargets);
    profiler.split("part scanner");

    if (rc.getRoundNum() % 200 == 0) {
      interestingTargets.cleanupRemovedTargets();
      profiler.split("cleanup removed targets");
    }

    MapLocation loc = repairSystem.repairMostHealthAllyThatNeedsRepairing(rc);
    profiler.split("repair");
    rc.setIndicatorString(2, "Repairing loc " + loc);

    MapLocation currentTarget = null;

    if (scoutsMade < SCOUTS_TO_MAKE
        && rc.getRoundNum() < MAX_MAKE_SCOUT_ROUND
        && makeUnit(rc, RobotType.SCOUT)) {
      scoutsMade++;
      profiler.split("after making scout");
    } else {
      MapLocation adjacentNeutralRobot = radar.getAdjacentNeutralRobot(rc.getLocation());

      if (rc.isCoreReady() && adjacentNeutralRobot != null) {
        rc.activate(adjacentNeutralRobot);
      } else {
        if (makeUnit(rc, nextUnit)) {
          if (nextUnit == RobotType.TURRET) {
            nextUnit = RobotType.SCOUT;
          } else if (rc.getRoundNum() >= MAKE_TURRET_ROUND) {
            nextUnit = Math.random() >= .5 ? RobotType.TURRET : RobotType.SOLDIER;
          } else {
            nextUnit = RobotType.SOLDIER;
          }
        }
      }

      InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
          rc.getLocation(),
          false /* includeDens */,
          true /* includeParts */,
          true /* includeNeutralRobots */);
      profiler.split("get interesting target");
      currentTarget = interestingTarget == null
          ? explorationCalculator.calculate(rc, patrolWaypointCalculator)
          : interestingTarget.loc;
      // Invalidate the current interesting target if necessary.
      if (interestingTarget != null && rc.canSenseLocation(currentTarget)) {
        if (interestingTarget.interest == Interest.PARTS && rc.senseParts(currentTarget) < 1) {
          interestingTargets.reportPartsRetrieved(currentTarget);
        } else if (interestingTarget.interest == Interest.NEUTRAL_ROBOT) {
          RobotInfo robot = rc.senseRobotAtLocation(currentTarget);
          if (robot == null || robot.team != Team.NEUTRAL) {
            interestingTargets.reportNeutralRobotActivated(currentTarget);
          }
        } else if (interestingTarget.interest == Interest.DEN) {
          RobotInfo robot = rc.senseRobotAtLocation(currentTarget);
          if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
            interestingTargets.reportDenDestroyed(currentTarget);
          }
        }
      }

      profiler.split("after invalidating target");

      rc.setIndicatorString(0, "going to target = " + currentTarget.toString());
      navigation.directTo(
          rc,
          currentTarget,
          true /* avoidAttackers */,
          true /* clearRubble */,
          false /* onlyForward */);
    }
    profiler.split("after moving");

    if ((rc.getRoundNum() - rc.getID()) % MAP_AND_TARGET_SHARING_FREQUENCY == 0) {
      interestingTargets.shareRandomTarget(rc, messageSender);
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }

    archonTracker.updateSelfInformation(rc, currentTarget);
    archonTracker.shareRandomArchonInformation(rc, messageSender);

    if (enemyTurretCache.getNumTurrets() >= TIMING_ATTACK_MIN_TURRETS
        && rc.getRobotCount() >= TIMING_ATTACK_MIN_SUPPLY) {
      eeHanTiming.reportTimingAttack(
          rc,
          rc.getRoundNum() + TIMING_ATTACK_RALLY_TIME,
          rc.getRoundNum() + TIMING_ATTACK_RALLY_TIME + TIMING_ATTACK_DURATION,
          enemyTurretCache.getATurret());
    }
    eeHanTiming.shareTimingAttack(rc, messageSender);

    profiler.split("after sharing");
  }

  private boolean makeUnit(RobotController rc, RobotType type) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }

    if (rc.getRoundNum() > MAKE_TURRET_ROUND && rc.getTeamParts() < 100) {
      return false;
    }

    if (radar.isAttackableByEnemiesOrZombies(rc.getLocation())) {
      return false;
    }

    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 0; i < 8; i++) {
      if (rc.hasBuildRequirements(type) && rc.canBuild(d, type)) {
        rc.build(d, type);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }
}
