package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface AttackSystem {

  public MapLocation attackLeastHealthEnemy(RobotController rc) throws GameActionException;

  public MapLocation attackLeastHealthZombie(RobotController rc) throws GameActionException;

  public MapLocation attackLeastTurnsToKillEnemy(RobotController rc) throws GameActionException;

  public MapLocation attackAnyoneInSensorRange(RobotController rc) throws GameActionException;
}
