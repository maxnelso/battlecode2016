package flashbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class ShootBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;

  private final NavigationSystem navigation;
  private final AttackSystem attackSystem;

  public ShootBehavior(NavigationSystem navigation, AttackSystem attackSystem) {
    this.navigation = navigation;
    this.attackSystem = attackSystem;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    attackSystem.attackLeastHealthEnemy(rc);
    attackSystem.attackLeastHealthZombie(rc);
    navigation.moveRandomly(rc);
  }
}
