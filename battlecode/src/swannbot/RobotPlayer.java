package swannbot;

import java.util.Random;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class RobotPlayer {

  public static void run(RobotController rc) throws GameActionException {
    while (true) {
      if (rc.isCoreReady()) {
        if (rc.getType() == RobotType.ARCHON) {
          archon(rc);
        } else if (rc.getType() == RobotType.TURRET) {
          turret(rc);
        }
        Clock.yield();
      }
    }
  }

  public static void archon(RobotController rc) throws GameActionException {
    if (rc.hasBuildRequirements(RobotType.TURRET)) {
      for (Direction d : Direction.values()) {
        if (rc.canBuild(d, RobotType.TURRET)) {
          rc.build(d, RobotType.TURRET);
          return;
        }
      }
    }

    Direction[] directions = {
      Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
      Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST
    };
    Random random = new Random(rc.getID());
    Direction d = Direction.values()[random.nextInt(8)];
    for (int i = 0; i < directions.length; i++) {
      if (rc.canMove(d)) {
        // rc.move(d);
        return;
      }
    }
  }

  public static void turret(RobotController rc) throws GameActionException {
    if (rc.isWeaponReady()) {
      RobotInfo[] nearbyEnemies = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared);

      double leastHp = 99999;
      MapLocation leastHpLoc = null;
      for (int i = 0; i < nearbyEnemies.length; i++) {
        RobotInfo robot = nearbyEnemies[i];
        if ((robot.team == rc.getTeam().opponent() || robot.team == Team.ZOMBIE)
            && robot.health < leastHp && rc.canAttackLocation(robot.location)) {
          leastHp = robot.health;
          leastHpLoc = robot.location;
        }
      }

      if (leastHpLoc != null && rc.canAttackLocation(leastHpLoc)) {
        rc.attackLocation(leastHpLoc);
      }
    }
  }
}
