package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class NoOpBehavior implements Behavior {

  @Override
  public void behave(
      RobotController rc, BehaviorContext context) throws GameActionException {}
}
