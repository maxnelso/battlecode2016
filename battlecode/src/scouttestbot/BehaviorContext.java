package scouttestbot;

import scouttestbot.MessageReceiver.MessageSummary;

public class BehaviorContext {
  public final MessageSender messageSender;
  public final AttackSystem attackSystem;
  public final NavigationSystem navigationSystem;
  public final MapBoundaryCalculator mapBoundaryCalculator;
  public final MessageSummary messageSummary;
  public final InterestingTargets interestingTargets;
  public final RadarSystem radarSystem;

  public BehaviorContext(
      MessageSender messageSender,
      AttackSystem attackSystem,
      NavigationSystem navigationSystem,
      MapBoundaryCalculator mapBoundaryCalculator,
      MessageSummary messageSummary,
      InterestingTargets interestingTargets,
      RadarSystem radarSystem) {
    this.messageSender = messageSender;
    this.attackSystem = attackSystem;
    this.navigationSystem = navigationSystem;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.messageSummary = messageSummary;
    this.interestingTargets = interestingTargets;
    this.radarSystem = radarSystem;
  }
}
