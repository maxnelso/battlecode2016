package scouttestbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ClosestMapLocationList {

  private int currentClosestIndex;
  private final MapLocation[] locs;
  private boolean[] seenLocations;

  public ClosestMapLocationList() {
    locs = new MapLocation[10];
    seenLocations = new boolean[10000];
  }

  public void addLocation(RobotController rc, MapLocation loc) {
    int locationIndex = MapLocationUtils.mapLocationToInt(loc);
    if (seenLocations[locationIndex]) {
      return;
    }
    seenLocations[locationIndex] = true;
    int furthestDistance = loc.distanceSquaredTo(rc.getLocation());
    int furthestDistanceIndex = -1;
    for (int i = 0; i < locs.length; i++) {
      // An empty space, just fill it in
      if (locs[i] == null) {
        locs[i] = loc;
        return;
      }
      int dist = locs[i].distanceSquaredTo(rc.getLocation());
      if (dist > furthestDistance) {
        furthestDistance = dist;
        furthestDistanceIndex = i;
      }
    }

    if (furthestDistanceIndex != -1) {
      // Replace furthest location with new location
      locs[furthestDistanceIndex] = loc;
    }
  }

  public void computeClosest(RobotController rc) {
    int closestDistance = Integer.MAX_VALUE;
    int closestDistanceIndex = 0;
    for (int i = 1; i < locs.length; i++) {
      if (locs[i] != null) {
        int dist = locs[i].distanceSquaredTo(rc.getLocation());
        if (dist < closestDistance) {
          closestDistance = dist;
          closestDistanceIndex = i;
        }
      }
    }

    currentClosestIndex = closestDistanceIndex;
  }

  public MapLocation getClosest() {
    return locs[currentClosestIndex];
  }

  public void removeClosestLocation() {
    locs[currentClosestIndex] = null;
  }
}