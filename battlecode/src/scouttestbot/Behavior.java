package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface Behavior {
  public void behave(
      RobotController rc, BehaviorContext behaviorContext) throws GameActionException;
}
