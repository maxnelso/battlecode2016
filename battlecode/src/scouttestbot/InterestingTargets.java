package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class InterestingTargets {

  public static class InterestingTarget<T> {
    public final MapLocation loc;
    public final T value;

    public InterestingTarget(MapLocation loc, T value) {
      this.loc = loc;
      this.value = value;
    }
  }

  private static final int LOCS_ARRAY_MAX_SIZE = 200;
  private static final int TARGET_NEARNESS_THRESHOLD = RobotType.ARCHON.sensorRadiusSquared;

  private final InterestingTarget<Integer>[] denLocs;
  private final InterestingTarget<Integer>[] partLocs;
  private final InterestingTarget<RobotType>[] neutralLocs;
  private final int[] enemyArchonIds;
  private final MapLocation[] enemyArchonLocs;

  private int numDens;
  private int numParts;
  private int numNeutrals;
  private int numEnemyArchons;

  private int shareIndex;

  @SuppressWarnings("unchecked")
  public InterestingTargets() {
    this.denLocs = (InterestingTarget<Integer>[]) new InterestingTarget[LOCS_ARRAY_MAX_SIZE];
    this.partLocs = (InterestingTarget<Integer>[]) new InterestingTarget[LOCS_ARRAY_MAX_SIZE];
    this.neutralLocs = (InterestingTarget<RobotType>[]) new InterestingTarget[LOCS_ARRAY_MAX_SIZE];
    this.enemyArchonIds = new int[LOCS_ARRAY_MAX_SIZE];
    this.enemyArchonLocs = new MapLocation[LOCS_ARRAY_MAX_SIZE];

    numDens = 0;
    numParts = 0;
    numNeutrals = 0;
    numEnemyArchons = 0;

    shareIndex = 0;
  }

  public void reportDen(MapLocation denLoc) {
    InterestingTarget<Integer> denValueLoc = new InterestingTarget<Integer>(denLoc, 0 /* value */);
    if (add(denLocs, denValueLoc, numDens)) {
      numDens++;
    }
  }

  public void reportParts(MapLocation partLoc, int parts) {
    InterestingTarget<Integer> partValueLoc = new InterestingTarget<Integer>(partLoc, parts);
    if (add(partLocs, partValueLoc, numParts)) {
      numParts++;
    }
  }

  public void reportNeutralRobot(MapLocation neutralLoc, RobotType robotType) {
    InterestingTarget<RobotType> neutralValueLoc = new InterestingTarget<RobotType>(neutralLoc, robotType);
    if (add(neutralLocs, neutralValueLoc, numNeutrals)) {
      numNeutrals++;
    }
  }

  public void reportEnemyArchon(int enemyArchonId, MapLocation enemyArchonLoc) {
    if (numEnemyArchons >= LOCS_ARRAY_MAX_SIZE) {
      return;
    }
    for (int i = 0; i < numEnemyArchons; i++) {
      if (enemyArchonIds[i] == enemyArchonId) {
        enemyArchonLocs[i] = enemyArchonLoc;
        return;
      }
    }

    enemyArchonIds[numEnemyArchons] = enemyArchonId;
    enemyArchonLocs[numEnemyArchons] = enemyArchonLoc;
    numEnemyArchons++;
  }

  public void shareRandomTarget(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    int totalTargets = numDens + numParts + numNeutrals + numEnemyArchons;
    if (totalTargets == 0) {
      return;
    }

    shareIndex = shareIndex % totalTargets;
    if (shareIndex < numDens) {
      messageSender.sendDenLocation(rc, denLocs[shareIndex].loc);
    } else if (shareIndex < numDens + numParts) {
      InterestingTarget<Integer> partLoc = partLocs[shareIndex - numDens];
      messageSender.sendPartLocation(rc, partLoc.loc, partLoc.value);
    } else if (shareIndex < numDens + numParts + numNeutrals) {
      InterestingTarget<RobotType> neutralLoc = neutralLocs[shareIndex - numDens - numParts];
      messageSender.sendNeutralRobotLocation(rc, neutralLoc.loc, neutralLoc.value);
    } else {
      int index = shareIndex - numDens - numParts - numNeutrals;
      messageSender.sendEnemyArchonLocation(rc, enemyArchonIds[index], enemyArchonLocs[index]);
    }
    shareIndex++;
  }

  private <T> boolean add(InterestingTarget<T>[] array, InterestingTarget<T> el, int currentSize) {
    if (currentSize >= LOCS_ARRAY_MAX_SIZE) {
      return false;
    }

    for (int i = 0; i < currentSize; i++) {
      if (array[i].loc.distanceSquaredTo(el.loc) <= TARGET_NEARNESS_THRESHOLD) {
        return false;
      }
    }

    array[currentSize] = el;
    return true;
  }

  public MapLocation[] getDens() {
    MapLocation[] dens = new MapLocation[numDens];
    for (int i = 0; i < numDens; i++) {
      dens[i] = denLocs[i].loc;
    }
    return dens;
  }

  @SuppressWarnings("unchecked")
  public InterestingTarget<Integer>[] getParts() {
    InterestingTarget<Integer>[] parts = new InterestingTarget[numParts];
    for (int i = 0; i < numParts; i++) {
      parts[i] = partLocs[i];
    }
    return parts;
  }

  @SuppressWarnings("unchecked")
  public InterestingTarget<RobotType>[] getNeutralRobots() {
    InterestingTarget<RobotType>[] neutrals = new InterestingTarget[numNeutrals];
    for (int i = 0; i < numNeutrals; i++) {
      neutrals[i] = neutralLocs[i];
    }
    return neutrals;
  }

  public MapLocation[] getEnemyArchonLocs() {
    MapLocation[] enemyArchons = new MapLocation[numEnemyArchons];
    for (int i = 0; i < numEnemyArchons; i++) {
      enemyArchons[i] = enemyArchonLocs[i];
    }
    return enemyArchons;
  }
}
