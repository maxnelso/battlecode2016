package scouttestbot;

import battlecode.common.GameConstants;
import battlecode.common.MapLocation;

public class MapLocationUtils {
  public static int mapLocationToInt(MapLocation loc) {
    return (loc.x + 16000 + GameConstants.MAP_MAX_HEIGHT * (loc.y + 16000))
        % (GameConstants.MAP_MAX_HEIGHT * GameConstants.MAP_MAX_WIDTH);
  }
}
