package scouttestbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import scouttestbot.InterestingTargets.InterestingTarget;

public class ArchonBehavior implements Behavior {

  ClosestMapLocationList nearbyPartLocations;
  int numScouts;

  public ArchonBehavior() {
    nearbyPartLocations = new ClosestMapLocationList();
    numScouts = 0;
  }

  @Override
  public void behave(RobotController rc, BehaviorContext context) throws GameActionException {
    context.radarSystem.update(rc);
    ArchonStatus archonStatus = context.messageSummary.archonStatus;
    InterestingTargets targets = context.interestingTargets;
    String indicator1 = "Allied archons:";
    for (int i = 0; i < archonStatus.locs.length; i++) {
      indicator1 += " " + archonStatus.locs[i].toString();
      rc.setIndicatorLine(rc.getLocation(), archonStatus.locs[i], 255, 0, 0);
    }
    String indicator2 = "Parts:";
    for (int i = 0; i < targets.getParts().length; i++) {
      InterestingTarget<Integer> parts = targets.getParts()[i];
      indicator2 += " " + parts.loc.toString() + "=" + parts.value;
      rc.setIndicatorLine(rc.getLocation(), parts.loc, 0, 255, 0);
    }
    String indicator3 = "Neutral robots:";
    for (int i = 0; i < targets.getNeutralRobots().length; i++) {
      InterestingTarget<RobotType> neutral = targets.getNeutralRobots()[i];
      indicator3 += " " + neutral.loc.toString() + "=" + neutral.value;
      rc.setIndicatorLine(rc.getLocation(), neutral.loc, 0, 0, 255);
    }

    rc.setIndicatorString(0, indicator1);
    rc.setIndicatorString(1, indicator2);
    rc.setIndicatorString(2, indicator3);

    context.mapBoundaryCalculator.update(rc);
    if ((rc.getRoundNum() - rc.getID()) % Constants.MESSAGE_FREQUENCY == 0) {
      context.messageSender.sendArchonStatus(rc);
      context.mapBoundaryCalculator.shareMapBoundaries(rc, context.messageSender);
    }

    if (!rc.isCoreReady()) {
      return;
    }

    if (rc.hasBuildRequirements(RobotType.SCOUT)
        && rc.canBuild(Direction.EAST, RobotType.SCOUT)
        && numScouts < 2) {
      rc.build(Direction.EAST, RobotType.SCOUT);
      numScouts++;
      return;
    }

    if (context.mapBoundaryCalculator.isMaxXKnown() &&
        context.mapBoundaryCalculator.isMaxYKnown() &&
        context.mapBoundaryCalculator.isMinXKnown() &&
        context.mapBoundaryCalculator.isMinYKnown() &&
        targets.getParts().length + targets.getNeutralRobots().length + targets
            .getDens().length >= 10) {
      rc.disintegrate();
    }
  }
}
