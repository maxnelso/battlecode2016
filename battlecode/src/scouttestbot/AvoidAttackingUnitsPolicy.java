package scouttestbot;

import battlecode.common.MapLocation;

public class AvoidAttackingUnitsPolicy implements NavigationSafetyPolicy {

  RadarSystem radarSystem;

  public AvoidAttackingUnitsPolicy(RadarSystem radarSystem) {
    this.radarSystem = radarSystem;
  }

  @Override
  public boolean isSafeToMoveTo(MapLocation loc) {
    for (int i = 0; i < radarSystem.numEnemies; i++) {
      MapLocation afterMoveLocation = radarSystem.enemyInfos[i].location.add(
          radarSystem.enemyInfos[i].location.directionTo(loc));
      if (radarSystem.enemyInfos[i].type.canAttack() &&
          radarSystem.enemyInfos[i].type.attackRadiusSquared >= loc.distanceSquaredTo(
              afterMoveLocation)) {
        return false;
      }
    }

    for (int i = 0; i < radarSystem.numZombies; i++) {
      MapLocation afterMoveLocation = radarSystem.zombieInfos[i].location.add(
          radarSystem.zombieInfos[i].location
              .directionTo(loc));
      if (radarSystem.zombieInfos[i].type.canAttack() &&
          radarSystem.zombieInfos[i].type.attackRadiusSquared >= loc.distanceSquaredTo(
              afterMoveLocation)) {
        return false;
      }
    }
    return true;
  }
}