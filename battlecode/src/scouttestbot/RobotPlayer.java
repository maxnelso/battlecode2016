package scouttestbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import scouttestbot.MessageReceiver.MessageSummary;

public class RobotPlayer {
  public static void run(RobotController rc) {
    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior();
        break;
      case SOLDIER:
      case VIPER:
      case GUARD:
      case TURRET:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
        behavior = new ShootBehavior();
        break;
      case SCOUT:
        behavior = new ScoutBehavior(rc);
        break;
      case TTM:
      case ZOMBIEDEN:
      default:
        behavior = new NoOpBehavior();
        break;
    }

    Messenger messenger = new Messenger();
    AttackSystem attackSystem = new AttackSystem();
    NavigationSystem navigationSystem = new NavigationSystem();
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator();
    InterestingTargets interestingTargets = new InterestingTargets();
    RadarSystem radarSystem = new RadarSystem();

    while (true) {
      try {
        MessageSummary messageSummary = messenger.receiveMessages(
            rc, interestingTargets, mapBoundaryCalculator);
        BehaviorContext context = new BehaviorContext(
            messenger /* messageSender */,
            attackSystem,
            navigationSystem,
            mapBoundaryCalculator,
            messageSummary,
            interestingTargets,
            radarSystem);
        behavior.behave(rc, context);
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
