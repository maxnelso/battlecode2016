package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class AttackSystem {

  public void attackLeastHealthEnemy(RobotController rc) throws GameActionException {
    if (!rc.isWeaponReady()) {
      return;
    }

    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    double leastHealth = 99999;
    MapLocation leastHealthLoc = null;

    for (int i = 0; i < nearbyRobots.length; i++) {
      RobotInfo robot = nearbyRobots[i];
      if ((robot.team == rc.getTeam().opponent() || robot.team == Team.ZOMBIE)
          && robot.health < leastHealth
          && rc.canAttackLocation(robot.location)) {
        leastHealth = robot.health;
        leastHealthLoc = robot.location;
      }
    }

    if (leastHealthLoc != null && rc.canAttackLocation(leastHealthLoc)) {
      rc.attackLocation(leastHealthLoc);
    }
  }
}
