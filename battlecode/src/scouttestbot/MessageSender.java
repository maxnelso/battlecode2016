package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public interface MessageSender {
  public void sendArchonStatus(RobotController rc) throws GameActionException;

  public void sendDenLocation(RobotController rc, MapLocation loc) throws GameActionException;

  public void sendPartLocation(
      RobotController rc, MapLocation loc, int numParts) throws GameActionException;

  public void sendNeutralRobotLocation(
      RobotController rc, MapLocation loc, RobotType robotType) throws GameActionException;

  public void sendEnemyArchonLocation(
      RobotController rc, int archonId, MapLocation loc) throws GameActionException;

  public void sendMinXBoundary(RobotController rc, int minX) throws GameActionException;

  public void sendMinYBoundary(RobotController rc, int minY) throws GameActionException;

  public void sendMinLocBoundary(
      RobotController rc, int minX, int minY) throws GameActionException;

  public void sendMaxXBoundary(RobotController rc, int maxX) throws GameActionException;

  public void sendMaxYBoundary(RobotController rc, int maxY) throws GameActionException;

  public void sendMaxLocBoundary(
      RobotController rc, int maxX, int maxY) throws GameActionException;
}
