package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class ShootBehavior implements Behavior {

  @Override
  public void behave(RobotController rc, BehaviorContext context) throws GameActionException {
    context.attackSystem.attackLeastHealthEnemy(rc);
  }
}
