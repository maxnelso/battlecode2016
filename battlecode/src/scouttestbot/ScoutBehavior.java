package scouttestbot;

import java.util.Random;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class ScoutBehavior implements Behavior {

  private final float PART_RUBBLE_THRESHOLD = 500;

  private enum ScoutState {
    DRAGGING,
    EXPLORING,
    RUN
  }

  private Direction exploringDirection;
  private boolean[] seenLocations;
  private ScoutState state;
  private Random random;

  public ScoutBehavior(RobotController rc) {
    random = new Random(rc.getID());
    exploringDirection = Constants.movableDirections[random.nextInt(
        Constants.movableDirections.length)];
    state = ScoutState.EXPLORING; // TODO Change later to be from archon or
                                  // something
    seenLocations = new boolean[10000];
  }

  @Override
  public void behave(RobotController rc, BehaviorContext context) throws GameActionException {
    context.radarSystem.update(rc);
    context.mapBoundaryCalculator.update(rc);
    if ((rc.getRoundNum() - rc.getID()) % Constants.MESSAGE_FREQUENCY == 0) {
      context.mapBoundaryCalculator.shareMapBoundaries(rc, context.messageSender);
    }

    if (rc.isCoreReady()) {
      determineState(rc, context.radarSystem);
      switch (state) {
        case DRAGGING:
          drag(rc);
          break;
        case EXPLORING:
          explore(
              rc,
              context.interestingTargets,
              context.navigationSystem,
              context.radarSystem,
              context.mapBoundaryCalculator);
          break;
        case RUN:
          run(rc, context.navigationSystem, context.radarSystem);
          break;
      }
    }

    context.interestingTargets.shareRandomTarget(rc, context.messageSender);
  }

  private void determineState(RobotController rc, RadarSystem radarSystem) {
    state = isLocationSafe(rc.getLocation(), radarSystem) ? ScoutState.EXPLORING : ScoutState.RUN;
  }

  private void drag(RobotController rc) {
    return;
  }

  private void explore(RobotController rc,
      InterestingTargets interestingTargets,
      NavigationSystem navigationSystem,
      RadarSystem radarSystem,
      MapBoundaryCalculator mapBoundaryCalculator) throws GameActionException {
    // TODO Broadcast around you which direction you are going, doesn't have to
    // be too far

    // Sense parts and broadcast them
    MapLocation[] nearbyLocations = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(),
        rc.getType().sensorRadiusSquared);
    for (int i = 0; i < nearbyLocations.length; i++) {
      int locationIndex = MapLocationUtils.mapLocationToInt(nearbyLocations[i]);
      if (seenLocations[locationIndex]) {
        continue;
      }
      seenLocations[locationIndex] = true;
      int numParts = (int) rc.senseParts(nearbyLocations[i]);
      if (numParts > 0 &&
          rc.senseRubble(nearbyLocations[i]) < PART_RUBBLE_THRESHOLD) {
        interestingTargets.reportParts(nearbyLocations[i], numParts);
      }
      RobotInfo robotInfo = rc.senseRobotAtLocation(nearbyLocations[i]);
      if (robotInfo != null && robotInfo.team == Team.NEUTRAL) {
        interestingTargets.reportNeutralRobot(robotInfo.location, robotInfo.type);
      }
    }

    rc.setIndicatorString(0, "Exploring in direction " + exploringDirection);

    boolean moved = navigationSystem.directTowards(rc,
        rc.getLocation().add(exploringDirection),
        new AvoidAttackingUnitsPolicy(radarSystem));

    setExploringDirection(rc, moved);
  }

  private void setExploringDirection(RobotController rc, boolean moved) throws GameActionException {
    Direction testDirection1 = null;
    Direction testDirection2 = null;
    boolean testDirection1Offmap = false;
    boolean testDirection2Offmap = false;
    if (exploringDirection.isDiagonal()) {
      testDirection1 = exploringDirection.rotateLeft();
      testDirection2 = exploringDirection.rotateRight();
      testDirection1Offmap = !rc.onTheMap(rc.getLocation().add(testDirection1, 7));
      testDirection2Offmap = !rc.onTheMap(rc.getLocation().add(testDirection2, 7));
    } else {
      testDirection1 = exploringDirection;
      testDirection1Offmap = !rc.onTheMap(rc.getLocation().add(testDirection1, 7));
    }

    // Still exploring just fine
    if (!testDirection1Offmap && !testDirection2Offmap) {
      return;
    }

    if (testDirection1Offmap && testDirection2Offmap) { // Corners
      changeDirectionCorner(testDirection1, testDirection2);
    } else { // Sides
      changeDirectionSide(rc, testDirection1);
    }
  }

  private void changeDirectionCorner(Direction testDirection1, Direction testDirection2) {
    boolean offscreenTop = testDirection1 == Direction.NORTH || testDirection2 == Direction.NORTH;
    boolean offscreenBottom = testDirection1 == Direction.SOUTH
        || testDirection2 == Direction.SOUTH;
    boolean offscreenLeft = testDirection1 == Direction.WEST || testDirection2 == Direction.WEST;
    boolean offscreenRight = testDirection1 == Direction.EAST || testDirection2 == Direction.EAST;

    Direction dir;
    if (offscreenLeft && offscreenTop) {
      dir = Direction.EAST;
    } else if (offscreenRight && offscreenTop) {
      dir = Direction.SOUTH;
    } else if (offscreenRight && offscreenBottom) {
      dir = Direction.WEST;
    } else {
      dir = Direction.NORTH;
    }

    int index = 0;
    Direction[] possibleNewExploringDirections = new Direction[3];
    for (int i = 0; i < 4; i++) {
      // Skip the direction we were already going
      if (!dir.equals(exploringDirection.opposite())) {
        possibleNewExploringDirections[index++] = dir;
      }
      dir = dir.rotateRight();
    }

    // Pick one randomly
    exploringDirection = possibleNewExploringDirections[random.nextInt(3)];
  }

  private void changeDirectionSide(RobotController rc, Direction testDirection1)
      throws GameActionException {
    Direction dir = testDirection1.rotateRight().rotateRight();

    boolean offscreenBottom = !rc.onTheMap(rc.getLocation().add(Direction.SOUTH, 7));
    boolean offscreenLeft = !rc.onTheMap(rc.getLocation().add(Direction.WEST, 7));
    boolean offscreenTop = !rc.onTheMap(rc.getLocation().add(Direction.NORTH, 7));
    boolean offscreenRight = !rc.onTheMap(rc.getLocation().add(Direction.EAST, 7));

    int index = 0;
    Direction[] possibleNewExploringDirections = new Direction[4];
    for (int i = 0; i < 5; i++) {
      // Skip the direction we were already going and also directions that will
      // take us offscreen
      if (!dir.equals(exploringDirection.opposite()) &&
          !(dir.equals(Direction.SOUTH) && offscreenBottom) &&
          !(dir.equals(Direction.WEST) && offscreenLeft) &&
          !(dir.equals(Direction.NORTH) && offscreenTop) &&
          !(dir.equals(Direction.EAST) && offscreenRight)) {
        possibleNewExploringDirections[index++] = dir;
      }
      dir = dir.rotateRight();
    }

    // Pick one randomly
    exploringDirection = possibleNewExploringDirections[random.nextInt(index)];
  }

  private boolean isLocationSafe(MapLocation loc, RadarSystem radarSystem) {
    for (int i = 0; i < radarSystem.numEnemies; i++) {
      MapLocation afterMoveLocation = radarSystem.enemyInfos[i].location.add(
          radarSystem.enemyInfos[i].location.directionTo(loc));
      if (radarSystem.enemyInfos[i].type.canAttack() &&
          radarSystem.enemyInfos[i].type.attackRadiusSquared >= loc.distanceSquaredTo(
              afterMoveLocation)) {
        return false;
      }
    }

    for (int i = 0; i < radarSystem.numZombies; i++) {
      MapLocation afterMoveLocation = radarSystem.zombieInfos[i].location.add(
          radarSystem.zombieInfos[i].location.directionTo(loc));
      if (radarSystem.zombieInfos[i].type.canAttack() &&
          radarSystem.zombieInfos[i].type.attackRadiusSquared >= loc.distanceSquaredTo(
              afterMoveLocation)) {
        return false;
      }
    }

    return true;
  }

  private void run(RobotController rc, NavigationSystem navigationSystem, RadarSystem radarSystem)
      throws GameActionException {
    // Get running direction, just run away from the average location for now
    MapLocation totalLocation = new MapLocation(0, 0);
    for (int i = 0; i < radarSystem.numEnemies; i++) {
      if (radarSystem.enemyInfos[i].type.canAttack()) {
        totalLocation.add(radarSystem.enemyInfos[i].location.x,
            radarSystem.enemyInfos[i].location.y);
      }
    }
    for (int i = 0; i < radarSystem.numZombies; i++) {
      totalLocation.add(radarSystem.zombieInfos[i].location.x,
          radarSystem.zombieInfos[i].location.y);
    }

    int averageX = totalLocation.x / (radarSystem.numEnemies + radarSystem.numZombies);
    int averageY = totalLocation.y / (radarSystem.numEnemies + radarSystem.numZombies);
    MapLocation averageLocation = new MapLocation(averageX, averageY);

    Direction newMoveDirection = rc.getLocation().directionTo(averageLocation).opposite();
    rc.setIndicatorString(0, "Running in direction " + newMoveDirection);
    navigationSystem.directTowards(rc,
        rc.getLocation().add(newMoveDirection),
        new NoSafetyPolicy());

    exploringDirection = Constants.movableDirections[random.nextInt(
        Constants.movableDirections.length)];
  }
}