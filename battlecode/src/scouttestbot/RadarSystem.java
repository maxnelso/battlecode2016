package scouttestbot;

import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class RadarSystem {

  public final static int MAX_ROBOTS = 4096;

  // TODO Types of enemies, directions of enemies, adjacent allies, dens?
  // Whatever we might want.

  // Allies
  public final RobotInfo[] allyInfos;
  public int numAllies;
  private int lastAllyUpdateRound;

  // Enemies
  public final RobotInfo[] enemyInfos;
  public int numEnemies;
  private int lastEnemyUpdateRound;

  public int enemyCenterX;
  public int enemyCenterY;

  // Zombies
  public final RobotInfo[] zombieInfos;
  public int numZombies;
  private int lastZombieUpdateRound;

  public RadarSystem() {
    enemyInfos = new RobotInfo[MAX_ROBOTS];
    allyInfos = new RobotInfo[MAX_ROBOTS];
    zombieInfos = new RobotInfo[MAX_ROBOTS];
  }

  /*
   * Update all info possible
   */
  public void update(RobotController rc) {
    if (lastAllyUpdateRound < rc.getRoundNum() ||
        lastEnemyUpdateRound < rc.getRoundNum() ||
        lastZombieUpdateRound < rc.getRoundNum()) {
      resetAllStats();
      RobotInfo[] nearbyRobots = rc.senseNearbyRobots();

      for (int i = 0; i < nearbyRobots.length; i++) {
        if (rc.getTeam() == nearbyRobots[i].team) {
          addAlly(nearbyRobots[i]);
        } else if (rc.getTeam().opponent() == nearbyRobots[i].team) {
          addEnemy(nearbyRobots[i]);
        } else if (nearbyRobots[i].team == Team.ZOMBIE) {
          addZombie(nearbyRobots[i]);
        }
      }
      computeCenterOfEnemies();

      lastAllyUpdateRound = rc.getRoundNum();
      lastEnemyUpdateRound = rc.getRoundNum();
      lastZombieUpdateRound = rc.getRoundNum();
    }
  }

  /*
   * Update only ally info, call if you only care about allies
   */
  public void updateAllies(RobotController rc) {
    if (lastAllyUpdateRound < rc.getRoundNum()) {
      resetAllyStats();
      RobotInfo[] nearbyRobots = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared,
          rc.getTeam());
      for (int i = 0; i < nearbyRobots.length; i++) {
        addAlly(nearbyRobots[i]);
      }
      lastAllyUpdateRound = rc.getRoundNum();
    }
  }

  /*
   * Update only enemy info, call if you only care about enemies
   */
  public void updateEnemies(RobotController rc) {
    if (lastEnemyUpdateRound < rc.getRoundNum()) {
      resetEnemyStats();
      RobotInfo[] nearbyRobots = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared,
          rc.getTeam().opponent());
      for (int i = 0; i < nearbyRobots.length; i++) {
        addEnemy(nearbyRobots[i]);
      }
      lastEnemyUpdateRound = rc.getRoundNum();
      computeCenterOfEnemies();
    }
  }

  /*
   * Update only zombie info, call if you only care about zombie
   */
  public void updateZombies(RobotController rc) {
    if (lastZombieUpdateRound < rc.getRoundNum()) {
      resetZombieStats();
      RobotInfo[] nearbyRobots = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared,
          Team.ZOMBIE);
      for (int i = 0; i < nearbyRobots.length; i++) {
        addZombie(nearbyRobots[i]);
      }
      lastZombieUpdateRound = rc.getRoundNum();
    }
  }

  private void addAlly(RobotInfo robotInfo) {
    allyInfos[numAllies++] = robotInfo;
  }

  private void addEnemy(RobotInfo robotInfo) {
    enemyInfos[numEnemies++] = robotInfo;
  }

  private void addZombie(RobotInfo robotInfo) {
    zombieInfos[numZombies++] = robotInfo;
  }

  private void resetAllyStats() {
    numAllies = 0;
  }

  private void resetEnemyStats() {
    numEnemies = 0;
    enemyCenterX = 0;
    enemyCenterY = 0;
  }

  private void resetZombieStats() {
    numZombies = 0;
  }

  private void resetAllStats() {
    resetAllyStats();
    resetEnemyStats();
    resetZombieStats();
  }

  private void computeCenterOfEnemies() {
    if (numEnemies > 0) {
      enemyCenterX /= numEnemies;
      enemyCenterY /= numEnemies;
    }
  }
}