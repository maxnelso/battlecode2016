package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public class Messenger implements MessageSender, MessageReceiver {

  private static final int MAX_OP_CODE_BITS = 4;
  private static final int OP_CODE_SHIFT = (30 - MAX_OP_CODE_BITS);
  private static final int MAX_SMALL_DATA = 1 << (OP_CODE_SHIFT - 1);
  private static final int UNKNOWN_BOUNDARY_COORDINATE = 16005;

  // NOTE: The maximum op code is (2 ** MAX_OP_CODE_BITS) - 1.
  private enum MessageType {
    ARCHON_STATUS(0),
    DEN_LOCATION(1),
    PART_LOCATION(2),
    NEUTRAL_LOCATION(3),
    ENEMY_ARCHON_LOCATION(4),
    MIN_BOUNDARY(5),
    MAX_BOUNDARY(6);

    int opCode;

    private MessageType(int opCode) {
      this.opCode = opCode;
    }

    static MessageType fromOpCode(int opCode) {
      for (MessageType t : MessageType.values()) {
        if (t.opCode == opCode) {
          return t;
        }
      }
      return null;
    }
  }

  @Override
  public void sendArchonStatus(RobotController rc) throws GameActionException {
    broadcast(
        rc,
        MessageType.ARCHON_STATUS,
        0 /* smallData */,
        0 /* bigData */);
  }

  @Override
  public void sendDenLocation(RobotController rc, MapLocation loc) throws GameActionException {
    broadcast(
        rc,
        MessageType.DEN_LOCATION,
        0 /* smallData */,
        mapLocationToMessageInt(loc));
  }

  @Override
  public void sendPartLocation(
      RobotController rc, MapLocation loc, int numParts) throws GameActionException {
    broadcast(
        rc,
        MessageType.PART_LOCATION,
        0 /* smallData */,
        mapLocationToMessageInt(loc) /* bigData */);
  }

  @Override
  public void sendNeutralRobotLocation(
      RobotController rc, MapLocation loc, RobotType robotType) throws GameActionException {
    broadcast(
        rc,
        MessageType.NEUTRAL_LOCATION,
        robotType.ordinal() /* smallData */,
        mapLocationToMessageInt(loc) /* bigData */);
  }

  @Override
  public void sendEnemyArchonLocation(
      RobotController rc, int archonId, MapLocation loc) throws GameActionException {
    broadcast(
        rc,
        MessageType.ENEMY_ARCHON_LOCATION,
        archonId /* smallData */,
        mapLocationToMessageInt(loc) /* bigData */);
  }

  @Override
  public void sendMinXBoundary(
      RobotController rc, int minX) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, minX, UNKNOWN_BOUNDARY_COORDINATE);
  }

  @Override
  public void sendMinYBoundary(
      RobotController rc, int minY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, UNKNOWN_BOUNDARY_COORDINATE, minY);
  }

  @Override
  public void sendMinLocBoundary(
      RobotController rc, int minX, int minY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MIN_BOUNDARY, minX, minY);
  }

  @Override
  public void sendMaxXBoundary(
      RobotController rc, int maxX) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, maxX, UNKNOWN_BOUNDARY_COORDINATE);
  }

  @Override
  public void sendMaxYBoundary(
      RobotController rc, int maxY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, UNKNOWN_BOUNDARY_COORDINATE, maxY);
  }

  @Override
  public void sendMaxLocBoundary(
      RobotController rc, int maxX, int maxY) throws GameActionException {
    sendLocBoundary(rc, MessageType.MAX_BOUNDARY, maxX, maxY);
  }

  private void sendLocBoundary(
      RobotController rc, MessageType messageType, int x, int y) throws GameActionException {
    broadcast(
        rc,
        messageType,
        0 /* smallData */,
        mapLocationToMessageInt(new MapLocation(x, y)) /* bigData */);
  }

  @Override
  public MessageSummary receiveMessages(
      RobotController rc,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator) throws GameActionException {
    ArchonStatus.Builder archonStatusBuilder = new ArchonStatus.Builder();

    Signal s;
    while ((s = rc.readSignal()) != null) {
      if (s.getTeam() != rc.getTeam()) {
        continue;
      }
      int[] data = s.getMessage();
      if (data == null || data.length != 2) {
        continue;
      }
      MessageType t = MessageType.fromOpCode(data[0] >> OP_CODE_SHIFT);
      if (t == null) {
        continue;
      }
      int smallData = data[0] % (1 << OP_CODE_SHIFT);
      int bigData = data[1];
      switch (t) {
        case ARCHON_STATUS:
          archonStatusBuilder.addArchon(s.getRobotID(), s.getLocation());
          break;
        case DEN_LOCATION:
          interestingTargets.reportDen(intToMapLocation(bigData) /* denLoc */);
          break;
        case PART_LOCATION:
          interestingTargets.reportParts(
              intToMapLocation(bigData) /* partsLoc */,
              smallData /* numParts */);
          break;
        case NEUTRAL_LOCATION:
          interestingTargets.reportNeutralRobot(
              intToMapLocation(bigData) /* neutralLoc */,
              RobotType.values()[smallData] /* robotType */);
          break;
        case ENEMY_ARCHON_LOCATION:
          interestingTargets.reportEnemyArchon(
              smallData /* enemyArchonId */,
              intToMapLocation(bigData) /* enemyArchonLoc */);
          break;
        case MIN_BOUNDARY:
          MapLocation minLoc = intToMapLocation(bigData);
          if (minLoc.x != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMinX(minLoc.x);
          }
          if (minLoc.y != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMinY(minLoc.y);
          }
          break;
        case MAX_BOUNDARY:
          MapLocation maxLoc = intToMapLocation(bigData);
          if (maxLoc.x != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMaxX(maxLoc.x);
          }
          if (maxLoc.y != UNKNOWN_BOUNDARY_COORDINATE) {
            mapBoundaryCalculator.reportMaxY(maxLoc.y);
          }
          break;
        default:
          break;
      }
    }

    return new MessageSummary(archonStatusBuilder.build());
  }

  private void broadcast(
      RobotController rc, MessageType type, int smallData, int bigData) throws GameActionException {
    // Coerce the small data into the available bits.
    smallData = smallData > MAX_SMALL_DATA ? MAX_SMALL_DATA : smallData;
    int data1 = (type.opCode << OP_CODE_SHIFT) + smallData;
    int data2 = bigData;
    rc.broadcastMessageSignal(data1, data2, 2 * rc.getType().sensorRadiusSquared);
  }

  private int mapLocationToMessageInt(MapLocation loc) {
    return ((loc.x + 16000) << 15) + (loc.y + 16000);
  }

  private MapLocation intToMapLocation(int message) {
    return new MapLocation((message >> 15) - 16000, (message % 32768) - 16000);
  }
}
