package scouttestbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface MessageReceiver {

  public class MessageSummary {

    public ArchonStatus archonStatus;

    public MessageSummary(ArchonStatus archonStatus) {
      this.archonStatus = archonStatus;
    }
  }

  public MessageSummary receiveMessages(
      RobotController rc,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator) throws GameActionException;
}