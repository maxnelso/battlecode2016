package dragoonbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class RadarAttackRepairSystem implements AttackSystem, RepairSystem {

  private final Radar radar;

  public RadarAttackRepairSystem(Radar radar) {
    this.radar = radar;
  }

  @Override
  public MapLocation attackLeastHealthEnemy(RobotController rc) throws GameActionException {
    return attackAttackersThenNonAttackers(
        rc,
        radar.getLeastHealthEnemyAttackerInAttackRange(),
        radar.getLeastHealthEnemyInAttackRange());
  }

  @Override
  public MapLocation attackLeastHealthZombie(RobotController rc) throws GameActionException {
    return attackAttackersThenNonAttackers(
        rc,
        radar.getLeastHealthZombieAttackerInAttackRange(),
        radar.getLeastHealthZombieInAttackRange());
  }

  private MapLocation attackAttackersThenNonAttackers(
      RobotController rc, MapLocation attacker, MapLocation nonAttacker)
          throws GameActionException {
    MapLocation target = attacker == null ? nonAttacker : attacker;
    if (rc.isWeaponReady() && target != null && rc.canAttackLocation(target)) {
      rc.attackLocation(target);
    }

    return target;
  }

  @Override
  public MapLocation repairLeastHealthAlly(RobotController rc) throws GameActionException {
    MapLocation ally = radar.getLeastHealthAllyInRepairRange();

    // TODO: need better filtering system to remove archons from healing targets
    if (ally != null && rc.senseRobotAtLocation(ally).type != RobotType.ARCHON &&
        rc.getLocation().distanceSquaredTo(ally) < RobotType.ARCHON.attackRadiusSquared) {
      rc.repair(ally);
      rc.setIndicatorLine(rc.getLocation(), ally, 0, 255, 100);
    }

    return ally;
  }
}
