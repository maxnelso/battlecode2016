package dragoonbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class TurretBehavior implements Behavior {

  private final Behavior packedBehavior;
  private final Behavior unpackedBehavior;

  private Behavior currentBehavior;

  public TurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      AttackSystem attackSystem,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator) {
    packedBehavior = new PackedTurretBehavior(
        navigation, radar, mapBoundaryCalculator, explorationCalculator, interestingTargets);
    unpackedBehavior = new UnpackedTurretBehavior(radar, attackSystem);
    currentBehavior = unpackedBehavior;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (rc.getType() == RobotType.TURRET) {
      unpackedBehavior.behave(rc);
    } else {
      packedBehavior.behave(rc);
    }
    currentBehavior.behave(rc);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return rc.getType() == RobotType.TURRET
        ? unpackedBehavior.getMessagingBytecodeLimit(rc)
        : packedBehavior.getMessagingBytecodeLimit(rc);
  }
}
