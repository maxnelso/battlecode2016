package dragoonbot;

import battlecode.common.MapLocation;

public interface NavigationSafetyPolicy {
  public boolean isSafeToMoveTo(MapLocation loc);
}