package dragoonbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface Radar {

  public void addReportedRobot(RobotController rc, RobotInfo robotInfo);

  public void update(RobotController rc, int bytecodeLimit);

  public MapLocation getClosestAlly();

  public MapLocation getClosestEnemy();

  public MapLocation getLeastHealthAllyInRepairRange();

  public MapLocation getClosestEnemyOrZombie();

  public MapLocation getClosestZombie();

  public MapLocation getLeastHealthEnemyAttackerInAttackRange();

  public MapLocation getLeastHealthEnemyInAttackRange();

  public MapLocation getLeastHealthZombieAttackerInAttackRange();

  public MapLocation getLeastHealthZombieInAttackRange();

  public MapLocation getAdjacentNeutralRobot(MapLocation loc);

  public boolean isAttackableByZombies(MapLocation loc);

  public boolean isAttackableByEnemiesOrZombies(MapLocation loc);

  public boolean shouldDragZombies();

  public int getEnemyStrength();

  public int getAllyStrength();

  public RobotInfo getClosestUnableToMoveEnemyOrZombie();

  public boolean shouldEngageEnemies();

  public RobotInfo closestAlliedArchon();
}
