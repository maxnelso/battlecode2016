package dragoonbot;

import battlecode.common.MapLocation;

public class MapLocationArray {
  public static class Builder {
    private final MapLocation[] locs;
    private int numLocs;

    public Builder() {
      locs = new MapLocation[100];
      numLocs = 0;
    }

    public void addLocation(MapLocation loc) {
      locs[numLocs] = loc;
      numLocs++;
    }

    public MapLocationArray build() {
      return new MapLocationArray(this);
    }
  }

  public final MapLocation[] locs;
  public final int numLocs;

  private MapLocationArray(Builder builder) {
    numLocs = builder.numLocs;
    locs = new MapLocation[numLocs];
    for (int i = 0; i < numLocs; i++) {
      locs[i] = builder.locs[i];
    }
  }
}