package dragoonbot;

import battlecode.common.MapLocation;

public class AvoidAttackingUnitsPolicy implements NavigationSafetyPolicy {

  private Radar radar;

  public AvoidAttackingUnitsPolicy(Radar radar) {
    this.radar = radar;
  }

  @Override
  public boolean isSafeToMoveTo(MapLocation loc) {
    return !radar.isAttackableByEnemiesOrZombies(loc);
  }
}