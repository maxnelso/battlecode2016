package dragoonbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public interface RepairSystem {

  public MapLocation repairLeastHealthAlly(RobotController rc) throws GameActionException;
}
