package dragoonbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import dragoonbot.InterestingTargets.InterestingTarget;

public class PackedTurretBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 5000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final InterestingTargets interestingTargets;

  public PackedTurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      InterestingTargets interestingTargets) {
    this.navigation = navigation;
    this.radar = radar;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.interestingTargets = interestingTargets;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void behave(RobotController rc) throws GameActionException {
    mapBoundaryCalculator.update(rc);
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);

    if (radar.getClosestEnemyOrZombie() != null) {
      rc.unpack();
      return;
    }

    InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
        rc.getLocation(),
        true /* includeDens */,
        false /* includeParts */,
        false /* includeNeutralRobots */);
    if (interestingTarget != null && rc.canSenseLocation(interestingTarget.loc)) {
      RobotInfo robot = rc.senseRobotAtLocation(interestingTarget.loc);
      if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
        interestingTargets.reportDenDestroyed(interestingTarget.loc);
        interestingTarget = null;
      }
    }

    MapLocation target = interestingTarget == null
        ? null
        : interestingTarget.loc;
    if (target == null) {
      target = explorationCalculator.calculate(rc);
    }

    rc.setIndicatorString(0, "EXPLORING TO " + target);
    navigation.bugTo(rc, target, true /* avoidAttackers */, false /* clearRubble */);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }
}
