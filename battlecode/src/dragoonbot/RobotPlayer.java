package dragoonbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {
  public static void run(RobotController rc) {
    InterestingTargets interestingTargets = new InterestingTargets();
    ArchonTracker archonTracker = new ArchonTracker();
    Radar radar = new DefaultRadar(interestingTargets, archonTracker);
    RadarAttackRepairSystem attackRepairSystem = new RadarAttackRepairSystem(radar);
    DuckNavigationSystem navigation = new DuckNavigationSystem(radar);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator();
    ExplorationCalculator explorationCalculator = new ExplorationCalculator(mapBoundaryCalculator);
    PartScanner partScanner = new PartScanner();
    ZombieDragger zombieDragger = new ZombieDragger(radar, navigation, archonTracker);
    Messenger messenger = new Messenger(interestingTargets, archonTracker, radar,
        mapBoundaryCalculator);

    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior(
            navigation,
            radar,
            attackRepairSystem /* repairSystem */,
            messenger /* messageSender */,
            interestingTargets,
            explorationCalculator,
            mapBoundaryCalculator,
            archonTracker,
            partScanner);
        break;
      case SOLDIER:
        behavior = new SoldierBehavior(
            navigation,
            attackRepairSystem /* attackSystem */,
            mapBoundaryCalculator,
            explorationCalculator,
            radar,
            interestingTargets,
            archonTracker);
        break;
      case VIPER:
      case GUARD:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
        behavior = new ShootBehavior(navigation, attackRepairSystem /* attackSystem */);
        break;
      case SCOUT:
        behavior = new ScoutBehavior(
            navigation,
            radar,
            messenger /* messageSender */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator,
            archonTracker,
            zombieDragger,
            partScanner);
        break;
      case TURRET:
      case TTM:
        behavior = new TurretBehavior(
            navigation,
            radar,
            attackRepairSystem /* attackSystem */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator);
        break;
      case ZOMBIEDEN:
      default:
        behavior = new NoOpBehavior();
        break;
    }

    while (true) {
      try {
        int currentRound = rc.getRoundNum();
        messenger.receiveMessages(
            rc, behavior.getMessagingBytecodeLimit(rc));
        behavior.behave(rc);
        if (rc.getRoundNum() != currentRound) {
          System.out.println("Over bytecode limit using " +
              ((rc.getRoundNum() - currentRound) *
                  rc.getType().bytecodeLimit + Clock.getBytecodeNum()) + " this step");
        }
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
