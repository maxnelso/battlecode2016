package viperzzzzbot;

import battlecode.common.RobotController;

public class ZombieUtils {

  public static double getOutbreakMultiplier(RobotController rc) {
    int outbreakLevel = rc.getRoundNum() / 300;
    switch (outbreakLevel) {
      case 0:
        return 1;
      case 1:
        return 1.1;
      case 2:
        return 1.2;
      case 3:
        return 1.3;
      case 4:
        return 1.5;
      case 5:
        return 1.7;
      case 6:
        return 2.0;
      case 7:
        return 2.3;
      case 8:
        return 2.6;
      case 9:
        return 3.0;
      default:
        outbreakLevel -= 9;
        return 3.0 + outbreakLevel;
    }
  }
}
