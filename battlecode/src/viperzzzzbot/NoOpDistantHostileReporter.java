package viperzzzzbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class NoOpDistantHostileReporter implements DistantHostileReporter {

  @Override
  public void reportDistantHostile(MapLocation loc, RobotType type, int coreDelayTenths,
      boolean isZombie) {}
}
