package viperzzzzbot;

import battlecode.common.Signal;

public class NoOpEnemyMessageProcessor implements EnemyMessageProcessor {

  @Override
  public void processEnemyMessage(Signal s) {}
}
