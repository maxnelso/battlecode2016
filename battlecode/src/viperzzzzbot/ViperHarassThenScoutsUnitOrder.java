package viperzzzzbot;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class ViperHarassThenScoutsUnitOrder implements UnitOrder {

  private final RobotController rc;

  private boolean shouldMakeViper;
  private RobotType nextUnit;
  private int scoutsMade;

  public ViperHarassThenScoutsUnitOrder(RobotController rc, boolean shouldMakeViper) {
    this.shouldMakeViper = shouldMakeViper;
    this.rc = rc;
    scoutsMade = 0;
    computeNextUnit();
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (rc.getRoundNum() == 0 && rc.getTeamParts() < 200) { // Someone else
                                                            // already built
      shouldMakeViper = false;
    }
    if (shouldMakeViper) {
      shouldMakeViper = false;
      nextUnit = RobotType.VIPER;
      return;
    }
    nextUnit = RobotType.SCOUT;
    scoutsMade++;
  }

  public int getScoutsMade() {
    return scoutsMade;
  }
}
