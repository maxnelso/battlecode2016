package viperzzzzbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ArchonBehavior implements Behavior {

  // Keep in sync with allied archon tracker.
  private final static int SELF_ARCHON_LOCATION_MESSAGE_FREQUENCY = 10;
  private final static int BUILD_VIPER_CLOSENESS = 1500;

  private final RobotController rc;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final Radar radar;
  private final MessageSender messageSender;
  private final RepairSystem repairSystem;
  private final ZombieDenReporter zombieDenReporter;
  private final ViperSacCalculator viperSacCalculator;
  private final ViperSacReporter viperSacReporter;
  private final ArchonHider archonHider;

  private final ViperHarassThenScoutsUnitOrder viperHarassThenScoutsUnitOrder;
  private final ScoutUnitOrder scoutUnitOrder;
  private final MarineUnitOrder marineUnitOrder;
  private final DryadArcherUnitOrder dryadArcherUnitOrder;

  private final OpeningArchonBehavior openingBehavior;
  private final HidingArchonBehavior hidingArchonBehavior;
  private final ApocalypseBehavior apocalypseBehavior;
  private final FightingArchonBehavior fightingBehavior;
  private final PickerUpperArchonBehavior pickerUpperBehavior;
  private final LeaderArchonBehavior leaderBehavior;
  private final ExploringBehavior exploringBehavior;

  private int justSpawnedTimer;

  public ArchonBehavior(
      RobotController rc,
      MapBoundaryCalculator mapBoundaryCalculator,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      RepairSystem repairSystem,
      ZombieDenReporter zombieDenReporter,
      PickupLocationReporter pickupLocationReporter,
      ViperSacCalculator viperSacCalculator,
      ViperSacReporter viperSacReporter,
      ArchonHider archonHider) {
    this.rc = rc;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.radar = radar;
    this.messageSender = messageSender;
    this.repairSystem = repairSystem;
    this.zombieDenReporter = zombieDenReporter;
    this.viperSacCalculator = viperSacCalculator;
    this.viperSacReporter = viperSacReporter;
    this.archonHider = archonHider;

    scoutUnitOrder = new ScoutUnitOrder();
    marineUnitOrder = new MarineUnitOrder();
    dryadArcherUnitOrder = new DryadArcherUnitOrder(rc);
    viperHarassThenScoutsUnitOrder = new ViperHarassThenScoutsUnitOrder(rc,
        shouldMakeInitialViper());

    openingBehavior = new OpeningArchonBehavior(rc, viperHarassThenScoutsUnitOrder);
    hidingArchonBehavior = new HidingArchonBehavior(
        rc, navigation, messageSender, archonHider, viperSacCalculator);
    apocalypseBehavior = new ApocalypseBehavior(rc, navigation, radar, viperSacReporter);
    fightingBehavior = new FightingArchonBehavior(rc, radar, navigation,
        messageSender, dryadArcherUnitOrder, marineUnitOrder);
    pickerUpperBehavior = new PickerUpperArchonBehavior(rc, navigation);
    leaderBehavior = new LeaderArchonBehavior(rc, navigation, pickupLocationReporter);
    PatrolWaypointCalculator patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);
    exploringBehavior = new ExploringBehavior(
        rc, navigation, messageSender, patrolWaypointCalculator);

    justSpawnedTimer = 0;
  }

  @Override
  public void run() throws GameActionException {
    if (rc.getRoundNum() % SELF_ARCHON_LOCATION_MESSAGE_FREQUENCY == 0) {
      messageSender.sendSelfArchonLocation();
    }

    if (rc.isCoreReady() && justSpawnedTimer == 0) {
      zombieDenReporter.shareAllDens(messageSender);
      // zombieDenReporter.showDebugInfo();
      justSpawnedTimer = -1;
    }
    if (openingBehavior.isOpeningOver()
        && fightingBehavior.getHostileFighter() == null
        && archonHider.getArchonHidingLocation() == null
        && UnitSpawner.spawn(rc, dryadArcherUnitOrder.getNextUnit())) {
      justSpawnedTimer = dryadArcherUnitOrder.getNextUnit().buildTurns;
      dryadArcherUnitOrder.computeNextUnit();
    }
    getCurrentBehavior().run();
    MapLocation target = repairSystem.getBestAllyToHeal(radar.getNearbyAllies());
    if (target != null) {
      rc.setIndicatorString(2, "Healing " + target + ".");
      rc.repair(target);
    } else {
      rc.setIndicatorString(2, "");
    }
    if (justSpawnedTimer > 0) {
      justSpawnedTimer--;
    }

    // mapBoundaryCalculator.showDebugInfo();
  }

  private Behavior getCurrentBehavior() throws GameActionException {
    if (!openingBehavior.isOpeningOver()) {
      return openingBehavior;
    }
    if (archonHider.getArchonHidingLocation() != null) {
      return hidingArchonBehavior;
    }
    if (viperSacReporter.getSacAttack() != null) {
      return apocalypseBehavior;
    }
    if (fightingBehavior.getHostileFighter() != null) {
      return fightingBehavior;
    }
    if (pickerUpperBehavior.getTarget() != null) {
      return pickerUpperBehavior;
    }
    if (leaderBehavior.getTarget() != null) {
      return leaderBehavior;
    }
    return exploringBehavior;
  }

  private boolean shouldMakeInitialViper() {
    MapLocation[] ourArchons = rc.getInitialArchonLocations(rc.getTeam());
    MapLocation[] theirArchons = rc.getInitialArchonLocations(rc.getTeam().opponent());
    int myDist = 999999;
    int allyDist = 999999;
    MapLocation myLoc = rc.getLocation();
    for (int i = ourArchons.length; --i >= 0;) {
      for (int j = theirArchons.length; --j >= 0;) {
        int dist = ourArchons[i].distanceSquaredTo(theirArchons[j]);
        if (myLoc.equals(ourArchons[i])) {
          myDist = Math.min(dist, myDist);
        } else {
          allyDist = Math.min(dist, allyDist);
        }
      }
    }

    return myDist <= allyDist && myDist <= BUILD_VIPER_CLOSENESS;
  }
}
