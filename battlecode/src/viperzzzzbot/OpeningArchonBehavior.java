package viperzzzzbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class OpeningArchonBehavior implements Behavior {

  private static final int EARLY_SCOUTS_AND_VIPERS = 1;
  private static final int EARLY_SCOUT_TIMEOUT = 100;

  private final RobotController rc;
  private final ViperHarassThenScoutsUnitOrder unitOrder;

  public OpeningArchonBehavior(RobotController rc, ViperHarassThenScoutsUnitOrder unitOrder) {
    this.rc = rc;
    this.unitOrder = unitOrder;
  }

  @Override
  public void run() throws GameActionException {
    rc.setIndicatorString(0, "gl hf");
    if (!isOpeningOver() && UnitSpawner.spawn(rc, unitOrder.getNextUnit())) {
      unitOrder.computeNextUnit();
    }
  }

  public boolean isOpeningOver() {
    return unitOrder.getScoutsMade() > EARLY_SCOUTS_AND_VIPERS || rc
        .getRoundNum() >= EARLY_SCOUT_TIMEOUT;
  }
}
