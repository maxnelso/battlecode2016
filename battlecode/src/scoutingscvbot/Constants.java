package scoutingscvbot;

import battlecode.common.Direction;

public final class Constants {
  static Direction[] movableDirections = {
    Direction.NORTH,
    Direction.NORTH_EAST,
    Direction.EAST,
    Direction.SOUTH_EAST,
    Direction.SOUTH,
    Direction.SOUTH_WEST,
    Direction.WEST,
    Direction.NORTH_WEST
  };
}