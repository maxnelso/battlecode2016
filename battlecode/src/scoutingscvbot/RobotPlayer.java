package scoutingscvbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {
  public static void run(RobotController rc) {
    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior();
        break;
      case SCOUT:
        behavior = new ScoutBehavior(rc);
        break;
      case SOLDIER:
        behavior = new SoldierBehavior();
        break;
      default:
        behavior = new NoOpBehavior();
        break;
    }

    while (true) {
      try {
        behavior.behave(rc);
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
