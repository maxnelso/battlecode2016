package scoutingscvbot;

import battlecode.common.Direction;

public class DirectionUtils {

  public static Direction[] getFrontDirections(Direction d) {
    Direction[] frontDirections = {
      d,
      d.rotateLeft(),
      d.rotateRight()
    };
    return frontDirections;
  }

  public static Direction[] orderDirectionsFromFront(Direction d) {
    Direction[] frontDirections = {
      d,
      d.rotateLeft(),
      d.rotateRight(),
      d.rotateLeft().rotateLeft(),
      d.rotateRight().rotateRight(),
      d.rotateLeft().rotateLeft().rotateLeft(),
      d.rotateRight().rotateRight().rotateRight(),
      d.opposite()
    };
    return frontDirections;
  }
}