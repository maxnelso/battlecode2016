package scoutingscvbot;

import java.util.Random;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class ScoutBehavior implements Behavior {

  private enum ScoutState {
    DRAGGING,
    EXPLORING,
    RUN
  }

  Direction exploringDirection;
  RobotInfo[] nearbyEnemies;
  RobotInfo[] nearbyZombies;
  boolean[] seenLocations;
  ScoutState state;

  public ScoutBehavior(RobotController rc) {
    Random random = new Random(rc.getID());
    exploringDirection = Constants.movableDirections[random.nextInt(
        Constants.movableDirections.length)];
    state = ScoutState.EXPLORING; // TODO Change later to be from archon or
                                  // something
    seenLocations = new boolean[10000];
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (rc.isCoreReady()) {
      nearbyEnemies = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared,
          rc.getTeam().opponent());
      nearbyZombies = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.ZOMBIE);
      determineState(rc);
      switch (state) {
        case DRAGGING:
          drag(rc);
          break;
        case EXPLORING:
          explore(rc);
          break;
        case RUN:
          run(rc);
          break;
      }
    }
  }

  private void determineState(RobotController rc) {
    state = isLocationSafe(rc.getLocation()) ? ScoutState.EXPLORING : ScoutState.RUN;
  }

  private void drag(RobotController rc) {
    return;
  }

  private void explore(RobotController rc) throws GameActionException {
    // TODO Broadcast around you which direction you are going, doesn't have to
    // be too far

    // Sense parts and broadcast them
    MapLocation[] nearbyLocations = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(),
        rc.getType().sensorRadiusSquared);
    for (int i = 0; i < nearbyLocations.length; i++) {
      int locationIndex = MapLocationUtils.mapLocationToInt(nearbyLocations[i]);
      if (seenLocations[locationIndex]) {
        continue;
      }
      seenLocations[locationIndex] = true;
      if (rc.senseParts(nearbyLocations[i]) > 0) {
        // TODO Broadcast parts at some distance
      }
      RobotInfo robotInfo = rc.senseRobotAtLocation(nearbyLocations[i]);
      if (robotInfo != null && robotInfo.team == Team.NEUTRAL) {
        // TODO Broadcast neutral robot at some distance
      }
    }

    // Explore in some direction while avoiding enemies
    if (exploringDirection == null) {
      exploringDirection = Direction.SOUTH;
    }

    setExploringDirection();

    rc.setIndicatorString(0, "Exploring in direction " + exploringDirection);
    Direction[] frontDirections = DirectionUtils.getFrontDirections(exploringDirection);
    for (int i = 0; i < frontDirections.length; i++) {
      if (rc.canMove(frontDirections[i]) &&
          isLocationSafe(rc.getLocation().add(frontDirections[i]))) {
        rc.move(frontDirections[i]);
        return;
      }
    }

    // Can't move this way try some other direction
    exploringDirection = Math.random() < .5 ? exploringDirection.rotateLeft()
        : exploringDirection.rotateRight();
  }

  private boolean isLocationSafe(MapLocation loc) {
    for (int i = 0; i < nearbyEnemies.length; i++) {
      MapLocation afterMoveLocation = nearbyEnemies[i].location.add(nearbyEnemies[i].location
          .directionTo(loc));
      if (nearbyEnemies[i].type.canAttack() &&
          nearbyEnemies[i].type.attackRadiusSquared >= loc.distanceSquaredTo(afterMoveLocation)) {
        return false;
      }
    }

    for (int i = 0; i < nearbyZombies.length; i++) {
      MapLocation afterMoveLocation = nearbyZombies[i].location.add(nearbyZombies[i].location
          .directionTo(loc));
      if (nearbyZombies[i].type.canAttack() &&
          nearbyZombies[i].type.attackRadiusSquared >= loc.distanceSquaredTo(afterMoveLocation)) {
        return false;
      }
    }
    return true;
  }

  private void setExploringDirection() {

  }

  private void run(RobotController rc) throws GameActionException {
    // Get running direction, just run away from the average location for now
    MapLocation totalLocation = new MapLocation(0, 0);
    for (int i = 0; i < nearbyEnemies.length; i++) {
      totalLocation.add(nearbyEnemies[i].location.x, nearbyEnemies[i].location.y);
    }
    for (int i = 0; i < nearbyZombies.length; i++) {
      totalLocation.add(nearbyZombies[i].location.x, nearbyZombies[i].location.y);
    }

    int averageX = totalLocation.x / (nearbyEnemies.length + nearbyZombies.length);
    int averageY = totalLocation.y / (nearbyEnemies.length + nearbyZombies.length);
    MapLocation averageLocation = new MapLocation(averageX, averageY);
    Direction runDirection = averageLocation.directionTo(rc.getLocation());
    Direction[] orderedDirections = DirectionUtils.orderDirectionsFromFront(runDirection);
    for (int i = 0; i < orderedDirections.length; i++) {
      if (rc.canMove(orderedDirections[i])) {
        rc.move(orderedDirections[i]);
        rc.setIndicatorString(0, "Running in direction " + orderedDirections[i]);
        return;
      }
    }
  }
}