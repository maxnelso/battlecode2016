package scoutingscvbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class ArchonBehavior implements Behavior {

  @Override
  public void behave(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    if (rc.hasBuildRequirements(RobotType.SCOUT)
        && rc.canBuild(Direction.EAST, RobotType.SCOUT)) {
      rc.build(Direction.EAST, RobotType.SCOUT);
      return;
    }

    if (rc.canMove(Direction.EAST)) {
      rc.build(Direction.EAST, RobotType.SCOUT);
    }
  }
}
