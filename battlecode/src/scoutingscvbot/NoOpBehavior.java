package scoutingscvbot;

import battlecode.common.RobotController;

public class NoOpBehavior implements Behavior {

  @Override
  public void behave(RobotController rc) {}
}
