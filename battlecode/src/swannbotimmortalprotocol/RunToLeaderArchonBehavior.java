package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RunToLeaderArchonBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;

  public RunToLeaderArchonBehavior(
      RobotController rc, NavigationSystem navigation, AlliedArchonTracker alliedArchonTracker) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
  }

  @Override
  public void run() throws GameActionException {
    navigation.directTo(rc.getInitialArchonLocations(rc.getTeam())[0], true /* avoidEnemies */,
        true /* clearRubble */);
  }
}
