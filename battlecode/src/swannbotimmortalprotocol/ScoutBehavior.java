package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ScoutBehavior implements Behavior {

  private final RobotController rc;
  private final Radar radar;
  private final BuddyScoutBehavior buddyBehavior;
  private final ExploringBehavior exploringBehavior;
  private final ZombieDraggingScoutBehavior zombieDraggingBehavior;
  private final EnemyArchonTracker enemyArchonTracker;

  public ScoutBehavior(
      RobotController rc,
      NavigationSystem navigation,
      Radar radar,
      BasicMessages basicMessages,
      EnemyArchonTracker enemyArchonTracker,
      AlliedArchonTracker alliedArchonTracker,
      MessageSender messageSender) {
    this.rc = rc;
    this.radar = radar;
    this.enemyArchonTracker = enemyArchonTracker;

    buddyBehavior = new BuddyScoutBehavior(
        rc,
        navigation,
        new BuddySystem(),
        basicMessages,
        radar,
        messageSender);
    PatrolWaypointCalculator patrolWaypointCalculator = new ReflectingPatrolWaypointCalculator(
        new LawnMowerPatrolWaypointCalculator(7 /* laneHalfWidth */, 5 /* mapBoundaryMargin */),
        (rc.getID() & 2) == 0 /* flipX */,
        (rc.getID() & 4) == 0 /* flipY */);
    exploringBehavior = new ExploringBehavior(rc, navigation, patrolWaypointCalculator);
    zombieDraggingBehavior = new ZombieDraggingScoutBehavior(rc, navigation, radar,
        enemyArchonTracker,
        alliedArchonTracker);
  }

  @Override
  public void run() throws GameActionException {
    updateEnemyArchonTracker();
    getBehavior().run();
  }

  private Behavior getBehavior() throws GameActionException {
    if (buddyBehavior.shouldBuddy()) {
      return buddyBehavior;
    } else if (zombieDraggingBehavior.shouldDrag()) {
      return zombieDraggingBehavior;
    } else {
      return exploringBehavior;
    }
  }

  private void updateEnemyArchonTracker() {
    RobotInfo[] enemies = radar.getNearbyEnemies();
    for (int i = enemies.length; --i >= 0;) {
      RobotInfo enemy = enemies[i];
      if (enemy.type == RobotType.ARCHON) {
        enemyArchonTracker.reportEnemyArchon(enemy.ID, enemy.location, rc.getRoundNum());
      }
    }
  }
}
