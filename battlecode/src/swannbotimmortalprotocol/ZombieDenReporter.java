package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public interface ZombieDenReporter {

  public void reportDen(MapLocation denLoc);

  public void reportDenDestroyed(MapLocation denLoc);

  public void invalidateNearbyDestroyedDens() throws GameActionException;

  public void shareRandomDen(MessageSender messageSender) throws GameActionException;

  public MapLocation getClosestDen();

  public void showDebugInfo();
}
