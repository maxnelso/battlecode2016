package swannbotimmortalprotocol;

import battlecode.common.RobotController;

public class BehaviorFactory {

  public static class BehaviorInfo {

    public final PreBehavior preBehavior;
    public final Behavior behavior;

    public BehaviorInfo(PreBehavior preBehavior, Behavior behavior) {
      this.preBehavior = preBehavior;
      this.behavior = behavior;
    }
  }

  public static BehaviorInfo createForRobotController(RobotController rc) {
    switch (rc.getType()) {
      case ARCHON:
        return archon(rc);
      case SCOUT:
        return scout(rc);
      case SOLDIER:
        return soldier(rc);
      case GUARD:
        return guard(rc);
      case VIPER:
        return viper(rc);
      case TURRET:
        return turret(rc);
      default:
        return new BehaviorInfo(new NoOpPreBehavior(), new NoOpBehavior());
    }
  }

  private static BehaviorInfo archon(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc);
    BasicMessages basicMessages = new NoOpBasicMessages();
    DistantHostileReporter distantHostileReporter = new NoOpDistantHostileReporter();
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    RepairSystem repairSystem = new DefaultRepairSystem(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        distantHostileReporter,
        zombieDenReporter,
        basicMessages,
        armyRallyReporter);
    Radar radar = new SenseRadar(rc);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);

    PreBehavior preBehavior = new DefaultPreBehavior(
        rc,
        radar,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        zombieDenReporter);
    Behavior behavior = new ArchonBehavior(
        rc,
        radar,
        navigation,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        repairSystem,
        zombieDenReporter);
    return new BehaviorInfo(preBehavior, behavior);
  }

  private static BehaviorInfo scout(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    EnemyArchonTracker enemyArchonTracker = new DefaultEnemyArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc);
    BasicMessages basicMessages = new DefaultBasicMessages();
    DistantHostileReporter distantHostileReporter = new NoOpDistantHostileReporter();
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        distantHostileReporter,
        zombieDenReporter,
        basicMessages,
        armyRallyReporter);
    Radar radar = new SenseRadar(rc);

    PreBehavior preBehavior = new DefaultPreBehavior(
        rc,
        radar,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        zombieDenReporter);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);
    Behavior behavior = new ScoutBehavior(
        rc,
        navigation,
        radar,
        basicMessages,
        enemyArchonTracker,
        alliedArchonTracker,
        messageSenderReceiver);
    return new BehaviorInfo(preBehavior, behavior);
  }

  private static BehaviorInfo soldier(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new NoOpZombieDenReporter();
    BasicMessages basicMessages = new NoOpBasicMessages();
    DistantHostileReporter distantHostileReporter = new NoOpDistantHostileReporter();
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        distantHostileReporter,
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */);
    Radar radar = new SenseRadar(rc);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior soldierBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */);
    return new BehaviorInfo(soldierBehavior /* preBehavior */, soldierBehavior /* behavior */);
  }

  private static BehaviorInfo guard(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new NoOpZombieDenReporter();
    BasicMessages basicMessages = new NoOpBasicMessages();
    DistantHostileReporter distantHostileReporter = new NoOpDistantHostileReporter();
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        distantHostileReporter,
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */);
    Radar radar = new SenseRadar(rc);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior rangedArmyBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */);
    return new BehaviorInfo(
        rangedArmyBehavior /* preBehavior */, rangedArmyBehavior /* behavior */);
  }

  private static BehaviorInfo viper(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new NoOpZombieDenReporter();
    BasicMessages basicMessages = new NoOpBasicMessages();
    DistantHostileReporter distantHostileReporter = new NoOpDistantHostileReporter();
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        distantHostileReporter,
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */);
    Radar radar = new SenseRadar(rc);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);
    AttackSystem attackSystem = new ViperAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior soldierBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */);
    return new BehaviorInfo(soldierBehavior /* preBehavior */, soldierBehavior /* behavior */);
  }

  private static BehaviorInfo turret(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new DefaultAlliedArchonTracker(rc);
    ZombieDenReporter zombieDenReporter = new NoOpZombieDenReporter();
    BasicMessages basicMessages = new NoOpBasicMessages();
    Radar radar = new SenseRadar(rc);
    ExtendedRadar extendedRadar = new ExtendedRadar(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        alliedArchonTracker,
        extendedRadar /* distantHostileReporter */,
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */);
    NavigationSystem navigation = new DuckNavigationSystem(rc, radar, alliedArchonTracker);
    AttackSystem attackSystem = new TurretAttackSystem();
    TurretBehavior turretBehavior = new TurretBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        extendedRadar /* radar */,
        navigation,
        alliedArchonTracker,
        attackSystem,
        defaultArmyRally);
    return new BehaviorInfo(turretBehavior /* preBehavior */, turretBehavior /* behavior */);
  }

}
