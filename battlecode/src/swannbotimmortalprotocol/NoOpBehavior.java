package swannbotimmortalprotocol;

import battlecode.common.GameActionException;

public class NoOpBehavior implements Behavior {

  @Override
  public void run() throws GameActionException {}
}
