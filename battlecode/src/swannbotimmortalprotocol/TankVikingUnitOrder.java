package swannbotimmortalprotocol;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class TankVikingUnitOrder implements UnitOrder {

  private RobotController rc;
  private RobotType nextUnit;

  public TankVikingUnitOrder(RobotController rc) {
    this.rc = rc;
    this.nextUnit = RobotType.TURRET;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (nextUnit == RobotType.TURRET) {
      nextUnit = RobotType.SCOUT;
    } else {
      nextUnit = RobotType.TURRET;
    }
  }
}
