package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ArchonBehavior implements Behavior {

  private static final int SELF_ARCHON_LOCATION_MESSAGE_DISTANCE_MULTIPLIER = 183;

  private final RobotController rc;
  private final Radar radar;
  private final MessageSender messageSender;
  private final AlliedArchonTracker alliedArchonTracker;
  private final RepairSystem repairSystem;

  private final TankVikingUnitOrder tankVikingUnitOrder;

  private final RunToLeaderArchonBehavior runToLeaderArchonBehavior;
  private final BuildTankVikingArchonBehavior buildTankVikingBehavior;
  private boolean foundLeader;

  public ArchonBehavior(
      RobotController rc,
      Radar radar,
      NavigationSystem navigation,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      RepairSystem repairSystem,
      ZombieDenReporter zombieDenReporter) {
    this.rc = rc;
    this.radar = radar;
    this.messageSender = messageSender;
    this.alliedArchonTracker = alliedArchonTracker;
    this.repairSystem = repairSystem;
    this.foundLeader = false;

    tankVikingUnitOrder = new TankVikingUnitOrder(rc);

    runToLeaderArchonBehavior = new RunToLeaderArchonBehavior(rc, navigation, alliedArchonTracker);
    buildTankVikingBehavior = new BuildTankVikingArchonBehavior(rc, navigation,
        tankVikingUnitOrder);
  }

  @Override
  public void run() throws GameActionException {
    if (rc.getRoundNum() == 0) {
      buildTankVikingBehavior.setStartingLocation(rc.getInitialArchonLocations(rc.getTeam())[0]);
      return;
    }
    getCurrentBehavior().run();
    MapLocation target = repairSystem.getBestAllyToHeal(radar.getNearbyAllies());
    if (target != null) {
      rc.setIndicatorString(2, "Healing target " + target + " " + rc.getRoundNum());
      rc.repair(target);
    }
  }

  private Behavior getCurrentBehavior() throws GameActionException {
    if (rc.getLocation().distanceSquaredTo(rc.getInitialArchonLocations(rc.getTeam())[0]) <= 9) {
      foundLeader = true;
    }
    if (!foundLeader) {
      return runToLeaderArchonBehavior;
    }
    return buildTankVikingBehavior;
  }
}
