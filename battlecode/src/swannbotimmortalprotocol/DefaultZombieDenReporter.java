package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DefaultZombieDenReporter implements ZombieDenReporter {

  private static final int LOCS_ARRAY_MAX_SIZE = 200;

  private final RobotController rc;

  private MapLocation[] locs;
  private MapLocationSet addedLocs;
  private MapLocationSet deletedLocs;
  private int totalLocs;
  private int shareIndex;

  public DefaultZombieDenReporter(RobotController rc) {
    this.rc = rc;
    locs = new MapLocation[LOCS_ARRAY_MAX_SIZE];
    addedLocs = new ArrayMapLocationIntMap();
    deletedLocs = new ArrayMapLocationIntMap();
    totalLocs = 0;
  }

  @Override
  public void reportDen(MapLocation denLoc) {
    if (deletedLocs.contains(denLoc) || addedLocs.contains(denLoc)) {
      return;
    }
    if (maybeAdd(locs, denLoc, totalLocs)) {
      addedLocs.add(denLoc);
      totalLocs++;
    }
  }

  private boolean maybeAdd(MapLocation[] array, MapLocation el, int currentSize) {
    if (currentSize >= array.length) {
      return false;
    }

    for (int i = currentSize; --i >= 0;) {
      MapLocation loc = array[i];
      if (loc.equals(el)) {
        return false;
      }
    }

    array[currentSize] = el;
    return true;
  }

  @Override
  public void invalidateNearbyDestroyedDens() throws GameActionException {
    for (int i = totalLocs; --i >= 0;) {
      MapLocation loc = locs[i];
      if (!deletedLocs.contains(loc) && rc.canSenseLocation(loc)) {
        RobotInfo robot = rc.senseRobotAtLocation(loc);
        if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
          reportDenDestroyed(loc);
        }
      }
    }
  }

  @Override
  public void reportDenDestroyed(MapLocation denLoc) {
    if (!deletedLocs.contains(denLoc)) {
      deletedLocs.add(denLoc);
    }
    if (!addedLocs.contains(denLoc) && maybeAdd(locs, denLoc, totalLocs)) {
      addedLocs.add(denLoc);
      totalLocs++;
    }
  }

  @Override
  public void showDebugInfo() {
    MapLocation myLoc = rc.getLocation();
    for (int i = totalLocs; --i >= 0;) {
      MapLocation loc = locs[i];
      if (!deletedLocs.contains(loc)) {
        rc.setIndicatorLine(myLoc, loc, 0, 255, 0);
      }
    }
  }

  @Override
  public void shareRandomDen(MessageSender messageSender) throws GameActionException {
    if (totalLocs == 0) {
      return;
    }

    shareIndex = shareIndex % totalLocs;
    MapLocation loc = locs[shareIndex];
    if (deletedLocs.contains(loc)) {
      messageSender.sendDenLocationRemoved(loc);
    } else {
      messageSender.sendDenLocation(loc);
    }
    shareIndex++;
  }

  @Override
  public MapLocation getClosestDen() {
    MapLocation myLoc = rc.getLocation();
    int closestDist = 99999;
    MapLocation closest = null;
    for (int i = totalLocs; --i >= 0;) {
      MapLocation loc = locs[i];
      int dist = myLoc.distanceSquaredTo(loc);
      if (!deletedLocs.contains(loc) && (closest == null || dist < closestDist)) {
        closestDist = dist;
        closest = loc;
      }
    }

    return closest;
  }
}
