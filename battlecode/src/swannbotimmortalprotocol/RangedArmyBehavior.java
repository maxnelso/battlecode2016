package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RangedArmyBehavior implements PreBehavior, Behavior {

  private final RobotController rc;
  private final MessageReceiver messageReceiver;
  private final Radar radar;
  private final ArmyRally armyRally;
  private final FightingRangedBehavior fightingBehavior;
  private final SwarmingBehavior swarmingBehavior;
  private final RallyBehavior rallyBehavior;

  public RangedArmyBehavior(
      RobotController rc,
      MessageReceiver messageReceiver,
      AlliedArchonTracker alliedArchonTracker,
      Radar radar,
      NavigationSystem navigation,
      AttackSystem attackSystem,
      ZombieSpawnScheduleInfo zombieSchedule,
      ArmyRally armyRally) {
    this.rc = rc;
    this.radar = radar;
    this.messageReceiver = messageReceiver;
    this.armyRally = armyRally;
    fightingBehavior = new FightingRangedBehavior(rc, radar, navigation, zombieSchedule,
        attackSystem);
    swarmingBehavior = new SwarmingBehavior(rc, navigation, alliedArchonTracker);
    rallyBehavior = new RallyBehavior(rc, armyRally, navigation);
  }

  @Override
  public void preRun() throws GameActionException {
    if (radar.getNearbyHostiles().length != 0) {
      rc.emptySignalQueue();
      return;
    }

    messageReceiver.receiveMessages();
    RobotPlayer.profiler.split("after receiving messages");
  }

  @Override
  public void run() throws GameActionException {
    getCurrentBehavior().run();
  }

  private Behavior getCurrentBehavior() {
    if (radar.getNearbyHostiles().length != 0) {
      return fightingBehavior;
    }

    return armyRally.getRally() != null ? rallyBehavior : swarmingBehavior;
  }
}
