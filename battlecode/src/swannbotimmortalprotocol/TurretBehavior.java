package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class TurretBehavior implements PreBehavior, Behavior {

  private final MessageReceiver messageReceiver;
  private final Behavior unpackedBehavior;
  private final RobotController rc;
  private final SwarmingBehavior swarmingBehavior;
  private final RallyBehavior rallyBehavior;
  private final Radar radar;
  private final ArmyRally armyRally;

  public TurretBehavior(
      RobotController rc,
      MessageReceiver messageReceiver,
      Radar radar,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker,
      AttackSystem attackSystem,
      ArmyRally armyRally) {
    this.rc = rc;
    this.messageReceiver = messageReceiver;
    this.radar = radar;
    this.armyRally = armyRally;
    this.swarmingBehavior = new SwarmingBehavior(rc, navigation, alliedArchonTracker);
    this.unpackedBehavior = new UnpackedTurretBehavior(rc, radar, attackSystem);
    this.rallyBehavior = new RallyBehavior(rc, armyRally, navigation);
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();

    RobotInfo[] adjacentRobots = rc.senseNearbyRobots(2);
    boolean adjacentScout = false;
    for (RobotInfo robot : adjacentRobots) {
      if (robot.type.equals(RobotType.SCOUT)) {
        adjacentScout = true;
        break;
      }
    }
    if (!adjacentScout) {
      rc.broadcastSignal(2 * rc.getType().sensorRadiusSquared);
    }
  }

  @Override
  public void run() throws GameActionException {
    if (rc.getType() == RobotType.TURRET) {
      unpackedBehavior.run();
    } else {
      RobotInfo closest = RadarUtils.getClosestRobot(radar.getNearbyHostiles(), rc.getLocation());
      if (closest != null && closest.location.distanceSquaredTo(
          rc.getLocation()) <= RobotType.TURRET.attackRadiusSquared) {
        rc.unpack();
      }
      Behavior b = armyRally.getRally() != null ? rallyBehavior : swarmingBehavior;
      b.run();
    }
  }
}
