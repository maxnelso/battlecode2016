package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class PickerUpperArchonBehavior implements Behavior {

  private final int TIMEOUT = 50;
  private final int MAX_TILES = 50;
  private final int RUBBLE_THRESHOLD = 10000;

  private final RobotController rc;
  private final NavigationSystem navigation;

  private MapLocation currentTarget;
  private MapLocationIntMap timeoutMap;
  private int currentTargetRound;

  public PickerUpperArchonBehavior(RobotController rc, NavigationSystem navigation) {
    this.rc = rc;
    this.navigation = navigation;

    currentTarget = null;
    currentTargetRound = -1;

    timeoutMap = new ArrayMapLocationIntMap();
  }

  @Override
  public void run() throws GameActionException {
    if (rc.isCoreReady()) {
      MapLocation target = getTarget();
      if (target != null) {
        timeoutMap.increment(target);
        rc.setIndicatorString(0, "I'm picking up neutrals or parts at " + target + "." + timeoutMap
            .getValue(target));
        if (rc.isCoreReady() && rc.getLocation().isAdjacentTo(target)) {
          RobotInfo robot = rc.senseRobotAtLocation(target);
          if (robot != null && robot.team == Team.NEUTRAL) {
            rc.activate(target);
            return;
          }
        }
        navigation.directTo(target, false /* avoidAttackers */, true /* clearRubble */);
      } else {
        rc.setIndicatorString(0, "I'm a lost picker-upper.");
        navigation.moveRandomly();
      }
    }
  }

  public MapLocation getTarget() {
    int roundNum = rc.getRoundNum();
    if (currentTargetRound >= roundNum) {
      return currentTarget;
    }

    currentTargetRound = roundNum;
    currentTarget = findClosestNeutralRobot();
    if (currentTarget != null) {
      return currentTarget;
    }

    currentTarget = findClosestParts();
    return currentTarget;
  }

  // TODO Some algorithm for picking one based on estimated time of arrival and
  // what robot you get?
  private MapLocation findClosestNeutralRobot() {
    RobotInfo[] neutrals = rc.senseNearbyRobots(
        rc.getLocation(), rc.getType().sensorRadiusSquared, Team.NEUTRAL);
    int closestNonTimeoutDist = 99999;
    MapLocation closestNonTimeout = null;
    for (int i = neutrals.length; --i >= 0;) {
      MapLocation loc = neutrals[i].location;
      int distance = loc.distanceSquaredTo(rc.getLocation());
      if (timeoutMap.getValue(loc) < TIMEOUT) {

        boolean isArchon = neutrals[i].type == RobotType.ARCHON;
        if (isArchon) { // Prioritize getting
                        // neutral archons
          distance = 0;
        }
        if (distance < closestNonTimeoutDist) {
          closestNonTimeoutDist = distance;
          closestNonTimeout = loc;
        }
      }
    }
    return closestNonTimeout;
  }

  // TODO Some algorithm for picking one based on estimated time of arrival and
  // how many parts you get?
  private MapLocation findClosestParts() {
    MapLocation[] parts = rc.sensePartLocations(rc.getType().sensorRadiusSquared);
    int closestNonTimeoutDist = 99999;
    MapLocation closestNonTimeout = null;
    for (int i = parts.length; --i >= 0;) {
      MapLocation loc = parts[i];
      if (rc.senseRubble(loc) >= RUBBLE_THRESHOLD) {
        continue;
      }
      int distance = loc.distanceSquaredTo(rc.getLocation());
      if (timeoutMap.getValue(loc) < TIMEOUT) {
        if (distance < closestNonTimeoutDist) {
          closestNonTimeoutDist = distance;
          closestNonTimeout = loc;
        }
      }
    }
    return closestNonTimeout;
  }
}
