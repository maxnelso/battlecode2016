package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class UnpackedTurretBehavior implements Behavior {

  private static final int MAX_IDLE_ROUNDS = 30;

  private final RobotController rc;
  private final Radar radar;
  private final AttackSystem attackSystem;

  private int idleRounds;

  public UnpackedTurretBehavior(RobotController rc, Radar radar, AttackSystem attackSystem) {
    this.rc = rc;
    this.radar = radar;
    this.attackSystem = attackSystem;
  }

  @Override
  public void run() throws GameActionException {
    attack();
  }

  private void attack() throws GameActionException {
    if (rc.isWeaponReady()) {
      RobotInfo[] enemies = radar.getNearbyEnemies();
      RobotInfo[] allies = radar.getNearbyAllies();
      MapLocation target = attackSystem.getBestEnemyToShoot(rc, enemies, enemies.length, allies,
          allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
      RobotInfo[] zombies = radar.getNearbyZombies();
      target = attackSystem.getBestEnemyToShoot(rc, zombies, zombies.length, allies,
          allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
    }
  }
}
