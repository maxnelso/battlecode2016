package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class NoOpZombieDenReporter implements ZombieDenReporter {

  @Override
  public void reportDen(MapLocation denLoc) {}

  @Override
  public void reportDenDestroyed(MapLocation denLoc) {}

  @Override
  public void invalidateNearbyDestroyedDens() throws GameActionException {}

  @Override
  public void shareRandomDen(MessageSender messageSender) throws GameActionException {}

  @Override
  public MapLocation getClosestDen() {
    return null;
  }

  @Override
  public void showDebugInfo() {}
}
