package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Signal;
import battlecode.common.Team;

public class DefaultMessageSenderReceiver implements MessageSender, MessageReceiver {

  private enum MessageType {
    SELF_ARCHON_LOCATION(0),
    DEN_LOCATION(1),
    DEN_LOCATION_REMOVED(2),
    DISTANT_HOSTILE_INFO(3),
    ARMY_RALLY_LOCATION(4);

    private final int opCode;

    private MessageType(int opCode) {
      this.opCode = opCode;
    }
  }

  private final RobotController rc;
  private final AlliedArchonTracker alliedArchonTracker;
  private final DistantHostileReporter distantHostileReporter;
  private final ZombieDenReporter zombieDenReporter;
  private final BasicMessages basicMessages;
  private final ArmyRallyReporter armyRallyReporter;

  public DefaultMessageSenderReceiver(
      RobotController rc,
      AlliedArchonTracker alliedArchonTracker,
      DistantHostileReporter distantHostileReporter,
      ZombieDenReporter zombieDenReporter,
      BasicMessages basicMessages,
      ArmyRallyReporter armyRallyReporter) {
    this.rc = rc;
    this.alliedArchonTracker = alliedArchonTracker;
    this.distantHostileReporter = distantHostileReporter;
    this.zombieDenReporter = zombieDenReporter;
    this.basicMessages = basicMessages;
    this.armyRallyReporter = armyRallyReporter;
  }

  @Override
  public void receiveMessages() throws GameActionException {
    basicMessages.clearBasicMessages();
    Signal[] signals = rc.emptySignalQueue();
    Team myTeam = rc.getTeam();
    for (int i = signals.length; --i >= 0;) {
      Signal s = signals[i];
      if (s.getTeam() != myTeam) {
        continue;
      }
      int[] data = s.getMessage();

      if (data == null) { // Basic message
        basicMessages.addMessage(s, rc);
        continue;
      }
      if (data.length != 2) {
        continue;
      }
      MessageData data1 = MessageData.fromSignal(s, true /* firstData */);
      MessageData data2 = MessageData.fromSignal(s, false /* firstData */);
      int opCode = data1.getPayload(0, 3);
      if (opCode == MessageType.SELF_ARCHON_LOCATION.opCode) {
        alliedArchonTracker.reportAlliedArchon(s.getID(), s.getLocation());
      } else if (opCode == MessageType.DEN_LOCATION.opCode) {
        MapLocation denLoc = data2.toMapLocation();
        zombieDenReporter.reportDen(denLoc);
      } else if (opCode == MessageType.DEN_LOCATION_REMOVED.opCode) {
        MapLocation denLoc = data2.toMapLocation();
        zombieDenReporter.reportDenDestroyed(denLoc);
      } else if (opCode == MessageType.DISTANT_HOSTILE_INFO.opCode) {
        MapLocation hostileLoc = data2.toMapLocation();
        int[] payloads = data1.getAllPayloads(new int[] {
          4, 8, 1
        });
        distantHostileReporter.reportDistantHostile(
            hostileLoc, payloads[1] /* coreDelayTenths */, payloads[2] == 1 /* isZombie */);
      } else if (opCode == MessageType.ARMY_RALLY_LOCATION.opCode) {
        MapLocation rallyLoc = data2.toMapLocation();
        armyRallyReporter.reportRally(rallyLoc);
      }
      RobotPlayer.profiler.split("received " + opCode);
    }
  }

  @Override
  public void sendSelfArchonLocation(int distanceMultiplier) throws GameActionException {
    MessageData data1 = new MessageData.Builder()
        .addBits(4, MessageType.SELF_ARCHON_LOCATION.opCode)
        .build();
    MessageData data2 = MessageData.empty();
    sendMessageWithMultiplier(data1, data2, distanceMultiplier);
  }

  @Override
  public void sendDenLocation(MapLocation loc) throws GameActionException {
    MessageData data1 = new MessageData.Builder()
        .addBits(4, MessageType.DEN_LOCATION.opCode)
        .build();
    MessageData data2 = MessageData.fromMapLocation(loc);
    sendMessage(data1, data2);
  }

  @Override
  public void sendDenLocationRemoved(MapLocation loc) throws GameActionException {
    MessageData data1 = new MessageData.Builder()
        .addBits(4, MessageType.DEN_LOCATION_REMOVED.opCode)
        .build();
    MessageData data2 = MessageData.fromMapLocation(loc);
    sendMessage(data1, data2);
  }

  @Override
  public void sendDistantHostileInfo(
      MapLocation loc, double coreDelay, boolean isZombie, int broadcastDistanceSquared)
          throws GameActionException {
    int coreDelayTenths = (10 * coreDelay) >= 250 ? 250 : (int) (10 * coreDelay);
    MessageData data1 = new MessageData.Builder()
        .addBits(4, MessageType.DISTANT_HOSTILE_INFO.opCode)
        .addBits(8, coreDelayTenths)
        .addBits(1, isZombie ? 1 : 0)
        .build();
    MessageData data2 = MessageData.fromMapLocation(loc);
    sendMessageWithDistanceSquared(data1, data2, broadcastDistanceSquared);
  }

  @Override
  public void sendArmyRallyLocation(MapLocation loc) throws GameActionException {
    MessageData data1 = new MessageData.Builder()
        .addBits(4, MessageType.ARMY_RALLY_LOCATION.opCode)
        .build();
    MessageData data2 = MessageData.fromMapLocation(loc);
    sendMessage(data1, data2);
  }

  private void sendMessage(MessageData data1, MessageData data2)
      throws GameActionException {
    sendMessageWithMultiplier(data1, data2, 2 /* distanceMultiplier */);
  }

  private void sendMessageWithDistanceSquared(MessageData data1, MessageData data2,
      int distanceSquared)
          throws GameActionException {
    rc.broadcastMessageSignal(
        data1.getData(),
        data2.getData(),
        distanceSquared);
  }

  private void sendMessageWithMultiplier(
      MessageData data1, MessageData data2, int distanceMultiplier) throws GameActionException {
    rc.broadcastMessageSignal(
        data1.getData(),
        data2.getData(),
        distanceMultiplier * rc.getType().sensorRadiusSquared);
  }
}
