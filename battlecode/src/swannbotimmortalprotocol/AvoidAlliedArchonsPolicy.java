package swannbotimmortalprotocol;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class AvoidAlliedArchonsPolicy implements NavigationSafetyPolicy {

  private final AlliedArchonTracker alliedArchonTracker;
  private final int avoidDist;

  public AvoidAlliedArchonsPolicy(AlliedArchonTracker alliedArchonTracker, int avoidDist) {
    this.alliedArchonTracker = alliedArchonTracker;
    this.avoidDist = avoidDist;
  }

  @Override
  public boolean isSafeToMoveTo(RobotController rc, MapLocation loc) {
    return !alliedArchonTracker.isInRangeOfAlliedArchon(loc, avoidDist);
  }
}
