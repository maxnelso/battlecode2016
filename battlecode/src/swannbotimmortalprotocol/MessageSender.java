package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public interface MessageSender {

  public void sendSelfArchonLocation(int distanceMultiplier) throws GameActionException;

  public void sendDenLocation(MapLocation loc) throws GameActionException;

  public void sendDenLocationRemoved(MapLocation loc) throws GameActionException;

  public void sendDistantHostileInfo(
      MapLocation loc, double coreDelay, boolean isZombie, int broadcastDistanceSquared)
          throws GameActionException;

  public void sendArmyRallyLocation(MapLocation loc) throws GameActionException;
}
