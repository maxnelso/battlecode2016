package swannbotimmortalprotocol;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class AvoidAttackingUnitsPolicy implements NavigationSafetyPolicy {

  private final Radar radar;

  public AvoidAttackingUnitsPolicy(Radar radar) {
    this.radar = radar;
  }

  @Override
  public boolean isSafeToMoveTo(RobotController rc, MapLocation loc) {
    RobotInfo[] hostiles = radar.getNearbyHostiles();
    for (int i = hostiles.length; --i >= 0;) {
      RobotInfo robot = hostiles[i];
      MapLocation robotAttackFromLoc = robot.type.canMove()
          ? robot.location.add(robot.location.directionTo(loc))
          : robot.location;
      if (robot.type.canAttack()
          && loc.distanceSquaredTo(robotAttackFromLoc) <= robot.type.attackRadiusSquared) {
        return false;
      }
    }
    return true;
  }
}