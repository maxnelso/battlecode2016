package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DefaultPreBehavior implements PreBehavior {

  private final RobotController rc;
  private final Radar radar;
  private final MessageReceiver messageReceiver;
  private final MessageSender messageSender;
  private final AlliedArchonTracker alliedArchonTracker;
  private final ZombieDenReporter zombieDenReporter;

  public DefaultPreBehavior(
      RobotController rc,
      Radar radar,
      MessageReceiver messageReceiver,
      MessageSender messageSender,
      AlliedArchonTracker alliedArchonTracker,
      ZombieDenReporter zombieDenReporter) {
    this.rc = rc;
    this.radar = radar;
    this.messageReceiver = messageReceiver;
    this.messageSender = messageSender;
    this.alliedArchonTracker = alliedArchonTracker;
    this.zombieDenReporter = zombieDenReporter;
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();
    updateAndShareZombieDens();
    zombieDenReporter.showDebugInfo();
  }

  private void updateAndShareZombieDens() throws GameActionException {
    zombieDenReporter.invalidateNearbyDestroyedDens();

    RobotInfo[] zombies = radar.getNearbyZombies();
    for (int i = zombies.length; --i >= 0;) {
      RobotInfo zombie = zombies[i];
      if (zombie.type == RobotType.ZOMBIEDEN) {
        zombieDenReporter.reportDen(zombie.location);
      }
    }

    int random = rc.getRoundNum() + rc.getID();
    if (random % getZombieDenSharingFrequency() == 0) {
      zombieDenReporter.shareRandomDen(messageSender);
    }
  }

  private int getZombieDenSharingFrequency() {
    return (2 * (rc.getRoundNum() / 300)) + 1;
  }
}
