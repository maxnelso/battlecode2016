package swannbotimmortalprotocol;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class BuildTankVikingArchonBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final UnitOrder unitOrder;

  private MapLocation startingLocation;

  public BuildTankVikingArchonBehavior(
      RobotController rc, NavigationSystem navigation, UnitOrder unitOrder) {
    this.rc = rc;
    this.navigation = navigation;
    this.unitOrder = unitOrder;
  }

  @Override
  public void run() throws GameActionException {
    rc.setIndicatorString(0, "Building tank viking wol beta style");
    if (rc.getTeamParts() > unitOrder.getNextUnit().partCost) { // Building
      MapLocation[] tiles = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(),
          RobotType.ARCHON.sensorRadiusSquared);
      int closestTileDist = 999999;
      MapLocation bestTile = null;
      for (int i = tiles.length; --i >= 0;) {
        if (!isCheckerboard(tiles[i])) {
          continue;
        }
        if (rc.senseRobotAtLocation(tiles[i]) != null) {
          continue;
        }
        if (tiles[i].equals(rc.getLocation())) {
          continue;
        }
        if (!rc.onTheMap(tiles[i])) {
          continue;
        }
        if (rc.senseRubble(tiles[i]) >= GameConstants.RUBBLE_OBSTRUCTION_THRESH) {
          continue;
        }
        int dist = rc.getLocation().distanceSquaredTo(tiles[i]);
        if (dist < closestTileDist) {
          closestTileDist = dist;
          bestTile = tiles[i];
        }
      }
      if (bestTile == null) {
        return;
      }
      if (rc.getLocation().isAdjacentTo(bestTile)) {
        if (UnitSpawner.spawn(rc, unitOrder.getNextUnit(), rc.getLocation().directionTo(
            bestTile))) {
          unitOrder.computeNextUnit();
        }
      } else {
        navigation.directTo(bestTile, true /* avoidAttackers */, true /* clearRubble */);
      }
    } else {
      navigation.directTo(startingLocation, true /* avoidAttackers */, true /* clearRubble */);
    }

  }

  public boolean isCheckerboard(MapLocation loc) {
    return (loc.x + loc.y) % 2 == 0;
  }

  public void setStartingLocation(MapLocation startLocation) {
    this.startingLocation = startLocation;
  }
}
