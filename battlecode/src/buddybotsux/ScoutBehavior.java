package buddybotsux;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public class ScoutBehavior implements Behavior {

  private final int MAP_BOUNDARY_SHARE_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 7500;
  private static final int MESSAGING_BYTECODE_LIMIT = 7500;
  private final int MAKE_TURRET_ROUND = 300;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final ArchonTracker archonTracker;
  private final ZombieDragger zombieDragger;
  private final PartScanner partScanner;
  private final EnemyTurretCache enemyTurretCache;
  private final BasicMessages basicMessages;
  private final PatrolWaypointCalculator patrolWaypointCalculator;
  private final BuddyTracker buddyTracker;
  private int birthday;

  public ScoutBehavior(
      int robotId,
      NavigationSystem navigation,
      Radar radar,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      ArchonTracker archonTracker,
      BasicMessages basicMessages,
      ZombieDragger zombieDragger,
      PartScanner partScanner,
      BuddyTracker buddyTracker,
      int birthday,
      EnemyTurretCache enemyTurretCache) {
    this.navigation = navigation;
    this.radar = radar;
    this.birthday = birthday;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.archonTracker = archonTracker;
    this.basicMessages = basicMessages;
    this.zombieDragger = zombieDragger;
    this.partScanner = partScanner;
    this.enemyTurretCache = enemyTurretCache;
    this.buddyTracker = buddyTracker;
    patrolWaypointCalculator = new ReflectingPatrolWaypointCalculator(
        new LawnMowerPatrolWaypointCalculator(
            7 /* laneHalfWidth */, 5 /* topBottomMargin */),
        (robotId & 2) == 0 /* flipX */,
        (robotId & 4) == 0 /* flipY */);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    partScanner.scanForParts(rc, interestingTargets);
    mapBoundaryCalculator.update(rc);

    if ((rc.getRoundNum() - rc.getID()) % MAP_BOUNDARY_SHARE_FREQUENCY == 0) {
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }

    interestingTargets.shareRandomTarget(rc, messageSender);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
    shareExtendedBroadcast(rc, messageSender);

    if (birthday >= MAKE_TURRET_ROUND && rc.getRoundNum()
        - birthday <= RobotType.TURRET.buildTurns + 50 && !buddyTracker.hasBuddy()) {
      return;
    }

    rc.setIndicatorString(2, "Sitting by turret buddy " + buddyTracker.getBuddy());

    if (buddyTracker.hasBuddy()) { // Spotting
      if (rc.canSenseRobot(buddyTracker.getBuddy())) { // Turret still alive and
                                                       // needing
        // spotting
        RobotInfo turret = rc.senseRobot(buddyTracker.getBuddy());
        if (!radar.shouldStayNextToTurret(rc, turret.location)) {
          buddyTracker.setBuddy(-1);
        } else {
          rc.setIndicatorString(2, "Sitting by turret buddy " + buddyTracker.getBuddy());
          if (rc.getLocation().distanceSquaredTo(turret.location) > 2) {
            navigation.directTo(rc, turret.location, true /* avoidAttackers */,
                false /* clearRubble */,
                false /* onlyForward */);
          }
          return;
        }
      } else {
        buddyTracker.setBuddy(-1);
      }
    }

    Signal[] messages = basicMessages.getAllyBasicMessages();
    RobotInfo closestTurretNeedingBuddy = null;
    for (int i = 0; i < messages.length; i++) {
      if (rc.canSenseLocation(messages[i].getLocation())) {
        RobotInfo robotInfo = rc.senseRobotAtLocation(messages[i].getLocation());
        if (robotInfo == null) {
          continue;
        }
        int dist = robotInfo.location.distanceSquaredTo(rc.getLocation());
        if (robotInfo.type == RobotType.TURRET &&
            (closestTurretNeedingBuddy == null || dist < closestTurretNeedingBuddy.location
                .distanceSquaredTo(rc.getLocation()))) {
          closestTurretNeedingBuddy = robotInfo;
          break;
        }
      }
    }

    rc.setIndicatorString(2, "Closest turret buddy " + closestTurretNeedingBuddy);
    if (closestTurretNeedingBuddy != null) {
      rc.setIndicatorString(2, "Going to turret buddy " + closestTurretNeedingBuddy.ID);
      navigation.directTo(rc, closestTurretNeedingBuddy.location, true /* avoidAttackers */,
          false /* clearRubble */,
          false /* onlyForward */); // Go to turret
      // Maybe don't need these, just the extended broadcast
      if (rc.getLocation().distanceSquaredTo(closestTurretNeedingBuddy.location) <= 2) {
        buddyTracker.setBuddy(closestTurretNeedingBuddy.ID);
      }
      return;
    }

    if (!zombieDragger.dragZombies(rc))

    {
      MapLocation target = explorationCalculator.calculate(rc, patrolWaypointCalculator);
      navigation.directTo(rc, target, true /* avoidAttackers */, false /* clearRubble */,
          false /* onlyForward */);
    }

    enemyTurretCache.showDebugInfo(rc);

  }

  private void shareExtendedBroadcast(RobotController rc, MessageSender sender)
      throws GameActionException {
    RobotInfo broadcastedRobot = null;
    if (buddyTracker.hasBuddy() && rc.canSenseRobot(buddyTracker.getBuddy())) {
      broadcastedRobot = radar.getBuddyTarget(rc, rc.senseRobot(buddyTracker.getBuddy()));
    } else {
      RobotInfo closestTurret = radar.getClosestFriendlyTurret();
      if (closestTurret != null) {
        broadcastedRobot = radar.getBuddyTarget(rc, closestTurret);
      }
    }
    rc.setIndicatorString(0, "Broadcasting closest robot " + broadcastedRobot + "round num " + rc
        .getRoundNum());
    if (broadcastedRobot != null) {
      sender.sendEnemyInfo(rc, broadcastedRobot);
    }
  }
}