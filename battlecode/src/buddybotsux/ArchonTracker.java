package buddybotsux;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class ArchonTracker {

  private static final int MAX_ARCHONS = 100;
  private static final int STALE_ARCHON_ROUNDS = 100;

  public class ArchonStatus {
    public MapLocation lastKnownLocation;
    public boolean notFoundAtLocation;
    public int timestamp;
    public int id;
    public MapLocation target;

    public ArchonStatus(MapLocation lastKnownLocation, MapLocation target, int timestamp, int id) {
      this.lastKnownLocation = lastKnownLocation;
      this.notFoundAtLocation = false;
      this.timestamp = timestamp;
      this.id = id;
      this.target = target;
    }
  }

  private ArchonStatus[] alliedArchons;
  private ArchonStatus[] enemyArchons;
  private int numAlliedArchons;
  private int numEnemyArchons;
  private int shareIndex;

  private ArchonStatus closestAlliedArchon;
  private ArchonStatus closetEnemyArchon;

  public ArchonTracker() {
    alliedArchons = new ArchonStatus[MAX_ARCHONS];
    enemyArchons = new ArchonStatus[MAX_ARCHONS];
    numAlliedArchons = 0;
    numEnemyArchons = 0;
  }

  public void updateClosest(RobotController rc) {
    int closestDist = Integer.MAX_VALUE;
    closestAlliedArchon = null;
    for (int i = numAlliedArchons; --i >= 0;) {
      if (!alliedArchons[i].notFoundAtLocation) {
        int dist = rc.getLocation().distanceSquaredTo(alliedArchons[i].lastKnownLocation);
        if (dist < closestDist) {
          closestDist = dist;
          closestAlliedArchon = alliedArchons[i];
        }
      }
    }

    closestDist = Integer.MAX_VALUE;
    closetEnemyArchon = null;
    for (int i = numEnemyArchons; --i >= 0;) {
      if (!enemyArchons[i].notFoundAtLocation) {
        int dist = rc.getLocation().distanceSquaredTo(enemyArchons[i].lastKnownLocation);
        if (dist < closestDist) {
          closestDist = dist;
          closetEnemyArchon = enemyArchons[i];
        }
      }
    }
  }

  public ArchonStatus getClosestAlliedArchon() {
    return closestAlliedArchon;
  }

  public ArchonStatus getClosestEnemyArchon() {
    return closetEnemyArchon;
  }

  public ArchonStatus[] getAlliedArchons(RobotController rc) {
    // Clear old archons (TODO cache that this has been done?)
    int index = 0;
    while (index < numAlliedArchons) {
      ArchonStatus archon = alliedArchons[index];
      if (rc.getRoundNum() - archon.timestamp > STALE_ARCHON_ROUNDS) {
        numAlliedArchons--;
        alliedArchons[index] = alliedArchons[numAlliedArchons];
        alliedArchons[numAlliedArchons] = null;
      } else {
        index++;
      }
    }

    ArchonStatus[] archons = new ArchonStatus[numAlliedArchons];
    for (int i = 0; i < numAlliedArchons; i++) {
      archons[i] = alliedArchons[i];
    }
    return archons;
  }

  public ArchonStatus[] getEnemyArchons(RobotController rc) {
    // Clear old archons (TODO cache that this has been done?)
    int index = 0;
    while (index < numEnemyArchons) {
      ArchonStatus archon = enemyArchons[index];
      if (rc.getRoundNum() - archon.timestamp > STALE_ARCHON_ROUNDS) {
        numEnemyArchons--;
        enemyArchons[index] = enemyArchons[numEnemyArchons];
        enemyArchons[numEnemyArchons] = null;
      } else {
        index++;
      }
    }
    ArchonStatus[] archons = new ArchonStatus[numEnemyArchons];
    for (int i = 0; i < numEnemyArchons; i++) {
      archons[i] = enemyArchons[i];
    }
    return archons;
  }

  public boolean isIdEnemyArchon(int id) {
    for (int i = 0; i < numEnemyArchons; i++) {
      if (enemyArchons[i].id == id) {
        return true;
      }
    }
    return false;
  }

  public void processArchon(int id, MapLocation loc, MapLocation target, int timestamp,
      boolean alliedArchon) {
    ArchonStatus[] archonList = alliedArchon ? alliedArchons : enemyArchons;
    int numArchons = alliedArchon ? numAlliedArchons : numEnemyArchons;
    for (int i = 0; i < numArchons; i++) {
      if (archonList[i].id == id) {
        if (archonList[i].timestamp > timestamp) {
          return;
        }
        archonList[i].lastKnownLocation = loc;
        archonList[i].notFoundAtLocation = false;
        if (target != null) {
          archonList[i].target = target;
        }
        archonList[i].timestamp = timestamp;
        return;
      }
    }
    if (numArchons > archonList.length) {
      return;
    }
    // A new archon, how exciting! Adding to list
    archonList[numArchons] = new ArchonStatus(loc, target, timestamp, id);
    if (alliedArchon) {
      numAlliedArchons++;
    } else {
      numEnemyArchons++;
    }
  }

  public void shareRandomArchonInformation(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    // TODO Targets!
    int totalArchons = numAlliedArchons + numEnemyArchons;
    if (totalArchons == 0) {
      return;
    }

    shareIndex = shareIndex % totalArchons;
    ArchonStatus archon;
    if (shareIndex < numAlliedArchons) {
      archon = alliedArchons[shareIndex];
      messageSender.sendArchonLocation(rc,
          archon.lastKnownLocation,
          archon.timestamp,
          archon.id,
          true /* alliedArchon */);
    } else {
      archon = enemyArchons[shareIndex - numAlliedArchons];
      messageSender.sendArchonLocation(rc,
          archon.lastKnownLocation,
          archon.timestamp,
          archon.id,
          false /* alliedArchon */);
    }
    shareIndex++;
  }

  public void updateSelfInformation(RobotController rc, MapLocation target) {
    processArchon(rc.getID(), rc.getLocation(), target, rc.getRoundNum(), true /* alliedArchon */);
  }

  public void showDebugInfo(RobotController rc) {
    int[] enemyArchonColor = new int[] {
      100, 0, 0
    };
    int[] alliedArchonColor = new int[] {
      0, 0, 100
    };

    MapLocation myLoc = rc.getLocation();
    ArchonStatus[] enemyArchons = getEnemyArchons(rc);
    for (int i = 0; i < enemyArchons.length; i++) {
      rc.setIndicatorLine(
          myLoc, enemyArchons[i].lastKnownLocation, enemyArchonColor[0],
          enemyArchonColor[1],
          enemyArchonColor[2]);
    }

    ArchonStatus[] alliedArchons = getAlliedArchons(rc);
    for (int i = 0; i < alliedArchons.length; i++) {
      rc.setIndicatorLine(
          myLoc, alliedArchons[i].lastKnownLocation, alliedArchonColor[0],
          alliedArchonColor[1],
          alliedArchonColor[2]);
    }
  }
}