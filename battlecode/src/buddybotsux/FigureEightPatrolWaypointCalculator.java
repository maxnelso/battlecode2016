package buddybotsux;

import battlecode.common.MapLocation;

public class FigureEightPatrolWaypointCalculator implements PatrolWaypointCalculator {

  @Override
  public MapLocation[] calculate(int minX, int maxX, int minY, int maxY) {
    return new MapLocation[] {
      computePatrolLoc(minX, maxX, minY, maxY, 1, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 3, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 5, 1),
      computePatrolLoc(minX, maxX, minY, maxY, 5, 3),
      computePatrolLoc(minX, maxX, minY, maxY, 3, 3),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 3),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 5),
      computePatrolLoc(minX, maxX, minY, maxY, 3, 5),
      computePatrolLoc(minX, maxX, minY, maxY, 5, 5),
      computePatrolLoc(minX, maxX, minY, maxY, 5, 3),
      computePatrolLoc(minX, maxX, minY, maxY, 3, 3),
      computePatrolLoc(minX, maxX, minY, maxY, 1, 3)
    };
  }

  private MapLocation computePatrolLoc(
      int minX, int maxX, int minY, int maxY, int maxXWeight, int maxYWeight) {
    return new MapLocation(
        ((6 - maxXWeight) * minX + maxXWeight * maxX) / 6,
        ((6 - maxYWeight) * minY + maxYWeight * maxY) / 6);
  }
}
