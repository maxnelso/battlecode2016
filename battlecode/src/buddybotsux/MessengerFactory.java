package buddybotsux;

import battlecode.common.RobotController;

public class MessengerFactory {

  public static Messenger create(
      RobotController rc,
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator,
      BasicMessages basicMessages,
      BuddyTracker buddyTracker,
      EnemyTurretCache enemyTurretCache) {
    boolean subscribeToArchons = false;
    boolean subscribeToDens = false;
    boolean subscribeToParts = false;
    boolean subscribeToNeutralRobots = false;
    boolean subscribeToMapBoundaries = false;
    boolean subscribeToEnemyInfo = false;
    boolean subscribeToEnemyTurrets = false;
    boolean subscribeToBuddies = false;
    switch (rc.getType()) {
      case ARCHON:
      case SCOUT:
        subscribeToArchons = true;
        subscribeToDens = true;
        subscribeToParts = true;
        subscribeToNeutralRobots = true;
        subscribeToMapBoundaries = true;
        subscribeToEnemyTurrets = true;
        subscribeToBuddies = true;
        break;
      case SOLDIER:
      case GUARD:
      case VIPER:
      case TURRET:
      case TTM:
        subscribeToDens = true;
        subscribeToMapBoundaries = true;
        subscribeToEnemyInfo = true;
        subscribeToArchons = true;
        break;
      default:
        break;
    }

    return new Messenger(
        interestingTargets,
        archonTracker,
        radar,
        mapBoundaryCalculator,
        basicMessages,
        buddyTracker,
        enemyTurretCache,
        subscribeToArchons,
        subscribeToDens,
        subscribeToParts,
        subscribeToNeutralRobots,
        subscribeToMapBoundaries,
        subscribeToEnemyInfo,
        subscribeToBuddies,
        subscribeToEnemyTurrets);
  }
}
