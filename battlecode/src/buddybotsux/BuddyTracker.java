package buddybotsux;

public class BuddyTracker {

  private int buddy;

  public BuddyTracker() {
    buddy = -1;
  }

  public boolean hasBuddy() {
    return buddy != -1;
  }

  public void setBuddy(int buddy) {
    this.buddy = buddy;
  }

  public int getBuddy() {
    return buddy;
  }
}
