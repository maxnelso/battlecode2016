package buddybotsux;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class RadarAttackRepairSystem implements AttackSystem, RepairSystem {

  private final Radar radar;

  public RadarAttackRepairSystem(Radar radar) {
    this.radar = radar;
  }

  @Override
  public MapLocation attackLeastHealthEnemy(RobotController rc) throws GameActionException {
    return attackAttackersThenNonAttackers(
        rc,
        radar.getLeastHealthEnemyAttackerInAttackRange(),
        radar.getLeastHealthEnemyInAttackRange());
  }

  @Override
  public MapLocation attackLeastHealthZombie(RobotController rc) throws GameActionException {
    return attackAttackersThenNonAttackers(
        rc,
        radar.getLeastHealthZombieAttackerInAttackRange(),
        radar.getLeastHealthZombieInAttackRange());
  }

  private MapLocation attackAttackersThenNonAttackers(
      RobotController rc, MapLocation attacker, MapLocation nonAttacker)
          throws GameActionException {
    MapLocation target = attacker == null ? nonAttacker : attacker;
    if (rc.isWeaponReady() && target != null && rc.canAttackLocation(target)) {
      rc.attackLocation(target);
    }

    return target;
  }

  @Override
  public MapLocation repairLeastHealthAlly(RobotController rc) throws GameActionException {
    MapLocation ally = radar.getLeastHealthAllyInRepairRange();

    // TODO: need better filtering system to remove archons from healing targets
    if (ally != null && rc.senseRobotAtLocation(ally).type != RobotType.ARCHON &&
        rc.getLocation().distanceSquaredTo(ally) < RobotType.ARCHON.attackRadiusSquared) {
      rc.repair(ally);
      rc.setIndicatorLine(rc.getLocation(), ally, 0, 255, 100);
    }

    return ally;
  }

  @Override
  public MapLocation repairMostHealthAllyThatNeedsRepairing(RobotController rc)
      throws GameActionException {
    MapLocation ally = radar.getMostHealthAllyWhoNeedsRepaired();

    // TODO: need better filtering system to remove archons from healing targets
    if (ally != null && rc.senseRobotAtLocation(ally).type != RobotType.ARCHON &&
        rc.getLocation().distanceSquaredTo(ally) < RobotType.ARCHON.attackRadiusSquared) {
      rc.repair(ally);
      rc.setIndicatorLine(rc.getLocation(), ally, 0, 255, 100);
    }

    return ally;
  }

  @Override
  public MapLocation attackLeastTurnsToKillEnemy(RobotController rc) throws GameActionException {
    MapLocation target = radar.getBestEnemyToShoot(rc);
    if (rc.isWeaponReady() && target != null && rc.canAttackLocation(target)) {
      rc.attackLocation(target);
    }
    return target;
  }

  @Override
  public MapLocation attackAnyoneInSensorRange(RobotController rc) throws GameActionException {
    RobotInfo[] robots = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam()
        .opponent());
    if (robots != null & robots.length > 0) {
      if (rc.isWeaponReady() && rc.canAttackLocation(robots[0].location)) {
        rc.attackLocation(robots[0].location);
        return robots[0].location;
      }
    }
    robots = rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.ZOMBIE);
    if (robots != null & robots.length > 0) {
      if (rc.isWeaponReady() && rc.canAttackLocation(robots[0].location)) {
        rc.attackLocation(robots[0].location);
        return robots[0].location;
      }
    }
    return null;
  }

}
