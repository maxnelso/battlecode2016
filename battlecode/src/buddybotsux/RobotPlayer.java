package buddybotsux;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {
  public static void run(RobotController rc) {
    ArchonTracker archonTracker = new ArchonTracker();
    PartScanner partScanner = new PartScanner();
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache();
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator();
    InterestingTargets interestingTargets = new InterestingTargets(enemyTurretCache);
    Radar radar = new DefaultRadar(interestingTargets, archonTracker, enemyTurretCache);
    RadarAttackRepairSystem attackRepairSystem = new RadarAttackRepairSystem(radar);
    DuckNavigationSystem navigation = new DuckNavigationSystem(radar, enemyTurretCache);
    ExplorationCalculator explorationCalculator = new ExplorationCalculator(mapBoundaryCalculator);
    ZombieDragger zombieDragger = new ZombieDragger(radar, navigation, archonTracker);
    BasicMessages basicMessages = new DefaultBasicMessages();
    BuddyTracker buddyTracker = new BuddyTracker();
    TurretSpawner turretSpawner = new TurretSpawner();
    Messenger messenger = MessengerFactory.create(
        rc,
        interestingTargets,
        archonTracker,
        radar,
        mapBoundaryCalculator,
        basicMessages,
        buddyTracker,
        enemyTurretCache);

    // BytecodeProfiler profiler = rc.getType() == RobotType.SOLDIER
    // ? new DefaultBytecodeProfiler(rc)
    // : new NoOpBytecodeProfiler();
    BytecodeProfiler profiler = new NoOpBytecodeProfiler();

    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior(
            navigation,
            radar,
            attackRepairSystem /* repairSystem */,
            messenger /* messageSender */,
            interestingTargets,
            explorationCalculator,
            mapBoundaryCalculator,
            archonTracker,
            partScanner);
        break;
      case SOLDIER:
        behavior = new SoldierBehavior(
            navigation,
            attackRepairSystem /* attackSystem */,
            mapBoundaryCalculator,
            explorationCalculator,
            radar,
            interestingTargets,
            archonTracker,
            enemyTurretCache,
            profiler);
        break;
      case VIPER:
      case GUARD:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
        behavior = new ShootBehavior(navigation, attackRepairSystem /* attackSystem */);
        break;
      case SCOUT:
        behavior = new ScoutBehavior(
            rc.getID(),
            navigation,
            radar,
            messenger /* messageSender */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator,
            archonTracker,
            basicMessages,
            zombieDragger,
            partScanner,
            buddyTracker,
            rc.getRoundNum(),
            enemyTurretCache);
        break;
      case TURRET:
      case TTM:
        behavior = new TurretBehavior(
            navigation,
            radar,
            attackRepairSystem /* attackSystem */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator);
        break;
      case ZOMBIEDEN:
      default:
        behavior = new NoOpBehavior();
        break;
    }

    while (true) {
      try {
        profiler.start();
        int currentRound = rc.getRoundNum();
        messenger.receiveMessages(
            rc, behavior.getMessagingBytecodeLimit(rc));
        profiler.split("after receiving messages");
        behavior.behave(rc);
        profiler.end();
        profiler.printToConsole();
        if (rc.getRoundNum() != currentRound) {
          System.out.println("Over bytecode limit using " +
              ((rc.getRoundNum() - currentRound) *
                  rc.getType().bytecodeLimit + Clock.getBytecodeNum()) + " this step");
        }
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
