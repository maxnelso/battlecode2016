package buddybotsux;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class DuckNavigationSystem implements NavigationSystem {

  private static final float CLEAR_RUBBLE_THRESHOLD = 40000;

  private final Radar radar;
  private final EnemyTurretCache enemyTurretCache;

  private MapLocation bugDestination;
  private BugState bugState;
  private WallSide bugWallSide;
  private int bugStartDistSq;
  private Direction bugLastMoveDir;
  private Direction bugLookStartDir;
  private int bugRotationCount;
  private int bugMovesSinceSeenObstacle;
  private int[] cachedNumEnemiesAttackingMoveDirs;

  private enum BugState {
    DIRECT,
    BUG
  }

  private enum WallSide {
    LEFT,
    RIGHT
  }

  public DuckNavigationSystem(Radar radar, EnemyTurretCache enemyTurretCache) {
    this.radar = radar;
    this.enemyTurretCache = enemyTurretCache;
    bugState = BugState.DIRECT;
    bugMovesSinceSeenObstacle = 0;
    bugWallSide = Math.random() < .5 ? WallSide.LEFT : WallSide.RIGHT;
  }

  @Override
  public boolean directTo(
      RobotController rc,
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble,
      boolean onlyForward) throws GameActionException {
    boolean success = directTowards(rc, loc, getPolicy(avoidAttackers), clearRubble, onlyForward);
    if (!success && avoidAttackers) {
      MapLocation closestEnemyZombie = radar.getClosestEnemyOrZombie();
      if (closestEnemyZombie != null) {
        success = retreatFrom(rc);
      }
    }

    return success;
  }

  @Override
  public boolean directToWithMaximumEnemyExposure(
      RobotController rc,
      MapLocation loc,
      int maximumEnemyExposure) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    int[] numEnemiesAttackingDirs = radar.getNumEnemiesAttackingMoveDirs(rc);

    Direction toEnemy = rc.getLocation().directionTo(loc);
    Direction[] tryDirs = new Direction[] {
      toEnemy, toEnemy.rotateLeft(), toEnemy.rotateRight()
    };
    for (int i = 0; i < tryDirs.length; i++) {
      Direction tryDir = tryDirs[i];
      // numEnemiesAttackingDirs[tryDir.ordinal()]));
      if (!rc.canMove(tryDir))
        continue;
      rc.setIndicatorString(1, String.format(
          "moving toward %s with max enemy exposure %d (actual exposure %d)", loc.toString(),
          maximumEnemyExposure, numEnemiesAttackingDirs[tryDir.ordinal()]));
      if (numEnemiesAttackingDirs[tryDir.ordinal()] > maximumEnemyExposure)
        continue;
      rc.move(tryDir);
      return true;
    }
    return false;
  }

  @Override
  public boolean retreatFrom(RobotController rc) throws GameActionException {
    Direction d = radar.getBestRetreatDirection(rc);
    if (d == null) {
      return false;
    }
    return directTo(rc, rc.getLocation().add(d),
        false /* avoidAttackers */,
        false /* clearRubble */,
        false /* onlyForward */);
  }

  @Override
  public boolean retreatFromZombies(RobotController rc) throws GameActionException {
    Direction d = radar.getBestRetreatDirectionZombies(rc);
    if (d == null) {
      return false;
    }
    return directTo(rc, rc.getLocation().add(d),
        false /* avoidAttackers */,
        false /* clearRubble */,
        false /* onlyForward */);
  }

  @Override
  public boolean moveRandomly(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }
    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 0; i < 8; i++) {
      if (rc.canMove(d)) {
        rc.move(d);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }

  @Override
  public boolean bugTo(
      RobotController rc,
      MapLocation loc,
      boolean avoidAttackers,
      boolean clearRubble) throws GameActionException {
    return bugTowards(rc, loc, getPolicy(avoidAttackers), clearRubble);
  }

  private boolean directTowards(
      RobotController rc,
      MapLocation destination,
      NavigationSafetyPolicy policy,
      boolean clearRubble,
      boolean onlyForward) throws GameActionException {

    Direction[] dirs = onlyForward ? getForwardDirections(rc, destination)
        : getAllDirections(rc, destination);

    for (Direction dir : dirs) {
      if (safeToMove(rc, dir, policy, clearRubble)) {
        move(rc, dir);
        return true;
      }
    }

    return false;
  }

  private boolean safeToMove(RobotController rc,
      Direction dir,
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {

    boolean policySafe = policy.isSafeToMoveTo(rc.getLocation().add(dir));
    if (!policySafe) {
      return false;
    }
    if (rc.canMove(dir)) {
      return true;
    }
    if (clearRubble && rc.onTheMap(rc.getLocation().add(dir)) &&
        // TODO Maybe this should be in the radar?
        rc.senseRobotAtLocation(rc.getLocation().add(dir)) == null) {
      return rc.senseRubble(rc.getLocation().add(dir)) < CLEAR_RUBBLE_THRESHOLD;
    }
    return false;
  }

  private void move(RobotController rc, Direction dir) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }
    if (rc.canMove(dir)) {
      rc.move(dir);
    } else {
      rc.clearRubble(dir);
    }
  }

  private Direction[] getForwardDirections(RobotController rc, MapLocation destination) {
    Direction d = rc.getLocation().directionTo(destination);
    return Math.random() < 0.5
        ? new Direction[] {
          d,
          d.rotateRight(),
          d.rotateLeft(),
    }
        : new Direction[] {
          d,
          d.rotateRight(),
          d.rotateLeft(),
    };
  }

  private Direction[] getAllDirections(RobotController rc, MapLocation destination) {
    Direction d = rc.getLocation().directionTo(destination);
    return Math.random() < 0.5
        ? new Direction[] {
          d,
          d.rotateRight(),
          d.rotateLeft(),
          d.rotateRight().rotateRight(),
          d.rotateLeft().rotateLeft(),
          d.opposite().rotateLeft(),
          d.opposite().rotateRight(),
          d.opposite()
    }
        : new Direction[] {
          d,
          d.rotateRight(),
          d.rotateLeft(),
          d.rotateRight().rotateRight(),
          d.rotateLeft().rotateLeft(),
          d.opposite().rotateLeft(),
          d.opposite().rotateRight(),
          d.opposite()
    };
  }

  private NavigationSafetyPolicy getPolicy(boolean avoidAttackers) {
    return avoidAttackers
        ? new AvoidAttackingUnitsPolicy(radar, enemyTurretCache)
        : new NoSafetyPolicy();
  }

  private boolean bugTowards(RobotController rc,
      MapLocation dest,
      NavigationSafetyPolicy policy,
      boolean clearRubble) throws GameActionException {
    if (!dest.equals(bugDestination)) {
      bugDestination = dest;
      bugState = BugState.DIRECT;
    }

    if (rc.getLocation().equals(dest)) {
      return false;
    }

    return bugMove(rc, policy, clearRubble);
  }

  private void startBug(RobotController rc, NavigationSafetyPolicy policy, boolean clearRubble)
      throws GameActionException {
    bugStartDistSq = rc.getLocation().distanceSquaredTo(bugDestination);
    bugLastMoveDir = rc.getLocation().directionTo(bugDestination);
    bugLookStartDir = rc.getLocation().directionTo(bugDestination);
    bugRotationCount = 0;
    bugMovesSinceSeenObstacle = 0;

    if (bugWallSide == null) {
      // try to intelligently choose on which side we will keep the wall
      Direction leftTryDir = bugLastMoveDir.rotateLeft();
      for (int i = 0; i < 3; i++) {
        if (!safeToMove(rc, leftTryDir, policy, clearRubble))
          leftTryDir = leftTryDir.rotateLeft();
        else
          break;
      }
      Direction rightTryDir = bugLastMoveDir.rotateRight();
      for (int i = 0; i < 3; i++) {
        if (!safeToMove(rc, rightTryDir, policy, clearRubble))
          rightTryDir = rightTryDir.rotateRight();
        else
          break;
      }
      if (bugDestination.distanceSquaredTo(rc.getLocation().add(leftTryDir)) < bugDestination
          .distanceSquaredTo(rc.getLocation().add(rightTryDir))) {
        bugWallSide = WallSide.RIGHT;
      } else {
        bugWallSide = WallSide.LEFT;
      }
    }
  }

  private Direction findBugMoveDir(RobotController rc, NavigationSafetyPolicy policy,
      boolean clearRubble)
          throws GameActionException {
    bugMovesSinceSeenObstacle++;
    Direction dir = bugLookStartDir;
    for (int i = 8; i-- > 0;) {
      if (safeToMove(rc, dir, policy, clearRubble))
        return dir;
      dir = (bugWallSide == WallSide.LEFT ? dir.rotateRight() : dir.rotateLeft());
      bugMovesSinceSeenObstacle = 0;
    }
    return null;
  }

  private int numRightRotations(Direction start, Direction end) {
    return (end.ordinal() - start.ordinal() + 8) % 8;
  }

  private int numLeftRotations(Direction start, Direction end) {
    return (-end.ordinal() + start.ordinal() + 8) % 8;
  }

  private int calculateBugRotation(Direction moveDir) {
    if (bugWallSide == WallSide.LEFT) {
      return numRightRotations(bugLookStartDir, moveDir) - numRightRotations(bugLookStartDir,
          bugLastMoveDir);
    } else {
      return numLeftRotations(bugLookStartDir, moveDir) - numLeftRotations(bugLookStartDir,
          bugLastMoveDir);
    }
  }

  private boolean bugMove(RobotController rc, Direction dir) throws GameActionException {
    move(rc, dir);
    bugRotationCount += calculateBugRotation(dir);
    bugLastMoveDir = dir;
    if (bugWallSide == WallSide.LEFT) {
      bugLookStartDir = dir.rotateLeft().rotateLeft();
    } else {
      bugLookStartDir = dir.rotateRight().rotateRight();
    }
    return true;
  }

  private boolean detectBugIntoEdge(RobotController rc, Direction proposedMoveDir)
      throws GameActionException {
    if (proposedMoveDir == null) {
      return false;
    }
    if (bugWallSide == WallSide.LEFT) {
      return !rc.onTheMap(rc.getLocation().add(proposedMoveDir.rotateLeft()));
    } else {
      return !rc.onTheMap(rc.getLocation().add(proposedMoveDir.rotateRight()));
    }
  }

  private void reverseBugWallFollowDir(RobotController rc, NavigationSafetyPolicy policy,
      boolean clearRubble)
          throws GameActionException {
    bugWallSide = (bugWallSide == WallSide.LEFT ? WallSide.RIGHT : WallSide.LEFT);
    startBug(rc, policy, clearRubble);
  }

  private boolean bugTurn(RobotController rc, NavigationSafetyPolicy policy, boolean clearRubble)
      throws GameActionException {
    Direction dir = findBugMoveDir(rc, policy, clearRubble);
    if (detectBugIntoEdge(rc, dir)) {
      reverseBugWallFollowDir(rc, policy, clearRubble);
      dir = findBugMoveDir(rc, policy, clearRubble);
    }
    if (dir != null) {
      return bugMove(rc, dir);
    }

    return false;
  }

  private boolean canEndBug(RobotController rc) {
    if (bugMovesSinceSeenObstacle >= 4)
      return true;
    return (bugRotationCount <= 0 || bugRotationCount >= 8) && rc.getLocation().distanceSquaredTo(
        bugDestination) <= bugStartDistSq;
  }

  private boolean bugMove(RobotController rc, NavigationSafetyPolicy policy, boolean clearRubble)
      throws GameActionException {
    // Check if we can stop bugging at the *beginning* of the turn
    if (bugState == BugState.BUG) {
      if (canEndBug(rc)) {
        bugState = BugState.DIRECT;
      }
    }

    // If DIRECT mode, try to go directly to target
    if (bugState == BugState.DIRECT) {
      if (!directTowards(rc, bugDestination, policy, clearRubble, false)) {
        bugState = BugState.BUG;
        startBug(rc, policy, clearRubble);
      } else {
        return true;
      }
    }

    // If that failed, or if bugging, bug
    if (bugState == BugState.BUG) {
      return bugTurn(rc, policy, clearRubble);
    }

    return false;
  }

}