package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public interface MessageSender {
  public void sendArchonLocation(RobotController rc,
      MapLocation location,
      int timestamp,
      int id,
      boolean alliedArchon)
          throws GameActionException;

  public void sendDenLocation(
      RobotController rc,
      MapLocation loc) throws GameActionException;

  public void sendRemoveDenLocation(
      RobotController rc,
      MapLocation loc) throws GameActionException;

  public void sendPartLocation(
      RobotController rc,
      MapLocation loc,
      int numParts) throws GameActionException;

  public void sendRemovePartLocation(
      RobotController rc,
      MapLocation loc) throws GameActionException;

  public void sendNeutralRobotLocation(
      RobotController rc,
      MapLocation loc,
      RobotType robotType) throws GameActionException;

  public void sendRemoveNeutralRobotLocation(
      RobotController rc,
      MapLocation loc) throws GameActionException;

  public void sendEnemyTurret(
      RobotController rc,
      MapLocation turretLoc,
      int timestamp) throws GameActionException;

  public void sendRemoveEnemyTurret(
      RobotController rc,
      MapLocation turretLoc,
      int timestamp) throws GameActionException;

  public void sendEnemyInfo(
      RobotController rc,
      RobotInfo robotInfo) throws GameActionException;

  public void sendMinXBoundary(RobotController rc, int minX) throws GameActionException;

  public void sendMinYBoundary(RobotController rc, int minY) throws GameActionException;

  public void sendMinLocBoundary(
      RobotController rc, int minX, int minY) throws GameActionException;

  public void sendMaxXBoundary(RobotController rc, int maxX) throws GameActionException;

  public void sendMaxYBoundary(RobotController rc, int maxY) throws GameActionException;

  public void sendMaxLocBoundary(
      RobotController rc, int maxX, int maxY) throws GameActionException;

  public void sendTimingAttack(
      RobotController rc,
      int startRound,
      int endRound,
      MapLocation rally) throws GameActionException;
}
