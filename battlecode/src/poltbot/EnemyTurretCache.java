package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface EnemyTurretCache {

  public void reportRobotInfo(RobotController rc, RobotInfo robot);

  public void reportEnemyTurretPresent(MapLocation loc, int timestamp);

  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp);

  public boolean isInEnemyTurretRange(MapLocation loc);

  public void invalidatePresentEnemyTurrets(RobotController rc) throws GameActionException;

  public void shareRandomEnemyTurret(
      RobotController rc, MessageSender messageSender) throws GameActionException;

  public void showDebugInfo(RobotController rc) throws GameActionException;

  // TODO(jven): Consider removing the methods below if they are no longer
  // needed for timing attack computations.
  public int getNumTurrets();

  public MapLocation getATurret();
}
