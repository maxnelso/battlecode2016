package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;
import poltbot.EeHanTiming.TimingAttack;

public class ScoutBehavior implements Behavior {

  private static final int MAP_BOUNDARY_SHARE_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 7500;
  private static final int MESSAGING_BYTECODE_LIMIT = 7500;
  private static final int TIMING_ATTACK_MIN_SUPPLY = 75;
  private static final int TIMING_ATTACK_MIN_TURRETS = 10;
  private static final int TIMING_ATTACK_RALLY_TIME = 200;
  private static final int TIMING_ATTACK_DURATION = 100;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final ArchonTracker archonTracker;
  private final ZombieDragger zombieDragger;
  private final PartScanner partScanner;
  private final EnemyTurretCache enemyTurretCache;
  private final BasicMessages basicMessages;
  private final EeHanTiming eeHanTiming;
  private final PatrolWaypointCalculator patrolWaypointCalculator;
  private int turretBuddyId;

  public ScoutBehavior(
      int robotId,
      NavigationSystem navigation,
      Radar radar,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      ArchonTracker archonTracker,
      BasicMessages basicMessages,
      ZombieDragger zombieDragger,
      PartScanner partScanner,
      EnemyTurretCache enemyTurretCache,
      EeHanTiming eeHanTiming) {
    this.navigation = navigation;
    this.radar = radar;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.archonTracker = archonTracker;
    this.basicMessages = basicMessages;
    this.zombieDragger = zombieDragger;
    this.partScanner = partScanner;
    this.enemyTurretCache = enemyTurretCache;
    this.eeHanTiming = eeHanTiming;
    this.turretBuddyId = -1;
    patrolWaypointCalculator = new ReflectingPatrolWaypointCalculator(
        new LawnMowerPatrolWaypointCalculator(
            7 /* laneHalfWidth */, 5 /* topBottomMargin */),
        (robotId & 2) == 0 /* flipX */,
        (robotId & 4) == 0 /* flipY */);
  }

  private boolean shouldAvoidAttackers(int roundNum) {
    TimingAttack timingAttack = eeHanTiming.getTimingAttack();
    return timingAttack == null
        || roundNum < timingAttack.startRound
        || roundNum >= timingAttack.endRound;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    partScanner.scanForParts(rc, interestingTargets);
    mapBoundaryCalculator.update(rc);
    enemyTurretCache.invalidatePresentEnemyTurrets(rc);

    if ((rc.getRoundNum() - rc.getID()) % MAP_BOUNDARY_SHARE_FREQUENCY == 0) {
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }

    if (enemyTurretCache.getNumTurrets() >= TIMING_ATTACK_MIN_TURRETS
        && rc.getRobotCount() >= TIMING_ATTACK_MIN_SUPPLY) {
      eeHanTiming.reportTimingAttack(
          rc,
          rc.getRoundNum() + TIMING_ATTACK_RALLY_TIME,
          rc.getRoundNum() + TIMING_ATTACK_RALLY_TIME + TIMING_ATTACK_DURATION,
          enemyTurretCache.getATurret());
    }

    interestingTargets.shareRandomTarget(rc, messageSender);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
    enemyTurretCache.shareRandomEnemyTurret(rc, messageSender);
    eeHanTiming.shareTimingAttack(rc, messageSender);

    shareExtendedBroadcast(rc, messageSender);

    if (turretBuddyId != -1) { // Spotting
      if (rc.canSenseRobot(turretBuddyId)) { // Turret still alive and needing
                                             // spotting
        RobotInfo turret = rc.senseRobot(turretBuddyId);
        if (!radar.shouldStayNextToTurret(rc, turret.location)) {
          turretBuddyId = -1;
        } else {
          rc.setIndicatorString(2, "Sitting by turret buddy " + turretBuddyId);
          if (rc.getLocation().distanceSquaredTo(turret.location) > 2) {
            navigation.directTo(
                rc,
                turret.location,
                shouldAvoidAttackers(rc.getRoundNum()),
                false /* clearRubble */,
                false /* onlyForward */);
          }
          return;
        }
      } else {
        turretBuddyId = -1;
      }
    }

    Signal[] messages = basicMessages.getAllyBasicMessages();
    RobotInfo closestTurretNeedingBuddy = null;
    for (int i = 0; i < messages.length; i++) {
      if (rc.canSenseLocation(messages[i].getLocation())) {
        RobotInfo robotInfo = rc.senseRobotAtLocation(messages[i].getLocation());
        if (robotInfo == null) {
          continue;
        }
        int dist = robotInfo.location.distanceSquaredTo(rc.getLocation());
        if (robotInfo.type == RobotType.TURRET &&
            (closestTurretNeedingBuddy == null || dist < closestTurretNeedingBuddy.location
                .distanceSquaredTo(rc.getLocation()))) {
          closestTurretNeedingBuddy = robotInfo;
          break;
        }
      }
    }

    rc.setIndicatorString(2, "Closest turret buddy " + closestTurretNeedingBuddy);
    if (closestTurretNeedingBuddy != null) {
      rc.setIndicatorString(2, "Going to turret buddy " + closestTurretNeedingBuddy.ID);
      navigation.directTo(
          rc,
          closestTurretNeedingBuddy.location,
          shouldAvoidAttackers(rc.getRoundNum()),
          false /* clearRubble */,
          false /* onlyForward */); // Go to turret
      // Maybe don't need these, just the extended broadcast
      if (rc.getLocation().distanceSquaredTo(closestTurretNeedingBuddy.location) <= 2) {
        turretBuddyId = closestTurretNeedingBuddy.ID;
      }
      return;
    }

    if (!zombieDragger.dragZombies(rc)) {
      MapLocation target = explorationCalculator.calculate(rc, patrolWaypointCalculator);
      navigation.directTo(
          rc,
          target,
          shouldAvoidAttackers(rc.getRoundNum()),
          false /* clearRubble */,
          false /* onlyForward */);
    }
  }

  private void shareExtendedBroadcast(RobotController rc, MessageSender sender)
      throws GameActionException {
    RobotInfo broadcastedRobot = null;
    if (turretBuddyId != -1 && rc.canSenseRobot(turretBuddyId)) {
      broadcastedRobot = radar.getBuddyTarget(rc, rc.senseRobot(turretBuddyId));
    } else {
      RobotInfo closestTurret = radar.getClosestFriendlyTurret();
      if (closestTurret != null) {
        broadcastedRobot = radar.getBuddyTarget(rc, closestTurret);
      }
    }
    rc.setIndicatorString(0, "Broadcasting closest robot " + broadcastedRobot + "round num " + rc
        .getRoundNum());
    if (broadcastedRobot != null) {
      sender.sendEnemyInfo(rc, broadcastedRobot);
    }
  }
}