package poltbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import poltbot.ArchonTracker.ArchonStatus;

public class ZombieDragger {

  private static final int DIVEBOMB_DISTANCE = 24;

  private final Radar radar;
  private final ArchonTracker archonTracker;
  private final NavigationSystem navigationSystem;

  public ZombieDragger(Radar radar,
      NavigationSystem navigationSystem,
      ArchonTracker archonTracker) {
    this.radar = radar;
    this.navigationSystem = navigationSystem;
    this.archonTracker = archonTracker;
  }

  public boolean dragZombies(RobotController rc) throws GameActionException {
    if (!radar.shouldDragZombies()) {
      return false;
    }

    ArchonStatus[] enemyArchons = archonTracker.getEnemyArchons(rc);

    // Sort enemy archons by distance
    for (int i = 0; i < enemyArchons.length - 1; i++) {
      int minIndex = i;
      int minDistance = enemyArchons[i].lastKnownLocation.distanceSquaredTo(rc.getLocation());
      for (int j = i + 1; j < enemyArchons.length; j++) {
        int dist = enemyArchons[j].lastKnownLocation.distanceSquaredTo(rc.getLocation());
        if (dist < minDistance) {
          minDistance = dist;
          minIndex = j;
        }
      }
      if (minIndex != i) {
        ArchonStatus tmp = enemyArchons[i];
        enemyArchons[i] = enemyArchons[minIndex];
        enemyArchons[minIndex] = tmp;
      }
    }

    if (!radar.isAttackableByZombieNextTwoTurns(rc.getLocation())) {
      return true;
    }

    // Try running to each enemy
    for (int i = 0; i < enemyArchons.length; i++) {
      MapLocation loc = enemyArchons[i].lastKnownLocation;
      boolean avoidEnemies = true;
      if (rc.getLocation().distanceSquaredTo(loc) <= DIVEBOMB_DISTANCE) {
        avoidEnemies = false;
      }
      if (navigationSystem.directTo(rc, enemyArchons[i].lastKnownLocation,
          avoidEnemies /* avoidEnemies */,
          false /* clearRubble */,
          false /* onlyForward */)) {
        rc.setIndicatorString(1, "Dragging " + enemyArchons[i].lastKnownLocation + " " + rc
            .getRoundNum() + " with divebomb " + avoidEnemies);
        return true;
      }
    }

    // If else fails just run away from the average direction of our archons
    ArchonStatus[] alliedArchons = archonTracker.getAlliedArchons(rc);
    if (alliedArchons.length == 0) {
      return false;
    }
    int totalX = 0;
    int totalY = 0;
    for (int i = 0; i < alliedArchons.length; i++) {
      totalX += alliedArchons[i].lastKnownLocation.x;
      totalY += alliedArchons[i].lastKnownLocation.y;
    }

    MapLocation averageMapLocation = new MapLocation(totalX / alliedArchons.length, totalY
        / alliedArchons.length);
    Direction d = rc.getLocation().directionTo(averageMapLocation).opposite();
    MapLocation dragLocation = rc.getLocation().add(d, 100);
    rc.setIndicatorString(1, "Dragging " + dragLocation + " " + rc.getRoundNum());
    return (navigationSystem.directTo(rc,
        dragLocation,
        true /* avoidEnemies */,
        false /* clearRubble */,
        false /* onlyForward */));
  }
}
