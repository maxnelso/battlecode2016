package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class UnpackedTurretBehavior implements Behavior {

  private static final int MAX_IDLE_ROUNDS = 30;

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 5000;

  private final AttackSystem attackSystem;
  private final Radar radar;

  private int idleRounds;

  public UnpackedTurretBehavior(Radar radar, AttackSystem attackSystem) {
    this.attackSystem = attackSystem;
    this.radar = radar;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    if (attackSystem.attackAnyoneInSensorRange(rc) == null &&
        attackSystem.attackLeastHealthEnemy(rc) == null) {
      attackSystem.attackLeastHealthZombie(rc);
    }
    maybeUnsiege(rc);
  }

  private void maybeUnsiege(RobotController rc) throws GameActionException {
    MapLocation closest = radar.getClosestEnemyOrZombie();
    rc.setIndicatorString(1, "closest " + closest + " idle rounds " + idleRounds);
    if (closest != null && closest.distanceSquaredTo(rc.getLocation()) <= rc
        .getType().attackRadiusSquared) {
      idleRounds = 0;
    } else {
      if (idleRounds++ > MAX_IDLE_ROUNDS && rc.isCoreReady()) {
        rc.pack();
      }
    }
  }
}
