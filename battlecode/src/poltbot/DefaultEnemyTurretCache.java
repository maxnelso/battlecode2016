package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class DefaultEnemyTurretCache implements EnemyTurretCache {

  private static final int MAX_TURRETS = 100;

  private static class TurretInfo {
    private final MapLocation loc;
    private final boolean present;
    private final int timestamp;

    private TurretInfo(MapLocation loc, boolean present, int timestamp) {
      this.loc = loc;
      this.present = present;
      this.timestamp = timestamp;
    }

    public TurretInfo asPresent(int newTimestamp) {
      if (present) {
        return this;
      }

      return new TurretInfo(loc, true /* present */, newTimestamp);
    }

    public TurretInfo asAbsent(int newTimestamp) {
      if (!present) {
        return this;
      }

      return new TurretInfo(loc, false /* present */, newTimestamp);
    }
  }

  private final TurretInfo[] turrets;
  private final MapLocationIntMap attackingTurrets;
  private int numTurrets;
  private int shareIndex;

  public DefaultEnemyTurretCache() {
    turrets = new TurretInfo[MAX_TURRETS];
    attackingTurrets = new ArrayMapLocationIntMap();
  }

  @Override
  public void reportRobotInfo(RobotController rc, RobotInfo robot) {
    if (robot.team == rc.getTeam().opponent() && robot.type == RobotType.TURRET) {
      reportEnemyTurretPresent(robot.location, rc.getRoundNum());
    }
  }

  @Override
  public void reportEnemyTurretPresent(MapLocation loc, int timestamp) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc)) {
        if (turret.timestamp < timestamp && !turret.present) {
          updateAttackingTurrets(loc, true /* increment */);
          turrets[i] = turret.asPresent(timestamp);
        }
        return;
      }
    }

    if (numTurrets < MAX_TURRETS) {
      turrets[numTurrets++] = new TurretInfo(loc, true /* present */, timestamp);
      updateAttackingTurrets(loc, true /* increment */);
    }
  }

  @Override
  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc)) {
        if (turret.timestamp < timestamp && turret.present) {
          updateAttackingTurrets(loc, false /* increment */);
          turrets[i] = turret.asAbsent(timestamp);
        }
        return;
      }
    }

    if (numTurrets < MAX_TURRETS) {
      turrets[numTurrets++] = new TurretInfo(loc, false /* present */, timestamp);
      updateAttackingTurrets(loc, false /* increment */);
    }
  }

  @Override
  public boolean isInEnemyTurretRange(MapLocation loc) {
    return attackingTurrets.getValue(loc) > 0;
  }

  @Override
  public void shareRandomEnemyTurret(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    if (numTurrets == 0) {
      return;
    }

    shareIndex = shareIndex % numTurrets;
    TurretInfo turret = turrets[shareIndex];
    if (turrets[shareIndex].present) {
      messageSender.sendEnemyTurret(rc, turret.loc, turret.timestamp);
    } else {
      messageSender.sendRemoveEnemyTurret(rc, turret.loc, turret.timestamp);
    }
    shareIndex++;
  }

  @Override
  public void invalidatePresentEnemyTurrets(RobotController rc) throws GameActionException {
    int roundNum = rc.getRoundNum();
    Team enemyTeam = rc.getTeam().opponent();
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.present && rc.canSenseLocation(turret.loc)) {
        RobotInfo robot = rc.senseRobotAtLocation(turret.loc);
        if (robot == null || robot.team != enemyTeam || robot.type != RobotType.TURRET) {
          turrets[i] = turret.asAbsent(roundNum /* newTimestamp */);
          updateAttackingTurrets(turret.loc, false /* increment */);
        }
      }
    }
  };

  @Override
  public int getNumTurrets() {
    int presentTurrets = 0;
    for (int i = 0; i < numTurrets; i++) {
      if (turrets[i].present) {
        presentTurrets++;
      }
    }
    return presentTurrets;
  }

  @Override
  public MapLocation getATurret() {
    for (int i = numTurrets - 1; i >= 0; i--) {
      if (turrets[i].present) {
        return turrets[i].loc;
      }
    }

    return null;
  }

  @Override
  public void showDebugInfo(RobotController rc) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.present) {
        rc.setIndicatorLine(rc.getLocation(), turret.loc, 50, 50, 50);
      }
    }
    rc.setIndicatorString(1, "Num turrets = " + numTurrets);
  }

  private void updateAttackingTurrets(MapLocation turretLoc, boolean increment) {
    MapLocation[] locs = MapLocation.getAllMapLocationsWithinRadiusSq(
        turretLoc, RobotType.TURRET.attackRadiusSquared);
    for (int i = 0; i < locs.length; i++) {
      if (increment) {
        attackingTurrets.increment(locs[i]);
      } else {
        attackingTurrets.decrement(locs[i]);
      }
    }
  }
}
