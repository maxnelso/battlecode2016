package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class EeHanTiming {

  public class TimingAttack {
    public final int startRound;
    public final int endRound;
    public final MapLocation rally;

    public TimingAttack(int startRound, int endRound, MapLocation rally) {
      this.startRound = startRound;
      this.endRound = endRound;
      this.rally = rally;
    }
  }

  private TimingAttack timingAttack;

  public EeHanTiming() {
    timingAttack = null;
  }

  public TimingAttack getTimingAttack() {
    return timingAttack;
  }

  public void reportTimingAttack(
      RobotController rc, int startRound, int endRound, MapLocation rally) {
    if (timingAttack == null
        || startRound < timingAttack.startRound
        || rc.getRoundNum() > timingAttack.endRound && startRound > timingAttack.endRound) {
      TimingAttack attack = new TimingAttack(startRound, endRound, rally);
      timingAttack = attack;
    }
  }

  public void shareTimingAttack(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    if (timingAttack != null) {
      messageSender.sendTimingAttack(
          rc, timingAttack.startRound, timingAttack.endRound, timingAttack.rally);
    }
  }
}
