package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface Behavior {
  public void behave(RobotController rc) throws GameActionException;

  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException;
}
