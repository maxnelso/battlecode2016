package poltbot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import poltbot.ArchonTracker.ArchonStatus;
import poltbot.EeHanTiming.TimingAttack;
import poltbot.InterestingTargets.InterestingTarget;

public class SoldierBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 2000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 5000;

  private static final int RUN_HEALTH = 10;

  private static final double STOP_HEALING_HEALTH = 60;

  private final NavigationSystem navigation;
  private final AttackSystem attackSystem;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final Radar radar;
  private final InterestingTargets interestingTargets;
  private final ArchonTracker archonTracker;
  private final PatrolWaypointCalculator patrolWaypointCalculator;
  private final EeHanTiming eeHanTiming;
  private final BytecodeProfiler profiler;

  public SoldierBehavior(
      NavigationSystem navigation,
      AttackSystem attackSystem,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      Radar radar,
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      EeHanTiming eeHanTiming,
      BytecodeProfiler profiler) {
    this.navigation = navigation;
    this.attackSystem = attackSystem;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.radar = radar;
    this.interestingTargets = interestingTargets;
    this.archonTracker = archonTracker;
    this.eeHanTiming = eeHanTiming;
    this.profiler = profiler;
    patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        4 /* laneHalfWidth */, 3 /* mapBoundaryMargin */);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  };

  @Override
  public void behave(RobotController rc) throws GameActionException {
    profiler.split("start of behave");
    mapBoundaryCalculator.update(rc);
    profiler.split("after map boundary update");
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    profiler.split("after radar update");
    archonTracker.updateClosest(rc);
    profiler.split("after archon tracker updateClosest");

    boolean canAct = rc.isCoreReady() || rc.isWeaponReady();

    ArchonStatus closestAlliedArchon = archonTracker.getClosestAlliedArchon();

    if (canAct) {
      if (doTimingAttack(rc)) {
        return;
      }

      boolean inHealingRange = closestAlliedArchon != null &&
          closestAlliedArchon.lastKnownLocation.distanceSquaredTo(rc
              .getLocation()) < RobotType.ARCHON.attackRadiusSquared;
      boolean attackersNearby = radar.getClosestEnemyOrZombie() != null;

      if (inHealingRange && rc.getHealth() < STOP_HEALING_HEALTH && !attackersNearby) {
        heal(rc);
        profiler.split("after heal");
      } else if (rc.getHealth() < RUN_HEALTH) {
        lowHealth(rc);
        profiler.split("after lowHealth");
      } else {
        defaultBehavior(rc);
        profiler.split("after default");
      }
    }

    // enemyTurretCache.showDebugInfo(rc);
    profiler.split("after enemyTurretCache showDebugInfo");
  }

  private boolean doTimingAttack(RobotController rc) throws GameActionException {
    TimingAttack timingAttack = eeHanTiming.getTimingAttack();
    if (timingAttack == null) {
      return false;
    }

    int roundNum = rc.getRoundNum();
    if (roundNum >= timingAttack.endRound) {
      return false;
    }

    rc.setIndicatorString(0, "EE HAN TIMING, rounds " + timingAttack.startRound + " to "
        + timingAttack.endRound + " at " + timingAttack.rally);
    attackSystem.attackLeastTurnsToKillEnemy(rc);
    navigation.directTo(
        rc,
        timingAttack.rally,
        roundNum < timingAttack.startRound /* avoidAttackers */,
        true /* clearRubble */,
        false /* onlyForward */);
    return true;
  }

  public void defaultBehavior(RobotController rc) throws GameActionException {
    ArchonStatus closestAlliedArchon = archonTracker.getClosestAlliedArchon();
    MapLocation closestEnemyOrZombie = radar.getClosestEnemyOrZombie();
    boolean attackersNearby = closestEnemyOrZombie != null
        && closestEnemyOrZombie.distanceSquaredTo(rc
            .getLocation()) <= RobotType.SOLDIER.sensorRadiusSquared;
    boolean inHealingRange = closestAlliedArchon != null &&
        closestAlliedArchon.lastKnownLocation.distanceSquaredTo(rc
            .getLocation()) < RobotType.ARCHON.attackRadiusSquared;
    if (attackersNearby) {
      fight(rc);
    } else {
      explore(rc);
    }
  }

  public void heal(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "HEAL");

    ArchonStatus closestAllyArchon = archonTracker.getClosestAlliedArchon();

    // If there's no archons with known positions, just return to default
    if (closestAllyArchon == null) {
      defaultBehavior(rc);
      return;
    }

    boolean nearArchon = rc.getLocation()
        .distanceSquaredTo(closestAllyArchon.lastKnownLocation) < 13;
    if (nearArchon) {
      RobotInfo robotAtLocation = rc.senseRobotAtLocation(closestAllyArchon.lastKnownLocation);
      if (robotAtLocation == null || robotAtLocation.type != RobotType.ARCHON) {
        // If we get to where we thought the closest archon was and no archon is
        // there, mark archon as not there and go to next
        archonTracker.getClosestAlliedArchon().notFoundAtLocation = true;
        archonTracker.updateClosest(rc);
        heal(rc);
      }
    } else {
      navigation.bugTo(rc, closestAllyArchon.lastKnownLocation, true, false);
    }
  }

  public void lowHealth(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "LOW HEALTH");
    boolean infected = rc.isInfected();
    boolean isViperInfection = rc.getViperInfectedTurns() > 0;

    boolean inDanger = radar.isAttackableByEnemiesOrZombies(rc.getLocation()) || isViperInfection;

    if (infected && inDanger) {
      // If in danger and infected, try to move towards enemy
      MapLocation closestEnemy = radar.getClosestEnemy();
      if (closestEnemy != null) {
        if (navigation.directTo(rc, closestEnemy, false, false, true)) {
          return;
        } else {
          attackSystem.attackLeastHealthEnemy(rc);
        }
      } else {
        MapLocation closestZombie = radar.getClosestZombie();
        if (navigation.retreatFrom(rc)) {
          return;
        } else {
          attackSystem.attackLeastHealthZombie(rc);
        }
      }
    } else if (infected) {
      // If infected but not in immediate danger, run away
      MapLocation closestDanger = radar.getClosestEnemyOrZombie();
      if (closestDanger != null) {
        navigation.retreatFrom(rc);
      }
    } else if (inDanger) {
      // If in danger, but not infected, try running
      // TODO: consider only running when running makes you safe
      MapLocation closestDanger = radar.getClosestEnemyOrZombie();
      if (closestDanger != null) {
        fight(rc);
        // navigation.retreatFrom(rc, closestDanger);
      } else {
        // If we can't run, just fight
        fight(rc);
      }
    } else {
      // Not in danger, not infected, move towards archon to heal
      heal(rc);
    }

  }

  private void fight(RobotController rc) throws GameActionException {
    rc.setIndicatorString(0, "FIGHT " + Clock.getBytecodesLeft());

    ArchonStatus closestAllyArchon = archonTracker.getClosestAlliedArchon();
    boolean inHealingState = rc.getHealth() < RUN_HEALTH && closestAllyArchon != null;
    MapLocation myLoc = rc.getLocation();

    if (radar.isAttackableByZombies(myLoc)) {
      MapLocation zombie = radar.getClosestZombie();
      if (zombie != null) {
        attackSystem.attackLeastHealthEnemy(rc);
        attackSystem.attackLeastHealthZombie(rc);
        navigation.retreatFrom(rc);
        return;
      }
    }

    // Zombies
    if (radar.getEnemyStrength() == 0) {
      if (inHealingState) {
        rc.setIndicatorString(1, "healing state");
        heal(rc);
        return;
      }

      if (attackSystem.attackLeastTurnsToKillEnemy(rc) != null) {
        return;
      }
      MapLocation target = attackSystem.attackLeastHealthZombie(rc);
      rc.setIndicatorString(2, "target " + target);
      if (target != null) {
        int dist = target.distanceSquaredTo(rc.getLocation());
        rc.setIndicatorString(3, "dist " + dist);
        if (dist < 8) {
          navigation.retreatFrom(rc);
        }
        return;
      } else if (radar.getZombieStrength() == 0 && radar.getClosestZombie() != null) {
        navigation.directTo(rc, radar.getClosestZombie(), false /* avoidAttackers */,
            true /* clearRubble */,
            false /* onlyForward */);
        return;
      }
    }

    int numEnemiesWhoCanAttackUs = radar.getNumEnemiesWhoCanAttackUs(rc);
    String s = "Round: " + rc.getRoundNum();
    s += " healing state " + inHealingState;
    s += " numEnemiesWhoCanAttackUs " + numEnemiesWhoCanAttackUs;

    if (numEnemiesWhoCanAttackUs >= 1) { // In combat
      int maxAlliesWhoCanAttackEnemy = radar.getMaxAlliesWhoCanAttackEnemy(rc) + 1;
      s += " maxAlliesWhoCanAttackEnemy " + maxAlliesWhoCanAttackEnemy;
      // A lone enemy!
      if (numEnemiesWhoCanAttackUs == 1) {
        s += "in 1v1 battle ";
        RobotInfo singleEnemy = rc.senseRobotAtLocation(radar.getClosestEnemy());

        if (singleEnemy != null && maxAlliesWhoCanAttackEnemy == 1) {
          // 1v1 obs on final destination, fight if we are winning, retreat
          // otherwise
          boolean weAreWinning1v1 = rc.getHealth() >= singleEnemy.health;
          if (weAreWinning1v1) {
            attackSystem.attackLeastTurnsToKillEnemy(rc);
            s += "and fightin";
            rc.setIndicatorString(2, s);
            return;
          } else {
            s += "and retreating ";
            rc.setIndicatorString(2, s);
            retreatOrFight(rc);
            attackSystem.attackLeastTurnsToKillEnemy(rc);
            return;
          }
        } else {
          s += "out number not retreating";
          // We outnumber the lone enemy don't retreat
          rc.setIndicatorString(2, s);
          if (singleEnemy != null && inHealingState && rc.getHealth() < singleEnemy.health) {
            retreatOrFight(rc);
          } else {
            attackSystem.attackLeastTurnsToKillEnemy(rc);
          }
          return;
        }
      } else if (inHealingState || numEnemiesWhoCanAttackUs > maxAlliesWhoCanAttackEnemy
          || !guessIfFightIsWinning()) {
        s += " retreating from bigger fight";
        rc.setIndicatorString(2, s);
        retreatOrFight(rc);
        return;
      } else {
        // Good enough double team
        s += " staying in bigger fight";
        rc.setIndicatorString(2, s);
        attackSystem.attackLeastTurnsToKillEnemy(rc);
        return;
      }
    } else if (!inHealingState) { // Not directly in combat
      MapLocation closestEnemy = radar.getClosestEnemy();
      if (attackSystem.attackLeastTurnsToKillEnemy(rc) != null) {
        rc.setIndicatorString(2, s);
        return;
      }
      s += "closest enemy " + closestEnemy;
      if (closestEnemy != null) {
        int numAlliesFighting = radar.getNumAlliesWhoCanAttackLocation(rc, closestEnemy);
        s += "numAlliesFighting " + numAlliesFighting;
        if (numAlliesFighting > 0) {
          int maxEnemyExposure = Math.min(numAlliesFighting + 1, 3);
          if (navigation.directToWithMaximumEnemyExposure(rc, closestEnemy, maxEnemyExposure)) {
            s += "and can safely go to them";
            rc.setIndicatorString(2, s);
            return;
          }
        }
      }
    } else {
      heal(rc);
      return;
    }
    // Didn't have to do anything walk to enemy!
    rc.setIndicatorString(2, s);
  }

  @SuppressWarnings("rawtypes")
  private void explore(RobotController rc) throws GameActionException {
    if (attackSystem.attackLeastTurnsToKillEnemy(rc) != null) {
      return;
    }

    InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
        rc.getLocation(),
        true /* includeDens */,
        false /* includeParts */,
        false /* includeNeutralRobots */);
    if (interestingTarget != null && rc.canSenseLocation(interestingTarget.loc)) {
      RobotInfo robot = rc.senseRobotAtLocation(interestingTarget.loc);
      if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
        interestingTargets.reportDenDestroyed(interestingTarget.loc);
        interestingTarget = null;
      }
    }

    MapLocation target = interestingTarget == null
        ? null
        : interestingTarget.loc;
    if (target == null) {
      target = explorationCalculator.calculate(rc, patrolWaypointCalculator);
      rc.setIndicatorString(0, "EXPLORING TO " + target + " " + rc.getRoundNum());
    }

    navigation.directTo(rc, target, true /* avoidAttackers */, true /* clearRubble */,
        false /* onlyForward */);
  }

  private boolean guessIfFightIsWinning() {
    int enemyStrength = radar.getEnemyStrength();
    int allyStrength = radar.getAllyStrength();
    if (enemyStrength == 0) {
      return true;
    } else if (enemyStrength <= 10) {
      return allyStrength >= 20;
    } else if (enemyStrength <= 20) {
      return allyStrength >= 40;
    } else if (enemyStrength <= 30) {
      return allyStrength >= 50;
    } else {
      return allyStrength >= 1.5 * enemyStrength;
    }
  }

  private void retreatOrFight(RobotController rc) throws GameActionException {
    // If all our opponents have really high action delay, we can fire a last
    // shot
    // and still be able to move before they can return fire. This would most
    // probably
    // happen if an enemy engaged us after several diagonal moves. This could
    // turn
    // a losing 1v1 into a winning one! Also, if we can one-hit an enemy we
    // should
    // do so instead of retreating even if we take hits to do so

    if (radar.canOneShotEnemyOrNotGetHit(rc)) {
      rc.setIndicatorString(0, "Round " + rc.getRoundNum()
          + " Can one shot or not be hit, staying");
      attackSystem.attackLeastTurnsToKillEnemy(rc);
      return;
    }
    if (!navigation.retreatFrom(rc)) {
      rc.setIndicatorString(0, "Rount " + rc.getRoundNum() + " Couldn't retreat");
      attackSystem.attackLeastTurnsToKillEnemy(rc);
    }
  }
}