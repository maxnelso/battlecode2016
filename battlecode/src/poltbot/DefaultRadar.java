package poltbot;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class DefaultRadar implements Radar {

  private static final int MAX_ROBOTS = 100;
  private static final int ZOMBIE_DRAGGING_STRENGTH = 10;

  private static class Forces {
    // private final boolean computeBuddyTurret;

    private RobotInfo[] robots;
    private int numRobots;
    private RobotInfo leastHealth;
    private RobotInfo leastHealthInAttackRange;
    private RobotInfo leastHealthAttackerInAttackRange;
    private RobotInfo leastHealthInfectedInAttackRange;
    private RobotInfo leastHealthNotInfectedInAttackRange;
    private RobotInfo mostHealthUnderMaxHealth;
    private RobotInfo closestTurret;
    private RobotInfo closest;
    private RobotInfo closestUnableToMove;
    private int closestDist;
    private int closestUnableToMoveDist;
    private int strength;

    private int closestArchonDist;
    private RobotInfo closestArchon;

    public Forces(
        ReportedRobotInfos reportedRobots,
        RobotController rc,
        boolean computeBuddyTurrets) {
      // this.computeBuddyTurret = computeBuddyTurret;
      robots = new RobotInfo[MAX_ROBOTS];
      numRobots = 0;
      leastHealth = null;
      leastHealthInAttackRange = null;
      leastHealthAttackerInAttackRange = null;
      leastHealthInfectedInAttackRange = null;
      leastHealthNotInfectedInAttackRange = null;
      mostHealthUnderMaxHealth = null;
      closestDist = 0;
      closestArchonDist = 0;
      closest = null;
      if (reportedRobots != null && rc != null) {
        for (int i = 0; i < reportedRobots.numReportedRobots; i++) {
          addRobot(reportedRobots.reportedRobots[i], rc);
        }
      }
    }

    public void addRobot(RobotInfo robot, RobotController rc) {
      MapLocation myLoc = rc.getLocation();
      int attackRange = rc.getType().attackRadiusSquared;

      if (numRobots < MAX_ROBOTS) {
        robots[numRobots] = robot;
        numRobots++;
      }

      if (rc.getLocation().distanceSquaredTo(robot.location) < rc.getType().sensorRadiusSquared) {
        strength += getStrengthOfRobot(robot);
      }
      int dist = myLoc.distanceSquaredTo(robot.location);
      if (closest == null || dist < closestDist) {
        closestDist = dist;
        closest = robot;
      }
      if (closestArchon == null || dist < closestArchonDist) {
        closestArchonDist = dist;
        closestArchon = robot;
      }

      if (leastHealth == null || robot.health < leastHealth.health) {
        leastHealth = robot;
      }

      double healthDiff = robot.type.maxHealth - robot.health;
      if (healthDiff > 0 && (mostHealthUnderMaxHealth == null
          || (mostHealthUnderMaxHealth.type.maxHealth
              - mostHealthUnderMaxHealth.health) > healthDiff)) {
        mostHealthUnderMaxHealth = robot;
      }

      if (myLoc.distanceSquaredTo(robot.location) <= attackRange) {
        if (leastHealthInAttackRange == null || robot.health < leastHealthInAttackRange.health) {
          leastHealthInAttackRange = robot;
        }
        if (robot.type.canAttack()
            && (leastHealthAttackerInAttackRange == null
                || robot.health < leastHealthAttackerInAttackRange.health)) {
          leastHealthAttackerInAttackRange = robot;
        }
        if (robot.viperInfectedTurns + robot.zombieInfectedTurns > 0) {
          if (leastHealthInfectedInAttackRange == null
              || robot.health < leastHealthInfectedInAttackRange.health) {
            leastHealthInfectedInAttackRange = robot;
          }
        } else {
          if (leastHealthNotInfectedInAttackRange == null
              || robot.health < leastHealthNotInfectedInAttackRange.health) {
            leastHealthNotInfectedInAttackRange = robot;
          }
        }
      }

      if (robot.type == RobotType.TURRET) {
        if (closestTurret == null || (dist < closestTurret.location.distanceSquaredTo(rc
            .getLocation()))) {
          closestTurret = robot;
        }
      }
    }

    private int getStrengthOfRobot(RobotInfo robotInfo) {
      switch (robotInfo.type) {
        case SOLDIER:
          return 10;
        case GUARD:
          return 2;
        case VIPER:
          return 4;
        case TTM:
          return 1;
        case TURRET:
          return 30;
        case SCOUT:
          return 0;
        case ARCHON:
          return 1;
        case STANDARDZOMBIE:
          return 2;
        case RANGEDZOMBIE:
          return 10;
        case FASTZOMBIE:
          return 10;
        case BIGZOMBIE:
          return 5;
        case ZOMBIEDEN:
        default:
          return 0;
      }
    }
  }

  public static class ReportedRobotInfos {

    public RobotInfo[] reportedRobots;
    public int numReportedRobots;

    public ReportedRobotInfos() {
      reportedRobots = new RobotInfo[MAX_ROBOTS];
      numReportedRobots = 0;
    }
  }

  private final InterestingTargets interestingTargets;
  private final ArchonTracker archonTracker;
  private final EnemyTurretCache enemyTurretCache;

  private Forces allyForces;
  private Forces enemyForces;
  private Forces neutralForces;
  private Forces zombieForces;

  private ReportedRobotInfos allyReportedRobots;
  private ReportedRobotInfos enemyReportedRobots;
  private ReportedRobotInfos zombieReportedRobots;

  private int[] cachedNumEnemiesAttackingMoveDirs;

  public DefaultRadar(
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      EnemyTurretCache enemyTurretCache) {
    this.interestingTargets = interestingTargets;
    this.archonTracker = archonTracker;
    this.enemyTurretCache = enemyTurretCache;

    allyReportedRobots = new ReportedRobotInfos();
    enemyReportedRobots = new ReportedRobotInfos();
    zombieReportedRobots = new ReportedRobotInfos();

    allyForces = new Forces(allyReportedRobots, null, true /* computeBuddyTurrets */);
    enemyForces = new Forces(enemyReportedRobots, null, false /* computeBuddyTurrets */);
    neutralForces = new Forces(null, null, false /* computeBuddyTurrets */);
    zombieForces = new Forces(zombieReportedRobots, null, false /* computeBuddyTurrets */);

  }

  @Override
  public void update(RobotController rc, int bytecodeLimit) {
    int initialBytecodesLeft = Clock.getBytecodesLeft();

    allyForces = new Forces(allyReportedRobots, rc, true /* computeBuddyTurrets */);
    enemyForces = new Forces(enemyReportedRobots, rc, false /* computeBuddyTurrets */);
    neutralForces = new Forces(null, null, false /* computeBuddyTurrets */);
    zombieForces = new Forces(zombieReportedRobots, rc, false /* computeBuddyTurrets */);

    RobotInfo[] nearbyRobots = rc.senseNearbyRobots();
    for (int i = 0; i < nearbyRobots.length; i++) {
      Team team = nearbyRobots[i].team;
      Forces forces;
      if (team == rc.getTeam()) {
        forces = allyForces;
      } else if (team == rc.getTeam().opponent()) {
        forces = enemyForces;
      } else if (team == Team.NEUTRAL) {
        forces = neutralForces;
      } else {
        forces = zombieForces;
      }

      forces.addRobot(nearbyRobots[i], rc);
      interestingTargets.reportRobotInfo(rc, nearbyRobots[i]);
      enemyTurretCache.reportRobotInfo(rc, nearbyRobots[i]);
      if (nearbyRobots[i].type.equals(RobotType.ARCHON)) {
        archonTracker.processArchon(nearbyRobots[i].ID,
            nearbyRobots[i].location,
            null /* target */,
            rc.getRoundNum(),
            nearbyRobots[i].team == rc.getTeam());
      }

      int bytecodesLeft = Clock.getBytecodesLeft();
      if (bytecodesLeft < 500 || initialBytecodesLeft - bytecodesLeft >= bytecodeLimit) {
        cleanupReportedRobots(rc);
        return;
      }
    }

    cleanupReportedRobots(rc);
  }

  @Override
  public MapLocation getClosestAlly() {
    return allyForces.closest != null
        ? allyForces.closest.location
        : null;
  }

  @Override
  public MapLocation getClosestEnemy() {
    return enemyForces.closest != null
        ? enemyForces.closest.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthAllyInRepairRange() {
    return allyForces.leastHealthNotInfectedInAttackRange != null
        ? allyForces.leastHealthNotInfectedInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getClosestEnemyOrZombie() {
    RobotInfo closest = enemyForces.closest;
    if (closest == null || (zombieForces.closest != null
        && zombieForces.closestDist < enemyForces.closestDist)) {
      closest = zombieForces.closest;
    }
    return closest == null ? null : closest.location;
  }

  @Override
  public MapLocation getClosestZombie() {
    return (zombieForces.closest != null ? zombieForces.closest.location : null);
  }

  @Override
  public MapLocation getLeastHealthEnemyAttackerInAttackRange() {
    return enemyForces.leastHealthAttackerInAttackRange != null
        ? enemyForces.leastHealthAttackerInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthEnemyInAttackRange() {
    return enemyForces.leastHealthInAttackRange != null
        ? enemyForces.leastHealthInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthZombieAttackerInAttackRange() {
    return zombieForces.leastHealthAttackerInAttackRange != null
        ? zombieForces.leastHealthAttackerInAttackRange.location
        : null;
  }

  @Override
  public MapLocation getLeastHealthZombieInAttackRange() {
    return zombieForces.leastHealthInAttackRange != null
        ? zombieForces.leastHealthInAttackRange.location
        : null;
  }

  @Override
  public boolean isAttackableByZombies(MapLocation loc) {
    return isAttackableByForces(zombieForces, loc);
  }

  @Override
  public boolean isAttackableByEnemiesOrZombies(MapLocation loc) {
    return isAttackableByForces(enemyForces, loc) || isAttackableByForces(zombieForces, loc);
  }

  @Override
  public void addReportedRobot(RobotController rc, RobotInfo robotInfo) {
    if (robotInfo.team == rc.getTeam() && allyReportedRobots.numReportedRobots < MAX_ROBOTS) {
      allyReportedRobots.reportedRobots[allyReportedRobots.numReportedRobots++] = robotInfo;
    } else if (robotInfo.team == rc.getTeam().opponent()
        && enemyReportedRobots.numReportedRobots < MAX_ROBOTS) {
      enemyReportedRobots.reportedRobots[enemyReportedRobots.numReportedRobots++] = robotInfo;
    } else if (robotInfo.team == Team.ZOMBIE
        && zombieReportedRobots.numReportedRobots < MAX_ROBOTS) {
      zombieReportedRobots.reportedRobots[zombieReportedRobots.numReportedRobots++] = robotInfo;
    }
  }

  @Override
  public RobotInfo getBuddyTarget(RobotController rc, RobotInfo buddy) {
    RobotInfo furthestEnemy = null;
    for (int i = 0; i < enemyForces.numRobots; i++) {
      RobotInfo enemy = enemyForces.robots[i];
      if (enemy.location.distanceSquaredTo(buddy.location) > RobotType.TURRET.attackRadiusSquared) {
        continue;
      }
      if (canHit(rc, buddy.ID, enemy)) {
        return enemy;
      }
      if (furthestEnemy == null || (enemy.location.distanceSquaredTo(rc
          .getLocation())) < furthestEnemy.location.distanceSquaredTo(rc.getLocation())) {
        furthestEnemy = enemy;
      }
    }
    if (furthestEnemy != null) {
      return furthestEnemy;
    }
    for (int i = 0; i < zombieForces.numRobots; i++) {
      RobotInfo zombie = zombieForces.robots[i];
      if (zombie.location.distanceSquaredTo(
          buddy.location) > RobotType.TURRET.attackRadiusSquared) {
        continue;
      }
      if (canHit(rc, buddy.ID, zombie)) {
        return zombie;
      }
      if (furthestEnemy == null || (zombie.location.distanceSquaredTo(rc
          .getLocation())) < furthestEnemy.location.distanceSquaredTo(rc.getLocation())) {
        furthestEnemy = zombie;
      }
    }
    return furthestEnemy;
  }

  private boolean canHit(RobotController rc, int buddyId, RobotInfo robot) {
    if (robot.coreDelay >= 2) {
      return true;
    }
    return false;
  }

  @Override
  public MapLocation getAdjacentNeutralRobot(MapLocation loc) {
    return neutralForces.closest != null && neutralForces.closest.location.isAdjacentTo(loc)
        ? neutralForces.closest.location
        : null;
  }

  @Override
  public int getEnemyStrength() {
    return enemyForces.strength;
  }

  @Override
  public int getAllyStrength() {
    return allyForces.strength;
  }

  @Override
  public boolean shouldEngageEnemies() {
    return (zombieForces.strength <= 12) && (allyForces.strength >= enemyForces.strength);
  }

  private boolean isAttackableByForces(Forces forces, MapLocation loc) {
    for (int i = 0; i < forces.numRobots; i++) {
      RobotInfo robot = forces.robots[i];
      MapLocation robotAttackFromLoc = robot.type.canMove()
          ? robot.location.add(robot.location.directionTo(loc))
          : robot.location;
      if (robot.type.canAttack()
          && loc.distanceSquaredTo(robotAttackFromLoc) <= robot.type.attackRadiusSquared) {
        return true;
      }
    }

    return false;
  }

  private void cleanupForceReportedRobots(ReportedRobotInfos reportedRobots, RobotController rc) {
    int index = 0;
    while (index < reportedRobots.numReportedRobots) {
      RobotInfo robotInfo = reportedRobots.reportedRobots[index];
      double newCoreDelay = robotInfo.coreDelay - 1;

      if (newCoreDelay < 1) { // Could have moved, remove them
        reportedRobots.numReportedRobots--;
        reportedRobots.reportedRobots[index] = reportedRobots.reportedRobots[reportedRobots.numReportedRobots];
        reportedRobots.reportedRobots[reportedRobots.numReportedRobots] = null;
      } else {
        RobotInfo newRobotInfo = new RobotInfo(robotInfo.ID,
            robotInfo.team,
            robotInfo.type,
            robotInfo.location,
            newCoreDelay,
            robotInfo.weaponDelay - 1,
            robotInfo.attackPower,
            robotInfo.health,
            robotInfo.maxHealth,
            robotInfo.zombieInfectedTurns,
            robotInfo.viperInfectedTurns);

        reportedRobots.reportedRobots[index] = newRobotInfo;
        ++index;
      }
    }
  }

  private void cleanupReportedRobots(RobotController rc) {
    cleanupForceReportedRobots(allyReportedRobots, rc);
    cleanupForceReportedRobots(enemyReportedRobots, rc);
    cleanupForceReportedRobots(zombieReportedRobots, rc);
  }

  @Override
  public boolean shouldDragZombies() {
    return zombieForces.strength >= ZOMBIE_DRAGGING_STRENGTH;
  }

  public RobotInfo closestAlliedArchon() {
    return allyForces.closestArchon;
  }

  @Override
  public int getNumEnemiesWhoCanAttackUs(RobotController rc) {
    RobotInfo[] enemies = enemyForces.robots;
    int numEnemiesWhoCanAttackUs = 0;
    for (int i = 0; i < enemyForces.numRobots; i++) {
      if (rc.getLocation().distanceSquaredTo(
          enemies[i].location) <= enemies[i].type.attackRadiusSquared) {
        ++numEnemiesWhoCanAttackUs;
      } else {}
    }
    return numEnemiesWhoCanAttackUs;
  }

  @Override
  public int getMaxAlliesWhoCanAttackEnemy(RobotController rc) {
    RobotInfo[] enemies = enemyForces.robots;
    int maxAlliesAttackingEnemy = 0;
    for (int i = 0; i < enemyForces.numRobots; i++) {
      // TODO Can definitely optimize this by just calling
      // rc.senseNearbyRobots(enemies[i].location,
      // rc.getType().attackRadiusSquared, rc.getTeam())
      // Only want soldiers :(
      int allyWhoCanHitCount = rc.senseNearbyRobots(enemies[i].location, rc
          .getType().attackRadiusSquared, rc.getTeam()).length;
      maxAlliesAttackingEnemy = Math.max(maxAlliesAttackingEnemy, allyWhoCanHitCount);
    }
    return maxAlliesAttackingEnemy;
  }

  @Override
  public int getNumAlliesWhoCanAttackLocation(RobotController rc, MapLocation location) {
    RobotInfo[] allies = allyForces.robots;
    int numAlliesWhoCanAttackLocation = 0;
    for (int i = 0; i < allyForces.numRobots; i++) {
      if (allies[i].location.distanceSquaredTo(location) <= allies[i].type.attackRadiusSquared) {
        ++numAlliesWhoCanAttackLocation;
      }
    }
    return numAlliesWhoCanAttackLocation;
  }

  // Built with
  // public static void tmp() {
  // MapLocation center = new MapLocation(0, 0);
  // for (int ex = -5; ex <= +5; ex++) {
  // System.out.print("{");
  // for (int ey = -5; ey <= +5; ey++) {
  // MapLocation enemyLoc = new MapLocation(ex, ey);
  // ArrayList<Integer> attacked = new ArrayList<Integer>();
  // for (int dir = 0; dir < 8; dir++) {
  // MapLocation moveLoc = center.add(Direction.values()[dir]);
  // if (moveLoc.distanceSquaredTo(enemyLoc) <=
  // RobotType.SOLDIER.attackRadiusSquared)
  // attacked.add(dir);
  // }
  // System.out.print("{");
  // for (int i = 0; i < attacked.size(); i++) {
  // System.out.print(attacked.get(i));
  // if (i < attacked.size() - 1)
  // System.out.print(",");
  // }
  // System.out.print("}");
  // if (ey < +5) {
  // System.out.print(",");
  // int spaces = Math.min(16, 17 - 2 * attacked.size());
  // for (int i = 0; i < spaces; i++)
  // System.out.print(" ");
  // }
  // }
  // System.out.println("}");
  // }
  // }

  private static int[][][] attackNotes = {
    {
      {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
      },
    {
      {}, {}, {
        7
      }, {
        6, 7
      }, {
        5, 6, 7
      }, {
        5, 6, 7
      }, {
        5, 6, 7
      }, {
        5, 6
      }, {
        5
      }, {}, {}
      },
    {
      {}, {
        7
      }, {
        0, 6, 7
      }, {
        0, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        0, 4, 5, 6, 7
      }, {
        4, 5, 6, 7
      }, {
        4, 5, 6
      }, {
        5
      }, {}
      },
    {
      {}, {
        0, 7
      }, {
        0, 1, 6, 7
      }, {
        0, 1, 2, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 2, 3, 4, 5, 6, 7
      }, {
        3, 4, 5, 6
      }, {
        4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1, 7
      }, {
        0, 1, 2, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        2, 3, 4, 5, 6
      }, {
        3, 4, 5
      }, {}
      },
    {
      {}, {
        0, 1
      }, {
        0, 1, 2, 7
      }, {
        0, 1, 2, 3, 4, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6, 7
      }, {
        0, 1, 2, 3, 4, 5, 6
      }, {
        2, 3, 4, 5
      }, {
        3, 4
      }, {}
      },
    {
      {}, {
        1
      }, {
        0, 1, 2
      }, {
        0, 1, 2, 3
      }, {
        0, 1, 2, 3, 4
      }, {
        0, 1, 2, 3, 4
      }, {
        0, 1, 2, 3, 4
      }, {
        1, 2, 3, 4
      }, {
        2, 3, 4
      }, {
        3
      }, {}
      },
    {
      {}, {}, {
        1
      }, {
        1, 2
      }, {
        1, 2, 3
      }, {
        1, 2, 3
      }, {
        1, 2, 3
      }, {
        2, 3
      }, {
        3
      }, {}, {}
      },
    {
      {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
      }
  };

  @Override
  public int[] getNumEnemiesAttackingMoveDirs(RobotController rc) {
    int[] numEnemiesAttackingMoveDirs = new int[8];
    for (int i = enemyForces.numRobots; i-- > 0;) {
      RobotInfo info = enemyForces.robots[i];
      if (info.type.canAttack()) {
        MapLocation enemyLoc = info.location;
        if (Math.abs(enemyLoc.x - rc.getLocation().x) <= 5 &&
            Math.abs(enemyLoc.y - rc.getLocation().y) <= 5) {
          int[] attackedDirs = attackNotes[5 + enemyLoc.x - rc.getLocation().x][5 + enemyLoc.y
              - rc.getLocation().y];
          for (int j = attackedDirs.length; j-- > 0;) {
            numEnemiesAttackingMoveDirs[attackedDirs[j]]++;
          }
        }
      }
    }
    return numEnemiesAttackingMoveDirs;
  }

  @Override
  public MapLocation getBestEnemyToShoot(RobotController rc) {
    RobotInfo ret = null;
    double bestTurnsToKill = 999;
    double bestWeaponDelay = 999;
    for (int i = enemyForces.numRobots; i-- > 0;) {
      if (!rc.canAttackLocation(enemyForces.robots[i].location)) {
        continue;
      }
      RobotInfo info = enemyForces.robots[i];
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location);
      double turnsToKill = info.health / numNearbyAllies;
      if (turnsToKill < bestTurnsToKill) {
        bestTurnsToKill = turnsToKill;
        bestWeaponDelay = info.weaponDelay;
        ret = info;
      } else if (turnsToKill == bestTurnsToKill) {
        double actionDelay = info.weaponDelay;
        if (actionDelay < bestWeaponDelay) {
          bestWeaponDelay = actionDelay;
          ret = info;
        }
      }
    }
    if (ret != null) {
      return ret.location;
    }
    return null;
  }

  @Override
  public int getNumAttackableEnemies(RobotController rc) {
    RobotInfo[] enemies = enemyForces.robots;
    int numAttackableEnemies = 0;
    for (int i = 0; i < enemyForces.numRobots; i++) {
      if (rc.getLocation().distanceSquaredTo(
          enemies[i].location) < rc.getType().attackRadiusSquared) {
        ++numAttackableEnemies;
      }
    }
    return numAttackableEnemies;

  }

  @Override
  public boolean canOneShotEnemyOrNotGetHit(RobotController rc) {

    boolean canOneHitEnemy = false;
    boolean enemyCanShootAtUs = false;

    for (int i = enemyForces.numRobots; i-- > 0;) {
      RobotInfo enemy = enemyForces.robots[i];
      if (enemy.location.distanceSquaredTo(rc.getLocation()) > rc.getType().attackRadiusSquared) {
        continue;
      }
      if (enemy.health <= rc.getType().attackPower) {
        canOneHitEnemy = true;
        break;
      }
      if (enemy.weaponDelay <= 2.0) {
        enemyCanShootAtUs = true;
      }
    }
    return canOneHitEnemy || !enemyCanShootAtUs;
  }

  @Override
  public Direction getBestRetreatDirection(RobotController rc) {
    int repelX = 0;
    int repelY = 0;
    for (int i = enemyForces.numRobots; i-- > 0;) {
      Direction repelDir = enemyForces.robots[i].location.directionTo(rc.getLocation());
      repelX += repelDir.dx;
      repelY += repelDir.dy;
    }
    for (int i = zombieForces.numRobots; i-- > 0;) {
      Direction repelDir = zombieForces.robots[i].location.directionTo(rc.getLocation());
      repelX += repelDir.dx;
      repelY += repelDir.dy;
    }
    int absRepelX = Math.abs(repelX);
    int absRepelY = Math.abs(repelY);
    Direction retreatDir;
    if (absRepelX >= 1.5 * absRepelY) {
      retreatDir = repelX > 0 ? Direction.EAST : Direction.WEST;
    } else if (absRepelY >= 1.5 * absRepelX) {
      retreatDir = repelY > 0 ? Direction.SOUTH : Direction.NORTH;
    } else if (repelX > 0) {
      retreatDir = repelY > 0 ? Direction.SOUTH_EAST : Direction.NORTH_EAST;
    } else {
      retreatDir = repelY > 0 ? Direction.SOUTH_WEST : Direction.NORTH_WEST;
    }

    int bestMinEnemyDistSq = 999999;
    for (int j = enemyForces.numRobots; j-- > 0;) {
      int enemyDistSq = rc.getLocation().distanceSquaredTo(enemyForces.robots[j].location);
      if (enemyDistSq < bestMinEnemyDistSq)
        bestMinEnemyDistSq = enemyDistSq;
    }
    for (int j = zombieForces.numRobots; j-- > 0;) {
      int zombieDistSq = rc.getLocation().distanceSquaredTo(zombieForces.robots[j].location);
      if (zombieDistSq < bestMinEnemyDistSq)
        bestMinEnemyDistSq = zombieDistSq;
    }
    Direction bestDir = null;
    int[] tryDirs = new int[] {
      0, 1, -1, 2, -2, 3, -3, 4
    };
    for (int i = 0; i < tryDirs.length; i++) {
      Direction tryDir = Direction.values()[(retreatDir.ordinal() + tryDirs[i] + 8) % 8];
      if (!rc.canMove(tryDir))
        continue;
      MapLocation tryLoc = rc.getLocation().add(tryDir);

      int minEnemyDistSq = 999999;
      for (int j = enemyForces.numRobots; j-- > 0;) {
        int enemyDistSq = tryLoc.distanceSquaredTo(enemyForces.robots[j].location);
        if (enemyDistSq < minEnemyDistSq)
          minEnemyDistSq = enemyDistSq;
      }
      for (int j = zombieForces.numRobots; j-- > 0;) {
        int zombieDistSq = tryLoc.distanceSquaredTo(zombieForces.robots[j].location);
        if (zombieDistSq < minEnemyDistSq)
          minEnemyDistSq = zombieDistSq;
      }
      if (minEnemyDistSq > RobotType.SOLDIER.attackRadiusSquared) {
        return tryDir; // we can escape!!
      }
      if (minEnemyDistSq > bestMinEnemyDistSq) {
        bestMinEnemyDistSq = minEnemyDistSq;
        bestDir = tryDir;
      }
    }

    return bestDir;
  }

  public MapLocation getMostHealthAllyWhoNeedsRepaired() {
    return allyForces.mostHealthUnderMaxHealth != null
        ? allyForces.mostHealthUnderMaxHealth.location
        : null;
  }

  public int getZombieStrength() {
    return zombieForces.strength;
  }

  @Override
  public boolean shouldStayNextToTurret(RobotController rc, MapLocation turret) {
    // I'm the lowest id by this turret
    return true;
    /*
     * for (int i = 0; i < allyForces.numRobots; i++) { RobotInfo robot =
     * allyForces.robots[i]; if (robot.type != RobotType.SCOUT) { continue; } if
     * (robot.ID < rc.getID() && robot.location.distanceSquaredTo(turret) <= 2)
     * { return false; } } return true;
     */
  }

  @Override
  public RobotInfo getClosestFriendlyTurret() {
    return allyForces.closestTurret;
  }

  @Override
  public boolean isAttackableByZombieNextTwoTurns(MapLocation loc) {
    for (int i = 0; i < zombieForces.numRobots; i++) {
      RobotInfo zombie = zombieForces.robots[i];
      int dist = zombie.location.distanceSquaredTo(loc);
      if (dist > 9) {
        if (zombie.type == RobotType.RANGEDZOMBIE
            && dist <= RobotType.RANGEDZOMBIE.attackRadiusSquared) {
          return true;
        }
        continue;
      } else if (dist <= 2) {
        if (zombie.weaponDelay < 3) {
          return true;
        }
      } else {
        if (zombie.coreDelay < 2 && zombie.weaponDelay < 2) {
          return true;
        }
      }
    }
    return false;
  }

}
