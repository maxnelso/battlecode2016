package poltbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import poltbot.EeHanTiming.TimingAttack;
import poltbot.InterestingTargets.InterestingTarget;

public class PackedTurretBehavior implements Behavior {

  private static final int MESSAGING_BYTECODE_LIMIT = 3000;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 5000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;
  private final InterestingTargets interestingTargets;
  private final EeHanTiming eeHanTiming;
  private final EnemyTurretCache enemyTurretCache;
  private final ArchonTracker archonTracker;
  private final PatrolWaypointCalculator patrolWaypointCalculator;

  public PackedTurretBehavior(
      NavigationSystem navigation,
      Radar radar,
      MapBoundaryCalculator mapBoundaryCalculator,
      ExplorationCalculator explorationCalculator,
      InterestingTargets interestingTargets,
      ArchonTracker archonTracker,
      EnemyTurretCache enemyTurretCache,
      EeHanTiming eeHanTiming) {
    this.navigation = navigation;
    this.radar = radar;
    this.archonTracker = archonTracker;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.explorationCalculator = explorationCalculator;
    this.interestingTargets = interestingTargets;
    this.enemyTurretCache = enemyTurretCache;
    this.eeHanTiming = eeHanTiming;
    patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        4 /* laneHalfWidth */, 3 /* mapBoundaryMargin */);
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void behave(RobotController rc) throws GameActionException {
    mapBoundaryCalculator.update(rc);
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    archonTracker.updateClosest(rc);

    if (doTimingAttack(rc)) {
      return;
    }

    MapLocation closest = radar.getClosestEnemyOrZombie();
    if (closest != null && closest.distanceSquaredTo(
        rc.getLocation()) <= RobotType.TURRET.attackRadiusSquared) {
      rc.unpack();
    }

    InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
        rc.getLocation(),
        true /* includeDens */,
        false /* includeParts */,
        false /* includeNeutralRobots */);
    if (interestingTarget != null && rc.canSenseLocation(interestingTarget.loc)) {
      RobotInfo robot = rc.senseRobotAtLocation(interestingTarget.loc);
      if (robot == null || robot.type != RobotType.ZOMBIEDEN) {
        interestingTargets.reportDenDestroyed(interestingTarget.loc);
        interestingTarget = null;
      }
    }
    MapLocation target = interestingTarget == null
        ? null
        : interestingTarget.loc;
    if (target == null) {
      target = explorationCalculator.calculate(rc, patrolWaypointCalculator);
    }

    rc.setIndicatorString(0, "EXPLORING TO " + target + " " + rc.getRoundNum());
    navigation.bugTo(rc, target, true /* avoidAttackers */, false /* clearRubble */);
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  private boolean doTimingAttack(RobotController rc) throws GameActionException {
    TimingAttack timingAttack = eeHanTiming.getTimingAttack();
    if (timingAttack == null) {
      return false;
    }

    int roundNum = rc.getRoundNum();
    if (roundNum >= timingAttack.endRound) {
      return false;
    }

    rc.setIndicatorString(0, "EE HAN TIMING, rounds " + timingAttack.startRound + " to "
        + timingAttack.endRound + " at " + timingAttack.rally);

    MapLocation closest = radar.getClosestEnemyOrZombie();
    int siegeDistance = roundNum < timingAttack.startRound
        ? RobotType.TURRET.attackRadiusSquared
        : 34; // :)
    if (closest != null && closest.distanceSquaredTo(rc.getLocation()) <= siegeDistance) {
      rc.unpack();
    }

    navigation.directTo(
        rc,
        timingAttack.rally,
        roundNum < timingAttack.startRound /* avoidAttackers */,
        false /* clearRubble */,
        false /* onlyForward */);
    return true;
  }
}
