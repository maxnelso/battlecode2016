package fibbybot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public interface EnemyTurretCache {

  public void reportRobotInfo(RobotController rc, RobotInfo robot);

  public void reportEnemyTurretPresent(MapLocation loc, int timestamp);

  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp);

  public boolean isInEnemyTurretRange(MapLocation loc);

  public void shareRandomEnemyTurret(
      RobotController rc, MessageSender messageSender) throws GameActionException;

  public void showDebugInfo(RobotController rc) throws GameActionException;
}
