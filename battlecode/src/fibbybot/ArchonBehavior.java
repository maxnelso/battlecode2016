package fibbybot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import fibbybot.InterestingTargets.Interest;
import fibbybot.InterestingTargets.InterestingTarget;

public class ArchonBehavior implements Behavior {

  private final int MAX_MAKE_SCOUT_ROUND = 50;
  private final int SCOUTS_TO_MAKE = 2;
  private final int MAP_AND_TARGET_SHARING_FREQUENCY = 5;
  private static final int RADAR_UPDATE_BYTECODE_LIMIT = 6000;
  private static final int MESSAGING_BYTECODE_LIMIT = 7000;

  private final NavigationSystem navigation;
  private final Radar radar;
  private final RepairSystem repairSystem;
  private final MessageSender messageSender;
  private final InterestingTargets interestingTargets;
  private final ExplorationCalculator explorationCalculator;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ArchonTracker archonTracker;
  private final PartScanner partScanner;
  private final EnemyTurretCache enemyTurretCache;
  private final PatrolWaypointCalculator patrolWaypointCalculator;

  private int scoutsMade;

  public ArchonBehavior(
      NavigationSystem navigation,
      Radar radar,
      RepairSystem repairSystem,
      MessageSender messageSender,
      InterestingTargets interestingTargets,
      ExplorationCalculator explorationCalculator,
      MapBoundaryCalculator mapBoundaryCalculator,
      ArchonTracker archonTracker,
      PartScanner partScanner,
      EnemyTurretCache enemyTurretCache) {
    this.navigation = navigation;
    this.radar = radar;
    this.repairSystem = repairSystem;
    this.messageSender = messageSender;
    this.interestingTargets = interestingTargets;
    this.explorationCalculator = explorationCalculator;
    this.mapBoundaryCalculator = mapBoundaryCalculator;
    this.archonTracker = archonTracker;
    this.partScanner = partScanner;
    this.enemyTurretCache = enemyTurretCache;
    this.patrolWaypointCalculator = new FigureEightPatrolWaypointCalculator();

    scoutsMade = 0;
  }

  @Override
  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return MESSAGING_BYTECODE_LIMIT;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public void behave(RobotController rc) throws GameActionException {
    radar.update(rc, RADAR_UPDATE_BYTECODE_LIMIT);
    mapBoundaryCalculator.update(rc);
    partScanner.scanForParts(rc, interestingTargets);
    MapLocation loc = repairSystem.repairMostHealthAllyThatNeedsRepairing(rc);
    rc.setIndicatorString(2, "Repairing loc " + loc);

    MapLocation currentTarget = null;

    if (scoutsMade < SCOUTS_TO_MAKE
        && rc.getRoundNum() < MAX_MAKE_SCOUT_ROUND
        && makeUnit(rc, RobotType.SCOUT)) {
      scoutsMade++;
    } else {
      MapLocation adjacentNeutralRobot = radar.getAdjacentNeutralRobot(rc.getLocation());

      if (rc.isCoreReady() && adjacentNeutralRobot != null) {
        rc.activate(adjacentNeutralRobot);
      } else {
        makeUnit(rc, RobotType.SOLDIER);
      }

      InterestingTarget interestingTarget = interestingTargets.getClosestTarget(
          rc.getLocation(),
          false /* includeDens */,
          true /* includeParts */,
          true /* includeNeutralRobots */);
      currentTarget = interestingTarget == null
          ? explorationCalculator.calculate(rc, patrolWaypointCalculator)
          : interestingTarget.loc;
      rc.setIndicatorString(0, "Target = " + currentTarget.toString());
      // Invalidate the current interesting target if necessary.
      if (interestingTarget != null && rc.canSenseLocation(currentTarget)) {
        if (interestingTarget.interest == Interest.PARTS && rc.senseParts(currentTarget) < 1) {
          interestingTargets.reportPartsRetrieved(currentTarget);
        } else if (interestingTarget.interest == Interest.NEUTRAL_ROBOT) {
          RobotInfo robot = rc.senseRobotAtLocation(currentTarget);
          if (robot == null || robot.team != Team.NEUTRAL) {
            interestingTargets.reportNeutralRobotActivated(currentTarget);
          }
        }
      }

      navigation.directTo(rc, currentTarget, true /* avoidAttackers */, true /* clearRubble */,
          false /* onlyForward */);
    }

    if ((rc.getRoundNum() - rc.getID()) % MAP_AND_TARGET_SHARING_FREQUENCY == 0) {
      interestingTargets.shareRandomTarget(rc, messageSender);
      mapBoundaryCalculator.shareMapBoundaries(rc, messageSender);
    }

    archonTracker.updateSelfInformation(rc, currentTarget);
    archonTracker.shareRandomArchonInformation(rc, messageSender);
  }

  private boolean makeUnit(RobotController rc, RobotType type) throws GameActionException {
    if (!rc.isCoreReady()) {
      return false;
    }

    if (radar.isAttackableByEnemiesOrZombies(rc.getLocation())) {
      return false;
    }

    Direction d = DirectionUtils.getRandomMovableDirection();
    for (int i = 0; i < 8; i++) {
      if (rc.hasBuildRequirements(type) && rc.canBuild(d, type)) {
        rc.build(d, type);
        return true;
      }
      d = d.rotateLeft();
    }

    return false;
  }
}
