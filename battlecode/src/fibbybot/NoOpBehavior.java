package fibbybot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class NoOpBehavior implements Behavior {

  @Override
  public void behave(RobotController rc) throws GameActionException {}

  public int getMessagingBytecodeLimit(RobotController rc) throws GameActionException {
    return 0;
  }

}
