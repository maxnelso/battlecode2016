package fibbybot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public interface MessageReceiver {

  public void receiveMessages(
      RobotController rc, int bytecodeLimit) throws GameActionException;
}