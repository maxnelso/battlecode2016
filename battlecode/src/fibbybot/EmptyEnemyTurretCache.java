package fibbybot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class EmptyEnemyTurretCache implements EnemyTurretCache {

  @Override
  public void reportRobotInfo(RobotController rc, RobotInfo robot) {}

  @Override
  public void reportEnemyTurretPresent(MapLocation loc, int timestamp) {}

  @Override
  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp) {}

  @Override
  public boolean isInEnemyTurretRange(MapLocation loc) {
    return false;
  }

  @Override
  public void shareRandomEnemyTurret(RobotController rc, MessageSender messageSender)
      throws GameActionException {}

  @Override
  public void showDebugInfo(RobotController rc) throws GameActionException {}
}
