package fibbybot;

import battlecode.common.Clock;
import battlecode.common.RobotController;

/**
 * Used to track bytecode usage
 * 
 * Example usage:
 * 
 * {@link ByteTracker}.init();
 * ....
 * {@link ByteTracker}.split("updateRadar");
 * ....
 * {@link ByteTracker}.split("processMessages");
 * {@link ByteTracker}.indicatorSplits(rc, 2);
 * 
 * @author ryancheu
 *
 */
public class ByteTracker {

  private static int lastBytecode = 0;

  private static final int MAX_SPLITS = 20;
  private static String[] tags;
  private static int[] splits;
  private static int numSplits;
  private static int startByte;

  public static void init() {
    startByte = lastBytecode = Clock.getBytecodeNum();
    tags = new String[MAX_SPLITS];
    splits = new int[MAX_SPLITS];
    numSplits = 0;
  }

  public static void split(String tag) {
    int diff = Clock.getBytecodeNum() - lastBytecode;
    tags[numSplits] = tag;
    splits[numSplits] = diff;
    numSplits++;
    lastBytecode = Clock.getBytecodeNum();
  }

  private static StringBuffer splitsAsBuffer() {
    StringBuffer s = new StringBuffer();
    s.append("start: " + startByte + "   ");
    for (int i = 0; i < numSplits; i++) {
      s.append(tags[i] + ": " + splits[i] + "   ");
    }
    return s;
  }

  public static void printSplits() {
    System.out.println(splitsAsBuffer());
  }

  public static void indicatorSplits(RobotController rc, int index) {
    rc.setIndicatorString(index, splitsAsBuffer().toString());
  }

}
