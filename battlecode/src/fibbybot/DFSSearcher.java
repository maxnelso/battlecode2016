package fibbybot;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

/**
 * Directed DFS searcher to be used for determining a way to an enemy unit
 * @author ryancheu
 *
 */
public class DFSSearcher {

  static int checkedSquares = 0;

  public static Direction search(RobotController rc, MapLocation destination)
      throws GameActionException {
    int start = Clock.getBytecodeNum();
    checkedSquares = 0;
    Direction d = rc.getLocation().directionTo(destination);
    Direction dir = d;
    ArrayMapLocationSet been = new ArrayMapLocationSet();
    for (int i = 5; --i >= 0;) {
      if (i == 3) {
        dir = d.rotateLeft();
      } else if (i == 2) {
        dir = d.rotateRight();
      } else if (i == 1) {
        dir = dir.rotateRight();
      } else if (i == 0) {
        dir = d.rotateLeft().rotateLeft();
      }

      if (rc.canMove(dir) && searchRecurse(rc, destination, rc.getLocation().add(dir),
          been, 0)) {
        rc.setIndicatorString(2, "true, bytes: " + (Clock.getBytecodeNum() - start) + " checked: "
            + checkedSquares);
        return dir;
      }
    }

    rc.setIndicatorString(2, "false, bytes: " + (Clock.getBytecodeNum() - start) + " checked: "
        + checkedSquares);
    return null;
  }

  public static boolean searchRecurse(RobotController rc, MapLocation destination,
      MapLocation current, MapLocationSet been, Integer numCheck) throws GameActionException {
    if (been.contains(current)) {
      return false;
    }

    checkedSquares++;

    been.add(current);
    rc.setIndicatorDot(current, 100, 100, 100);
    if ((numCheck > 5) ||
        (numCheck > 0 && rc.getLocation().isAdjacentTo(current) ||
            !rc.canSense(current) ||
            rc.senseParts(current) > 100 ||
            rc.isLocationOccupied(current) ||
            !rc.onTheMap(current))) {
      return false;
    }

    numCheck += 1;

    // TODO: consider just passing the direction to the target from orignal, or
    // recomputing this every other
    Direction d = current.directionTo(destination);
    Direction dir = d;
    for (int i = 5; --i >= 0;) {
      if (i == 3) {
        dir = d.rotateLeft();
      } else if (i == 2) {
        dir = d.rotateRight();
      } else if (i == 1) {
        dir = dir.rotateRight();
      } else if (i == 0) {
        dir = d.rotateLeft().rotateLeft();
      }

      MapLocation next = current.add(dir);
      if (next.equals(destination) || searchRecurse(rc, destination, next, been, numCheck)) {
        rc.setIndicatorDot(current, (50 - numCheck * 2), (50 - numCheck * 2), (50 - numCheck * 2));
        return true;
      }
    }

    return false;
  }
}
