package fibbybot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class DefaultEnemyTurretCache implements EnemyTurretCache {

  private static final int MAX_TURRETS = 100;

  private static class TurretInfo {
    private final MapLocation loc;
    private final boolean present;
    private final int timestamp;

    private TurretInfo(MapLocation loc, boolean present, int timestamp) {
      this.loc = loc;
      this.present = present;
      this.timestamp = timestamp;
    }

    public TurretInfo asPresent(int newTimestamp) {
      if (present) {
        return this;
      }

      return new TurretInfo(loc, true /* present */, newTimestamp);
    }

    public TurretInfo asAbsent(int newTimestamp) {
      if (!present) {
        return this;
      }

      return new TurretInfo(loc, false /* present */, newTimestamp);
    }
  }

  private final TurretInfo[] turrets;
  private int numTurrets;
  private int shareIndex;

  public DefaultEnemyTurretCache() {
    turrets = new TurretInfo[MAX_TURRETS];
  }

  @Override
  public void reportRobotInfo(RobotController rc, RobotInfo robot) {
    if (robot.team == rc.getTeam().opponent() && robot.type == RobotType.TURRET) {
      reportEnemyTurretPresent(robot.location, rc.getRoundNum());
    }
  }

  @Override
  public void reportEnemyTurretPresent(MapLocation loc, int timestamp) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc) && turret.timestamp < timestamp) {
        turrets[i] = turret.asPresent(timestamp);
        return;
      }
    }

    if (numTurrets < MAX_TURRETS) {
      turrets[numTurrets++] = new TurretInfo(loc, true /* present */, timestamp);
    }
  }

  @Override
  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.loc.equals(loc) && turret.timestamp < timestamp) {
        turrets[i] = turret.asAbsent(timestamp);
        return;
      }
    }

    if (numTurrets < MAX_TURRETS) {
      turrets[numTurrets++] = new TurretInfo(loc, false /* present */, timestamp);
    }
  }

  @Override
  public boolean isInEnemyTurretRange(MapLocation loc) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.present &&
          turret.loc.distanceSquaredTo(loc) <= RobotType.TURRET.attackRadiusSquared) {
        return true;
      }
    }

    return false;
  }

  @Override
  public void shareRandomEnemyTurret(
      RobotController rc, MessageSender messageSender) throws GameActionException {
    if (numTurrets == 0) {
      return;
    }

    shareIndex = shareIndex % numTurrets;
    TurretInfo turret = turrets[shareIndex];
    if (turrets[shareIndex].present) {
      messageSender.sendEnemyTurret(rc, turret.loc, turret.timestamp);
    } else {
      messageSender.sendRemoveEnemyTurret(rc, turret.loc, turret.timestamp);
    }
    shareIndex++;
  }

  @Override
  public void showDebugInfo(RobotController rc) {
    for (int i = 0; i < numTurrets; i++) {
      TurretInfo turret = turrets[i];
      if (turret.present) {
        rc.setIndicatorLine(rc.getLocation(), turret.loc, 50, 50, 50);
      }
    }
  }
}
