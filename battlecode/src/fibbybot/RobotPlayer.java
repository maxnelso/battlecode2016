package fibbybot;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {
  public static void run(RobotController rc) {
    ArchonTracker archonTracker = new ArchonTracker();
    PartScanner partScanner = new PartScanner();
    EnemyTurretCache enemyTurretCache = new EmptyEnemyTurretCache();
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator();
    InterestingTargets interestingTargets = new InterestingTargets(enemyTurretCache);
    Radar radar = new DefaultRadar(interestingTargets, archonTracker, enemyTurretCache);
    RadarAttackRepairSystem attackRepairSystem = new RadarAttackRepairSystem(radar);
    DuckNavigationSystem navigation = new DuckNavigationSystem(radar);
    ExplorationCalculator explorationCalculator = new ExplorationCalculator(mapBoundaryCalculator);
    ZombieDragger zombieDragger = new ZombieDragger(radar, navigation, archonTracker);
    Messenger messenger = new Messenger(
        interestingTargets,
        archonTracker,
        radar,
        mapBoundaryCalculator,
        enemyTurretCache);

    Behavior behavior;
    switch (rc.getType()) {
      case ARCHON:
        behavior = new ArchonBehavior(
            navigation,
            radar,
            attackRepairSystem /* repairSystem */,
            messenger /* messageSender */,
            interestingTargets,
            explorationCalculator,
            mapBoundaryCalculator,
            archonTracker,
            partScanner,
            enemyTurretCache);
        break;
      case SOLDIER:
        behavior = new SoldierBehavior(
            navigation,
            attackRepairSystem /* attackSystem */,
            mapBoundaryCalculator,
            explorationCalculator,
            radar,
            interestingTargets,
            archonTracker);
        break;
      case VIPER:
      case GUARD:
      case BIGZOMBIE:
      case FASTZOMBIE:
      case RANGEDZOMBIE:
      case STANDARDZOMBIE:
        behavior = new ShootBehavior(navigation, attackRepairSystem /* attackSystem */);
        break;
      case SCOUT:
        behavior = new ScoutBehavior(
            rc.getID(),
            navigation,
            radar,
            messenger /* messageSender */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator,
            archonTracker,
            zombieDragger,
            partScanner,
            enemyTurretCache);
        break;
      case TURRET:
      case TTM:
        behavior = new TurretBehavior(
            navigation,
            radar,
            attackRepairSystem /* attackSystem */,
            interestingTargets,
            mapBoundaryCalculator,
            explorationCalculator);
        break;
      case ZOMBIEDEN:
      default:
        behavior = new NoOpBehavior();
        break;
    }

    while (true) {
      try {
        messenger.receiveMessages(
            rc, behavior.getMessagingBytecodeLimit(rc));
        behavior.behave(rc);
      } catch (GameActionException e) {
        e.printStackTrace();
      }
      Clock.yield();
    }
  }
}
