package kerriganbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ViperAttackSystem implements AttackSystem {

  @Override
  public MapLocation getBestEnemyToShoot(RobotController rc,
      RobotInfo[] enemies,
      int numEnemies,
      RobotInfo[] allies,
      int numAllies) {
    RobotPlayer.profiler.split("before best enemy");
    RobotInfo ret = null;
    double bestTurnsToKill = 99999;
    double bestWeaponDelay = 99999;
    for (int i = numEnemies; --i >= 0;) {
      if (!rc.canAttackLocation(enemies[i].location)) {
        continue;
      }
      RobotInfo info = enemies[i];
      if (info.type == RobotType.GUARD) {
        continue;
      }

      if (info.type == RobotType.ARCHON && numAllies >= 4 && info.health <= 750) {
        // Don't create a zombie outbreak if we are just gonna kill him anyway
        continue;
      }

      RobotInfo[] alliesNearTarget = rc.senseNearbyRobots(info.location, 2, rc.getTeam());

      // Always prefer to poison people
      if (info.viperInfectedTurns == 0 && alliesNearTarget.length == 0) {
        return info.location;
      }
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location, allies,
          numAllies);
      double turnsToKill = info.health / numNearbyAllies;
      if (turnsToKill < bestTurnsToKill) {
        bestTurnsToKill = turnsToKill;
        bestWeaponDelay = info.weaponDelay;
        ret = info;
      } else if (turnsToKill == bestTurnsToKill) {
        double actionDelay = info.weaponDelay;
        if (actionDelay < bestWeaponDelay) {
          bestWeaponDelay = actionDelay;
          ret = info;
        }
      }
    }
    RobotPlayer.profiler.split("after best enemy");
    if (ret != null) {
      return ret.location;
    }
    return null;
  }

  public MapLocation getBestZombieToShoot(RobotController rc,
      RobotInfo[] zombies,
      int numZombies,
      RobotInfo[] allies,
      int numAllies) {

    RobotPlayer.profiler.split("before best enemy");
    RobotInfo bestNonRanged = null;
    double bestNonTurnsToKill = 99999;
    double bestNonWeaponDelay = 99999;

    RobotInfo bestRanged = null;
    double bestRangedTurnsToKill = 99999;
    double bestRangedWeaponDelay = 99999;
    for (int i = numZombies; --i >= 0;) {
      if (!rc.canAttackLocation(zombies[i].location)) {
        continue;
      }

      RobotInfo info = zombies[i];
      int numNearbyAllies = 1 + getNumAlliesWhoCanAttackLocation(rc, info.location, allies,
          numAllies);
      double turnsToKill = info.health / numNearbyAllies;
      if (info.type == RobotType.RANGEDZOMBIE) {
        if (turnsToKill < bestRangedTurnsToKill) {
          bestRangedTurnsToKill = turnsToKill;
          bestRangedWeaponDelay = info.weaponDelay;
          bestRanged = info;
        } else if (turnsToKill == bestRangedTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestRangedWeaponDelay) {
            bestRangedWeaponDelay = actionDelay;
            bestRanged = info;
          }
        }
      } else {
        if (turnsToKill < bestNonTurnsToKill) {
          bestNonTurnsToKill = turnsToKill;
          bestNonWeaponDelay = info.weaponDelay;
          bestNonRanged = info;
        } else if (turnsToKill == bestNonTurnsToKill) {
          double actionDelay = info.weaponDelay;
          if (actionDelay < bestNonWeaponDelay) {
            bestNonWeaponDelay = actionDelay;
            bestNonRanged = info;
          }
        }
      }
    }

    if (bestRanged != null) {
      return bestRanged.location;
    }
    if (bestNonRanged != null) {
      return bestNonRanged.location;
    }
    return null;
  }

  // TODO Could be optimized by assuming soldier range?
  private static int getNumAlliesWhoCanAttackLocation(RobotController rc, MapLocation location,
      RobotInfo[] allies, int numAllies) {
    return rc.senseNearbyRobots(location, RobotType.SOLDIER.attackRadiusSquared, rc
        .getTeam()).length;
  }
}
