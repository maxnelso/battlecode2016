package kerriganbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import battlecode.common.Signal;

public class RangedArmyBehavior implements PreBehavior, Behavior {

  private final RobotController rc;
  private final MessageReceiver messageReceiver;
  private final BasicMessages basicMessages;
  private final Radar radar;
  private final ArmyRally armyRally;
  private final EeHanTimingReporter eeHanTimingReporter;
  private final ZombieDenReporter zombieDenReporter;
  private final ViperSacReporter viperSacReporter;
  private final HostileUnitTracker hostileUnitTracker;

  private final ApocalypseBehavior apocalypseBehavior;
  private final FightingRangedBehavior fightingBehavior;
  private final SwarmingBehavior swarmingBehavior;
  private final RallyBehavior rallyBehavior;
  private final TimingAttackBehavior timingAttackBehavior;
  private final HuntingBehavior huntingBehavior;
  private final ExploringBehavior exploringBehavior;
  private final HealingBehavior healingBehavior;

  private double healingHealth;
  private boolean healing;

  public RangedArmyBehavior(
      RobotController rc,
      MessageReceiver messageReceiver,
      AlliedArchonTracker alliedArchonTracker,
      Radar radar,
      NavigationSystem navigation,
      AttackSystem attackSystem,
      ZombieSpawnScheduleInfo zombieSchedule,
      ArmyRally armyRally,
      EeHanTimingReporter eeHanTimingReporter,
      ZombieDenReporter zombieDenReporter,
      ViperSacReporter viperSacReporter,
      HostileUnitTracker hostileUnitTracker,
      MessageSender messageSender,
      BasicMessages basicMessages) {
    this.radar = radar;
    this.rc = rc;
    this.messageReceiver = messageReceiver;
    this.basicMessages = basicMessages;
    this.armyRally = armyRally;
    this.eeHanTimingReporter = eeHanTimingReporter;
    this.zombieDenReporter = zombieDenReporter;
    this.viperSacReporter = viperSacReporter;
    this.hostileUnitTracker = hostileUnitTracker;

    apocalypseBehavior = new ApocalypseBehavior(rc, navigation, radar, viperSacReporter);
    fightingBehavior = new FightingRangedBehavior(rc, radar, navigation, zombieSchedule,
        attackSystem, hostileUnitTracker, messageSender, basicMessages);
    swarmingBehavior = new SwarmingBehavior(rc, navigation, alliedArchonTracker);
    rallyBehavior = new RallyBehavior(rc, armyRally, navigation);
    timingAttackBehavior = new TimingAttackBehavior(rc, eeHanTimingReporter, navigation);
    huntingBehavior = new HuntingBehavior(rc, navigation, zombieDenReporter);
    healingBehavior = new HealingBehavior(rc, navigation, alliedArchonTracker, radar, attackSystem);
    PatrolWaypointCalculator patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);
    exploringBehavior = new ExploringBehavior(rc, navigation, radar, patrolWaypointCalculator);
    healingHealth = rc.getType().maxHealth / 2;
    healing = false;
  }

  @Override
  public void preRun() throws GameActionException {
    // Don't receive messages in battle.
    if (radar.getNearbyHostiles().length != 0) {
      return;
    }

    if (rc.getHealth() >= rc.getType().maxHealth) {
      healing = false;
    }

    messageReceiver.receiveMessages();
    zombieDenReporter.invalidateNearbyDestroyedDens();
    RobotPlayer.profiler.split("after receiving messages");
  }

  @Override
  public void run() throws GameActionException {
    getCurrentBehavior().run();
  }

  private Behavior getCurrentBehavior() throws GameActionException {
    if ((rc.getHealth() < healingHealth || healing) && healingBehavior.shouldHeal()) {
      healing = true;
      return healingBehavior;
    }
    if (radar.getNearbyHostiles().length == 0) {
      Signal[] allyMessages = basicMessages.getAllyBasicMessages();
      for (Signal s : allyMessages) {
        boolean interpretAsEnemyLoc = false;
        if (rc.canSenseRobot(s.getRobotID())) {
          interpretAsEnemyLoc = rc.senseRobot(s.getRobotID()).type == RobotType.SOLDIER;
        } else {
          interpretAsEnemyLoc = true;
        }

        if (interpretAsEnemyLoc) {
          int broadcastRound = s.getRobotID() < rc.getID() ? rc.getRoundNum()
              : rc.getRoundNum() - 1;
          Direction offsetDir = Direction.values()[broadcastRound % 8];
          MapLocation loc = s.getLocation().add(offsetDir, 3);

          rc.setIndicatorString(1, "I think offset dir is: " + offsetDir + " with val: "
              + (broadcastRound % 8) + " on turn: " + rc.getRoundNum() + " from: " + s
                  .getRobotID() + "for location: " + loc);

          hostileUnitTracker.reportEnemy(loc);
        }
      }
    }

    if (viperSacReporter.getSacAttack() != null) {
      return apocalypseBehavior;
    } else if (hostileUnitTracker.getClosestHostileLoc() != null ||
        radar.getNearbyHostiles().length != 0) {
      return fightingBehavior;
    }

    MapLocation huntingTarget = huntingBehavior.getTarget();
    MapLocation rallyLoc = armyRally.getRally();
    MapLocation myLoc = rc.getLocation();
    if (rallyLoc != null && (huntingTarget == null || (huntingTarget.distanceSquaredTo(
        myLoc) >= rallyLoc.distanceSquaredTo(myLoc)))) {
      return rallyBehavior;
    }
    if (eeHanTimingReporter.getTimingAttack() != null) {
      return timingAttackBehavior;
    }
    if (huntingBehavior.getTarget() != null) {
      return huntingBehavior;
    }
    if (swarmingBehavior.shouldSwarm()) {
      return swarmingBehavior;
    }
    return exploringBehavior;
  }
}
