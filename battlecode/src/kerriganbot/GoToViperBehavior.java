package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class GoToViperBehavior implements Behavior {

  private static final int ARCHON_TIMEOUT = 50;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final Radar radar;

  private RobotInfo closestViper;

  public GoToViperBehavior(
      RobotController rc,
      NavigationSystem navigation,
      Radar radar) {
    this.rc = rc;
    this.navigation = navigation;
    this.radar = radar;
  }

  @Override
  public void run() throws GameActionException {
    if (closestViper != null) {
      rc.setIndicatorString(0, "I'm going to viper " + rc.getRoundNum());
      MapLocation target = getViperSurroundTarget(closestViper.location);
      navigation.directTo(target, true /* avoidAttackers */, false);
      RobotPlayer.profiler.split("after navigation to closest archon");
    } else {
      rc.setIndicatorString(0, "I'm lost.");
      navigation.moveRandomly();
      RobotPlayer.profiler.split("after moving randomly");
    }
  }

  private MapLocation getViperSurroundTarget(MapLocation viperLoc) {
    return viperLoc.add(DirectionUtils.movableDirections[rc.getID() % 8], 2);
  }

  public boolean shouldGoToViper() {
    RobotInfo[] allies = radar.getNearbyAllies();
    int closestDist = 99999;
    closestViper = null;
    for (int i = allies.length; --i >= 0;) {
      if (allies[i].type != RobotType.VIPER) {
        continue;
      }
      int dist = allies[i].location.distanceSquaredTo(rc.getLocation());
      if (dist < closestDist) {
        closestDist = dist;
        closestViper = allies[i];
      }
    }

    return closestViper != null;
  }
}
