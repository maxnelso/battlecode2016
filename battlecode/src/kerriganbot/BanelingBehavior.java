package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class BanelingBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;
  private final Radar radar;
  private final BanelingReporter banelingReporter;
  private final MessageSender messageSender;

  private RobotInfo bestBaneling;
  private boolean alreadyABaneling;

  public BanelingBehavior(
      RobotController rc,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker,
      MessageSender messageSender,
      BanelingReporter banelingReporter,
      Radar radar) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
    this.radar = radar;
    this.banelingReporter = banelingReporter;
    this.messageSender = messageSender;
    this.alreadyABaneling = false;
  }

  @Override
  public void run() throws GameActionException {
    alreadyABaneling = true;
    if (rc.getViperInfectedTurns() > 0) {
      RobotInfo closestEnemy = RadarUtils.getClosestRobot(radar.getNearbyEnemies(), rc
          .getLocation());
      if (closestEnemy == null) {
        MapLocation[] banelings = banelingReporter.getBanelings();
        if (banelings.length > 0) {
          int closestDist = 99999;
          int closestIndex = -1;
          for (int i = banelings.length; --i >= 0;) {
            int dist = banelings[i].distanceSquaredTo(rc.getLocation());
            if (dist < closestDist) {
              closestDist = dist;
              closestIndex = i;
            }
          }
          navigation.directToOnlyForward(banelings[closestIndex], false /* avoidAttackers */,
              false /* clearRubble */);
        }
      } else {
        navigation.directToOnlyForward(closestEnemy.location, false /* avoidAttackers */,
            false /* clearRubble */);
      }
    }
    if (rc.getViperInfectedTurns() == 1) {
      rc.disintegrate();
    }
    if (rc.getViperInfectedTurns() == 0) {
      messageSender.sendWantToBomb();
    }
  }

  public boolean shouldBaneling() throws GameActionException {
    if (alreadyABaneling && rc.getInfectedTurns() > 0) {
      return true;
    }
    RobotInfo[] enemies = radar.getNearbyEnemies();
    RobotInfo[] allies = radar.getNearbyAllies();

    MapLocation myLoc = rc.getLocation();
    int scoutCount = 0;
    boolean viperInRange = false;
    for (int i = allies.length; --i >= 0;) {
      if (allies[i].type == RobotType.SCOUT) {
        scoutCount++;
      }
      if (allies[i].type == RobotType.VIPER && allies[i].location.distanceSquaredTo(
          myLoc) <= RobotType.VIPER.attackRadiusSquared) {
        viperInRange = true;
      }
    }
    if (!viperInRange) {
      return false;
    }
    return (enemies.length > 4 && scoutCount >= 3) || banelingReporter.getBanelings().length >= 2;
  }
}
