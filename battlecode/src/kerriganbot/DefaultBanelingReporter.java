package kerriganbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class DefaultBanelingReporter implements BanelingReporter {

  private static final int MAX_ROBOTS = 100;

  private final RobotController rc;

  private int numBanelings;
  private MapLocation[] banelings;
  private MapLocation[] cachedBanelings;
  private int lastUpdateRound;

  public DefaultBanelingReporter(RobotController rc) {
    this.rc = rc;
    numBanelings = 0;
    lastUpdateRound = 0;
    banelings = new MapLocation[MAX_ROBOTS];
  }

  public MapLocation[] getBanelings() {
    if (rc.getRoundNum() == lastUpdateRound) {
      return cachedBanelings;
    }
    cachedBanelings = new MapLocation[numBanelings];
    for (int i = numBanelings; --i >= 0;) {
      cachedBanelings[i] = banelings[i];
    }
    return cachedBanelings;
  }

  public void addBaneling(MapLocation loc) {
    banelings[numBanelings++] = loc;
  }

  public void clearBanelings() {
    numBanelings = 0;
  }
}
