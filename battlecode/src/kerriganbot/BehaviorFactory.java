package kerriganbot;

import battlecode.common.RobotController;

public class BehaviorFactory {

  public static class BehaviorInfo {

    public final PreBehavior preBehavior;
    public final Behavior behavior;

    public BehaviorInfo(PreBehavior preBehavior, Behavior behavior) {
      this.preBehavior = preBehavior;
      this.behavior = behavior;
    }
  }

  public static BehaviorInfo createForRobotController(RobotController rc) {
    switch (rc.getType()) {
      case ARCHON:
        return archon(rc);
      case SCOUT:
        return scout(rc);
      case SOLDIER:
        return soldier(rc);
      case GUARD:
        return guard(rc);
      case VIPER:
        return viper(rc);
      case TURRET:
        return turret(rc);
      default:
        NoOpBehavior b = new NoOpBehavior(rc);
        return new BehaviorInfo(b /* preBehavior */, b /* behavior */);
    }
  }

  private static BehaviorInfo archon(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    RepairSystem repairSystem = new DefaultRepairSystem(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(rc, radar, enemyTurretCache);
    EnemyMessageProcessor enemyMessageProcessor = new MatchObservationEnemyMessageProcessor(rc);
    PickupLocationReporter pickupLocationReporter = new DefaultPickupLocationReporter(rc);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    ArchonHider archonHider = new DefaultArchonHider();
    ViperSacManager viperSacManager = new ViperSacManager(
        rc, mapBoundaryCalculator, zombieDenReporter, enemyTurretCache, archonHider);
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BanelingReporter banelingReporter = new NoOpBanelingReporter();

    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        new NoOpBasicMessages(),
        armyRallyReporter,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        pickupLocationReporter,
        viperSacManager /* viperSacReporter */,
        enemyMessageProcessor,
        banelingReporter,
        hostileUnitTracker);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);

    ArchonBehavior b = new ArchonBehavior(
        rc,
        mapBoundaryCalculator,
        radar,
        navigation,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        repairSystem,
        zombieDenReporter,
        enemyTurretCache,
        pickupLocationReporter,
        viperSacManager /* viperSacCalculator */,
        viperSacManager /* viperSacReporter */,
        archonHider);
    return new BehaviorInfo(b /* preBehavior */, b /* behavior */);
  }

  private static BehaviorInfo scout(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    EnemyArchonTracker enemyArchonTracker = new DefaultEnemyArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    BasicMessages basicMessages = new DefaultBasicMessages();
    ArmyRallyReporter armyRallyReporter = new DefaultArmyRally(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(rc, radar, enemyTurretCache);
    PickupLocationReporter pickupLocationReporter = new DefaultPickupLocationReporter(rc);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    BuddyDistantHostileReporter buddyHostileReporter = new BuddyDistantHostileReporter(rc);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyTurretCache,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BanelingReporter banelingReporter = new DefaultBanelingReporter(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        buddyHostileReporter,
        zombieDenReporter,
        basicMessages,
        armyRallyReporter,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        pickupLocationReporter,
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor(),
        banelingReporter,
        hostileUnitTracker);

    PreBehavior preBehavior = new ScoutPreBehavior(
        rc,
        radar,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        alliedArchonTracker,
        zombieDenReporter,
        enemyTurretCache,
        mapBoundaryCalculator,
        eeHanTimingManager /* eeHanTimingCalculator */);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    Behavior behavior = new ScoutBehavior(
        rc,
        navigation,
        radar,
        basicMessages,
        enemyArchonTracker,
        alliedArchonTracker,
        enemyTurretCache,
        messageSenderReceiver,
        pickupLocationReporter,
        buddyHostileReporter,
        banelingReporter,
        viperSacManager /* viperSacReporter */);
    return new BehaviorInfo(preBehavior, behavior);
  }

  private static BehaviorInfo soldier(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(rc, radar, enemyTurretCache);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    BasicMessages basicMessages = new DefaultBasicMessages();

    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyTurretCache,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BanelingReporter banelingReporter = new NoOpBanelingReporter();
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor(),
        banelingReporter,
        hostileUnitTracker);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior soldierBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        basicMessages);
    return new BehaviorInfo(soldierBehavior /* preBehavior */, soldierBehavior /* behavior */);
  }

  private static BehaviorInfo guard(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new NonPersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(rc, radar, enemyTurretCache);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    BasicMessages basicMessages = new DefaultBasicMessages();

    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyTurretCache,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BanelingReporter banelingReporter = new NoOpBanelingReporter();
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor(),
        banelingReporter,
        hostileUnitTracker);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new DefaultAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(rc, rc
        .getZombieSpawnSchedule());
    RangedArmyBehavior rangedArmyBehavior = new RangedArmyBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        basicMessages);
    return new BehaviorInfo(
        rangedArmyBehavior /* preBehavior */, rangedArmyBehavior /* behavior */);
  }

  private static BehaviorInfo viper(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(rc, radar, enemyTurretCache);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyTurretCache,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BasicMessages basicMessages = new DefaultBasicMessages();
    BanelingReporter banelingReporter = new DefaultBanelingReporter(rc);
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        new NoOpDistantHostileReporter(),
        zombieDenReporter,
        basicMessages,
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor(),
        banelingReporter,
        hostileUnitTracker);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new ViperAttackSystem();
    ZombieSpawnScheduleInfo zombieSpawnScheduleInfo = new ZombieSpawnScheduleInfo(
        rc, rc.getZombieSpawnSchedule());
    ViperBehavior viperBehavior = new ViperBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        alliedArchonTracker,
        radar,
        navigation,
        attackSystem,
        zombieSpawnScheduleInfo,
        defaultArmyRally /* armyRally */,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */,
        hostileUnitTracker,
        messageSenderReceiver,
        banelingReporter,
        basicMessages);
    return new BehaviorInfo(viperBehavior /* preBehavior */, viperBehavior /* behavior */);
  }

  private static BehaviorInfo turret(RobotController rc) {
    AlliedArchonTracker alliedArchonTracker = new PersistentAlliedArchonTracker(rc);
    Radar radar = new SenseRadar(rc);
    ZombieDenReporter zombieDenReporter = new DefaultZombieDenReporter(rc, radar);
    ExtendedRadar extendedRadar = new ExtendedRadar(rc, radar);
    DefaultArmyRally defaultArmyRally = new DefaultArmyRally(rc);
    EnemyTurretCache enemyTurretCache = new DefaultEnemyTurretCache(rc);
    EeHanTimingManager eeHanTimingManager = new EeHanTimingManager(
        rc, extendedRadar /* radar */, enemyTurretCache);
    MapBoundaryCalculator mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    ViperSacManager viperSacManager = new ViperSacManager(
        rc,
        mapBoundaryCalculator,
        zombieDenReporter,
        enemyTurretCache,
        new DefaultArchonHider());
    HostileUnitTracker hostileUnitTracker = new HostileUnitTracker(rc);
    BanelingReporter banelingReporter = new NoOpBanelingReporter();
    DefaultMessageSenderReceiver messageSenderReceiver = new DefaultMessageSenderReceiver(
        rc,
        mapBoundaryCalculator,
        alliedArchonTracker,
        extendedRadar /* distantHostileReporter */,
        zombieDenReporter,
        new NoOpBasicMessages(),
        defaultArmyRally /* armyRallyReporter */,
        enemyTurretCache,
        eeHanTimingManager /* eeHanTimingReporter */,
        new NoOpPickupLocationReporter(),
        viperSacManager /* viperSacReporter */,
        new NoOpEnemyMessageProcessor(),
        banelingReporter,
        hostileUnitTracker);
    NavigationSystem navigation = new DuckNavigationSystem(
        rc, radar, alliedArchonTracker, enemyTurretCache);
    AttackSystem attackSystem = new TurretAttackSystem();
    TurretBehavior turretBehavior = new TurretBehavior(
        rc,
        messageSenderReceiver /* messageReceiver */,
        messageSenderReceiver /* messageSender */,
        extendedRadar /* radar */,
        navigation,
        alliedArchonTracker,
        attackSystem,
        defaultArmyRally,
        eeHanTimingManager /* eeHanTimingReporter */,
        zombieDenReporter,
        viperSacManager /* viperSacReporter */);
    return new BehaviorInfo(turretBehavior /* preBehavior */, turretBehavior /* behavior */);
  }
}
