package kerriganbot;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class HostileUnitTracker {

  private MapLocation closestHostile;

  private int lastReportRound = -1;

  private RobotController rc;

  private int distToClosest = Integer.MAX_VALUE;
  private int lastCalcDistRound = -1;

  private static final int REPORT_TIME_OUT_TURNS = 10;

  public HostileUnitTracker(RobotController rc) {
    this.rc = rc;
  }

  public void reportEnemy(MapLocation loc) {
    int curRound = rc.getRoundNum();
    MapLocation myLoc = rc.getLocation();

    if (closestHostile != null && rc.getRoundNum() != lastCalcDistRound) {
      distToClosest = myLoc.distanceSquaredTo(closestHostile);
      lastCalcDistRound = rc.getRoundNum();
    }

    int tempDist = loc.distanceSquaredTo(myLoc);
    if (curRound - lastReportRound > REPORT_TIME_OUT_TURNS || tempDist < distToClosest) {
      distToClosest = tempDist;
      closestHostile = loc;
      lastReportRound = curRound;
    }

  }

  public MapLocation getClosestHostileLoc() {
    if (rc.getRoundNum() - lastReportRound > REPORT_TIME_OUT_TURNS) {
      closestHostile = null;
    }

    return closestHostile;
  }
}
