package kerriganbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public class BuddyScoutBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final BuddySystem buddySystem;
  private final Radar radar;
  private final BuddyDistantHostileReporter buddyDistantHostileReporter;
  private final MessageSender messageSender;

  public BuddyScoutBehavior(
      RobotController rc,
      NavigationSystem navigation,
      BuddySystem buddySystem,
      Radar radar,
      BuddyDistantHostileReporter buddyDistantHostileReporter,
      MessageSender messageSender) {
    this.rc = rc;
    this.navigation = navigation;
    this.radar = radar;
    this.buddySystem = buddySystem;
    this.buddyDistantHostileReporter = buddyDistantHostileReporter;
    this.messageSender = messageSender;
  }

  @Override
  public void run() throws GameActionException {
    if (!buddySystem.hasBuddy()) {
      return;
    }
    RobotInfo turret = rc.senseRobot(buddySystem.getBuddy());
    rc.setIndicatorString(0, "I'm sitting by turret buddy " + buddySystem.getBuddy());
    RobotInfo closestHostile = RadarUtils.getClosestRobot(radar
        .getNearbyHostiles(), rc.getLocation());
    if (closestHostile != null) {
      Direction awayFromEnemies = rc.getLocation().directionTo(closestHostile.location).opposite();
      MapLocation desiredLocation = turret.location.add(awayFromEnemies);
      if (desiredLocation.distanceSquaredTo(turret.location) == 2) {
        desiredLocation = Math.random() < .5 ? turret.location.add(awayFromEnemies.rotateLeft())
            : turret.location.add(awayFromEnemies.rotateRight());
      }
      rc.setIndicatorString(2, "Desired location " + desiredLocation);
      if (!rc.getLocation().equals(desiredLocation)) {
        if (!navigation.directToOnlyForward(
            desiredLocation,
            true /* avoidAttackers */,
            false /* clearRubble */)) {
          navigation.directTo(desiredLocation, false /* avoidAttackers */, false /* clearRubble */);
        }
      }
    } else {
      if (rc.getLocation().distanceSquaredTo(turret.location) > 2) {
        if (!navigation.directTo(
            turret.location,
            true /* avoidAttackers */,
            false /* clearRubble */)) {
          navigation.directTo(turret.location, false /* avoidAttackers */, false /* clearRubble */);
        }
      }
    }
    shareExtendedBroadcast();
    return;
  }

  public boolean shouldBuddy() throws GameActionException {
    if (buddySystem.hasBuddy() &&
        rc.canSenseRobot(buddySystem.getBuddy()) &&
        amLowestIdAdjacent()) { // Turret's
                                // still
                                // there
      return true;
    }
    buddySystem.removeBuddy();

    RobotInfo closestTurretNeedingBuddy = null;
    RobotInfo[] allies = radar.getNearbyAllies();
    for (int i = allies.length; --i >= 0;) {
      RobotInfo ally = allies[i];
      RobotType type = ally.type;
      if (type != RobotType.TTM && type != RobotType.TURRET) {
        continue;
      }
      RobotInfo[] adjacentRobots = rc.senseNearbyRobots(ally.location, 2, rc.getTeam());
      boolean foundOtherScout = false;
      for (int j = adjacentRobots.length; --j >= 0;) {
        RobotInfo adjacentRobot = adjacentRobots[j];
        if (adjacentRobot.type == RobotType.SCOUT &&
            adjacentRobot.ID < rc.getID()) {
          foundOtherScout = true;
          break;
        }
      }
      if (!foundOtherScout) {
        closestTurretNeedingBuddy = ally;
      }
    }
    if (closestTurretNeedingBuddy != null) {
      buddySystem.setBuddy(closestTurretNeedingBuddy.ID);
    }
    return buddySystem.hasBuddy();
  }

  private void shareExtendedBroadcast()
      throws GameActionException {
    RobotInfo broadcastedRobot = null;
    RobotInfo buddy = null;
    if (buddySystem.hasBuddy() && rc.canSenseRobot(buddySystem.getBuddy())) {
      buddy = rc.senseRobot(buddySystem.getBuddy());
    } else {
      buddy = getClosestFriendlyTurret();
    }
    if (buddy != null) {
      broadcastedRobot = getBuddyTarget(buddy);
    }
    if (broadcastedRobot != null) {
      rc.setIndicatorString(1, "Broadcasting " + broadcastedRobot.location + " " + rc
          .getRoundNum());
      messageSender.sendDistantHostileInfo(broadcastedRobot.location, broadcastedRobot.type,
          broadcastedRobot.coreDelay,
          broadcastedRobot.team == Team.ZOMBIE, Math.max(9, rc.getLocation().distanceSquaredTo(
              buddy.location)));
    }
  }

  private RobotInfo getClosestFriendlyTurret() {
    RobotInfo[] allies = radar.getNearbyAllies();
    int closestDist = 999999;
    RobotInfo closestTurret = null;
    for (int i = allies.length; --i >= 0;) {
      RobotInfo robot = allies[i];
      if (robot.type != RobotType.TURRET) {
        continue;
      }
      int dist = rc.getLocation().distanceSquaredTo(robot.location);
      if (dist < closestDist) {
        closestTurret = robot;
      }
    }
    return closestTurret;
  }

  private RobotInfo getBuddyTarget(RobotInfo buddy) {
    RobotInfo[] enemies = radar.getNearbyEnemies();
    double leastHealthTurret = 99999;
    RobotInfo bestTurretInfo = null;

    double leastHealthCanHitEnemy = 99999;
    RobotInfo bestCanHitEnemy = null;

    double leastHealthOtherEnemy = 99999;
    RobotInfo bestOtherEnemy = null;
    for (int i = enemies.length; --i >= 0;) {
      RobotInfo enemy = enemies[i];
      int dist = enemy.location.distanceSquaredTo(buddy.location);
      if (dist > RobotType.TURRET.attackRadiusSquared) {
        continue;
      }
      if (buddyDistantHostileReporter.alreadyReported(enemy.location)) {
        continue;
      }
      if (enemy.type == RobotType.TURRET) {
        if (enemy.health < leastHealthTurret) {
          leastHealthTurret = enemy.health;
          bestTurretInfo = enemy;
        }
      } else if (canHit(enemy)) {
        if (enemy.health < leastHealthCanHitEnemy) {
          leastHealthCanHitEnemy = enemy.health;
          bestCanHitEnemy = enemy;
        }
      } else {
        if (enemy.health < leastHealthOtherEnemy) {
          leastHealthOtherEnemy = enemy.health;
          bestOtherEnemy = enemy;
        }
      }
    }

    if (bestTurretInfo != null) {
      return bestTurretInfo;
    } else if (bestCanHitEnemy != null) {
      return bestCanHitEnemy;
    } else if (bestOtherEnemy != null) {
      return bestOtherEnemy;
    }

    RobotInfo[] zombies = radar.getNearbyZombies();
    double leastHealthZombie = 99999;
    RobotInfo bestZombie = null;
    // TODO FIX
    for (int i = zombies.length; --i >= 0;) {
      RobotInfo zombie = zombies[i];
      int dist = zombie.location.distanceSquaredTo(buddy.location);
      if (dist > RobotType.TURRET.attackRadiusSquared) {
        continue;
      }
      if (canHit(zombie)) {
        return zombie;
      }
      if (zombie.health < leastHealthZombie) {
        leastHealthZombie = zombie.health;
        bestZombie = zombie;
      }
    }
    return bestZombie;
  }

  private boolean canHit(RobotInfo robot) {
    return robot.coreDelay >= 2 || robot.type == RobotType.TURRET;
  }

  private boolean amLowestIdAdjacent() throws GameActionException {
    // TODO Check if that scout is already a buddy for another turret
    RobotInfo buddy = rc.senseRobot(buddySystem.getBuddy());
    RobotInfo[] nearbyRobots = rc.senseNearbyRobots(buddy.location, 2, rc.getTeam());
    for (int i = nearbyRobots.length; --i >= 0;) {
      if (nearbyRobots[i].type == RobotType.SCOUT && nearbyRobots[i].ID < rc.getID()) {
        RobotInfo[] hisNearby = rc.senseNearbyRobots(nearbyRobots[i].location, 2, rc.getTeam());
        boolean hasSomeoneToBuddyWith = false;
        for (int j = hisNearby.length; --j >= 0;) {
          RobotInfo hisAdjacent = hisNearby[j];
          if ((hisAdjacent.type == RobotType.TURRET || hisAdjacent.type == RobotType.TTM)
              && hisAdjacent.ID != buddy.ID) {
            hasSomeoneToBuddyWith = true;
          }
        }
        if (!hasSomeoneToBuddyWith) {
          return false;
        }
      }
    }
    return true;
  }
}
