package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import kerriganbot.ViperSacReporter.SacAttack;

public class ApocalypseBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final Radar radar;
  private final ViperSacReporter viperSacReporter;

  public ApocalypseBehavior(
      RobotController rc,
      NavigationSystem navigation,
      Radar radar,
      ViperSacReporter viperSacReporter) {
    this.rc = rc;
    this.navigation = navigation;
    this.radar = radar;
    this.viperSacReporter = viperSacReporter;
  }

  @Override
  public void run() throws GameActionException {
    SacAttack sacAttack = viperSacReporter.getSacAttack();
    int roundNum = rc.getRoundNum();
    if (sacAttack != null) {
      rc.setIndicatorString(0, "Aaaaahhhh! End of the world at round " + sacAttack.sacRound
          + "! Taking shelter at " + sacAttack.armyRallyLoc + ".");
      if (roundNum >= sacAttack.sacRound) {
        vipersAttackAllies();
        if (rc.getInfectedTurns() > 0) {
          rc.disintegrate();
          return;
        }
      }
      boolean clearRubble = rc.getType() != RobotType.TTM;
      navigation.directTo(sacAttack.armyRallyLoc, true /* avoidAttackers */, clearRubble);
    } else {
      rc.setIndicatorString(0, "I'm lost during the apocalypse.");
    }
  }

  private void vipersAttackAllies() throws GameActionException {
    if (rc.getType() == RobotType.VIPER && rc.isWeaponReady()) {
      rc.attackLocation(rc.getLocation());
    }
  }
}
