package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class ExploringBehavior implements Behavior {

  private final Radar radar;
  private final RobotController rc;
  private final NavigationSystem navigation;
  private final MapBoundaryCalculator mapBoundaryCalculator;
  private final ExplorationCalculator explorationCalculator;

  public ExploringBehavior(
      RobotController rc,
      NavigationSystem navigation,
      Radar radar,
      PatrolWaypointCalculator patrolWaypointCalculator) {
    this.rc = rc;
    this.navigation = navigation;
    this.radar = radar;

    mapBoundaryCalculator = new MapBoundaryCalculator(rc);
    explorationCalculator = new ExplorationCalculator(
        rc, mapBoundaryCalculator, patrolWaypointCalculator);
  }

  @Override
  public void run() throws GameActionException {
    RobotPlayer.profiler.split("start of exploring behavior");
    mapBoundaryCalculator.update();
    RobotPlayer.profiler.split("after map boundary calculator");
    MapLocation loc = explorationCalculator.calculate();
    RobotPlayer.profiler.split("after exploration calculator calculate");
    rc.setIndicatorString(0, "I'm exploring " + loc + "." + rc.getRoundNum());
    boolean clearRubble = rc.getType() != RobotType.TTM && rc.getType() != RobotType.SCOUT;
    RobotInfo[] hostiles = radar.getNearbyHostiles();
    boolean attackingEnemies = false;
    MapLocation myLoc = rc.getLocation();
    for (int i = hostiles.length; --i >= 0;) {
      RobotInfo hostile = hostiles[i];
      if (hostile.type.canAttack() && myLoc.distanceSquaredTo(
          hostile.location) <= hostile.type.attackRadiusSquared) {
        attackingEnemies = true;
        break;
      }
    }
    if (attackingEnemies) {
      navigation.smartRetreat(loc, hostiles);
    } else {
      navigation.directTo(loc, true /* avoidAttackers */, clearRubble);
    }
  }
}
