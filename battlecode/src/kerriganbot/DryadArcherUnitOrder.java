package kerriganbot;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class DryadArcherUnitOrder implements UnitOrder {

  private RobotController rc;
  private RobotType nextUnit;
  private static final int MAKE_VIPER_ROUND = 275;
  private int scoutsMade;
  private int vipersMade;

  public DryadArcherUnitOrder(RobotController rc) {
    this.rc = rc;
    this.nextUnit = RobotType.SOLDIER;
    this.scoutsMade = 0;
    this.vipersMade = 0;
  }

  @Override
  public RobotType getNextUnit() {
    return nextUnit;
  }

  @Override
  public void computeNextUnit() {
    if (rc.getRoundNum() <= MAKE_VIPER_ROUND) {
      nextUnit = RobotType.SOLDIER;
      return;
    }
    double rand = Math.random();
    if (rand <= .5) {
      if (3 * vipersMade <= scoutsMade) {
        nextUnit = RobotType.VIPER;
        vipersMade++;
      } else {
        nextUnit = RobotType.SCOUT;
        scoutsMade++;
      }
    } else {
      nextUnit = RobotType.SOLDIER;
    }
  }
}
