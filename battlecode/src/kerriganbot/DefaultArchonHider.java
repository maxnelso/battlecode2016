package kerriganbot;

import battlecode.common.MapLocation;

public class DefaultArchonHider implements ArchonHider {

  private MapLocation archonHidingLoc;

  public DefaultArchonHider() {
    archonHidingLoc = null;
  }

  @Override
  public void reportArchonHidingLocation(MapLocation archonHidingLoc) {
    if (archonHidingLoc != null) {
      this.archonHidingLoc = archonHidingLoc;
    }
  }

  @Override
  public MapLocation getArchonHidingLocation() {
    return archonHidingLoc;
  }
}
