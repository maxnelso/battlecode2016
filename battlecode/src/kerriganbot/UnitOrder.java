package kerriganbot;

import battlecode.common.RobotType;

public interface UnitOrder {

  public RobotType getNextUnit();

  public void computeNextUnit();
}
