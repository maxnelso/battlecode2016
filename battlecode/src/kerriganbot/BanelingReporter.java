package kerriganbot;

import battlecode.common.MapLocation;

public interface BanelingReporter {

  public MapLocation[] getBanelings();

  public void addBaneling(MapLocation loc);

  public void clearBanelings();
}
