package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class HidingArchonBehavior implements Behavior {

  private static final int MIN_DISTANCE_TO_SHARE_VIPER_SAC = 18;

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final MessageSender messageSender;
  private final ArchonHider archonHider;
  private final ViperSacCalculator viperSacCalculator;

  public HidingArchonBehavior(
      RobotController rc,
      NavigationSystem navigation,
      MessageSender messageSender,
      ArchonHider archonHider,
      ViperSacCalculator viperSacCalculator) {
    this.rc = rc;
    this.navigation = navigation;
    this.messageSender = messageSender;
    this.archonHider = archonHider;
    this.viperSacCalculator = viperSacCalculator;
  }

  @Override
  public void run() throws GameActionException {
    MapLocation archonHidingLoc = archonHider.getArchonHidingLocation();
    if (archonHidingLoc != null) {
      rc.setIndicatorString(0, "I'm hiding at " + archonHidingLoc + ". :)");
      maybeShareViperSac(archonHidingLoc);
      navigation.bugTo(archonHidingLoc, true /* avoidAttackers */, true /* clearRubble */);
    } else {
      rc.setIndicatorString(0, "I'm a lost hiding archon.");
    }
  }

  private void maybeShareViperSac(MapLocation archonHidingLoc) throws GameActionException {
    if (rc.getLocation().distanceSquaredTo(archonHidingLoc) <= MIN_DISTANCE_TO_SHARE_VIPER_SAC
        && rc.isCoreReady()) {
      viperSacCalculator.computeAndShareViperSac(messageSender);
    }
  }
}
