package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class BuddyDistantHostileReporter implements DistantHostileReporter {

  private final RobotController rc;
  private int lastRound;
  private MapLocationSet mapLocationSet;

  public BuddyDistantHostileReporter(RobotController rc) {
    this.rc = rc;
    this.lastRound = rc.getRoundNum();
    this.mapLocationSet = new ArrayMapLocationIntMap();
  }

  @Override
  public void reportDistantHostile(MapLocation loc, RobotType type, int coreDelayTenths,
      boolean isZombie) throws GameActionException {
    if (rc.getRoundNum() != lastRound) {
      this.mapLocationSet = new ArrayMapLocationIntMap();
    }

    mapLocationSet.add(loc);
  }

  public boolean alreadyReported(MapLocation loc) {
    return mapLocationSet.contains(loc);
  }
}
