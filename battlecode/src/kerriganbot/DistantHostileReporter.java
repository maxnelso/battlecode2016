package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public interface DistantHostileReporter {

  public void reportDistantHostile(
      MapLocation loc, RobotType type, int coreDelayTenths, boolean isZombie)
          throws GameActionException;
}
