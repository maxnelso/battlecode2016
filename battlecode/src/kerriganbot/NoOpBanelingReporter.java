package kerriganbot;

import battlecode.common.MapLocation;

public class NoOpBanelingReporter implements BanelingReporter {

  @Override
  public MapLocation[] getBanelings() {
    return null;
  }

  @Override
  public void addBaneling(MapLocation loc) {}

  @Override
  public void clearBanelings() {}

}
