package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class TurretBehavior implements PreBehavior, Behavior {

  private final RobotController rc;
  private final MessageReceiver messageReceiver;
  private final MessageSender messageSender;
  private final Radar radar;
  private final ArmyRally armyRally;
  private final EeHanTimingReporter eeHanTimingReporter;
  private final ZombieDenReporter zombieDenReporter;
  private final ViperSacReporter viperSacReporter;

  private final Behavior unpackedBehavior;
  private final ApocalypseBehavior apocalypseBehavior;
  private final SwarmingBehavior swarmingBehavior;
  private final RallyBehavior rallyBehavior;
  private final TimingAttackBehavior timingAttackBehavior;
  private final HuntingBehavior huntingBehavior;
  private final ExploringBehavior exploringBehavior;

  public TurretBehavior(
      RobotController rc,
      MessageReceiver messageReceiver,
      MessageSender messageSender,
      Radar radar,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker,
      AttackSystem attackSystem,
      ArmyRally armyRally,
      EeHanTimingReporter eeHanTimingReporter,
      ZombieDenReporter zombieDenReporter,
      ViperSacReporter viperSacReporter) {
    this.rc = rc;
    this.messageReceiver = messageReceiver;
    this.messageSender = messageSender;
    this.radar = radar;
    this.armyRally = armyRally;
    this.eeHanTimingReporter = eeHanTimingReporter;
    this.zombieDenReporter = zombieDenReporter;
    this.viperSacReporter = viperSacReporter;

    apocalypseBehavior = new ApocalypseBehavior(rc, navigation, radar, viperSacReporter);
    swarmingBehavior = new SwarmingBehavior(rc, navigation, alliedArchonTracker);
    unpackedBehavior = new UnpackedTurretBehavior(rc, radar, attackSystem);
    rallyBehavior = new RallyBehavior(rc, armyRally, navigation);
    timingAttackBehavior = new TimingAttackBehavior(rc, eeHanTimingReporter, navigation);
    huntingBehavior = new HuntingBehavior(rc, navigation, zombieDenReporter);

    PatrolWaypointCalculator patrolWaypointCalculator = new LawnMowerPatrolWaypointCalculator(
        5 /* laneHalfWidth */, 4 /* mapBoundaryMargin */);
    exploringBehavior = new ExploringBehavior(rc, navigation, radar, patrolWaypointCalculator);
  }

  @Override
  public void preRun() throws GameActionException {
    messageReceiver.receiveMessages();
  }

  @Override
  public void run() throws GameActionException {
    if (rc.getType() == RobotType.TURRET) {
      unpackedBehavior.run();
    } else {
      RobotInfo closest = RadarUtils.getClosestRobot(radar.getNearbyHostiles(), rc.getLocation());
      if (closest != null) {
        int dist = closest.location.distanceSquaredTo(rc.getLocation());
        if (dist <= RobotType.TURRET.attackRadiusSquared
            && dist >= GameConstants.TURRET_MINIMUM_RANGE) {
          rc.unpack();
        }
      }
      getCurrentBehavior().run();
    }
  }

  private Behavior getCurrentBehavior() {
    if (viperSacReporter.getSacAttack() != null) {
      return apocalypseBehavior;
    } else if (armyRally.getRally() != null) {
      return rallyBehavior;
    }
    if (eeHanTimingReporter.getTimingAttack() != null) {
      return timingAttackBehavior;
    }
    if (huntingBehavior.getTarget() != null) {
      return huntingBehavior;
    }
    if (swarmingBehavior.shouldSwarm()) {
      return swarmingBehavior;
    }
    return exploringBehavior;
  }
}
