package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public interface EnemyTurretCache {

  public void reportEnemyTurretPresent(MapLocation loc, int timestamp);

  public void reportEnemyTurretAbsent(MapLocation loc, int timestamp);

  public void reportTurretBoundingBox(
      MapLocation topLeftCorner, int xRange, int yRange, int numTurrets);

  public boolean isInEnemyTurretRange(MapLocation loc);

  public void invalidateNearbyEnemyTurrets() throws GameActionException;

  public BoundingBox getTurretBoundingBox();

  public void shareRandomEnemyTurret(MessageSender messageSender) throws GameActionException;

  public void shareTurretBoundingBox(MessageSender messageSender) throws GameActionException;

  public void showDebugInfo() throws GameActionException;
}
