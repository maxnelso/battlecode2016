package kerriganbot;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

public class UnpackedTurretBehavior implements Behavior {

  private static final int MIN_IDLE_ROUNDS = 10;
  private static final int MAX_IDLE_ROUNDS = 30;

  private final RobotController rc;
  private final Radar radar;
  private final AttackSystem attackSystem;

  private int idleRounds;

  public UnpackedTurretBehavior(RobotController rc, Radar radar, AttackSystem attackSystem) {
    this.rc = rc;
    this.radar = radar;
    this.attackSystem = attackSystem;
    idleRounds = MIN_IDLE_ROUNDS;
  }

  @Override
  public void run() throws GameActionException {
    rc.setIndicatorString(0, "I'm sieged, will unsiege in " + idleRounds + " rounds.");
    attack();
    maybeUnsiege(rc);
  }

  private void maybeUnsiege(RobotController rc) throws GameActionException {
    RobotInfo[] infos = radar.getNearbyHostiles();
    boolean canShootSomeone = false;
    boolean someoneOnMe = false;
    MapLocation myLoc = rc.getLocation();
    int attackRadiusSquared = rc.getType().attackRadiusSquared;
    for (int i = infos.length; --i >= 0;) {
      MapLocation loc = infos[i].location;
      int dist = loc.distanceSquaredTo(myLoc);
      if (dist >= GameConstants.TURRET_MINIMUM_RANGE && dist <= attackRadiusSquared) {
        canShootSomeone = true;
        break;
      }
      if (dist < GameConstants.TURRET_MINIMUM_RANGE) {
        someoneOnMe = true;
      }
    }

    if (someoneOnMe && !canShootSomeone) {
      idleRounds = 0;
    } else if (canShootSomeone) {
      idleRounds = computeIdleRounds();
    }

    if (idleRounds-- <= 0 && rc.isCoreReady()) {
      rc.pack();
      idleRounds = computeIdleRounds();
    }
  }

  private void attack() throws GameActionException {
    if (rc.isWeaponReady()) {
      RobotInfo[] enemies = radar.getNearbyEnemies();
      RobotInfo[] allies = radar.getNearbyAllies();
      MapLocation target = attackSystem.getBestEnemyToShoot(rc, enemies, enemies.length, allies,
          allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
      RobotInfo[] zombies = radar.getNearbyZombies();
      target = attackSystem.getBestZombieToShoot(rc, zombies, zombies.length, allies,
          allies.length);
      if (target != null) {
        rc.attackLocation(target);
        return;
      }
    }
  }

  private int computeIdleRounds() {
    return MIN_IDLE_ROUNDS + (int) (Math.random() * (MAX_IDLE_ROUNDS - MIN_IDLE_ROUNDS));
  }
}
