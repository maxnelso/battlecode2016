package kerriganbot;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class KerriganBehavior implements Behavior {

  private final RobotController rc;
  private final NavigationSystem navigation;
  private final AlliedArchonTracker alliedArchonTracker;
  private final AttackSystem attackSystem;
  private final BanelingReporter banelingReporter;
  private final Radar radar;

  private RobotInfo bestBaneling;

  public KerriganBehavior(
      RobotController rc,
      NavigationSystem navigation,
      AlliedArchonTracker alliedArchonTracker,
      Radar radar,
      BanelingReporter banelingReporter,
      AttackSystem attackSystem) {
    this.rc = rc;
    this.navigation = navigation;
    this.alliedArchonTracker = alliedArchonTracker;
    this.attackSystem = attackSystem;
    this.banelingReporter = banelingReporter;
    this.radar = radar;
  }

  @Override
  public void run() throws GameActionException {
    if (bestBaneling != null) {
      if (rc.isWeaponReady()) {
        rc.attackLocation(bestBaneling.location);
      }
      Direction d = rc.getLocation().directionTo(bestBaneling.location);
      if (rc.isCoreReady()) {
        navigation.smartRetreat(rc.getLocation().add(d.opposite(), 5), radar.getNearbyHostiles());
      }
    }
  }

  public boolean shouldKerrigan() throws GameActionException {
    MapLocation[] banelings = banelingReporter.getBanelings();
    MapLocation myLoc = rc.getLocation();
    for (int i = banelings.length; --i >= 0;) {
      if (!rc.canSense(banelings[i])) {
        continue;
      }
      RobotInfo baneling = rc.senseRobotAtLocation(banelings[i]);
      if (baneling == null) {
        continue;
      }
      if (baneling.location.distanceSquaredTo(myLoc) > RobotType.VIPER.attackRadiusSquared) {
        continue;
      }
      if (baneling.viperInfectedTurns > 0) { // Already infected
        continue;
      }
      bestBaneling = baneling;
      return true;
    }
    return false;
  }

}
