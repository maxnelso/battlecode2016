package randomnavbot;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class RobotPlayer {

  public static void run(RobotController rc) {
    while (true) {
      try {
        randomNav(rc);
      } catch (GameActionException e) {}
      Clock.yield();
    }
  }

  private static void randomNav(RobotController rc) throws GameActionException {
    if (!rc.isCoreReady()) {
      return;
    }

    Direction[] dirs = new Direction[] {
      Direction.NORTH,
      Direction.NORTH_EAST,
      Direction.EAST,
      Direction.SOUTH_EAST,
      Direction.SOUTH,
      Direction.SOUTH_WEST,
      Direction.WEST,
      Direction.NORTH_WEST
    };
    Direction d = dirs[(int) (Math.random() * 8)];
    for (int i = 0; i < 8; i++) {
      if (rc.canMove(d)) {
        rc.move(d);
        return;
      }
      d = d.rotateLeft();
    }
  }
}
